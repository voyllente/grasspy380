/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(array_array___copy____doc__,
"__copy__($self, /)\n"
"--\n"
"\n"
"Return a copy of the array.");

#define ARRAY_ARRAY___COPY___METHODDEF    \
    {"__copy__", (PyCFunction)array_array___copy__, METH_NOARGS, array_array___copy____doc__},

static PyObject *
array_array___copy___impl(arrayobject *self);

static PyObject *
array_array___copy__(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array___copy___impl(self);
}

PyDoc_STRVAR(array_array___deepcopy____doc__,
"__deepcopy__($self, unused, /)\n"
"--\n"
"\n"
"Return a copy of the array.");

#define ARRAY_ARRAY___DEEPCOPY___METHODDEF    \
    {"__deepcopy__", (PyCFunction)array_array___deepcopy__, METH_O, array_array___deepcopy____doc__},

PyDoc_STRVAR(array_array_count__doc__,
"计数($self, v, /)\n"
"--\n"
"\n"
"返回 v 在数组中出现的次数.");

#define ARRAY_ARRAY_COUNT_METHODDEF    \
    {"count", (PyCFunction)array_array_count, METH_O, array_array_count__doc__},

#define ARRAY_ARRAY_计数_METHODDEF    \
    {"计数", (PyCFunction)array_array_count, METH_O, array_array_count__doc__},

PyDoc_STRVAR(array_array_index__doc__,
"索引($self, v, /)\n"
"--\n"
"\n"
"返回 v 在数组中第一次出现的索引.");

#define ARRAY_ARRAY_INDEX_METHODDEF    \
    {"index", (PyCFunction)array_array_index, METH_O, array_array_index__doc__},

#define ARRAY_ARRAY_索引_METHODDEF    \
    {"索引", (PyCFunction)array_array_index, METH_O, array_array_index__doc__},

PyDoc_STRVAR(array_array_remove__doc__,
"移除($self, v, /)\n"
"--\n"
"\n"
"移除数组中第一次出现的 v.");

#define ARRAY_ARRAY_REMOVE_METHODDEF    \
    {"remove", (PyCFunction)array_array_remove, METH_O, array_array_remove__doc__},

#define ARRAY_ARRAY_移除_METHODDEF    \
    {"移除", (PyCFunction)array_array_remove, METH_O, array_array_remove__doc__},

PyDoc_STRVAR(array_array_pop__doc__,
"弹出($self, i=-1, /)\n"
"--\n"
"\n"
"返回第 i 个元素并将其从数组中删除.\n"
"\n"
"i 默认值为 -1.");

#define ARRAY_ARRAY_POP_METHODDEF    \
    {"pop", (PyCFunction)(void(*)(void))array_array_pop, METH_FASTCALL, array_array_pop__doc__},

#define ARRAY_ARRAY_弹出_METHODDEF    \
    {"弹出", (PyCFunction)(void(*)(void))array_array_pop, METH_FASTCALL, array_array_pop__doc__},

static PyObject *
array_array_pop_impl(arrayobject *self, Py_ssize_t i);

static PyObject *
array_array_pop(arrayobject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t i = -1;

    if (!_PyArg_CheckPositional("pop", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        i = ival;
    }
skip_optional:
    return_value = array_array_pop_impl(self, i);

exit:
    return return_value;
}

PyDoc_STRVAR(array_array_extend__doc__,
"扩充($self, bb, /)\n"
"--\n"
"\n"
"将多个元素追加到数组末尾.");

#define ARRAY_ARRAY_EXTEND_METHODDEF    \
    {"extend", (PyCFunction)array_array_extend, METH_O, array_array_extend__doc__},

#define ARRAY_ARRAY_扩充_METHODDEF    \
    {"扩充", (PyCFunction)array_array_extend, METH_O, array_array_extend__doc__},

PyDoc_STRVAR(array_array_insert__doc__,
"插入($self, i, v, /)\n"
"--\n"
"\n"
"将一个新元素 v 插入数组中的位置 i 之前.");

#define ARRAY_ARRAY_INSERT_METHODDEF    \
    {"insert", (PyCFunction)(void(*)(void))array_array_insert, METH_FASTCALL, array_array_insert__doc__},

#define ARRAY_ARRAY_插入_METHODDEF    \
    {"插入", (PyCFunction)(void(*)(void))array_array_insert, METH_FASTCALL, array_array_insert__doc__},

static PyObject *
array_array_insert_impl(arrayobject *self, Py_ssize_t i, PyObject *v);

static PyObject *
array_array_insert(arrayobject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t i;
    PyObject *v;

    if (!_PyArg_CheckPositional("insert", nargs, 2, 2)) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        i = ival;
    }
    v = args[1];
    return_value = array_array_insert_impl(self, i, v);

exit:
    return return_value;
}

PyDoc_STRVAR(array_array_buffer_info__doc__,
"缓冲区信息($self, /)\n"
"--\n"
"\n"
"返回一个元组 (地址, 长度), 告知用来保存数组内容的缓冲区的当前内存地址和长度.\n"
"\n"
"长度乘以 元素大小 属性即得到缓冲区长度 (字节数).");

#define ARRAY_ARRAY_BUFFER_INFO_METHODDEF    \
    {"buffer_info", (PyCFunction)array_array_buffer_info, METH_NOARGS, array_array_buffer_info__doc__},

#define ARRAY_ARRAY_缓冲区信息_METHODDEF    \
    {"缓冲区信息", (PyCFunction)array_array_buffer_info, METH_NOARGS, array_array_buffer_info__doc__},

static PyObject *
array_array_buffer_info_impl(arrayobject *self);

static PyObject *
array_array_buffer_info(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_buffer_info_impl(self);
}

PyDoc_STRVAR(array_array_append__doc__,
"追加($self, v, /)\n"
"--\n"
"\n"
"将一个新值 v 追加到数组末尾.");

#define ARRAY_ARRAY_APPEND_METHODDEF    \
    {"append", (PyCFunction)array_array_append, METH_O, array_array_append__doc__},

#define ARRAY_ARRAY_追加_METHODDEF    \
    {"追加", (PyCFunction)array_array_append, METH_O, array_array_append__doc__},

PyDoc_STRVAR(array_array_byteswap__doc__,
"字节交换($self, /)\n"
"--\n"
"\n"
"对数组中的所有元素进行字节交换.\n"
"\n"
"如果数组中的元素大小不是 1, 2, 4 或 8 字节, 则报 运行时错误类 错误.");

#define ARRAY_ARRAY_BYTESWAP_METHODDEF    \
    {"byteswap", (PyCFunction)array_array_byteswap, METH_NOARGS, array_array_byteswap__doc__},

#define ARRAY_ARRAY_字节交换_METHODDEF    \
    {"字节交换", (PyCFunction)array_array_byteswap, METH_NOARGS, array_array_byteswap__doc__},

static PyObject *
array_array_byteswap_impl(arrayobject *self);

static PyObject *
array_array_byteswap(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_byteswap_impl(self);
}

PyDoc_STRVAR(array_array_reverse__doc__,
"反转($self, /)\n"
"--\n"
"\n"
"反转数组中的元素顺序.");

#define ARRAY_ARRAY_REVERSE_METHODDEF    \
    {"reverse", (PyCFunction)array_array_reverse, METH_NOARGS, array_array_reverse__doc__},

#define ARRAY_ARRAY_反转_METHODDEF    \
    {"反转", (PyCFunction)array_array_reverse, METH_NOARGS, array_array_reverse__doc__},

static PyObject *
array_array_reverse_impl(arrayobject *self);

static PyObject *
array_array_reverse(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_reverse_impl(self);
}

PyDoc_STRVAR(array_array_fromfile__doc__,
"从文件($self, f, n, /)\n"
"--\n"
"\n"
"从文件对象 f 读取 n 个对象并将其追加到数组末尾.");

#define ARRAY_ARRAY_FROMFILE_METHODDEF    \
    {"fromfile", (PyCFunction)(void(*)(void))array_array_fromfile, METH_FASTCALL, array_array_fromfile__doc__},

#define ARRAY_ARRAY_从文件_METHODDEF    \
    {"从文件", (PyCFunction)(void(*)(void))array_array_fromfile, METH_FASTCALL, array_array_fromfile__doc__},

static PyObject *
array_array_fromfile_impl(arrayobject *self, PyObject *f, Py_ssize_t n);

static PyObject *
array_array_fromfile(arrayobject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *f;
    Py_ssize_t n;

    if (!_PyArg_CheckPositional("fromfile", nargs, 2, 2)) {
        goto exit;
    }
    f = args[0];
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[1]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        n = ival;
    }
    return_value = array_array_fromfile_impl(self, f, n);

exit:
    return return_value;
}

PyDoc_STRVAR(array_array_tofile__doc__,
"转文件($self, f, /)\n"
"--\n"
"\n"
"将所有元素 (作为机器值) 写入文件对象 f.");

#define ARRAY_ARRAY_TOFILE_METHODDEF    \
    {"tofile", (PyCFunction)array_array_tofile, METH_O, array_array_tofile__doc__},

#define ARRAY_ARRAY_转文件_METHODDEF    \
    {"转文件", (PyCFunction)array_array_tofile, METH_O, array_array_tofile__doc__},

PyDoc_STRVAR(array_array_fromlist__doc__,
"从列表($self, l, /)\n"
"--\n"
"\n"
"将列表 l 中的元素追加到数组.");

#define ARRAY_ARRAY_FROMLIST_METHODDEF    \
    {"fromlist", (PyCFunction)array_array_fromlist, METH_O, array_array_fromlist__doc__},

#define ARRAY_ARRAY_从列表_METHODDEF    \
    {"从列表", (PyCFunction)array_array_fromlist, METH_O, array_array_fromlist__doc__},

PyDoc_STRVAR(array_array_tolist__doc__,
"转列表($self, /)\n"
"--\n"
"\n"
"将数组转换为具有相同元素的普通列表.");

#define ARRAY_ARRAY_TOLIST_METHODDEF    \
    {"tolist", (PyCFunction)array_array_tolist, METH_NOARGS, array_array_tolist__doc__},

#define ARRAY_ARRAY_转列表_METHODDEF    \
    {"转列表", (PyCFunction)array_array_tolist, METH_NOARGS, array_array_tolist__doc__},

static PyObject *
array_array_tolist_impl(arrayobject *self);

static PyObject *
array_array_tolist(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_tolist_impl(self);
}

PyDoc_STRVAR(array_array_fromstring__doc__,
"fromstring($self, buffer, /)\n"
"--\n"
"\n"
"Appends items from the string, interpreting it as an array of machine values, as if it had been read from a file using the fromfile() method).\n"
"\n"
"This method is deprecated. Use frombytes instead.");

#define ARRAY_ARRAY_FROMSTRING_METHODDEF    \
    {"fromstring", (PyCFunction)array_array_fromstring, METH_O, array_array_fromstring__doc__},

static PyObject *
array_array_fromstring_impl(arrayobject *self, Py_buffer *buffer);

static PyObject *
array_array_fromstring(arrayobject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    Py_buffer buffer = {NULL, NULL};

    if (PyUnicode_Check(arg)) {
        Py_ssize_t len;
        const char *ptr = PyUnicode_AsUTF8AndSize(arg, &len);
        if (ptr == NULL) {
            goto exit;
        }
        PyBuffer_FillInfo(&buffer, arg, (void *)ptr, len, 1, 0);
    }
    else { /* any bytes-like object */
        if (PyObject_GetBuffer(arg, &buffer, PyBUF_SIMPLE) != 0) {
            goto exit;
        }
        if (!PyBuffer_IsContiguous(&buffer, 'C')) {
            _PyArg_BadArgument("fromstring", "参数", "连续缓冲区", arg);
            goto exit;
        }
    }
    return_value = array_array_fromstring_impl(self, &buffer);

exit:
    /* Cleanup for buffer */
    if (buffer.obj) {
       PyBuffer_Release(&buffer);
    }

    return return_value;
}

PyDoc_STRVAR(array_array_frombytes__doc__,
"从字节($self, 缓冲区, /)\n"
"--\n"
"\n"
"从字符串追加元素, 将字符串解读为机器值数组, 好像它是利用 从文件() 方法从一个文件读取的一样.");

#define ARRAY_ARRAY_FROMBYTES_METHODDEF    \
    {"frombytes", (PyCFunction)array_array_frombytes, METH_O, array_array_frombytes__doc__},

#define ARRAY_ARRAY_从字节_METHODDEF    \
    {"从字节", (PyCFunction)array_array_frombytes, METH_O, array_array_frombytes__doc__},

static PyObject *
array_array_frombytes_impl(arrayobject *self, Py_buffer *buffer);

static PyObject *
array_array_frombytes(arrayobject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    Py_buffer buffer = {NULL, NULL};

    if (PyObject_GetBuffer(arg, &buffer, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&buffer, 'C')) {
        _PyArg_BadArgument("从字节", "参数", "连续缓冲区", arg);
        goto exit;
    }
    return_value = array_array_frombytes_impl(self, &buffer);

exit:
    /* Cleanup for buffer */
    if (buffer.obj) {
       PyBuffer_Release(&buffer);
    }

    return return_value;
}

PyDoc_STRVAR(array_array_tobytes__doc__,
"转字节($self, /)\n"
"--\n"
"\n"
"将该数组转换为机器值数组并返回字节表示.");

#define ARRAY_ARRAY_TOBYTES_METHODDEF    \
    {"tobytes", (PyCFunction)array_array_tobytes, METH_NOARGS, array_array_tobytes__doc__},

#define ARRAY_ARRAY_转字节_METHODDEF    \
    {"转字节", (PyCFunction)array_array_tobytes, METH_NOARGS, array_array_tobytes__doc__},

static PyObject *
array_array_tobytes_impl(arrayobject *self);

static PyObject *
array_array_tobytes(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_tobytes_impl(self);
}

PyDoc_STRVAR(array_array_tostring__doc__,
"tostring($self, /)\n"
"--\n"
"\n"
"Convert the array to an array of machine values and return the bytes representation.\n"
"\n"
"This method is deprecated. Use tobytes instead.");

#define ARRAY_ARRAY_TOSTRING_METHODDEF    \
    {"tostring", (PyCFunction)array_array_tostring, METH_NOARGS, array_array_tostring__doc__},

static PyObject *
array_array_tostring_impl(arrayobject *self);

static PyObject *
array_array_tostring(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_tostring_impl(self);
}

PyDoc_STRVAR(array_array_fromunicode__doc__,
"fromunicode($self, ustr, /)\n"
"--\n"
"\n"
"Extends this array with data from the unicode string ustr.\n"
"\n"
"The array must be a unicode type array; otherwise a ValueError is raised.\n"
"Use array.frombytes(ustr.encode(...)) to append Unicode data to an array of\n"
"some other type.");

#define ARRAY_ARRAY_FROMUNICODE_METHODDEF    \
    {"fromunicode", (PyCFunction)array_array_fromunicode, METH_O, array_array_fromunicode__doc__},

static PyObject *
array_array_fromunicode_impl(arrayobject *self, const Py_UNICODE *ustr,
                             Py_ssize_clean_t ustr_length);

static PyObject *
array_array_fromunicode(arrayobject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    const Py_UNICODE *ustr;
    Py_ssize_clean_t ustr_length;

    if (!PyArg_Parse(arg, "u#:fromunicode", &ustr, &ustr_length)) {
        goto exit;
    }
    return_value = array_array_fromunicode_impl(self, ustr, ustr_length);

exit:
    return return_value;
}

PyDoc_STRVAR(array_array_tounicode__doc__,
"tounicode($self, /)\n"
"--\n"
"\n"
"Extends this array with data from the unicode string ustr.\n"
"\n"
"Convert the array to a unicode string.  The array must be a unicode type array;\n"
"otherwise a ValueError is raised.  Use array.tobytes().decode() to obtain a\n"
"unicode string from an array of some other type.");

#define ARRAY_ARRAY_TOUNICODE_METHODDEF    \
    {"tounicode", (PyCFunction)array_array_tounicode, METH_NOARGS, array_array_tounicode__doc__},

static PyObject *
array_array_tounicode_impl(arrayobject *self);

static PyObject *
array_array_tounicode(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array_tounicode_impl(self);
}

PyDoc_STRVAR(array_array___sizeof____doc__,
"__内存占用__($self, /)\n"
"--\n"
"\n"
"数组在内存中占用的字节数.");

#define ARRAY_ARRAY___SIZEOF___METHODDEF    \
    {"__sizeof__", (PyCFunction)array_array___sizeof__, METH_NOARGS, array_array___sizeof____doc__},

#define ARRAY_ARRAY___内存占用___METHODDEF    \
    {"__内存占用__", (PyCFunction)array_array___sizeof__, METH_NOARGS, array_array___sizeof____doc__},

static PyObject *
array_array___sizeof___impl(arrayobject *self);

static PyObject *
array_array___sizeof__(arrayobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_array___sizeof___impl(self);
}

PyDoc_STRVAR(array__array_reconstructor__doc__,
"_array_reconstructor($module, arraytype, typecode, mformat_code, items,\n"
"                     /)\n"
"--\n"
"\n"
"Internal. Used for pickling support.");

#define ARRAY__ARRAY_RECONSTRUCTOR_METHODDEF    \
    {"_array_reconstructor", (PyCFunction)(void(*)(void))array__array_reconstructor, METH_FASTCALL, array__array_reconstructor__doc__},

static PyObject *
array__array_reconstructor_impl(PyObject *module, PyTypeObject *arraytype,
                                int typecode,
                                enum machine_format_code mformat_code,
                                PyObject *items);

static PyObject *
array__array_reconstructor(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyTypeObject *arraytype;
    int typecode;
    enum machine_format_code mformat_code;
    PyObject *items;

    if (!_PyArg_CheckPositional("_array_reconstructor", nargs, 4, 4)) {
        goto exit;
    }
    arraytype = (PyTypeObject *)args[0];
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("_array_reconstructor", "参数 2", "统一码字符", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1])) {
        goto exit;
    }
    if (PyUnicode_GET_LENGTH(args[1]) != 1) {
        _PyArg_BadArgument("_array_reconstructor", "参数 2", "统一码字符", args[1]);
        goto exit;
    }
    typecode = PyUnicode_READ_CHAR(args[1], 0);
    if (PyFloat_Check(args[2])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    mformat_code = _PyLong_AsInt(args[2]);
    if (mformat_code == -1 && PyErr_Occurred()) {
        goto exit;
    }
    items = args[3];
    return_value = array__array_reconstructor_impl(module, arraytype, typecode, mformat_code, items);

exit:
    return return_value;
}

PyDoc_STRVAR(array_array___reduce_ex____doc__,
"__reduce_ex__($self, value, /)\n"
"--\n"
"\n"
"返回状态信息用于序列化.");

#define ARRAY_ARRAY___REDUCE_EX___METHODDEF    \
    {"__reduce_ex__", (PyCFunction)array_array___reduce_ex__, METH_O, array_array___reduce_ex____doc__},

PyDoc_STRVAR(array_arrayiterator___reduce____doc__,
"__reduce__($self, /)\n"
"--\n"
"\n"
"返回状态信息用于序列化.");

#define ARRAY_ARRAYITERATOR___REDUCE___METHODDEF    \
    {"__reduce__", (PyCFunction)array_arrayiterator___reduce__, METH_NOARGS, array_arrayiterator___reduce____doc__},

static PyObject *
array_arrayiterator___reduce___impl(arrayiterobject *self);

static PyObject *
array_arrayiterator___reduce__(arrayiterobject *self, PyObject *Py_UNUSED(ignored))
{
    return array_arrayiterator___reduce___impl(self);
}

PyDoc_STRVAR(array_arrayiterator___setstate____doc__,
"__setstate__($self, state, /)\n"
"--\n"
"\n"
"设置状态信息用于反序列化.");

#define ARRAY_ARRAYITERATOR___SETSTATE___METHODDEF    \
    {"__setstate__", (PyCFunction)array_arrayiterator___setstate__, METH_O, array_arrayiterator___setstate____doc__},
/*[clinic end generated code: output=6aa421571e2c0756 input=a9049054013a1b77]*/
