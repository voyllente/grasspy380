/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(_heapq_heappush__doc__,
"堆压入($module, 堆, 元素, /)\n"
"--\n"
"\n"
"向堆中压入元素, 保持堆的不变性.");

#define _HEAPQ_HEAPPUSH_METHODDEF    \
    {"heappush", (PyCFunction)(void(*)(void))_heapq_heappush, METH_FASTCALL, _heapq_heappush__doc__},

#define _HEAPQ_堆压入_METHODDEF    \
    {"堆压入", (PyCFunction)(void(*)(void))_heapq_heappush, METH_FASTCALL, _heapq_heappush__doc__},

static PyObject *
_heapq_heappush_impl(PyObject *module, PyObject *heap, PyObject *item);

static PyObject *
_heapq_heappush(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *heap;
    PyObject *item;

    if (!_PyArg_CheckPositional("堆压入", nargs, 2, 2)) {
        goto exit;
    }
    heap = args[0];
    item = args[1];
    return_value = _heapq_heappush_impl(module, heap, item);

exit:
    return return_value;
}

PyDoc_STRVAR(_heapq_heappop__doc__,
"堆弹出($module, 堆, /)\n"
"--\n"
"\n"
"从堆中弹出最小元素, 保持堆的不变性.");

#define _HEAPQ_HEAPPOP_METHODDEF    \
    {"heappop", (PyCFunction)_heapq_heappop, METH_O, _heapq_heappop__doc__},

#define _HEAPQ_堆弹出_METHODDEF    \
    {"堆弹出", (PyCFunction)_heapq_heappop, METH_O, _heapq_heappop__doc__},

PyDoc_STRVAR(_heapq_heapreplace__doc__,
"堆替换($module, 堆, 元素, /)\n"
"--\n"
"\n"
"弹出并返回当前最小值, 然后添加新元素.\n"
"\n"
"这个操作比先调用 堆弹出() 再调用 堆压入() 更高效, \n"
"而且对于固定大小的堆更为适宜. 注意: 返回的值可能比添加的元素更大!\n"
"如果不希望如此, 可考虑改用 堆压入弹出().");

#define _HEAPQ_HEAPREPLACE_METHODDEF    \
    {"heapreplace", (PyCFunction)(void(*)(void))_heapq_heapreplace, METH_FASTCALL, _heapq_heapreplace__doc__},

#define _HEAPQ_堆替换_METHODDEF    \
    {"堆替换", (PyCFunction)(void(*)(void))_heapq_heapreplace, METH_FASTCALL, _heapq_heapreplace__doc__},

static PyObject *
_heapq_heapreplace_impl(PyObject *module, PyObject *heap, PyObject *item);

static PyObject *
_heapq_heapreplace(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *heap;
    PyObject *item;

    if (!_PyArg_CheckPositional("堆替换", nargs, 2, 2)) {
        goto exit;
    }
    heap = args[0];
    item = args[1];
    return_value = _heapq_heapreplace_impl(module, heap, item);

exit:
    return return_value;
}

PyDoc_STRVAR(_heapq_heappushpop__doc__,
"堆压入弹出($module, 堆, 元素, /)\n"
"--\n"
"\n"
"向堆中压入元素, 然后弹出并返回最小元素.\n"
"\n"
"这个操作比先调用 堆压入() 再调用 堆弹出() 更高效.");

#define _HEAPQ_HEAPPUSHPOP_METHODDEF    \
    {"heappushpop", (PyCFunction)(void(*)(void))_heapq_heappushpop, METH_FASTCALL, _heapq_heappushpop__doc__},

#define _HEAPQ_堆压入弹出_METHODDEF    \
    {"堆压入弹出", (PyCFunction)(void(*)(void))_heapq_heappushpop, METH_FASTCALL, _heapq_heappushpop__doc__},

static PyObject *
_heapq_heappushpop_impl(PyObject *module, PyObject *heap, PyObject *item);

static PyObject *
_heapq_heappushpop(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *heap;
    PyObject *item;

    if (!_PyArg_CheckPositional("堆压入弹出", nargs, 2, 2)) {
        goto exit;
    }
    heap = args[0];
    item = args[1];
    return_value = _heapq_heappushpop_impl(module, heap, item);

exit:
    return return_value;
}

PyDoc_STRVAR(_heapq_heapify__doc__,
"堆起来($module, 堆, /)\n"
"--\n"
"\n"
"将列表原位转换为堆, 时间为 O(长(堆)).");

#define _HEAPQ_HEAPIFY_METHODDEF    \
    {"heapify", (PyCFunction)_heapq_heapify, METH_O, _heapq_heapify__doc__},

#define _HEAPQ_堆起来_METHODDEF    \
    {"堆起来", (PyCFunction)_heapq_heapify, METH_O, _heapq_heapify__doc__},

PyDoc_STRVAR(_heapq__heappop_max__doc__,
"_heappop_max($module, heap, /)\n"
"--\n"
"\n"
"Maxheap variant of heappop.");

#define _HEAPQ__HEAPPOP_MAX_METHODDEF    \
    {"_heappop_max", (PyCFunction)_heapq__heappop_max, METH_O, _heapq__heappop_max__doc__},

PyDoc_STRVAR(_heapq__heapreplace_max__doc__,
"_heapreplace_max($module, heap, item, /)\n"
"--\n"
"\n"
"Maxheap variant of heapreplace.");

#define _HEAPQ__HEAPREPLACE_MAX_METHODDEF    \
    {"_heapreplace_max", (PyCFunction)(void(*)(void))_heapq__heapreplace_max, METH_FASTCALL, _heapq__heapreplace_max__doc__},

static PyObject *
_heapq__heapreplace_max_impl(PyObject *module, PyObject *heap,
                             PyObject *item);

static PyObject *
_heapq__heapreplace_max(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *heap;
    PyObject *item;

    if (!_PyArg_CheckPositional("_heapreplace_max", nargs, 2, 2)) {
        goto exit;
    }
    heap = args[0];
    item = args[1];
    return_value = _heapq__heapreplace_max_impl(module, heap, item);

exit:
    return return_value;
}

PyDoc_STRVAR(_heapq__heapify_max__doc__,
"_heapify_max($module, heap, /)\n"
"--\n"
"\n"
"Maxheap variant of heapify.");

#define _HEAPQ__HEAPIFY_MAX_METHODDEF    \
    {"_heapify_max", (PyCFunction)_heapq__heapify_max, METH_O, _heapq__heapify_max__doc__},
/*[clinic end generated code: output=37ef2a3319971c8d input=a9049054013a1b77]*/
