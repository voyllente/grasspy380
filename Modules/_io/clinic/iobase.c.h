/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(_io__IOBase_tell__doc__,
"tell($self, /)\n"
"--\n"
"\n"
"Return current stream position.");

#define _IO__IOBASE_TELL_METHODDEF    \
    {"tell", (PyCFunction)_io__IOBase_tell, METH_NOARGS, _io__IOBase_tell__doc__},

PyDoc_STRVAR(_io__IOBase_定位__doc__,
"定位($self, /)\n"
"--\n"
"\n"
"返回当前流位置.");

#define _IO__IOBASE_定位_METHODDEF    \
    {"定位", (PyCFunction)_io__IOBase_tell, METH_NOARGS, _io__IOBase_定位__doc__},

static PyObject *
_io__IOBase_tell_impl(PyObject *self);

static PyObject *
_io__IOBase_tell(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_tell_impl(self);
}

PyDoc_STRVAR(_io__IOBase_flush__doc__,
"flush($self, /)\n"
"--\n"
"\n"
"Flush write buffers, if applicable.\n"
"\n"
"This is not implemented for read-only and non-blocking streams.");

#define _IO__IOBASE_FLUSH_METHODDEF    \
    {"flush", (PyCFunction)_io__IOBase_flush, METH_NOARGS, _io__IOBase_flush__doc__},

PyDoc_STRVAR(_io__IOBase_强制刷新__doc__,
"强制刷新($self, /)\n"
"--\n"
"\n"
"强制刷新写缓冲区, 如适用的话.\n"
"\n"
"这不是针对只读和非阻塞流实现的.");

#define _IO__IOBASE_强制刷新_METHODDEF    \
    {"强制刷新", (PyCFunction)_io__IOBase_flush, METH_NOARGS, _io__IOBase_强制刷新__doc__},

static PyObject *
_io__IOBase_flush_impl(PyObject *self);

static PyObject *
_io__IOBase_flush(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_flush_impl(self);
}

PyDoc_STRVAR(_io__IOBase_close__doc__,
"close($self, /)\n"
"--\n"
"\n"
"Flush and close the IO object.\n"
"\n"
"This method has no effect if the file is already closed.");

#define _IO__IOBASE_CLOSE_METHODDEF    \
    {"close", (PyCFunction)_io__IOBase_close, METH_NOARGS, _io__IOBase_close__doc__},

PyDoc_STRVAR(_io__IOBase_关闭__doc__,
"关闭($self, /)\n"
"--\n"
"\n"
"强制刷新并关闭 IO 对象.\n"
"\n"
"若文件已经关闭, 则此方法无作用.");

#define _IO__IOBASE_关闭_METHODDEF    \
    {"关闭", (PyCFunction)_io__IOBase_close, METH_NOARGS, _io__IOBase_关闭__doc__},

static PyObject *
_io__IOBase_close_impl(PyObject *self);

static PyObject *
_io__IOBase_close(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_close_impl(self);
}

PyDoc_STRVAR(_io__IOBase_seekable__doc__,
"seekable($self, /)\n"
"--\n"
"\n"
"Return whether object supports random access.\n"
"\n"
"If False, seek(), tell() and truncate() will raise OSError.\n"
"This method may need to do a test seek().");

#define _IO__IOBASE_SEEKABLE_METHODDEF    \
    {"seekable", (PyCFunction)_io__IOBase_seekable, METH_NOARGS, _io__IOBase_seekable__doc__},

PyDoc_STRVAR(_io__IOBase_可寻__doc__,
"可寻($self, /)\n"
"--\n"
"\n"
"返回文件是否支持随机访问.\n"
"\n"
"若为假, 寻位(), 定位() 和 截取() 将抛出 OSError.\n"
"此方法可能需要进行一次测试 寻位().");

#define _IO__IOBASE_可寻_METHODDEF    \
    {"可寻", (PyCFunction)_io__IOBase_seekable, METH_NOARGS, _io__IOBase_可寻__doc__},

static PyObject *
_io__IOBase_seekable_impl(PyObject *self);

static PyObject *
_io__IOBase_seekable(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_seekable_impl(self);
}

PyDoc_STRVAR(_io__IOBase_readable__doc__,
"readable($self, /)\n"
"--\n"
"\n"
"Return whether object was opened for reading.\n"
"\n"
"If False, read() will raise OSError.");

#define _IO__IOBASE_READABLE_METHODDEF    \
    {"readable", (PyCFunction)_io__IOBase_readable, METH_NOARGS, _io__IOBase_readable__doc__},

PyDoc_STRVAR(_io__IOBase_可读__doc__,
"可读($self, /)\n"
"--\n"
"\n"
"返回对象是否是为读取而打开.\n"
"\n"
"若为假, 读取() 将抛出 OSError.");

#define _IO__IOBASE_可读_METHODDEF    \
    {"可读", (PyCFunction)_io__IOBase_readable, METH_NOARGS, _io__IOBase_可读__doc__},

static PyObject *
_io__IOBase_readable_impl(PyObject *self);

static PyObject *
_io__IOBase_readable(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_readable_impl(self);
}

PyDoc_STRVAR(_io__IOBase_writable__doc__,
"writable($self, /)\n"
"--\n"
"\n"
"Return whether object was opened for writing.\n"
"\n"
"If False, write() will raise OSError.");

#define _IO__IOBASE_WRITABLE_METHODDEF    \
    {"writable", (PyCFunction)_io__IOBase_writable, METH_NOARGS, _io__IOBase_writable__doc__},

PyDoc_STRVAR(_io__IOBase_可写__doc__,
"可写($self, /)\n"
"--\n"
"\n"
"返回对象是否是为写入而打开.\n"
"\n"
"若为假, 写入() 将抛出 OSError.");

#define _IO__IOBASE_可写_METHODDEF    \
    {"可写", (PyCFunction)_io__IOBase_writable, METH_NOARGS, _io__IOBase_可写__doc__},

static PyObject *
_io__IOBase_writable_impl(PyObject *self);

static PyObject *
_io__IOBase_writable(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_writable_impl(self);
}

PyDoc_STRVAR(_io__IOBase_fileno__doc__,
"fileno($self, /)\n"
"--\n"
"\n"
"Returns underlying file descriptor if one exists.\n"
"\n"
"OSError is raised if the IO object does not use a file descriptor.");

#define _IO__IOBASE_FILENO_METHODDEF    \
    {"fileno", (PyCFunction)_io__IOBase_fileno, METH_NOARGS, _io__IOBase_fileno__doc__},

PyDoc_STRVAR(_io__IOBase_文件号__doc__,
"文件号($self, /)\n"
"--\n"
"\n"
"返回底层文件描述符 (如有).\n"
"\n"
"若 IO 对象不使用文件描述符, 则抛出 OSError.");

#define _IO__IOBASE_文件号_METHODDEF    \
    {"文件号", (PyCFunction)_io__IOBase_fileno, METH_NOARGS, _io__IOBase_文件号__doc__},

static PyObject *
_io__IOBase_fileno_impl(PyObject *self);

static PyObject *
_io__IOBase_fileno(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_fileno_impl(self);
}

PyDoc_STRVAR(_io__IOBase_isatty__doc__,
"isatty($self, /)\n"
"--\n"
"\n"
"Return whether this is an \'interactive\' stream.\n"
"\n"
"Return False if it can\'t be determined.");

#define _IO__IOBASE_ISATTY_METHODDEF    \
    {"isatty", (PyCFunction)_io__IOBase_isatty, METH_NOARGS, _io__IOBase_isatty__doc__},

PyDoc_STRVAR(_io__IOBase_是终端__doc__,
"是终端($self, /)\n"
"--\n"
"\n"
"返回这是否是一个 \'交互\' 流.\n"
"\n"
"若无法确定则返回假.");

#define _IO__IOBASE_是终端_METHODDEF    \
    {"是终端", (PyCFunction)_io__IOBase_isatty, METH_NOARGS, _io__IOBase_是终端__doc__},

static PyObject *
_io__IOBase_isatty_impl(PyObject *self);

static PyObject *
_io__IOBase_isatty(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__IOBase_isatty_impl(self);
}

PyDoc_STRVAR(_io__IOBase_readline__doc__,
"readline($self, size=-1, /)\n"
"--\n"
"\n"
"Read and return a line from the stream.\n"
"\n"
"If size is specified, at most size bytes will be read.\n"
"\n"
"The line terminator is always b\'\\n\' for binary files; for text\n"
"files, the newlines argument to open can be used to select the line\n"
"terminator(s) recognized.");

#define _IO__IOBASE_READLINE_METHODDEF    \
    {"readline", (PyCFunction)(void(*)(void))_io__IOBase_readline, METH_FASTCALL, _io__IOBase_readline__doc__},

PyDoc_STRVAR(_io__IOBase_读一行__doc__,
"读一行($self, 大小=-1, /)\n"
"--\n"
"\n"
"从流中读取并返回一行.\n"
"\n"
"若指定了 大小, 则至多读取 大小 个字节.\n"
"\n"
"对于二进制文件, 行结束符总是 b\'\\n\';\n"
"对于文本文件, 打开 方法的换行符参数可用来选择所识别的行结束符.");

#define _IO__IOBASE_读一行_METHODDEF    \
    {"读一行", (PyCFunction)(void(*)(void))_io__IOBase_readline, METH_FASTCALL, _io__IOBase_读一行__doc__},

static PyObject *
_io__IOBase_readline_impl(PyObject *self, Py_ssize_t limit);

static PyObject *
_io__IOBase_readline(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t limit = -1;

    if (!_PyArg_CheckPositional("readline", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (!_Py_convert_optional_to_ssize_t(args[0], &limit)) {
        goto exit;
    }
skip_optional:
    return_value = _io__IOBase_readline_impl(self, limit);

exit:
    return return_value;
}

PyDoc_STRVAR(_io__IOBase_readlines__doc__,
"readlines($self, hint=-1, /)\n"
"--\n"
"\n"
"Return a list of lines from the stream.\n"
"\n"
"hint can be specified to control the number of lines read: no more\n"
"lines will be read if the total size (in bytes/characters) of all\n"
"lines so far exceeds hint.");

#define _IO__IOBASE_READLINES_METHODDEF    \
    {"readlines", (PyCFunction)(void(*)(void))_io__IOBase_readlines, METH_FASTCALL, _io__IOBase_readlines__doc__},

PyDoc_STRVAR(_io__IOBase_读多行__doc__,
"读多行($self, 提示=-1, /)\n"
"--\n"
"\n"
"返回流中行的列表.\n"
"\n"
"提示 可用来控制读取的行数: 如果截至当前读取的所有行的\n"
"总字节数/字符数超过 提示 值, 则不再读取更多行.");

#define _IO__IOBASE_读多行_METHODDEF    \
    {"读多行", (PyCFunction)(void(*)(void))_io__IOBase_readlines, METH_FASTCALL, _io__IOBase_读多行__doc__},

static PyObject *
_io__IOBase_readlines_impl(PyObject *self, Py_ssize_t hint);

static PyObject *
_io__IOBase_readlines(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t hint = -1;

    if (!_PyArg_CheckPositional("readlines", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (!_Py_convert_optional_to_ssize_t(args[0], &hint)) {
        goto exit;
    }
skip_optional:
    return_value = _io__IOBase_readlines_impl(self, hint);

exit:
    return return_value;
}

PyDoc_STRVAR(_io__IOBase_writelines__doc__,
"writelines($self, lines, /)\n"
"--\n"
"\n"
"Write a list of lines to stream.\n"
"\n"
"Line separators are not added, so it is usual for each of the\n"
"lines provided to have a line separator at the end.");

#define _IO__IOBASE_WRITELINES_METHODDEF    \
    {"writelines", (PyCFunction)_io__IOBase_writelines, METH_O, _io__IOBase_writelines__doc__},

PyDoc_STRVAR(_io__IOBase_写入行__doc__,
"写入行($self, 行, /)\n"
"--\n"
"\n"
"将一个行列表写入流.\n"
"\n"
"不添加行分隔符, 因此所提供的每行末尾通常应有一个行分隔符.");

#define _IO__IOBASE_写入行_METHODDEF    \
    {"写入行", (PyCFunction)_io__IOBase_writelines, METH_O, _io__IOBase_写入行__doc__},

PyDoc_STRVAR(_io__RawIOBase_read__doc__,
"read($self, size=-1, /)\n"
"--\n"
"\n");

#define _IO__RAWIOBASE_READ_METHODDEF    \
    {"read", (PyCFunction)(void(*)(void))_io__RawIOBase_read, METH_FASTCALL, _io__RawIOBase_read__doc__},

PyDoc_STRVAR(_io__RawIOBase_读取__doc__,
"读取($self, 大小=-1, /)\n"
"--\n"
"\n");

#define _IO__RAWIOBASE_读取_METHODDEF    \
    {"读取", (PyCFunction)(void(*)(void))_io__RawIOBase_read, METH_FASTCALL, _io__RawIOBase_读取__doc__},

static PyObject *
_io__RawIOBase_read_impl(PyObject *self, Py_ssize_t n);

static PyObject *
_io__RawIOBase_read(PyObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_ssize_t n = -1;

    if (!_PyArg_CheckPositional("read", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        n = ival;
    }
skip_optional:
    return_value = _io__RawIOBase_read_impl(self, n);

exit:
    return return_value;
}

PyDoc_STRVAR(_io__RawIOBase_readall__doc__,
"readall($self, /)\n"
"--\n"
"\n"
"Read until EOF, using multiple read() call.");

#define _IO__RAWIOBASE_READALL_METHODDEF    \
    {"readall", (PyCFunction)_io__RawIOBase_readall, METH_NOARGS, _io__RawIOBase_readall__doc__},

PyDoc_STRVAR(_io__RawIOBase_读取全部__doc__,
"读取全部($self, /)\n"
"--\n"
"\n"
"读取到 EOF 为止, 使用多次 读取() 调用.");

#define _IO__RAWIOBASE_读取全部_METHODDEF    \
    {"读取全部", (PyCFunction)_io__RawIOBase_readall, METH_NOARGS, _io__RawIOBase_读取全部__doc__},

static PyObject *
_io__RawIOBase_readall_impl(PyObject *self);

static PyObject *
_io__RawIOBase_readall(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return _io__RawIOBase_readall_impl(self);
}
/*[clinic end generated code: output=61b6ea7153ef9940 input=a9049054013a1b77]*/
