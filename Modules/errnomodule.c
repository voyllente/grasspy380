
/* Errno module */

#include "Python.h"

/* Windows socket errors (WSA*)  */
#ifdef MS_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
/* The following constants were added to errno.h in VS2010 but have
   preferred WSA equivalents. */
#undef EADDRINUSE
#undef EADDRNOTAVAIL
#undef EAFNOSUPPORT
#undef EALREADY
#undef ECONNABORTED
#undef ECONNREFUSED
#undef ECONNRESET
#undef EDESTADDRREQ
#undef EHOSTUNREACH
#undef EINPROGRESS
#undef EISCONN
#undef ELOOP
#undef EMSGSIZE
#undef ENETDOWN
#undef ENETRESET
#undef ENETUNREACH
#undef ENOBUFS
#undef ENOPROTOOPT
#undef ENOTCONN
#undef ENOTSOCK
#undef EOPNOTSUPP
#undef EPROTONOSUPPORT
#undef EPROTOTYPE
#undef ETIMEDOUT
#undef EWOULDBLOCK
#endif

/*
 * Pull in the system error definitions
 */

static PyMethodDef errno_methods[] = {
    {NULL,              NULL}
};

/* Helper function doing the dictionary inserting */

static void
_inscode(PyObject *d, PyObject *de, const char *name, int code)
{
    PyObject *u = PyUnicode_FromString(name);
    PyObject *v = PyLong_FromLong((long) code);

    /* Don't bother checking for errors; they'll be caught at the end
     * of the module initialization function by the caller of
     * initerrno().
     */
    if (u && v) {
        /* insert in modules dict */
        PyDict_SetItem(d, u, v);
        /* insert in errorcode dict */
        PyDict_SetItem(de, v, u);
    }
    Py_XDECREF(u);
    Py_XDECREF(v);
}

PyDoc_STRVAR(errno__doc__,
"This module makes available standard errno system symbols.\n\
\n\
The value of each symbol is the corresponding integer value,\n\
e.g., on most systems, errno.ENOENT equals the integer 2.\n\
\n\
The dictionary errno.errorcode maps numeric codes to symbol names,\n\
e.g., errno.errorcode[2] could be the string 'ENOENT'.\n\
\n\
Symbols that are not relevant to the underlying system are not defined.\n\
\n\
To map error codes to error messages, use the function os.strerror(),\n\
e.g. os.strerror(2) could return 'No such file or directory'.");

static struct PyModuleDef errnomodule = {
    PyModuleDef_HEAD_INIT,
    "errno",
    errno__doc__,
    -1,
    errno_methods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC
PyInit_errno(void)
{
    PyObject *m, *d, *de;
    m = PyModule_Create(&errnomodule);
    if (m == NULL)
        return NULL;
    d = PyModule_GetDict(m);
    de = PyDict_New();
    if (!d || !de || PyDict_SetItemString(d, "errorcode", de) < 0)
        return NULL;

/* Macro so I don't have to edit each and every line below... */
#define inscode(d, ds, de, name, code, comment) _inscode(d, de, name, code)

    /*
     * The names and comments are borrowed from linux/include/errno.h,
     * which should be pretty all-inclusive.  However, the Solaris specific
     * names and comments are borrowed from sys/errno.h in Solaris.
     * MacOSX specific names and comments are borrowed from sys/errno.h in
     * MacOSX.
     */

#ifdef ENODEV
    inscode(d, ds, de, "ENODEV", ENODEV, "无此设备");
#endif
#ifdef ENOCSI
    inscode(d, ds, de, "ENOCSI", ENOCSI, "无可用 CSI 结构");
#endif
#ifdef EHOSTUNREACH
    inscode(d, ds, de, "EHOSTUNREACH", EHOSTUNREACH, "没有到主机的路由");
#else
#ifdef WSAEHOSTUNREACH
    inscode(d, ds, de, "EHOSTUNREACH", WSAEHOSTUNREACH, "没有到主机的路由");
#endif
#endif
#ifdef ENOMSG
    inscode(d, ds, de, "ENOMSG", ENOMSG, "无所需类型的消息");
#endif
#ifdef EUCLEAN
    inscode(d, ds, de, "EUCLEAN", EUCLEAN, "结构需要清理");
#endif
#ifdef EL2NSYNC
    inscode(d, ds, de, "EL2NSYNC", EL2NSYNC, "级别 2 未同步");
#endif
#ifdef EL2HLT
    inscode(d, ds, de, "EL2HLT", EL2HLT, "级别 2 已停止");
#endif
#ifdef ENODATA
    inscode(d, ds, de, "ENODATA", ENODATA, "无可用数据");
#endif
#ifdef ENOTBLK
    inscode(d, ds, de, "ENOTBLK", ENOTBLK, "需要块设备");
#endif
#ifdef ENOSYS
    inscode(d, ds, de, "ENOSYS", ENOSYS, "功能未实现");
#endif
#ifdef EPIPE
    inscode(d, ds, de, "EPIPE", EPIPE, "管道已损坏");
#endif
#ifdef EINVAL
    inscode(d, ds, de, "EINVAL", EINVAL, "无效参数");
#else
#ifdef WSAEINVAL
    inscode(d, ds, de, "EINVAL", WSAEINVAL, "无效参数");
#endif
#endif
#ifdef EOVERFLOW
    inscode(d, ds, de, "EOVERFLOW", EOVERFLOW, "值相对于所定义数据类型过大");
#endif
#ifdef EADV
    inscode(d, ds, de, "EADV", EADV, "广告错误");
#endif
#ifdef EINTR
    inscode(d, ds, de, "EINTR", EINTR, "系统调用被中断");
#else
#ifdef WSAEINTR
    inscode(d, ds, de, "EINTR", WSAEINTR, "系统调用被中断");
#endif
#endif
#ifdef EUSERS
    inscode(d, ds, de, "EUSERS", EUSERS, "用户过多");
#else
#ifdef WSAEUSERS
    inscode(d, ds, de, "EUSERS", WSAEUSERS, "用户过多");
#endif
#endif
#ifdef ENOTEMPTY
    inscode(d, ds, de, "ENOTEMPTY", ENOTEMPTY, "目录非空");
#else
#ifdef WSAENOTEMPTY
    inscode(d, ds, de, "ENOTEMPTY", WSAENOTEMPTY, "目录非空");
#endif
#endif
#ifdef ENOBUFS
    inscode(d, ds, de, "ENOBUFS", ENOBUFS, "无可用缓冲空间");
#else
#ifdef WSAENOBUFS
    inscode(d, ds, de, "ENOBUFS", WSAENOBUFS, "无可用缓冲空间");
#endif
#endif
#ifdef EPROTO
    inscode(d, ds, de, "EPROTO", EPROTO, "协议错误");
#endif
#ifdef EREMOTE
    inscode(d, ds, de, "EREMOTE", EREMOTE, "对象在远程");
#else
#ifdef WSAEREMOTE
    inscode(d, ds, de, "EREMOTE", WSAEREMOTE, "对象在远程");
#endif
#endif
#ifdef ENAVAIL
    inscode(d, ds, de, "ENAVAIL", ENAVAIL, "无可用 XENIX 信号量");
#endif
#ifdef ECHILD
    inscode(d, ds, de, "ECHILD", ECHILD, "无子进程");
#endif
#ifdef ELOOP
    inscode(d, ds, de, "ELOOP", ELOOP, "遇到过多符号链接");
#else
#ifdef WSAELOOP
    inscode(d, ds, de, "ELOOP", WSAELOOP, "遇到过多符号链接");
#endif
#endif
#ifdef EXDEV
    inscode(d, ds, de, "EXDEV", EXDEV, "跨设备链接");
#endif
#ifdef E2BIG
    inscode(d, ds, de, "E2BIG", E2BIG, "参数列表过长");
#endif
#ifdef ESRCH
    inscode(d, ds, de, "ESRCH", ESRCH, "无此进程");
#endif
#ifdef EMSGSIZE
    inscode(d, ds, de, "EMSGSIZE", EMSGSIZE, "消息过长");
#else
#ifdef WSAEMSGSIZE
    inscode(d, ds, de, "EMSGSIZE", WSAEMSGSIZE, "消息过长");
#endif
#endif
#ifdef EAFNOSUPPORT
    inscode(d, ds, de, "EAFNOSUPPORT", EAFNOSUPPORT, "地址族不受协议支持");
#else
#ifdef WSAEAFNOSUPPORT
    inscode(d, ds, de, "EAFNOSUPPORT", WSAEAFNOSUPPORT, "地址族不受协议支持");
#endif
#endif
#ifdef EBADR
    inscode(d, ds, de, "EBADR", EBADR, "无效请求描述符");
#endif
#ifdef EHOSTDOWN
    inscode(d, ds, de, "EHOSTDOWN", EHOSTDOWN, "主机已关闭");
#else
#ifdef WSAEHOSTDOWN
    inscode(d, ds, de, "EHOSTDOWN", WSAEHOSTDOWN, "主机已关闭");
#endif
#endif
#ifdef EPFNOSUPPORT
    inscode(d, ds, de, "EPFNOSUPPORT", EPFNOSUPPORT, "协议族不受支持");
#else
#ifdef WSAEPFNOSUPPORT
    inscode(d, ds, de, "EPFNOSUPPORT", WSAEPFNOSUPPORT, "协议族不受支持");
#endif
#endif
#ifdef ENOPROTOOPT
    inscode(d, ds, de, "ENOPROTOOPT", ENOPROTOOPT, "协议不可用");
#else
#ifdef WSAENOPROTOOPT
    inscode(d, ds, de, "ENOPROTOOPT", WSAENOPROTOOPT, "协议不可用");
#endif
#endif
#ifdef EBUSY
    inscode(d, ds, de, "EBUSY", EBUSY, "设备或资源忙");
#endif
#ifdef EWOULDBLOCK
    inscode(d, ds, de, "EWOULDBLOCK", EWOULDBLOCK, "操作会阻塞");
#else
#ifdef WSAEWOULDBLOCK
    inscode(d, ds, de, "EWOULDBLOCK", WSAEWOULDBLOCK, "操作会阻塞");
#endif
#endif
#ifdef EBADFD
    inscode(d, ds, de, "EBADFD", EBADFD, "文件描述符处于错误状态");
#endif
#ifdef EDOTDOT
    inscode(d, ds, de, "EDOTDOT", EDOTDOT, "RFS 特定错误");
#endif
#ifdef EISCONN
    inscode(d, ds, de, "EISCONN", EISCONN, "传输端点已连接");
#else
#ifdef WSAEISCONN
    inscode(d, ds, de, "EISCONN", WSAEISCONN, "传输端点已连接");
#endif
#endif
#ifdef ENOANO
    inscode(d, ds, de, "ENOANO", ENOANO, "无阳极");
#endif
#ifdef ESHUTDOWN
    inscode(d, ds, de, "ESHUTDOWN", ESHUTDOWN, "传输端点关闭后无法发送");
#else
#ifdef WSAESHUTDOWN
    inscode(d, ds, de, "ESHUTDOWN", WSAESHUTDOWN, "传输端点关闭后无法发送");
#endif
#endif
#ifdef ECHRNG
    inscode(d, ds, de, "ECHRNG", ECHRNG, "信道编号超出范围");
#endif
#ifdef ELIBBAD
    inscode(d, ds, de, "ELIBBAD", ELIBBAD, "访问已损坏共享库");
#endif
#ifdef ENONET
    inscode(d, ds, de, "ENONET", ENONET, "机器不在网络上");
#endif
#ifdef EBADE
    inscode(d, ds, de, "EBADE", EBADE, "无效交换");
#endif
#ifdef EBADF
    inscode(d, ds, de, "EBADF", EBADF, "错误文件号");
#else
#ifdef WSAEBADF
    inscode(d, ds, de, "EBADF", WSAEBADF, "错误文件号");
#endif
#endif
#ifdef EMULTIHOP
    inscode(d, ds, de, "EMULTIHOP", EMULTIHOP, "已尝试多跳");
#endif
#ifdef EIO
    inscode(d, ds, de, "EIO", EIO, "I/O 错误");
#endif
#ifdef EUNATCH
    inscode(d, ds, de, "EUNATCH", EUNATCH, "未附加协议驱动");
#endif
#ifdef EPROTOTYPE
    inscode(d, ds, de, "EPROTOTYPE", EPROTOTYPE, "套接字协议类型错误");
#else
#ifdef WSAEPROTOTYPE
    inscode(d, ds, de, "EPROTOTYPE", WSAEPROTOTYPE, "套接字协议类型错误");
#endif
#endif
#ifdef ENOSPC
    inscode(d, ds, de, "ENOSPC", ENOSPC, "设备已无可用空间");
#endif
#ifdef ENOEXEC
    inscode(d, ds, de, "ENOEXEC", ENOEXEC, "执行格式错误");
#endif
#ifdef EALREADY
    inscode(d, ds, de, "EALREADY", EALREADY, "操作正在进行");
#else
#ifdef WSAEALREADY
    inscode(d, ds, de, "EALREADY", WSAEALREADY, "操作正在进行");
#endif
#endif
#ifdef ENETDOWN
    inscode(d, ds, de, "ENETDOWN", ENETDOWN, "网络已断开");
#else
#ifdef WSAENETDOWN
    inscode(d, ds, de, "ENETDOWN", WSAENETDOWN, "网络已断开");
#endif
#endif
#ifdef ENOTNAM
    inscode(d, ds, de, "ENOTNAM", ENOTNAM, "非 XENIX 命名类型文件");
#endif
#ifdef EACCES
    inscode(d, ds, de, "EACCES", EACCES, "没有权限");
#else
#ifdef WSAEACCES
    inscode(d, ds, de, "EACCES", WSAEACCES, "没有权限");
#endif
#endif
#ifdef ELNRNG
    inscode(d, ds, de, "ELNRNG", ELNRNG, "链接编号超出范围");
#endif
#ifdef EILSEQ
    inscode(d, ds, de, "EILSEQ", EILSEQ, "非法字节序列");
#endif
#ifdef ENOTDIR
    inscode(d, ds, de, "ENOTDIR", ENOTDIR, "不是目录");
#endif
#ifdef ENOTUNIQ
    inscode(d, ds, de, "ENOTUNIQ", ENOTUNIQ, "名称在网络上不唯一");
#endif
#ifdef EPERM
    inscode(d, ds, de, "EPERM", EPERM, "操作不被允许");
#endif
#ifdef EDOM
    inscode(d, ds, de, "EDOM", EDOM, "数学参数超出函数范围");
#endif
#ifdef EXFULL
    inscode(d, ds, de, "EXFULL", EXFULL, "交换已满");
#endif
#ifdef ECONNREFUSED
    inscode(d, ds, de, "ECONNREFUSED", ECONNREFUSED, "连接被拒");
#else
#ifdef WSAECONNREFUSED
    inscode(d, ds, de, "ECONNREFUSED", WSAECONNREFUSED, "连接被拒");
#endif
#endif
#ifdef EISDIR
    inscode(d, ds, de, "EISDIR", EISDIR, "是目录");
#endif
#ifdef EPROTONOSUPPORT
    inscode(d, ds, de, "EPROTONOSUPPORT", EPROTONOSUPPORT, "协议不受支持");
#else
#ifdef WSAEPROTONOSUPPORT
    inscode(d, ds, de, "EPROTONOSUPPORT", WSAEPROTONOSUPPORT, "协议不受支持");
#endif
#endif
#ifdef EROFS
    inscode(d, ds, de, "EROFS", EROFS, "只读文件系统");
#endif
#ifdef EADDRNOTAVAIL
    inscode(d, ds, de, "EADDRNOTAVAIL", EADDRNOTAVAIL, "无法分配请求的地址");
#else
#ifdef WSAEADDRNOTAVAIL
    inscode(d, ds, de, "EADDRNOTAVAIL", WSAEADDRNOTAVAIL, "无法分配请求的地址");
#endif
#endif
#ifdef EIDRM
    inscode(d, ds, de, "EIDRM", EIDRM, "标识符被移除");
#endif
#ifdef ECOMM
    inscode(d, ds, de, "ECOMM", ECOMM, "发送时通信错误");
#endif
#ifdef ESRMNT
    inscode(d, ds, de, "ESRMNT", ESRMNT, "挂载错误");
#endif
#ifdef EREMOTEIO
    inscode(d, ds, de, "EREMOTEIO", EREMOTEIO, "远程 I/O 错误");
#endif
#ifdef EL3RST
    inscode(d, ds, de, "EL3RST", EL3RST, "级别 3 重置");
#endif
#ifdef EBADMSG
    inscode(d, ds, de, "EBADMSG", EBADMSG, "不是数据消息");
#endif
#ifdef ENFILE
    inscode(d, ds, de, "ENFILE", ENFILE, "文件表溢出");
#endif
#ifdef ELIBMAX
    inscode(d, ds, de, "ELIBMAX", ELIBMAX, "尝试链接过多共享库");
#endif
#ifdef ESPIPE
    inscode(d, ds, de, "ESPIPE", ESPIPE, "非法查找");
#endif
#ifdef ENOLINK
    inscode(d, ds, de, "ENOLINK", ENOLINK, "链接已被切断");
#endif
#ifdef ENETRESET
    inscode(d, ds, de, "ENETRESET", ENETRESET, "网络因重置而断开连接");
#else
#ifdef WSAENETRESET
    inscode(d, ds, de, "ENETRESET", WSAENETRESET, "网络因重置而断开连接");
#endif
#endif
#ifdef ETIMEDOUT
    inscode(d, ds, de, "ETIMEDOUT", ETIMEDOUT, "连接超时");
#else
#ifdef WSAETIMEDOUT
    inscode(d, ds, de, "ETIMEDOUT", WSAETIMEDOUT, "连接超时");
#endif
#endif
#ifdef ENOENT
    inscode(d, ds, de, "ENOENT", ENOENT, "无此文件或目录");
#endif
#ifdef EEXIST
    inscode(d, ds, de, "EEXIST", EEXIST, "文件已存在");
#endif
#ifdef EDQUOT
    inscode(d, ds, de, "EDQUOT", EDQUOT, "超出配额");
#else
#ifdef WSAEDQUOT
    inscode(d, ds, de, "EDQUOT", WSAEDQUOT, "超出配额");
#endif
#endif
#ifdef ENOSTR
    inscode(d, ds, de, "ENOSTR", ENOSTR, "设备不是流");
#endif
#ifdef EBADSLT
    inscode(d, ds, de, "EBADSLT", EBADSLT, "无效槽位");
#endif
#ifdef EBADRQC
    inscode(d, ds, de, "EBADRQC", EBADRQC, "无效请求码");
#endif
#ifdef ELIBACC
    inscode(d, ds, de, "ELIBACC", ELIBACC, "无法访问所需共享库");
#endif
#ifdef EFAULT
    inscode(d, ds, de, "EFAULT", EFAULT, "地址错误");
#else
#ifdef WSAEFAULT
    inscode(d, ds, de, "EFAULT", WSAEFAULT, "地址错误");
#endif
#endif
#ifdef EFBIG
    inscode(d, ds, de, "EFBIG", EFBIG, "文件过大");
#endif
#ifdef EDEADLK
    inscode(d, ds, de, "EDEADLK", EDEADLK, "会发生资源死锁");
#endif
#ifdef ENOTCONN
    inscode(d, ds, de, "ENOTCONN", ENOTCONN, "传输端点未连接");
#else
#ifdef WSAENOTCONN
    inscode(d, ds, de, "ENOTCONN", WSAENOTCONN, "传输端点未连接");
#endif
#endif
#ifdef EDESTADDRREQ
    inscode(d, ds, de, "EDESTADDRREQ", EDESTADDRREQ, "需要目标地址");
#else
#ifdef WSAEDESTADDRREQ
    inscode(d, ds, de, "EDESTADDRREQ", WSAEDESTADDRREQ, "需要目标地址");
#endif
#endif
#ifdef ELIBSCN
    inscode(d, ds, de, "ELIBSCN", ELIBSCN, "a.out 中的 .lib 部分已损坏");
#endif
#ifdef ENOLCK
    inscode(d, ds, de, "ENOLCK", ENOLCK, "无可用记录锁");
#endif
#ifdef EISNAM
    inscode(d, ds, de, "EISNAM", EISNAM, "是命名类型文件");
#endif
#ifdef ECONNABORTED
    inscode(d, ds, de, "ECONNABORTED", ECONNABORTED, "软件导致连接中止");
#else
#ifdef WSAECONNABORTED
    inscode(d, ds, de, "ECONNABORTED", WSAECONNABORTED, "软件导致连接中止");
#endif
#endif
#ifdef ENETUNREACH
    inscode(d, ds, de, "ENETUNREACH", ENETUNREACH, "网络不可达");
#else
#ifdef WSAENETUNREACH
    inscode(d, ds, de, "ENETUNREACH", WSAENETUNREACH, "网络不可达");
#endif
#endif
#ifdef ESTALE
    inscode(d, ds, de, "ESTALE", ESTALE, "NFS 文件句柄过期");
#else
#ifdef WSAESTALE
    inscode(d, ds, de, "ESTALE", WSAESTALE, "NFS 文件句柄过期");
#endif
#endif
#ifdef ENOSR
    inscode(d, ds, de, "ENOSR", ENOSR, "流资源不足");
#endif
#ifdef ENOMEM
    inscode(d, ds, de, "ENOMEM", ENOMEM, "内存不足");
#endif
#ifdef ENOTSOCK
    inscode(d, ds, de, "ENOTSOCK", ENOTSOCK, "在非套接字上执行套接字操作");
#else
#ifdef WSAENOTSOCK
    inscode(d, ds, de, "ENOTSOCK", WSAENOTSOCK, "在非套接字上执行套接字操作");
#endif
#endif
#ifdef ESTRPIPE
    inscode(d, ds, de, "ESTRPIPE", ESTRPIPE, "流管道错误");
#endif
#ifdef EMLINK
    inscode(d, ds, de, "EMLINK", EMLINK, "链接过多");
#endif
#ifdef ERANGE
    inscode(d, ds, de, "ERANGE", ERANGE, "数学运算结果无法表示");
#endif
#ifdef ELIBEXEC
    inscode(d, ds, de, "ELIBEXEC", ELIBEXEC, "无法直接执行共享库");
#endif
#ifdef EL3HLT
    inscode(d, ds, de, "EL3HLT", EL3HLT, "级别 3 已停止");
#endif
#ifdef ECONNRESET
    inscode(d, ds, de, "ECONNRESET", ECONNRESET, "连接被对方重置");
#else
#ifdef WSAECONNRESET
    inscode(d, ds, de, "ECONNRESET", WSAECONNRESET, "连接被对方重置");
#endif
#endif
#ifdef EADDRINUSE
    inscode(d, ds, de, "EADDRINUSE", EADDRINUSE, "地址已被使用");
#else
#ifdef WSAEADDRINUSE
    inscode(d, ds, de, "EADDRINUSE", WSAEADDRINUSE, "地址已被使用");
#endif
#endif
#ifdef EOPNOTSUPP
    inscode(d, ds, de, "EOPNOTSUPP", EOPNOTSUPP, "操作在传输端点上不受支持");
#else
#ifdef WSAEOPNOTSUPP
    inscode(d, ds, de, "EOPNOTSUPP", WSAEOPNOTSUPP, "操作在传输端点上不受支持");
#endif
#endif
#ifdef EREMCHG
    inscode(d, ds, de, "EREMCHG", EREMCHG, "远程地址已改变");
#endif
#ifdef EAGAIN
    inscode(d, ds, de, "EAGAIN", EAGAIN, "请重试");
#endif
#ifdef ENAMETOOLONG
    inscode(d, ds, de, "ENAMETOOLONG", ENAMETOOLONG, "文件名过长");
#else
#ifdef WSAENAMETOOLONG
    inscode(d, ds, de, "ENAMETOOLONG", WSAENAMETOOLONG, "文件名过长");
#endif
#endif
#ifdef ENOTTY
    inscode(d, ds, de, "ENOTTY", ENOTTY, "不是打字机");
#endif
#ifdef ERESTART
    inscode(d, ds, de, "ERESTART", ERESTART, "已中断系统调用需要重启");
#endif
#ifdef ESOCKTNOSUPPORT
    inscode(d, ds, de, "ESOCKTNOSUPPORT", ESOCKTNOSUPPORT, "套接字类型不受支持");
#else
#ifdef WSAESOCKTNOSUPPORT
    inscode(d, ds, de, "ESOCKTNOSUPPORT", WSAESOCKTNOSUPPORT, "套接字类型不受支持");
#endif
#endif
#ifdef ETIME
    inscode(d, ds, de, "ETIME", ETIME, "定时器已到期");
#endif
#ifdef EBFONT
    inscode(d, ds, de, "EBFONT", EBFONT, "字体文件格式错误");
#endif
#ifdef EDEADLOCK
    inscode(d, ds, de, "EDEADLOCK", EDEADLOCK, "文件锁定死锁错误");
#endif
#ifdef ETOOMANYREFS
    inscode(d, ds, de, "ETOOMANYREFS", ETOOMANYREFS, "引用过多: 无法拼接");
#else
#ifdef WSAETOOMANYREFS
    inscode(d, ds, de, "ETOOMANYREFS", WSAETOOMANYREFS, "引用过多: 无法拼接");
#endif
#endif
#ifdef EMFILE
    inscode(d, ds, de, "EMFILE", EMFILE, "打开文件过多");
#else
#ifdef WSAEMFILE
    inscode(d, ds, de, "EMFILE", WSAEMFILE, "打开文件过多");
#endif
#endif
#ifdef ETXTBSY
    inscode(d, ds, de, "ETXTBSY", ETXTBSY, "文本文件忙");
#endif
#ifdef EINPROGRESS
    inscode(d, ds, de, "EINPROGRESS", EINPROGRESS, "操作正在进行");
#else
#ifdef WSAEINPROGRESS
    inscode(d, ds, de, "EINPROGRESS", WSAEINPROGRESS, "操作正在进行");
#endif
#endif
#ifdef ENXIO
    inscode(d, ds, de, "ENXIO", ENXIO, "无此设备或地址");
#endif
#ifdef ENOPKG
    inscode(d, ds, de, "ENOPKG", ENOPKG, "包未安装");
#endif
#ifdef WSASY
    inscode(d, ds, de, "WSASY", WSASY, "Error WSASY");
#endif
#ifdef WSAEHOSTDOWN
    inscode(d, ds, de, "WSAEHOSTDOWN", WSAEHOSTDOWN, "主机已关闭");
#endif
#ifdef WSAENETDOWN
    inscode(d, ds, de, "WSAENETDOWN", WSAENETDOWN, "网络已断开");
#endif
#ifdef WSAENOTSOCK
    inscode(d, ds, de, "WSAENOTSOCK", WSAENOTSOCK, "在非套接字上执行套接字操作");
#endif
#ifdef WSAEHOSTUNREACH
    inscode(d, ds, de, "WSAEHOSTUNREACH", WSAEHOSTUNREACH, "没有到主机的路由");
#endif
#ifdef WSAELOOP
    inscode(d, ds, de, "WSAELOOP", WSAELOOP, "遇到过多符号链接");
#endif
#ifdef WSAEMFILE
    inscode(d, ds, de, "WSAEMFILE", WSAEMFILE, "打开文件过多");
#endif
#ifdef WSAESTALE
    inscode(d, ds, de, "WSAESTALE", WSAESTALE, "NFS 文件句柄过期");
#endif
#ifdef WSAVERNOTSUPPORTED
    inscode(d, ds, de, "WSAVERNOTSUPPORTED", WSAVERNOTSUPPORTED, "Error WSAVERNOTSUPPORTED");
#endif
#ifdef WSAENETUNREACH
    inscode(d, ds, de, "WSAENETUNREACH", WSAENETUNREACH, "网络不可达");
#endif
#ifdef WSAEPROCLIM
    inscode(d, ds, de, "WSAEPROCLIM", WSAEPROCLIM, "Error WSAEPROCLIM");
#endif
#ifdef WSAEFAULT
    inscode(d, ds, de, "WSAEFAULT", WSAEFAULT, "地址错误");
#endif
#ifdef WSANOTINITIALISED
    inscode(d, ds, de, "WSANOTINITIALISED", WSANOTINITIALISED, "Error WSANOTINITIALISED");
#endif
#ifdef WSAEUSERS
    inscode(d, ds, de, "WSAEUSERS", WSAEUSERS, "用户过多");
#endif
#ifdef WSAMAKEASYNCREPL
    inscode(d, ds, de, "WSAMAKEASYNCREPL", WSAMAKEASYNCREPL, "Error WSAMAKEASYNCREPL");
#endif
#ifdef WSAENOPROTOOPT
    inscode(d, ds, de, "WSAENOPROTOOPT", WSAENOPROTOOPT, "协议不可用");
#endif
#ifdef WSAECONNABORTED
    inscode(d, ds, de, "WSAECONNABORTED", WSAECONNABORTED, "软件导致连接中止");
#endif
#ifdef WSAENAMETOOLONG
    inscode(d, ds, de, "WSAENAMETOOLONG", WSAENAMETOOLONG, "文件名过长");
#endif
#ifdef WSAENOTEMPTY
    inscode(d, ds, de, "WSAENOTEMPTY", WSAENOTEMPTY, "目录非空");
#endif
#ifdef WSAESHUTDOWN
    inscode(d, ds, de, "WSAESHUTDOWN", WSAESHUTDOWN, "传输端点关闭后无法发送");
#endif
#ifdef WSAEAFNOSUPPORT
    inscode(d, ds, de, "WSAEAFNOSUPPORT", WSAEAFNOSUPPORT, "地址族不受协议支持");
#endif
#ifdef WSAETOOMANYREFS
    inscode(d, ds, de, "WSAETOOMANYREFS", WSAETOOMANYREFS, "引用过多: 无法拼接");
#endif
#ifdef WSAEACCES
    inscode(d, ds, de, "WSAEACCES", WSAEACCES, "没有权限");
#endif
#ifdef WSATR
    inscode(d, ds, de, "WSATR", WSATR, "Error WSATR");
#endif
#ifdef WSABASEERR
    inscode(d, ds, de, "WSABASEERR", WSABASEERR, "Error WSABASEERR");
#endif
#ifdef WSADESCRIPTIO
    inscode(d, ds, de, "WSADESCRIPTIO", WSADESCRIPTIO, "Error WSADESCRIPTIO");
#endif
#ifdef WSAEMSGSIZE
    inscode(d, ds, de, "WSAEMSGSIZE", WSAEMSGSIZE, "消息过长");
#endif
#ifdef WSAEBADF
    inscode(d, ds, de, "WSAEBADF", WSAEBADF, "错误文件号");
#endif
#ifdef WSAECONNRESET
    inscode(d, ds, de, "WSAECONNRESET", WSAECONNRESET, "连接被对方重置");
#endif
#ifdef WSAGETSELECTERRO
    inscode(d, ds, de, "WSAGETSELECTERRO", WSAGETSELECTERRO, "Error WSAGETSELECTERRO");
#endif
#ifdef WSAETIMEDOUT
    inscode(d, ds, de, "WSAETIMEDOUT", WSAETIMEDOUT, "连接超时");
#endif
#ifdef WSAENOBUFS
    inscode(d, ds, de, "WSAENOBUFS", WSAENOBUFS, "无可用缓冲空间");
#endif
#ifdef WSAEDISCON
    inscode(d, ds, de, "WSAEDISCON", WSAEDISCON, "Error WSAEDISCON");
#endif
#ifdef WSAEINTR
    inscode(d, ds, de, "WSAEINTR", WSAEINTR, "系统调用被中断");
#endif
#ifdef WSAEPROTOTYPE
    inscode(d, ds, de, "WSAEPROTOTYPE", WSAEPROTOTYPE, "套接字协议类型错误");
#endif
#ifdef WSAHOS
    inscode(d, ds, de, "WSAHOS", WSAHOS, "Error WSAHOS");
#endif
#ifdef WSAEADDRINUSE
    inscode(d, ds, de, "WSAEADDRINUSE", WSAEADDRINUSE, "地址已被使用");
#endif
#ifdef WSAEADDRNOTAVAIL
    inscode(d, ds, de, "WSAEADDRNOTAVAIL", WSAEADDRNOTAVAIL, "无法分配请求的地址");
#endif
#ifdef WSAEALREADY
    inscode(d, ds, de, "WSAEALREADY", WSAEALREADY, "操作正在进行");
#endif
#ifdef WSAEPROTONOSUPPORT
    inscode(d, ds, de, "WSAEPROTONOSUPPORT", WSAEPROTONOSUPPORT, "协议不受支持");
#endif
#ifdef WSASYSNOTREADY
    inscode(d, ds, de, "WSASYSNOTREADY", WSASYSNOTREADY, "Error WSASYSNOTREADY");
#endif
#ifdef WSAEWOULDBLOCK
    inscode(d, ds, de, "WSAEWOULDBLOCK", WSAEWOULDBLOCK, "操作会阻塞");
#endif
#ifdef WSAEPFNOSUPPORT
    inscode(d, ds, de, "WSAEPFNOSUPPORT", WSAEPFNOSUPPORT, "协议族不受支持");
#endif
#ifdef WSAEOPNOTSUPP
    inscode(d, ds, de, "WSAEOPNOTSUPP", WSAEOPNOTSUPP, "操作在传输端点上不受支持");
#endif
#ifdef WSAEISCONN
    inscode(d, ds, de, "WSAEISCONN", WSAEISCONN, "传输端点已连接");
#endif
#ifdef WSAEDQUOT
    inscode(d, ds, de, "WSAEDQUOT", WSAEDQUOT, "超出配额");
#endif
#ifdef WSAENOTCONN
    inscode(d, ds, de, "WSAENOTCONN", WSAENOTCONN, "传输端点未连接");
#endif
#ifdef WSAEREMOTE
    inscode(d, ds, de, "WSAEREMOTE", WSAEREMOTE, "对象在远程");
#endif
#ifdef WSAEINVAL
    inscode(d, ds, de, "WSAEINVAL", WSAEINVAL, "无效参数");
#endif
#ifdef WSAEINPROGRESS
    inscode(d, ds, de, "WSAEINPROGRESS", WSAEINPROGRESS, "操作正在进行");
#endif
#ifdef WSAGETSELECTEVEN
    inscode(d, ds, de, "WSAGETSELECTEVEN", WSAGETSELECTEVEN, "Error WSAGETSELECTEVEN");
#endif
#ifdef WSAESOCKTNOSUPPORT
    inscode(d, ds, de, "WSAESOCKTNOSUPPORT", WSAESOCKTNOSUPPORT, "套接字类型不受支持");
#endif
#ifdef WSAGETASYNCERRO
    inscode(d, ds, de, "WSAGETASYNCERRO", WSAGETASYNCERRO, "Error WSAGETASYNCERRO");
#endif
#ifdef WSAMAKESELECTREPL
    inscode(d, ds, de, "WSAMAKESELECTREPL", WSAMAKESELECTREPL, "Error WSAMAKESELECTREPL");
#endif
#ifdef WSAGETASYNCBUFLE
    inscode(d, ds, de, "WSAGETASYNCBUFLE", WSAGETASYNCBUFLE, "Error WSAGETASYNCBUFLE");
#endif
#ifdef WSAEDESTADDRREQ
    inscode(d, ds, de, "WSAEDESTADDRREQ", WSAEDESTADDRREQ, "需要目标地址");
#endif
#ifdef WSAECONNREFUSED
    inscode(d, ds, de, "WSAECONNREFUSED", WSAECONNREFUSED, "连接被拒");
#endif
#ifdef WSAENETRESET
    inscode(d, ds, de, "WSAENETRESET", WSAENETRESET, "网络因重置而断开连接");
#endif
#ifdef WSAN
    inscode(d, ds, de, "WSAN", WSAN, "Error WSAN");
#endif
#ifdef ENOMEDIUM
    inscode(d, ds, de, "ENOMEDIUM", ENOMEDIUM, "未找到媒体");
#endif
#ifdef EMEDIUMTYPE
    inscode(d, ds, de, "EMEDIUMTYPE", EMEDIUMTYPE, "媒体类型错误");
#endif
#ifdef ECANCELED
    inscode(d, ds, de, "ECANCELED", ECANCELED, "操作已取消");
#endif
#ifdef ENOKEY
    inscode(d, ds, de, "ENOKEY", ENOKEY, "所需密钥不可用");
#endif
#ifdef EKEYEXPIRED
    inscode(d, ds, de, "EKEYEXPIRED", EKEYEXPIRED, "密钥已过期");
#endif
#ifdef EKEYREVOKED
    inscode(d, ds, de, "EKEYREVOKED", EKEYREVOKED, "密钥已被废除");
#endif
#ifdef EKEYREJECTED
    inscode(d, ds, de, "EKEYREJECTED", EKEYREJECTED, "密钥被服务拒绝");
#endif
#ifdef EOWNERDEAD
    inscode(d, ds, de, "EOWNERDEAD", EOWNERDEAD, "所有者已死");
#endif
#ifdef ENOTRECOVERABLE
    inscode(d, ds, de, "ENOTRECOVERABLE", ENOTRECOVERABLE, "状态无法恢复");
#endif
#ifdef ERFKILL
    inscode(d, ds, de, "ERFKILL", ERFKILL, "操作因 RF-kill 而不可行");
#endif

    /* Solaris-specific errnos */
#ifdef ECANCELED
    inscode(d, ds, de, "ECANCELED", ECANCELED, "操作已取消");
#endif
#ifdef ENOTSUP
    inscode(d, ds, de, "ENOTSUP", ENOTSUP, "操作不受支持");
#endif
#ifdef EOWNERDEAD
    inscode(d, ds, de, "EOWNERDEAD", EOWNERDEAD, "进程随锁一同死亡");
#endif
#ifdef ENOTRECOVERABLE
    inscode(d, ds, de, "ENOTRECOVERABLE", ENOTRECOVERABLE, "锁无法恢复");
#endif
#ifdef ELOCKUNMAPPED
    inscode(d, ds, de, "ELOCKUNMAPPED", ELOCKUNMAPPED, "已锁定锁被解除映射");
#endif
#ifdef ENOTACTIVE
    inscode(d, ds, de, "ENOTACTIVE", ENOTACTIVE, "设施未激活");
#endif

    /* MacOSX specific errnos */
#ifdef EAUTH
    inscode(d, ds, de, "EAUTH", EAUTH, "身份验证错误");
#endif
#ifdef EBADARCH
    inscode(d, ds, de, "EBADARCH", EBADARCH, "可执行文件中的 CPU 类型错误");
#endif
#ifdef EBADEXEC
    inscode(d, ds, de, "EBADEXEC", EBADEXEC, "可执行文件(或共享库)错误");
#endif
#ifdef EBADMACHO
    inscode(d, ds, de, "EBADMACHO", EBADMACHO, "Mach-o 文件格式错误");
#endif
#ifdef EBADRPC
    inscode(d, ds, de, "EBADRPC", EBADRPC, "RPC 结构体错误");
#endif
#ifdef EDEVERR
    inscode(d, ds, de, "EDEVERR", EDEVERR, "设备错误");
#endif
#ifdef EFTYPE
    inscode(d, ds, de, "EFTYPE", EFTYPE, "文件类型或格式不当");
#endif
#ifdef ENEEDAUTH
    inscode(d, ds, de, "ENEEDAUTH", ENEEDAUTH, "需要身份验证官");
#endif
#ifdef ENOATTR
    inscode(d, ds, de, "ENOATTR", ENOATTR, "属性未找到");
#endif
#ifdef ENOPOLICY
    inscode(d, ds, de, "ENOPOLICY", ENOPOLICY, "策略未找到");
#endif
#ifdef EPROCLIM
    inscode(d, ds, de, "EPROCLIM", EPROCLIM, "进程过多");
#endif
#ifdef EPROCUNAVAIL
    inscode(d, ds, de, "EPROCUNAVAIL", EPROCUNAVAIL, "程序流程错误");
#endif
#ifdef EPROGMISMATCH
    inscode(d, ds, de, "EPROGMISMATCH", EPROGMISMATCH, "程序版本错误");
#endif
#ifdef EPROGUNAVAIL
    inscode(d, ds, de, "EPROGUNAVAIL", EPROGUNAVAIL, "RPC prog. 不可用");
#endif
#ifdef EPWROFF
    inscode(d, ds, de, "EPWROFF", EPWROFF, "设备断电");
#endif
#ifdef ERPCMISMATCH
    inscode(d, ds, de, "ERPCMISMATCH", ERPCMISMATCH, "RPC 版本错误");
#endif
#ifdef ESHLIBVERS
    inscode(d, ds, de, "ESHLIBVERS", ESHLIBVERS, "共享库版本不匹配");
#endif

    Py_DECREF(de);
    return m;
}
