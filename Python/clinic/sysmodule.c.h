/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(sys_addaudithook__doc__,
"addaudithook($module, /, hook)\n"
"--\n"
"\n"
"添加新的审计钩子回调函数.");

#define SYS_ADDAUDITHOOK_METHODDEF    \
    {"addaudithook", (PyCFunction)(void(*)(void))sys_addaudithook, METH_FASTCALL|METH_KEYWORDS, sys_addaudithook__doc__},

#define SYS_添加审计钩子_METHODDEF    \
    {"添加审计钩子", (PyCFunction)(void(*)(void))sys_addaudithook, METH_FASTCALL|METH_KEYWORDS, sys_addaudithook__doc__},

static PyObject *
sys_addaudithook_impl(PyObject *module, PyObject *hook);

static PyObject *
sys_addaudithook(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"hook", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "addaudithook", 0};
    PyObject *argsbuf[1];
    PyObject *hook;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    hook = args[0];
    return_value = sys_addaudithook_impl(module, hook);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_displayhook__doc__,
"displayhook($module, 对象, /)\n"
"--\n"
"\n"
"打印一个对象到 系统.标准输出, 同时将其保存到 builtins._");

#define SYS_DISPLAYHOOK_METHODDEF    \
    {"displayhook", (PyCFunction)sys_displayhook, METH_O, sys_displayhook__doc__},

#define SYS_显示钩子_METHODDEF    \
    {"显示钩子", (PyCFunction)sys_displayhook, METH_O, sys_displayhook__doc__},

PyDoc_STRVAR(sys_excepthook__doc__,
"excepthook($module, 异常类型, 值, 回溯, /)\n"
"--\n"
"\n"
"处理一个异常, 在 系统.错误输出 上显示该异常及回溯信息.");

#define SYS_EXCEPTHOOK_METHODDEF    \
    {"excepthook", (PyCFunction)(void(*)(void))sys_excepthook, METH_FASTCALL, sys_excepthook__doc__},

#define SYS_异常钩子_METHODDEF    \
    {"异常钩子", (PyCFunction)(void(*)(void))sys_excepthook, METH_FASTCALL, sys_excepthook__doc__},

static PyObject *
sys_excepthook_impl(PyObject *module, PyObject *exctype, PyObject *value,
                    PyObject *traceback);

static PyObject *
sys_excepthook(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *exctype;
    PyObject *value;
    PyObject *traceback;

    if (!_PyArg_CheckPositional("excepthook", nargs, 3, 3)) {
        goto exit;
    }
    exctype = args[0];
    value = args[1];
    traceback = args[2];
    return_value = sys_excepthook_impl(module, exctype, value, traceback);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_exc_info__doc__,
"exc_info($module, /)\n"
"--\n"
"\n"
"返回当前异常信息: (类型, 值, 回溯).\n"
"\n"
"返回当前或较早堆栈帧中由一个异常语句捕获的最新异常信息.");

#define SYS_EXC_INFO_METHODDEF    \
    {"exc_info", (PyCFunction)sys_exc_info, METH_NOARGS, sys_exc_info__doc__},

#define SYS_异常信息_METHODDEF    \
    {"异常信息", (PyCFunction)sys_exc_info, METH_NOARGS, sys_exc_info__doc__},

static PyObject *
sys_exc_info_impl(PyObject *module);

static PyObject *
sys_exc_info(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_exc_info_impl(module);
}

PyDoc_STRVAR(sys_unraisablehook__doc__,
"unraisablehook($module, 无法抛出异常, /)\n"
"--\n"
"\n"
"处理无法抛出的异常.\n"
"\n"
"无法抛出异常 参数有如下属性:\n"
"\n"
"* exc_type: 异常类型.\n"
"* exc_value: 异常值, 可以为 空.\n"
"* exc_traceback: 异常回溯, 可以为 空.\n"
"* err_msg: 错误消息, 可以为 空.\n"
"* object: 引发异常的对象, 可以为 空.");

#define SYS_UNRAISABLEHOOK_METHODDEF    \
    {"unraisablehook", (PyCFunction)sys_unraisablehook, METH_O, sys_unraisablehook__doc__},

#define SYS_无法抛出异常钩子_METHODDEF    \
    {"无法抛出异常钩子", (PyCFunction)sys_unraisablehook, METH_O, sys_unraisablehook__doc__},

PyDoc_STRVAR(sys_exit__doc__,
"退出($module, 状态=空, /)\n"
"--\n"
"\n"
"退出解释器并抛出 系统退出类(状态).\n"
"\n"
"若状态值未提供或为 空, 则其默认为 0 (即成功).\n"
"若状态值是一个整数, 它将用作系统退出状态.\n"
"若状态值是其他类对象, 则会将它打印出来, 系统退出状态将为 1 (即失败).");

#define SYS_EXIT_METHODDEF    \
    {"exit", (PyCFunction)(void(*)(void))sys_exit, METH_FASTCALL, sys_exit__doc__},

#define SYS_退出_METHODDEF    \
    {"退出", (PyCFunction)(void(*)(void))sys_exit, METH_FASTCALL, sys_exit__doc__},

static PyObject *
sys_exit_impl(PyObject *module, PyObject *status);

static PyObject *
sys_exit(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *status = Py_None;

    if (!_PyArg_CheckPositional("exit", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    status = args[0];
skip_optional:
    return_value = sys_exit_impl(module, status);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_getdefaultencoding__doc__,
"getdefaultencoding($module, /)\n"
"--\n"
"\n"
"返回统一码实现所用的当前默认编码.");

#define SYS_GETDEFAULTENCODING_METHODDEF    \
    {"getdefaultencoding", (PyCFunction)sys_getdefaultencoding, METH_NOARGS, sys_getdefaultencoding__doc__},

#define SYS_获取默认编码_METHODDEF    \
    {"获取默认编码", (PyCFunction)sys_getdefaultencoding, METH_NOARGS, sys_getdefaultencoding__doc__},

static PyObject *
sys_getdefaultencoding_impl(PyObject *module);

static PyObject *
sys_getdefaultencoding(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getdefaultencoding_impl(module);
}

PyDoc_STRVAR(sys_getfilesystemencoding__doc__,
"getfilesystemencoding($module, /)\n"
"--\n"
"\n"
"返回将统一码文件名转换为 OS 文件名所用的编码.");

#define SYS_GETFILESYSTEMENCODING_METHODDEF    \
    {"getfilesystemencoding", (PyCFunction)sys_getfilesystemencoding, METH_NOARGS, sys_getfilesystemencoding__doc__},

#define SYS_获取文件系统编码_METHODDEF    \
    {"获取文件系统编码", (PyCFunction)sys_getfilesystemencoding, METH_NOARGS, sys_getfilesystemencoding__doc__},

static PyObject *
sys_getfilesystemencoding_impl(PyObject *module);

static PyObject *
sys_getfilesystemencoding(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getfilesystemencoding_impl(module);
}

PyDoc_STRVAR(sys_getfilesystemencodeerrors__doc__,
"getfilesystemencodeerrors($module, /)\n"
"--\n"
"\n"
"返回统一码文件名转换为 OS 文件名所用的错误模式.");

#define SYS_GETFILESYSTEMENCODEERRORS_METHODDEF    \
    {"getfilesystemencodeerrors", (PyCFunction)sys_getfilesystemencodeerrors, METH_NOARGS, sys_getfilesystemencodeerrors__doc__},

#define SYS_获取文件系统编码错误_METHODDEF    \
    {"获取文件系统编码错误", (PyCFunction)sys_getfilesystemencodeerrors, METH_NOARGS, sys_getfilesystemencodeerrors__doc__},

static PyObject *
sys_getfilesystemencodeerrors_impl(PyObject *module);

static PyObject *
sys_getfilesystemencodeerrors(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getfilesystemencodeerrors_impl(module);
}

PyDoc_STRVAR(sys_intern__doc__,
"intern($module, 字符串, /)\n"
"--\n"
"\n"
"``留存\'\' 给定字符串.\n"
"\n"
"这会将字符串输入到留存的全局字符串表中, 其目的是加快字典查找.\n"
"返回字符串本身或先前留存的同值字符串对象.");

#define SYS_INTERN_METHODDEF    \
    {"intern", (PyCFunction)sys_intern, METH_O, sys_intern__doc__},

#define SYS_留存_METHODDEF    \
    {"留存", (PyCFunction)sys_intern, METH_O, sys_intern__doc__},

static PyObject *
sys_intern_impl(PyObject *module, PyObject *s);

static PyObject *
sys_intern(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    PyObject *s;

    if (!PyUnicode_Check(arg)) {
        _PyArg_BadArgument("intern", "参数", "字符串", arg);
        goto exit;
    }
    if (PyUnicode_READY(arg) == -1) {
        goto exit;
    }
    s = arg;
    return_value = sys_intern_impl(module, s);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_gettrace__doc__,
"gettrace($module, /)\n"
"--\n"
"\n"
"返回由 系统.设置追踪 所设置的全局调试追踪函数.\n"
"\n"
"参见库手册中的调试器章节.");

#define SYS_GETTRACE_METHODDEF    \
    {"gettrace", (PyCFunction)sys_gettrace, METH_NOARGS, sys_gettrace__doc__},

#define SYS_获取追踪_METHODDEF    \
    {"获取追踪", (PyCFunction)sys_gettrace, METH_NOARGS, sys_gettrace__doc__},

static PyObject *
sys_gettrace_impl(PyObject *module);

static PyObject *
sys_gettrace(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_gettrace_impl(module);
}

PyDoc_STRVAR(sys_getprofile__doc__,
"getprofile($module, /)\n"
"--\n"
"\n"
"返回由 系统.设置性能检测 所设置的性能检测函数.\n"
"\n"
"参见库手册中的性能检测章节.");

#define SYS_GETPROFILE_METHODDEF    \
    {"getprofile", (PyCFunction)sys_getprofile, METH_NOARGS, sys_getprofile__doc__},

#define SYS_获取性能检测_METHODDEF    \
    {"获取性能检测", (PyCFunction)sys_getprofile, METH_NOARGS, sys_getprofile__doc__},

static PyObject *
sys_getprofile_impl(PyObject *module);

static PyObject *
sys_getprofile(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getprofile_impl(module);
}

PyDoc_STRVAR(sys_setcheckinterval__doc__,
"setcheckinterval($module, n, /)\n"
"--\n"
"\n"
"Set the async event check interval to n instructions.\n"
"\n"
"This tells the Python interpreter to check for asynchronous events\n"
"every n instructions.\n"
"\n"
"This also affects how often thread switches occur.");

#define SYS_SETCHECKINTERVAL_METHODDEF    \
    {"setcheckinterval", (PyCFunction)sys_setcheckinterval, METH_O, sys_setcheckinterval__doc__},

static PyObject *
sys_setcheckinterval_impl(PyObject *module, int n);

static PyObject *
sys_setcheckinterval(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    int n;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    n = _PyLong_AsInt(arg);
    if (n == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = sys_setcheckinterval_impl(module, n);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_getcheckinterval__doc__,
"getcheckinterval($module, /)\n"
"--\n"
"\n"
"Return the current check interval; see sys.setcheckinterval().");

#define SYS_GETCHECKINTERVAL_METHODDEF    \
    {"getcheckinterval", (PyCFunction)sys_getcheckinterval, METH_NOARGS, sys_getcheckinterval__doc__},

static PyObject *
sys_getcheckinterval_impl(PyObject *module);

static PyObject *
sys_getcheckinterval(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getcheckinterval_impl(module);
}

PyDoc_STRVAR(sys_setswitchinterval__doc__,
"setswitchinterval($module, 间隔, /)\n"
"--\n"
"\n"
"设置 Python 解释器内部的理想线程切换延迟时间.\n"
"\n"
"如果解释器执行很长的不可中断代码序列, 线程的实际切换频率可能会降低\n"
"(这是具体实现细节, 取决于工作负载).\n"
"\n"
"参数须代表期望的切换延迟, 单位为秒.\n"
"典型值为 0.005 (5 毫秒).");

#define SYS_SETSWITCHINTERVAL_METHODDEF    \
    {"setswitchinterval", (PyCFunction)sys_setswitchinterval, METH_O, sys_setswitchinterval__doc__},

#define SYS_设置切换间隔_METHODDEF    \
    {"设置切换间隔", (PyCFunction)sys_setswitchinterval, METH_O, sys_setswitchinterval__doc__},

static PyObject *
sys_setswitchinterval_impl(PyObject *module, double interval);

static PyObject *
sys_setswitchinterval(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    double interval;

    if (PyFloat_CheckExact(arg)) {
        interval = PyFloat_AS_DOUBLE(arg);
    }
    else
    {
        interval = PyFloat_AsDouble(arg);
        if (interval == -1.0 && PyErr_Occurred()) {
            goto exit;
        }
    }
    return_value = sys_setswitchinterval_impl(module, interval);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_getswitchinterval__doc__,
"getswitchinterval($module, /)\n"
"--\n"
"\n"
"返回当前线程切换间隔时间; 参见 系统.设置切换间隔().");

#define SYS_GETSWITCHINTERVAL_METHODDEF    \
    {"getswitchinterval", (PyCFunction)sys_getswitchinterval, METH_NOARGS, sys_getswitchinterval__doc__},

#define SYS_获取切换间隔_METHODDEF    \
    {"获取切换间隔", (PyCFunction)sys_getswitchinterval, METH_NOARGS, sys_getswitchinterval__doc__},

static double
sys_getswitchinterval_impl(PyObject *module);

static PyObject *
sys_getswitchinterval(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    PyObject *return_value = NULL;
    double _return_value;

    _return_value = sys_getswitchinterval_impl(module);
    if ((_return_value == -1.0) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyFloat_FromDouble(_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_setrecursionlimit__doc__,
"setrecursionlimit($module, 限值, /)\n"
"--\n"
"\n"
"设置 Python 解释器堆栈的最大深度.\n"
"\n"
"此限值防止无限递归导致 C 堆栈溢出和 Python 崩溃.\n"
"最高限值取决于平台.");

#define SYS_SETRECURSIONLIMIT_METHODDEF    \
    {"setrecursionlimit", (PyCFunction)sys_setrecursionlimit, METH_O, sys_setrecursionlimit__doc__},

#define SYS_设置递归限值_METHODDEF    \
    {"设置递归限值", (PyCFunction)sys_setrecursionlimit, METH_O, sys_setrecursionlimit__doc__},

static PyObject *
sys_setrecursionlimit_impl(PyObject *module, int new_limit);

static PyObject *
sys_setrecursionlimit(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    int new_limit;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    new_limit = _PyLong_AsInt(arg);
    if (new_limit == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = sys_setrecursionlimit_impl(module, new_limit);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_set_coroutine_origin_tracking_depth__doc__,
"set_coroutine_origin_tracking_depth($module, /, depth)\n"
"--\n"
"\n"
"Enable or disable origin tracking for coroutine objects in this thread.\n"
"\n"
"Coroutine objects will track \'depth\' frames of traceback information\n"
"about where they came from, available in their cr_origin attribute.\n"
"\n"
"Set a depth of 0 to disable.");

#define SYS_SET_COROUTINE_ORIGIN_TRACKING_DEPTH_METHODDEF    \
    {"set_coroutine_origin_tracking_depth", (PyCFunction)(void(*)(void))sys_set_coroutine_origin_tracking_depth, METH_FASTCALL|METH_KEYWORDS, sys_set_coroutine_origin_tracking_depth__doc__},

static PyObject *
sys_set_coroutine_origin_tracking_depth_impl(PyObject *module, int depth);

static PyObject *
sys_set_coroutine_origin_tracking_depth(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"depth", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "set_coroutine_origin_tracking_depth", 0};
    PyObject *argsbuf[1];
    int depth;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    depth = _PyLong_AsInt(args[0]);
    if (depth == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = sys_set_coroutine_origin_tracking_depth_impl(module, depth);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_get_coroutine_origin_tracking_depth__doc__,
"get_coroutine_origin_tracking_depth($module, /)\n"
"--\n"
"\n"
"Check status of origin tracking for coroutine objects in this thread.");

#define SYS_GET_COROUTINE_ORIGIN_TRACKING_DEPTH_METHODDEF    \
    {"get_coroutine_origin_tracking_depth", (PyCFunction)sys_get_coroutine_origin_tracking_depth, METH_NOARGS, sys_get_coroutine_origin_tracking_depth__doc__},

static int
sys_get_coroutine_origin_tracking_depth_impl(PyObject *module);

static PyObject *
sys_get_coroutine_origin_tracking_depth(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    PyObject *return_value = NULL;
    int _return_value;

    _return_value = sys_get_coroutine_origin_tracking_depth_impl(module);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyLong_FromLong((long)_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_get_asyncgen_hooks__doc__,
"get_asyncgen_hooks($module, /)\n"
"--\n"
"\n"
"返回所安装的异步生成器钩子.\n"
"\n"
"这会返回一个具名元组 (firstiter, finalizer).");

#define SYS_GET_ASYNCGEN_HOOKS_METHODDEF    \
    {"get_asyncgen_hooks", (PyCFunction)sys_get_asyncgen_hooks, METH_NOARGS, sys_get_asyncgen_hooks__doc__},

#define SYS_获取异步生成器钩子_METHODDEF    \
    {"获取异步生成器钩子", (PyCFunction)sys_get_asyncgen_hooks, METH_NOARGS, sys_get_asyncgen_hooks__doc__},

static PyObject *
sys_get_asyncgen_hooks_impl(PyObject *module);

static PyObject *
sys_get_asyncgen_hooks(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_get_asyncgen_hooks_impl(module);
}

PyDoc_STRVAR(sys_getrecursionlimit__doc__,
"getrecursionlimit($module, /)\n"
"--\n"
"\n"
"返回当前递归限值.\n"
"\n"
"递归限值是 Python 解释器堆栈的最大深度.\n"
"此限值防止无限递归导致 C 堆栈溢出和 Python 崩溃.");

#define SYS_GETRECURSIONLIMIT_METHODDEF    \
    {"getrecursionlimit", (PyCFunction)sys_getrecursionlimit, METH_NOARGS, sys_getrecursionlimit__doc__},

#define SYS_获取递归限值_METHODDEF    \
    {"获取递归限值", (PyCFunction)sys_getrecursionlimit, METH_NOARGS, sys_getrecursionlimit__doc__},

static PyObject *
sys_getrecursionlimit_impl(PyObject *module);

static PyObject *
sys_getrecursionlimit(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getrecursionlimit_impl(module);
}

#if defined(MS_WINDOWS)

PyDoc_STRVAR(sys_getwindowsversion__doc__,
"getwindowsversion($module, /)\n"
"--\n"
"\n"
"以具名元组返回当前运行的 Windows 版本信息.\n"
"\n"
"成员名称为: major, minor, build, platform, service_pack,\n"
"service_pack_major, service_pack_minor, suite_mask, product_type and\n"
"platform_version. 为了向后兼容, 仅前 5 项可通过索引获得.\n"
"大部分元素为数字, service_pack 和 platform_type 为字符串, platform_version\n"
"为 3-元组. Platform 始终为 2. Product_type 为 1 表示工作站,\n"
"2 表示域控制器, 3 表示服务器. Platform_version 是一个 3-元组, 包含版本号,\n"
"用于识别 OS 而非特性检测.");

#define SYS_GETWINDOWSVERSION_METHODDEF    \
    {"getwindowsversion", (PyCFunction)sys_getwindowsversion, METH_NOARGS, sys_getwindowsversion__doc__},

#define SYS_获取WINDOWS版本_METHODDEF    \
    {"获取windows版本", (PyCFunction)sys_getwindowsversion, METH_NOARGS, sys_getwindowsversion__doc__},

static PyObject *
sys_getwindowsversion_impl(PyObject *module);

static PyObject *
sys_getwindowsversion(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getwindowsversion_impl(module);
}

#endif /* defined(MS_WINDOWS) */

#if defined(MS_WINDOWS)

PyDoc_STRVAR(sys__enablelegacywindowsfsencoding__doc__,
"_enablelegacywindowsfsencoding($module, /)\n"
"--\n"
"\n"
"Changes the default filesystem encoding to mbcs:replace.\n"
"\n"
"This is done for consistency with earlier versions of Python. See PEP\n"
"529 for more information.\n"
"\n"
"This is equivalent to defining the PYTHONLEGACYWINDOWSFSENCODING\n"
"environment variable before launching Python.");

#define SYS__ENABLELEGACYWINDOWSFSENCODING_METHODDEF    \
    {"_enablelegacywindowsfsencoding", (PyCFunction)sys__enablelegacywindowsfsencoding, METH_NOARGS, sys__enablelegacywindowsfsencoding__doc__},

static PyObject *
sys__enablelegacywindowsfsencoding_impl(PyObject *module);

static PyObject *
sys__enablelegacywindowsfsencoding(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys__enablelegacywindowsfsencoding_impl(module);
}

#endif /* defined(MS_WINDOWS) */

#if defined(HAVE_DLOPEN)

PyDoc_STRVAR(sys_setdlopenflags__doc__,
"setdlopenflags($module, flags, /)\n"
"--\n"
"\n"
"Set the flags used by the interpreter for dlopen calls.\n"
"\n"
"This is used, for example, when the interpreter loads extension\n"
"modules. Among other things, this will enable a lazy resolving of\n"
"symbols when importing a module, if called as sys.setdlopenflags(0).\n"
"To share symbols across extension modules, call as\n"
"sys.setdlopenflags(os.RTLD_GLOBAL).  Symbolic names for the flag\n"
"modules can be found in the os module (RTLD_xxx constants, e.g.\n"
"os.RTLD_LAZY).");

#define SYS_SETDLOPENFLAGS_METHODDEF    \
    {"setdlopenflags", (PyCFunction)sys_setdlopenflags, METH_O, sys_setdlopenflags__doc__},

static PyObject *
sys_setdlopenflags_impl(PyObject *module, int new_val);

static PyObject *
sys_setdlopenflags(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    int new_val;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    new_val = _PyLong_AsInt(arg);
    if (new_val == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = sys_setdlopenflags_impl(module, new_val);

exit:
    return return_value;
}

#endif /* defined(HAVE_DLOPEN) */

#if defined(HAVE_DLOPEN)

PyDoc_STRVAR(sys_getdlopenflags__doc__,
"getdlopenflags($module, /)\n"
"--\n"
"\n"
"Return the current value of the flags that are used for dlopen calls.\n"
"\n"
"The flag constants are defined in the os module.");

#define SYS_GETDLOPENFLAGS_METHODDEF    \
    {"getdlopenflags", (PyCFunction)sys_getdlopenflags, METH_NOARGS, sys_getdlopenflags__doc__},

static PyObject *
sys_getdlopenflags_impl(PyObject *module);

static PyObject *
sys_getdlopenflags(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getdlopenflags_impl(module);
}

#endif /* defined(HAVE_DLOPEN) */

#if defined(USE_MALLOPT)

PyDoc_STRVAR(sys_mdebug__doc__,
"mdebug($module, flag, /)\n"
"--\n"
"\n");

#define SYS_MDEBUG_METHODDEF    \
    {"mdebug", (PyCFunction)sys_mdebug, METH_O, sys_mdebug__doc__},

static PyObject *
sys_mdebug_impl(PyObject *module, int flag);

static PyObject *
sys_mdebug(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    int flag;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    flag = _PyLong_AsInt(arg);
    if (flag == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = sys_mdebug_impl(module, flag);

exit:
    return return_value;
}

#endif /* defined(USE_MALLOPT) */

PyDoc_STRVAR(sys_getrefcount__doc__,
"getrefcount($module, 对象, /)\n"
"--\n"
"\n"
"返回对象的引用计数.\n"
"\n"
"返回的计数一般比预期的大 1, 因为其中包含 获取引用计数() 参数的(临时)引用.");

#define SYS_GETREFCOUNT_METHODDEF    \
    {"getrefcount", (PyCFunction)sys_getrefcount, METH_O, sys_getrefcount__doc__},

#define SYS_获取引用计数_METHODDEF    \
    {"获取引用计数", (PyCFunction)sys_getrefcount, METH_O, sys_getrefcount__doc__},

static Py_ssize_t
sys_getrefcount_impl(PyObject *module, PyObject *object);

static PyObject *
sys_getrefcount(PyObject *module, PyObject *object)
{
    PyObject *return_value = NULL;
    Py_ssize_t _return_value;

    _return_value = sys_getrefcount_impl(module, object);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyLong_FromSsize_t(_return_value);

exit:
    return return_value;
}

#if defined(Py_REF_DEBUG)

PyDoc_STRVAR(sys_gettotalrefcount__doc__,
"gettotalrefcount($module, /)\n"
"--\n"
"\n");

#define SYS_GETTOTALREFCOUNT_METHODDEF    \
    {"gettotalrefcount", (PyCFunction)sys_gettotalrefcount, METH_NOARGS, sys_gettotalrefcount__doc__},

static Py_ssize_t
sys_gettotalrefcount_impl(PyObject *module);

static PyObject *
sys_gettotalrefcount(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    PyObject *return_value = NULL;
    Py_ssize_t _return_value;

    _return_value = sys_gettotalrefcount_impl(module);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyLong_FromSsize_t(_return_value);

exit:
    return return_value;
}

#endif /* defined(Py_REF_DEBUG) */

PyDoc_STRVAR(sys_getallocatedblocks__doc__,
"getallocatedblocks($module, /)\n"
"--\n"
"\n"
"Return the number of memory blocks currently allocated.");

#define SYS_GETALLOCATEDBLOCKS_METHODDEF    \
    {"getallocatedblocks", (PyCFunction)sys_getallocatedblocks, METH_NOARGS, sys_getallocatedblocks__doc__},

static Py_ssize_t
sys_getallocatedblocks_impl(PyObject *module);

static PyObject *
sys_getallocatedblocks(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    PyObject *return_value = NULL;
    Py_ssize_t _return_value;

    _return_value = sys_getallocatedblocks_impl(module);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyLong_FromSsize_t(_return_value);

exit:
    return return_value;
}

#if defined(COUNT_ALLOCS)

PyDoc_STRVAR(sys_getcounts__doc__,
"getcounts($module, /)\n"
"--\n"
"\n");

#define SYS_GETCOUNTS_METHODDEF    \
    {"getcounts", (PyCFunction)sys_getcounts, METH_NOARGS, sys_getcounts__doc__},

static PyObject *
sys_getcounts_impl(PyObject *module);

static PyObject *
sys_getcounts(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getcounts_impl(module);
}

#endif /* defined(COUNT_ALLOCS) */

PyDoc_STRVAR(sys__getframe__doc__,
"_getframe($module, depth=0, /)\n"
"--\n"
"\n"
"Return a frame object from the call stack.\n"
"\n"
"If optional integer depth is given, return the frame object that many\n"
"calls below the top of the stack.  If that is deeper than the call\n"
"stack, ValueError is raised.  The default for depth is zero, returning\n"
"the frame at the top of the call stack.\n"
"\n"
"This function should be used for internal and specialized purposes\n"
"only.");

#define SYS__GETFRAME_METHODDEF    \
    {"_getframe", (PyCFunction)(void(*)(void))sys__getframe, METH_FASTCALL, sys__getframe__doc__},

static PyObject *
sys__getframe_impl(PyObject *module, int depth);

static PyObject *
sys__getframe(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    int depth = 0;

    if (!_PyArg_CheckPositional("_getframe", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    depth = _PyLong_AsInt(args[0]);
    if (depth == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional:
    return_value = sys__getframe_impl(module, depth);

exit:
    return return_value;
}

PyDoc_STRVAR(sys__current_frames__doc__,
"_current_frames($module, /)\n"
"--\n"
"\n"
"Return a dict mapping each thread\'s thread id to its current stack frame.\n"
"\n"
"This function should be used for specialized purposes only.");

#define SYS__CURRENT_FRAMES_METHODDEF    \
    {"_current_frames", (PyCFunction)sys__current_frames, METH_NOARGS, sys__current_frames__doc__},

static PyObject *
sys__current_frames_impl(PyObject *module);

static PyObject *
sys__current_frames(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys__current_frames_impl(module);
}

PyDoc_STRVAR(sys_call_tracing__doc__,
"call_tracing($module, func, args, /)\n"
"--\n"
"\n"
"Call func(*args), while tracing is enabled.\n"
"\n"
"The tracing state is saved, and restored afterwards.  This is intended\n"
"to be called from a debugger from a checkpoint, to recursively debug\n"
"some other code.");

#define SYS_CALL_TRACING_METHODDEF    \
    {"call_tracing", (PyCFunction)(void(*)(void))sys_call_tracing, METH_FASTCALL, sys_call_tracing__doc__},

static PyObject *
sys_call_tracing_impl(PyObject *module, PyObject *func, PyObject *funcargs);

static PyObject *
sys_call_tracing(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *func;
    PyObject *funcargs;

    if (!_PyArg_CheckPositional("call_tracing", nargs, 2, 2)) {
        goto exit;
    }
    func = args[0];
    if (!PyTuple_Check(args[1])) {
        _PyArg_BadArgument("call_tracing", "参数 2", "元组", args[1]);
        goto exit;
    }
    funcargs = args[1];
    return_value = sys_call_tracing_impl(module, func, funcargs);

exit:
    return return_value;
}

PyDoc_STRVAR(sys_callstats__doc__,
"callstats($module, /)\n"
"--\n"
"\n"
"Return a tuple of function call statistics.\n"
"\n"
"A tuple is returned only if CALL_PROFILE was defined when Python was\n"
"built.  Otherwise, this returns None.\n"
"\n"
"When enabled, this function returns detailed, implementation-specific\n"
"details about the number of function calls executed. The return value\n"
"is a 11-tuple where the entries in the tuple are counts of:\n"
"0. all function calls\n"
"1. calls to PyFunction_Type objects\n"
"2. PyFunction calls that do not create an argument tuple\n"
"3. PyFunction calls that do not create an argument tuple\n"
"   and bypass PyEval_EvalCodeEx()\n"
"4. PyMethod calls\n"
"5. PyMethod calls on bound methods\n"
"6. PyType calls\n"
"7. PyCFunction calls\n"
"8. generator calls\n"
"9. All other calls\n"
"10. Number of stack pops performed by call_function()");

#define SYS_CALLSTATS_METHODDEF    \
    {"callstats", (PyCFunction)sys_callstats, METH_NOARGS, sys_callstats__doc__},

static PyObject *
sys_callstats_impl(PyObject *module);

static PyObject *
sys_callstats(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_callstats_impl(module);
}

PyDoc_STRVAR(sys__debugmallocstats__doc__,
"_debugmallocstats($module, /)\n"
"--\n"
"\n"
"Print summary info to stderr about the state of pymalloc\'s structures.\n"
"\n"
"In Py_DEBUG mode, also perform some expensive internal consistency\n"
"checks.");

#define SYS__DEBUGMALLOCSTATS_METHODDEF    \
    {"_debugmallocstats", (PyCFunction)sys__debugmallocstats, METH_NOARGS, sys__debugmallocstats__doc__},

static PyObject *
sys__debugmallocstats_impl(PyObject *module);

static PyObject *
sys__debugmallocstats(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys__debugmallocstats_impl(module);
}

PyDoc_STRVAR(sys__clear_type_cache__doc__,
"_clear_type_cache($module, /)\n"
"--\n"
"\n"
"Clear the internal type lookup cache.");

#define SYS__CLEAR_TYPE_CACHE_METHODDEF    \
    {"_clear_type_cache", (PyCFunction)sys__clear_type_cache, METH_NOARGS, sys__clear_type_cache__doc__},

static PyObject *
sys__clear_type_cache_impl(PyObject *module);

static PyObject *
sys__clear_type_cache(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys__clear_type_cache_impl(module);
}

PyDoc_STRVAR(sys_is_finalizing__doc__,
"is_finalizing($module, /)\n"
"--\n"
"\n"
"若 Python 正在退出则返回 真.");

#define SYS_IS_FINALIZING_METHODDEF    \
    {"is_finalizing", (PyCFunction)sys_is_finalizing, METH_NOARGS, sys_is_finalizing__doc__},

#define SYS_是否正在退出_METHODDEF    \
    {"是否正在退出", (PyCFunction)sys_is_finalizing, METH_NOARGS, sys_is_finalizing__doc__},

static PyObject *
sys_is_finalizing_impl(PyObject *module);

static PyObject *
sys_is_finalizing(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_is_finalizing_impl(module);
}

#if defined(ANDROID_API_LEVEL)

PyDoc_STRVAR(sys_getandroidapilevel__doc__,
"getandroidapilevel($module, /)\n"
"--\n"
"\n"
"Return the build time API version of Android as an integer.");

#define SYS_GETANDROIDAPILEVEL_METHODDEF    \
    {"getandroidapilevel", (PyCFunction)sys_getandroidapilevel, METH_NOARGS, sys_getandroidapilevel__doc__},

static PyObject *
sys_getandroidapilevel_impl(PyObject *module);

static PyObject *
sys_getandroidapilevel(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return sys_getandroidapilevel_impl(module);
}

#endif /* defined(ANDROID_API_LEVEL) */

#ifndef SYS_GETWINDOWSVERSION_METHODDEF
    #define SYS_GETWINDOWSVERSION_METHODDEF
#endif /* !defined(SYS_GETWINDOWSVERSION_METHODDEF) */

#ifndef SYS_获取WINDOWS版本_METHODDEF
    #define SYS_获取WINDOWS版本_METHODDEF
#endif /* !defined(SYS_获取WINDOWS版本_METHODDEF) */

#ifndef SYS__ENABLELEGACYWINDOWSFSENCODING_METHODDEF
    #define SYS__ENABLELEGACYWINDOWSFSENCODING_METHODDEF
#endif /* !defined(SYS__ENABLELEGACYWINDOWSFSENCODING_METHODDEF) */

#ifndef SYS_SETDLOPENFLAGS_METHODDEF
    #define SYS_SETDLOPENFLAGS_METHODDEF
#endif /* !defined(SYS_SETDLOPENFLAGS_METHODDEF) */

#ifndef SYS_GETDLOPENFLAGS_METHODDEF
    #define SYS_GETDLOPENFLAGS_METHODDEF
#endif /* !defined(SYS_GETDLOPENFLAGS_METHODDEF) */

#ifndef SYS_MDEBUG_METHODDEF
    #define SYS_MDEBUG_METHODDEF
#endif /* !defined(SYS_MDEBUG_METHODDEF) */

#ifndef SYS_GETTOTALREFCOUNT_METHODDEF
    #define SYS_GETTOTALREFCOUNT_METHODDEF
#endif /* !defined(SYS_GETTOTALREFCOUNT_METHODDEF) */

#ifndef SYS_GETCOUNTS_METHODDEF
    #define SYS_GETCOUNTS_METHODDEF
#endif /* !defined(SYS_GETCOUNTS_METHODDEF) */

#ifndef SYS_GETANDROIDAPILEVEL_METHODDEF
    #define SYS_GETANDROIDAPILEVEL_METHODDEF
#endif /* !defined(SYS_GETANDROIDAPILEVEL_METHODDEF) */
/*[clinic end generated code: output=273f9cec8bfcab91 input=a9049054013a1b77]*/
