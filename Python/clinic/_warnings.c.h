/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(warnings_warn__doc__,
"警告($module, /, 消息, 类别=空, 栈级别=1, 来源=空)\n"
"--\n"
"\n"
"发出警告，可以忽略它或抛出异常。");

#define WARNINGS_WARN_METHODDEF    \
    {"warn", (PyCFunction)(void(*)(void))warnings_warn, METH_FASTCALL|METH_KEYWORDS, warnings_warn__doc__},

#define WARNINGS_警告_METHODDEF    \
    {"警告", (PyCFunction)(void(*)(void))warnings_警告, METH_FASTCALL|METH_KEYWORDS, warnings_warn__doc__},

static PyObject *
warnings_warn_impl(PyObject *module, PyObject *message, PyObject *category,
                   Py_ssize_t stacklevel, PyObject *source);

static PyObject *
warnings_warn(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"message", "category", "stacklevel", "source", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "warn", 0};
    PyObject *argsbuf[4];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *message;
    PyObject *category = Py_None;
    Py_ssize_t stacklevel = 1;
    PyObject *source = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 4, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    message = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[1]) {
        category = args[1];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[2]) {
        if (PyFloat_Check(args[2])) {
            PyErr_SetString(PyExc_TypeError,
                            "integer argument expected, got float" );
            goto exit;
        }
        {
            Py_ssize_t ival = -1;
            PyObject *iobj = PyNumber_Index(args[2]);
            if (iobj != NULL) {
                ival = PyLong_AsSsize_t(iobj);
                Py_DECREF(iobj);
            }
            if (ival == -1 && PyErr_Occurred()) {
                goto exit;
            }
            stacklevel = ival;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    source = args[3];
skip_optional_pos:
    return_value = warnings_warn_impl(module, message, category, stacklevel, source);

exit:
    return return_value;
}

static PyObject *
warnings_警告(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"消息", "类别", "栈级别", "来源", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "警告", 0};
    PyObject *argsbuf[4];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *message;
    PyObject *category = Py_None;
    Py_ssize_t stacklevel = 1;
    PyObject *source = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 4, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    message = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[1]) {
        category = args[1];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[2]) {
        if (PyFloat_Check(args[2])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        {
            Py_ssize_t ival = -1;
            PyObject *iobj = PyNumber_Index(args[2]);
            if (iobj != NULL) {
                ival = PyLong_AsSsize_t(iobj);
                Py_DECREF(iobj);
            }
            if (ival == -1 && PyErr_Occurred()) {
                goto exit;
            }
            stacklevel = ival;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    source = args[3];
skip_optional_pos:
    return_value = warnings_warn_impl(module, message, category, stacklevel, source);

exit:
    return return_value;
}
/*[clinic end generated code: output=b7bb54c73b5433ec input=a9049054013a1b77]*/
