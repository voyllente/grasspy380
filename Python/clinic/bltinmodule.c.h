/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(builtin_abs__doc__,
"abs($module, x, /)\n"
"--\n"
"\n"
"Return the absolute value of the argument.");

#define BUILTIN_ABS_METHODDEF    \
    {"abs", (PyCFunction)builtin_abs, METH_O, builtin_abs__doc__},

PyDoc_STRVAR(builtin_绝对值__doc__,
"绝对值($module, x, /)\n"
"--\n"
"\n"
"返回参数的绝对值.");

#define BUILTIN_绝对值_METHODDEF    \
    {"绝对值", (PyCFunction)builtin_abs, METH_O, builtin_绝对值__doc__},

PyDoc_STRVAR(builtin_all__doc__,
"all($module, iterable, /)\n"
"--\n"
"\n"
"Return True if bool(x) is True for all values x in the iterable.\n"
"\n"
"If the iterable is empty, return True.");

#define BUILTIN_ALL_METHODDEF    \
    {"all", (PyCFunction)builtin_all, METH_O, builtin_all__doc__},

PyDoc_STRVAR(builtin_全为真__doc__,
"全为真($module, 可迭代对象, /)\n"
"--\n"
"\n"
"对于可迭代对象中的所有值 x, 如果 布尔(x) 全为 真, 则返回 真.\n"
"\n"
"如果可迭代对象为空, 返回 真.");

#define BUILTIN_全为真_METHODDEF    \
    {"全为真", (PyCFunction)builtin_all, METH_O, builtin_全为真__doc__},

PyDoc_STRVAR(builtin_any__doc__,
"any($module, iterable, /)\n"
"--\n"
"\n"
"Return True if bool(x) is True for any x in the iterable.\n"
"\n"
"If the iterable is empty, return False.");

#define BUILTIN_ANY_METHODDEF    \
    {"any", (PyCFunction)builtin_any, METH_O, builtin_any__doc__},

PyDoc_STRVAR(builtin_任一为真__doc__,
"任一为真($module, 可迭代对象, /)\n"
"--\n"
"\n"
"对于可迭代对象中的任一 x, 如果 布尔(x) 为 真, 则返回 真.\n"
"\n"
"如果可迭代对象为空, 返回 假.");

#define BUILTIN_任一为真_METHODDEF    \
    {"任一为真", (PyCFunction)builtin_any, METH_O, builtin_任一为真__doc__},

PyDoc_STRVAR(builtin_ascii__doc__,
"ascii($module, 对象, /)\n"
"--\n"
"\n"
"返回一个对象的仅 ASCII 码表示.\n"
"\n"
"类似 表示(), 返回一个包含对象的可打印表示的字符串,\n"
"但会用 \\\\x, \\\\u 或 \\\\U 转义 表示() 返回的\n"
"字符串中的非 ASCII 字符. 此函数产生的字符串类似于\n"
"Python 2 中的 表示() 所返回的字符串.");

#define BUILTIN_ASCII_METHODDEF    \
    {"ascii", (PyCFunction)builtin_ascii, METH_O, builtin_ascii__doc__},

PyDoc_STRVAR(builtin_bin__doc__,
"bin($module, number, /)\n"
"--\n"
"\n"
"Return the binary representation of an integer.\n"
"\n"
"   >>> bin(2796202)\n"
"   \'0b1010101010101010101010\'");

#define BUILTIN_BIN_METHODDEF    \
    {"bin", (PyCFunction)builtin_bin, METH_O, builtin_bin__doc__},

PyDoc_STRVAR(builtin_二进制__doc__,
"二进制($module, 数, /)\n"
"--\n"
"\n"
"Return the binary representation of an integer.\n"
"\n"
"   >>> 二进制(2796202)\n"
"   \'0b1010101010101010101010\'");

#define BUILTIN_二进制_METHODDEF    \
    {"二进制", (PyCFunction)builtin_bin, METH_O, builtin_二进制__doc__},

PyDoc_STRVAR(builtin_callable__doc__,
"callable($module, obj, /)\n"
"--\n"
"\n"
"Return whether the object is callable (i.e., some kind of function).\n"
"\n"
"Note that classes are callable, as are instances of classes with a\n"
"__call__() method.");

#define BUILTIN_CALLABLE_METHODDEF    \
    {"callable", (PyCFunction)builtin_callable, METH_O, builtin_callable__doc__},

PyDoc_STRVAR(builtin_可调用__doc__,
"可调用($module, 对象, /)\n"
"--\n"
"\n"
"判断对象是否可调用 (即是否为某种函数).\n"
"\n"
"注意: 类是可调用对象, 具有 __调用__() 方法的类的实例\n"
"也是可调用对象.");

#define BUILTIN_可调用_METHODDEF    \
    {"可调用", (PyCFunction)builtin_callable, METH_O, builtin_可调用__doc__},

PyDoc_STRVAR(builtin_format__doc__,
"format($module, value, format_spec=\'\', /)\n"
"--\n"
"\n"
"Return value.__format__(format_spec)\n"
"\n"
"format_spec defaults to the empty string.\n"
"See the Format Specification Mini-Language section of help(\'FORMATTING\') for\n"
"details.");

#define BUILTIN_FORMAT_METHODDEF    \
    {"format", (PyCFunction)(void(*)(void))builtin_format, METH_FASTCALL, builtin_format__doc__},

PyDoc_STRVAR(builtin_格式化__doc__,
"格式化($module, 值, 格式规范=\'\', /)\n"
"--\n"
"\n"
"返回 值.__格式化__(格式规范)\n"
"\n"
"格式规范 默认为空字符串.\n"
"参见 帮助(\'FORMATTING\') 中的 Format Specification Mini-Language 部分以\n"
"了解详情.");

#define BUILTIN_格式化_METHODDEF    \
    {"格式化", (PyCFunction)(void(*)(void))builtin_format, METH_FASTCALL, builtin_格式化__doc__},

static PyObject *
builtin_format_impl(PyObject *module, PyObject *value, PyObject *format_spec);

static PyObject *
builtin_format(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *value;
    PyObject *format_spec = NULL;

    if (!_PyArg_CheckPositional("格式化", nargs, 1, 2)) {
        goto exit;
    }
    value = args[0];
    if (nargs < 2) {
        goto skip_optional;
    }
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("格式化", "参数 2", "字符串", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1]) == -1) {
        goto exit;
    }
    format_spec = args[1];
skip_optional:
    return_value = builtin_format_impl(module, value, format_spec);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_chr__doc__,
"chr($module, i, /)\n"
"--\n"
"\n"
"Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.");

#define BUILTIN_CHR_METHODDEF    \
    {"chr", (PyCFunction)builtin_chr, METH_O, builtin_chr__doc__},

PyDoc_STRVAR(builtin_符__doc__,
"符($module, i, /)\n"
"--\n"
"\n"
"返回符值为 i 的统一码字符; 0 <= i <= 0x10ffff.");

#define BUILTIN_符_METHODDEF    \
    {"符", (PyCFunction)builtin_chr, METH_O, builtin_符__doc__},

static PyObject *
builtin_chr_impl(PyObject *module, int i);

static PyObject *
builtin_chr(PyObject *module, PyObject *arg)
{
    PyObject *return_value = NULL;
    int i;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    i = _PyLong_AsInt(arg);
    if (i == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = builtin_chr_impl(module, i);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_compile__doc__,
"compile($module, /, source, filename, mode, flags=0,\n"
"        dont_inherit=False, optimize=-1, *, _feature_version=-1)\n"
"--\n"
"\n"
"Compile source into a code object that can be executed by exec() or eval().\n"
"\n"
"The source code may represent a Python module, statement or expression.\n"
"The filename will be used for run-time error messages.\n"
"The mode must be \'exec\' to compile a module, \'single\' to compile a\n"
"single (interactive) statement, or \'eval\' to compile an expression.\n"
"The flags argument, if present, controls which future statements influence\n"
"the compilation of the code.\n"
"The dont_inherit argument, if true, stops the compilation inheriting\n"
"the effects of any future statements in effect in the code calling\n"
"compile; if absent or false these statements do influence the compilation,\n"
"in addition to any features explicitly specified.");

#define BUILTIN_COMPILE_METHODDEF    \
    {"compile", (PyCFunction)(void(*)(void))builtin_compile, METH_FASTCALL|METH_KEYWORDS, builtin_compile__doc__},

PyDoc_STRVAR(builtin_编译__doc__,
"编译($module, /, 源代码, 文件名, 模式, 标志=0,\n"
"        不继承=假, 优化=-1, *, _特性版本=-1)\n"
"--\n"
"\n"
"将 源代码 编译为能被 执行() 或 评估() 执行的代码对象.\n"
"\n"
"源代码 可以是 Python 模块, 语句或表达式.\n"
"文件名 将用于运行时错误消息.\n"
"模式 选项如下: 编译一个模块 (语句序列) 须为 \'执行\', 编译单个(交互)语句\n"
"须为\'单句\', 编译一个表达式须为 \'评估\'.\n"
"标志 参数 (如果存在) 控制哪些 期货 语句会影响\n"
"代码的编译.\n"
"不继承 参数 (如果为真) 阻止编译继承\n"
"调用编译的代码中生效的 期货 语句的影响;\n"
"如果未指明或为假, 则除了明确指定的特性之外, \n"
"这些语句也会影响编译.");

#define BUILTIN_编译_METHODDEF    \
    {"编译", (PyCFunction)(void(*)(void))builtin_编译, METH_FASTCALL|METH_KEYWORDS, builtin_编译__doc__},

static PyObject *
builtin_compile_impl(PyObject *module, PyObject *source, PyObject *filename,
                     const char *mode, int flags, int dont_inherit,
                     int optimize, int feature_version);

static PyObject *
builtin_compile(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"source", "filename", "mode", "flags", "dont_inherit", "optimize", "_feature_version", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "compile", 0};
    PyObject *argsbuf[7];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 3;
    PyObject *source;
    PyObject *filename;
    const char *mode;
    int flags = 0;
    int dont_inherit = 0;
    int optimize = -1;
    int feature_version = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 3, 6, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    source = args[0];
    if (!PyUnicode_FSDecoder(args[1], &filename)) {
        goto exit;
    }
    if (!PyUnicode_Check(args[2])) {
        _PyArg_BadArgument("compile", "argument 'mode'", "str", args[2]);
        goto exit;
    }
    Py_ssize_t mode_length;
    mode = PyUnicode_AsUTF8AndSize(args[2], &mode_length);
    if (mode == NULL) {
        goto exit;
    }
    if (strlen(mode) != (size_t)mode_length) {
        PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[3]) {
        if (PyFloat_Check(args[3])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        flags = _PyLong_AsInt(args[3]);
        if (flags == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[4]) {
        if (PyFloat_Check(args[4])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        dont_inherit = _PyLong_AsInt(args[4]);
        if (dont_inherit == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[5]) {
        if (PyFloat_Check(args[5])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        optimize = _PyLong_AsInt(args[5]);
        if (optimize == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
skip_optional_pos:
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (PyFloat_Check(args[6])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    feature_version = _PyLong_AsInt(args[6]);
    if (feature_version == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = builtin_compile_impl(module, source, filename, mode, flags, dont_inherit, optimize, feature_version);

exit:
    return return_value;
}

static PyObject *
builtin_编译(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"源代码", "文件名", "模式", "标志", "不继承", "优化", "_特性版本", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "编译", 0};
    PyObject *argsbuf[7];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 3;
    PyObject *source;
    PyObject *filename;
    const char *mode;
    int flags = 0;
    int dont_inherit = 0;
    int optimize = -1;
    int feature_version = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 3, 6, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    source = args[0];
    if (!PyUnicode_FSDecoder(args[1], &filename)) {
        goto exit;
    }
    if (!PyUnicode_Check(args[2])) {
        _PyArg_BadArgument("编译", "参数 '模式'", "字符串", args[2]);
        goto exit;
    }
    Py_ssize_t mode_length;
    mode = PyUnicode_AsUTF8AndSize(args[2], &mode_length);
    if (mode == NULL) {
        goto exit;
    }
    if (strlen(mode) != (size_t)mode_length) {
        PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[3]) {
        if (PyFloat_Check(args[3])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        flags = _PyLong_AsInt(args[3]);
        if (flags == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[4]) {
        if (PyFloat_Check(args[4])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        dont_inherit = _PyLong_AsInt(args[4]);
        if (dont_inherit == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (args[5]) {
        if (PyFloat_Check(args[5])) {
            PyErr_SetString(PyExc_TypeError,
                            "期望整数参数，得到了浮点数" );
            goto exit;
        }
        optimize = _PyLong_AsInt(args[5]);
        if (optimize == -1 && PyErr_Occurred()) {
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
skip_optional_pos:
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    if (PyFloat_Check(args[6])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    feature_version = _PyLong_AsInt(args[6]);
    if (feature_version == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = builtin_compile_impl(module, source, filename, mode, flags, dont_inherit, optimize, feature_version);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_divmod__doc__,
"divmod($module, x, y, /)\n"
"--\n"
"\n"
"Return the tuple (x//y, x%y).  Invariant: div*y + mod == x.");

#define BUILTIN_DIVMOD_METHODDEF    \
    {"divmod", (PyCFunction)(void(*)(void))builtin_divmod, METH_FASTCALL, builtin_divmod__doc__},

PyDoc_STRVAR(builtin_商余__doc__,
"商余($module, x, y, /)\n"
"--\n"
"\n"
"返回元组 (x//y, x%y).  不变式: 商*y + 余数 == x.");

#define BUILTIN_商余_METHODDEF    \
    {"商余", (PyCFunction)(void(*)(void))builtin_divmod, METH_FASTCALL, builtin_商余__doc__},

static PyObject *
builtin_divmod_impl(PyObject *module, PyObject *x, PyObject *y);

static PyObject *
builtin_divmod(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *x;
    PyObject *y;

    if (!_PyArg_CheckPositional("divmod", nargs, 2, 2)) {
        goto exit;
    }
    x = args[0];
    y = args[1];
    return_value = builtin_divmod_impl(module, x, y);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_eval__doc__,
"eval($module, source, globals=None, locals=None, /)\n"
"--\n"
"\n"
"Evaluate the given source in the context of globals and locals.\n"
"\n"
"The source may be a string representing a Python expression\n"
"or a code object as returned by compile().\n"
"The globals must be a dictionary and locals can be any mapping,\n"
"defaulting to the current globals and locals.\n"
"If only globals is given, locals defaults to it.");

#define BUILTIN_EVAL_METHODDEF    \
    {"eval", (PyCFunction)(void(*)(void))builtin_eval, METH_FASTCALL, builtin_eval__doc__},

PyDoc_STRVAR(builtin_评估__doc__,
"评估($module, 源代码, 全局字典=空, 局部字典=空, /)\n"
"--\n"
"\n"
"以全局字典和局部字典为上下文评估给定的源代码.\n"
"\n"
"源代码 可以是代表 Python 表达式的字符串, \n"
"或是 编译() 返回的代码对象.\n"
"全局字典必须是一个字典, 局部字典可以是任何映射对象, \n"
"默认为当前的全局字典和局部字典.\n"
"如果只给出了全局字典, 局部字典默认为该字典.");

#define BUILTIN_评估_METHODDEF    \
    {"评估", (PyCFunction)(void(*)(void))builtin_eval, METH_FASTCALL, builtin_评估__doc__},

static PyObject *
builtin_eval_impl(PyObject *module, PyObject *source, PyObject *globals,
                  PyObject *locals);

static PyObject *
builtin_eval(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *source;
    PyObject *globals = Py_None;
    PyObject *locals = Py_None;

    if (!_PyArg_CheckPositional("eval", nargs, 1, 3)) {
        goto exit;
    }
    source = args[0];
    if (nargs < 2) {
        goto skip_optional;
    }
    globals = args[1];
    if (nargs < 3) {
        goto skip_optional;
    }
    locals = args[2];
skip_optional:
    return_value = builtin_eval_impl(module, source, globals, locals);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_exec__doc__,
"exec($module, source, globals=None, locals=None, /)\n"
"--\n"
"\n"
"Execute the given source in the context of globals and locals.\n"
"\n"
"The source may be a string representing one or more Python statements\n"
"or a code object as returned by compile().\n"
"The globals must be a dictionary and locals can be any mapping,\n"
"defaulting to the current globals and locals.\n"
"If only globals is given, locals defaults to it.");

#define BUILTIN_EXEC_METHODDEF    \
    {"exec", (PyCFunction)(void(*)(void))builtin_exec, METH_FASTCALL, builtin_exec__doc__},

PyDoc_STRVAR(builtin_执行__doc__,
"执行($module, 源代码, 全局字典=空, 局部字典=空, /)\n"
"--\n"
"\n"
"以全局字典和局部字典为上下文执行给定的源代码.\n"
"\n"
"源代码 可以是代表一个或多个 Python 语句的字符串, \n"
"或是 编译() 返回的代码对象.\n"
"全局字典必须是一个字典, 局部字典可以是任何映射对象, \n"
"默认为当前的全局字典和局部字典.\n"
"如果只给出了全局字典, 局部字典默认为该字典.");

#define BUILTIN_执行_METHODDEF    \
    {"执行", (PyCFunction)(void(*)(void))builtin_exec, METH_FASTCALL, builtin_执行__doc__},

static PyObject *
builtin_exec_impl(PyObject *module, PyObject *source, PyObject *globals,
                  PyObject *locals);

static PyObject *
builtin_exec(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *source;
    PyObject *globals = Py_None;
    PyObject *locals = Py_None;

    if (!_PyArg_CheckPositional("exec", nargs, 1, 3)) {
        goto exit;
    }
    source = args[0];
    if (nargs < 2) {
        goto skip_optional;
    }
    globals = args[1];
    if (nargs < 3) {
        goto skip_optional;
    }
    locals = args[2];
skip_optional:
    return_value = builtin_exec_impl(module, source, globals, locals);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_globals__doc__,
"globals($module, /)\n"
"--\n"
"\n"
"Return the dictionary containing the current scope\'s global variables.\n"
"\n"
"NOTE: Updates to this dictionary *will* affect name lookups in the current\n"
"global scope and vice-versa.");

#define BUILTIN_GLOBALS_METHODDEF    \
    {"globals", (PyCFunction)builtin_globals, METH_NOARGS, builtin_globals__doc__},

PyDoc_STRVAR(builtin_全局字典__doc__,
"全局字典($module, /)\n"
"--\n"
"\n"
"返回包含当前作用域的全局变量的字典.\n"
"\n"
"注意: 更新此字典 *会* 影响当前\n"
"全局作用域中的名称查找, 反之亦然.");

#define BUILTIN_全局字典_METHODDEF    \
    {"全局字典", (PyCFunction)builtin_globals, METH_NOARGS, builtin_全局字典__doc__},

static PyObject *
builtin_globals_impl(PyObject *module);

static PyObject *
builtin_globals(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return builtin_globals_impl(module);
}

PyDoc_STRVAR(builtin_hasattr__doc__,
"hasattr($module, obj, name, /)\n"
"--\n"
"\n"
"Return whether the object has an attribute with the given name.\n"
"\n"
"This is done by calling getattr(obj, name) and catching AttributeError.");

#define BUILTIN_HASATTR_METHODDEF    \
    {"hasattr", (PyCFunction)(void(*)(void))builtin_hasattr, METH_FASTCALL, builtin_hasattr__doc__},

PyDoc_STRVAR(builtin_有属性__doc__,
"有属性($module, 对象, 名称, /)\n"
"--\n"
"\n"
"返回给定对象是否有给定名称的属性.\n"
"\n"
"这是通过调用 取属性(对象, 名称) 实现的, 捕捉 属性错误类.");

#define BUILTIN_有属性_METHODDEF    \
    {"有属性", (PyCFunction)(void(*)(void))builtin_hasattr, METH_FASTCALL, builtin_有属性__doc__},

static PyObject *
builtin_hasattr_impl(PyObject *module, PyObject *obj, PyObject *name);

static PyObject *
builtin_hasattr(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *obj;
    PyObject *name;

    if (!_PyArg_CheckPositional("hasattr", nargs, 2, 2)) {
        goto exit;
    }
    obj = args[0];
    name = args[1];
    return_value = builtin_hasattr_impl(module, obj, name);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_id__doc__,
"id($module, 对象, /)\n"
"--\n"
"\n"
"返回一个对象的 ID.\n"
"\n"
"在同时存在的对象中, 此值保证是唯一值.\n"
"(CPython 使用对象的内存地址.)");

#define BUILTIN_ID_METHODDEF    \
    {"id", (PyCFunction)builtin_id, METH_O, builtin_id__doc__},

PyDoc_STRVAR(builtin_setattr__doc__,
"setattr($module, obj, name, value, /)\n"
"--\n"
"\n"
"Sets the named attribute on the given object to the specified value.\n"
"\n"
"setattr(x, \'y\', v) is equivalent to ``x.y = v\'\'");

#define BUILTIN_SETATTR_METHODDEF    \
    {"setattr", (PyCFunction)(void(*)(void))builtin_setattr, METH_FASTCALL, builtin_setattr__doc__},

PyDoc_STRVAR(builtin_设属性__doc__,
"设属性($module, 对象, 名称, 值, /)\n"
"--\n"
"\n"
"将对象的给定名称的属性设置为指定值.\n"
"\n"
"设属性(x, \'y\', v) 相当于 ``x.y = v\'\'");

#define BUILTIN_设属性_METHODDEF    \
    {"设属性", (PyCFunction)(void(*)(void))builtin_setattr, METH_FASTCALL, builtin_设属性__doc__},

static PyObject *
builtin_setattr_impl(PyObject *module, PyObject *obj, PyObject *name,
                     PyObject *value);

static PyObject *
builtin_setattr(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *obj;
    PyObject *name;
    PyObject *value;

    if (!_PyArg_CheckPositional("setattr", nargs, 3, 3)) {
        goto exit;
    }
    obj = args[0];
    name = args[1];
    value = args[2];
    return_value = builtin_setattr_impl(module, obj, name, value);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_delattr__doc__,
"delattr($module, obj, name, /)\n"
"--\n"
"\n"
"Deletes the named attribute from the given object.\n"
"\n"
"delattr(x, \'y\') is equivalent to ``del x.y\'\'");

#define BUILTIN_DELATTR_METHODDEF    \
    {"delattr", (PyCFunction)(void(*)(void))builtin_delattr, METH_FASTCALL, builtin_delattr__doc__},

PyDoc_STRVAR(builtin_删属性__doc__,
"删属性($module, 对象, 名称, /)\n"
"--\n"
"\n"
"从对象中删除给定名称的属性.\n"
"\n"
"删属性(x, \'y\') 相当于 ``删 x.y\'\'");

#define BUILTIN_删属性_METHODDEF    \
    {"删属性", (PyCFunction)(void(*)(void))builtin_delattr, METH_FASTCALL, builtin_删属性__doc__},

static PyObject *
builtin_delattr_impl(PyObject *module, PyObject *obj, PyObject *name);

static PyObject *
builtin_delattr(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *obj;
    PyObject *name;

    if (!_PyArg_CheckPositional("delattr", nargs, 2, 2)) {
        goto exit;
    }
    obj = args[0];
    name = args[1];
    return_value = builtin_delattr_impl(module, obj, name);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_hash__doc__,
"hash($module, obj, /)\n"
"--\n"
"\n"
"Return the hash value for the given object.\n"
"\n"
"Two objects that compare equal must also have the same hash value, but the\n"
"reverse is not necessarily true.");

#define BUILTIN_HASH_METHODDEF    \
    {"hash", (PyCFunction)builtin_hash, METH_O, builtin_hash__doc__},

PyDoc_STRVAR(builtin_哈希__doc__,
"哈希($module, 对象, /)\n"
"--\n"
"\n"
"返回给定对象的哈希值.\n"
"\n"
"两个相等的对象必定有相同的哈希值, 但\n"
"反过来不一定正确.");

#define BUILTIN_哈希_METHODDEF    \
    {"哈希", (PyCFunction)builtin_hash, METH_O, builtin_哈希__doc__},

PyDoc_STRVAR(builtin_hex__doc__,
"hex($module, number, /)\n"
"--\n"
"\n"
"Return the hexadecimal representation of an integer.\n"
"\n"
"   >>> hex(12648430)\n"
"   \'0xc0ffee\'");

#define BUILTIN_HEX_METHODDEF    \
    {"hex", (PyCFunction)builtin_hex, METH_O, builtin_hex__doc__},

PyDoc_STRVAR(builtin_十六进制__doc__,
"十六进制($module, 数, /)\n"
"--\n"
"\n"
"返回一个整数的十六进制表示.\n"
"\n"
"   >>> 十六进制(12648430)\n"
"   \'0xc0ffee\'");

#define BUILTIN_十六进制_METHODDEF    \
    {"十六进制", (PyCFunction)builtin_hex, METH_O, builtin_十六进制__doc__},

PyDoc_STRVAR(builtin_len__doc__,
"len($module, obj, /)\n"
"--\n"
"\n"
"Return the number of items in a container.");

#define BUILTIN_LEN_METHODDEF    \
    {"len", (PyCFunction)builtin_len, METH_O, builtin_len__doc__},

PyDoc_STRVAR(builtin_长__doc__,
"长($module, 对象, /)\n"
"--\n"
"\n"
"返回一个容器中的元素数目.");

#define BUILTIN_长_METHODDEF    \
    {"长", (PyCFunction)builtin_len, METH_O, builtin_长__doc__},

PyDoc_STRVAR(builtin_locals__doc__,
"locals($module, /)\n"
"--\n"
"\n"
"Return a dictionary containing the current scope\'s local variables.\n"
"\n"
"NOTE: Whether or not updates to this dictionary will affect name lookups in\n"
"the local scope and vice-versa is *implementation dependent* and not\n"
"covered by any backwards compatibility guarantees.");

#define BUILTIN_LOCALS_METHODDEF    \
    {"locals", (PyCFunction)builtin_locals, METH_NOARGS, builtin_locals__doc__},

PyDoc_STRVAR(builtin_局部字典__doc__,
"局部字典($module, /)\n"
"--\n"
"\n"
"返回一个包含当前作用域的局部变量的字典.\n"
"\n"
"注意: 更新此字典是否会影响局部作用域中的\n"
"名称查找 (以及反过来的情况), *取决于实现*;\n"
"向后兼容性的保证未涉及这一点.");

#define BUILTIN_局部字典_METHODDEF    \
    {"局部字典", (PyCFunction)builtin_locals, METH_NOARGS, builtin_局部字典__doc__},

static PyObject *
builtin_locals_impl(PyObject *module);

static PyObject *
builtin_locals(PyObject *module, PyObject *Py_UNUSED(ignored))
{
    return builtin_locals_impl(module);
}

PyDoc_STRVAR(builtin_oct__doc__,
"oct($module, number, /)\n"
"--\n"
"\n"
"Return the octal representation of an integer.\n"
"\n"
"   >>> oct(342391)\n"
"   \'0o1234567\'");

#define BUILTIN_OCT_METHODDEF    \
    {"oct", (PyCFunction)builtin_oct, METH_O, builtin_oct__doc__},

PyDoc_STRVAR(builtin_八进制__doc__,
"八进制($module, 数, /)\n"
"--\n"
"\n"
"返回一个整数的八进制表示.\n"
"\n"
"   >>> 八进制(342391)\n"
"   \'0o1234567\'");

#define BUILTIN_八进制_METHODDEF    \
    {"八进制", (PyCFunction)builtin_oct, METH_O, builtin_八进制__doc__},

PyDoc_STRVAR(builtin_ord__doc__,
"ord($module, c, /)\n"
"--\n"
"\n"
"Return the Unicode code point for a one-character string.");

#define BUILTIN_ORD_METHODDEF    \
    {"ord", (PyCFunction)builtin_ord, METH_O, builtin_ord__doc__},

PyDoc_STRVAR(builtin_符值__doc__,
"符值($module, 字符, /)\n"
"--\n"
"\n"
"返回一个字符的统一码码位.");

#define BUILTIN_符值_METHODDEF    \
    {"符值", (PyCFunction)builtin_ord, METH_O, builtin_符值__doc__},

PyDoc_STRVAR(builtin_pow__doc__,
"pow($module, /, base, exp, mod=None)\n"
"--\n"
"\n"
"Equivalent to base**exp with 2 arguments or base**exp % mod with 3 arguments\n"
"\n"
"Some types, such as ints, are able to use a more efficient algorithm when\n"
"invoked using the three argument form.");

#define BUILTIN_POW_METHODDEF    \
    {"pow", (PyCFunction)(void(*)(void))builtin_pow, METH_FASTCALL|METH_KEYWORDS, builtin_pow__doc__},

PyDoc_STRVAR(builtin_乘方__doc__,
"乘方($module, /, 基数, 指数, 模=空)\n"
"--\n"
"\n"
"只有两个参数时, 等价于 基数**指数; 有三个参数时, 等价于 基数**指数 % 模\n"
"\n"
"使用三参数形式调用时, 整数等类型可以使用\n"
"更高效的算法.");

#define BUILTIN_乘方_METHODDEF    \
    {"乘方", (PyCFunction)(void(*)(void))builtin_pow, METH_FASTCALL|METH_KEYWORDS, builtin_乘方__doc__},

static PyObject *
builtin_pow_impl(PyObject *module, PyObject *base, PyObject *exp,
                 PyObject *mod);

static PyObject *
builtin_pow(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"base", "exp", "mod", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "pow", 0};
    PyObject *argsbuf[3];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    PyObject *base;
    PyObject *exp;
    PyObject *mod = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 3, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    base = args[0];
    exp = args[1];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    mod = args[2];
skip_optional_pos:
    return_value = builtin_pow_impl(module, base, exp, mod);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_input__doc__,
"input($module, prompt=None, /)\n"
"--\n"
"\n"
"Read a string from standard input.  The trailing newline is stripped.\n"
"\n"
"The prompt string, if given, is printed to standard output without a\n"
"trailing newline before reading input.\n"
"\n"
"If the user hits EOF (*nix: Ctrl-D, Windows: Ctrl-Z+Return), raise EOFError.\n"
"On *nix systems, readline is used if available.");

#define BUILTIN_INPUT_METHODDEF    \
    {"input", (PyCFunction)(void(*)(void))builtin_input, METH_FASTCALL, builtin_input__doc__},

PyDoc_STRVAR(builtin_输入__doc__,
"输入($module, 提示语=空, /)\n"
"--\n"
"\n"
"从标准输入读取一个字符串. 结尾的换行符会被剔除.\n"
"\n"
"提示字符串 (如有) 会在读取输入前打印到标准输出,\n"
"结尾无换行符.\n"
"\n"
"如果用户按下 EOF (*nix: Ctrl-D, Windows: Ctrl-Z+回车), 则报 EOFError.\n"
"在 *nix 系统上使用 readline (如果可用).");

#define BUILTIN_输入_METHODDEF    \
    {"输入", (PyCFunction)(void(*)(void))builtin_input, METH_FASTCALL, builtin_输入__doc__},

static PyObject *
builtin_input_impl(PyObject *module, PyObject *prompt);

static PyObject *
builtin_input(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *prompt = NULL;

    if (!_PyArg_CheckPositional("input", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    prompt = args[0];
skip_optional:
    return_value = builtin_input_impl(module, prompt);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_repr__doc__,
"repr($module, obj, /)\n"
"--\n"
"\n"
"Return the canonical string representation of the object.\n"
"\n"
"For many object types, including most builtins, eval(repr(obj)) == obj.");

#define BUILTIN_REPR_METHODDEF    \
    {"repr", (PyCFunction)builtin_repr, METH_O, builtin_repr__doc__},

PyDoc_STRVAR(builtin_表示__doc__,
"表示($module, 对象, /)\n"
"--\n"
"\n"
"返回对象的规范化字符串表示.\n"
"\n"
"对于许多对象类型, 包括大部分内置对象, 评估(表示(对象)) == 对象.");

#define BUILTIN_表示_METHODDEF    \
    {"表示", (PyCFunction)builtin_repr, METH_O, builtin_表示__doc__},

PyDoc_STRVAR(builtin_round__doc__,
"round($module, /, number, ndigits=None)\n"
"--\n"
"\n"
"Round a number to a given precision in decimal digits.\n"
"\n"
"The return value is an integer if ndigits is omitted or None.  Otherwise\n"
"the return value has the same type as the number.  ndigits may be negative.");

#define BUILTIN_ROUND_METHODDEF    \
    {"round", (PyCFunction)(void(*)(void))builtin_round, METH_FASTCALL|METH_KEYWORDS, builtin_round__doc__},

PyDoc_STRVAR(builtin_舍入__doc__,
"舍入($module, /, 数, 位数=空)\n"
"--\n"
"\n"
"将一个数舍入到给定精度的值.\n"
"\n"
"如果 位数 未给出或为 空, 返回值为整数. 否则,\n"
"返回值与原数类型相同. 位数 可以是负数.");

#define BUILTIN_舍入_METHODDEF    \
    {"舍入", (PyCFunction)(void(*)(void))builtin_round, METH_FASTCALL|METH_KEYWORDS, builtin_舍入__doc__},

static PyObject *
builtin_round_impl(PyObject *module, PyObject *number, PyObject *ndigits);

static PyObject *
builtin_round(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"number", "ndigits", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "round", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *number;
    PyObject *ndigits = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    number = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    ndigits = args[1];
skip_optional_pos:
    return_value = builtin_round_impl(module, number, ndigits);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_sum__doc__,
"sum($module, iterable, /, start=0)\n"
"--\n"
"\n"
"Return the sum of a \'start\' value (default: 0) plus an iterable of numbers\n"
"\n"
"When the iterable is empty, return the start value.\n"
"This function is intended specifically for use with numeric values and may\n"
"reject non-numeric types.");

#define BUILTIN_SUM_METHODDEF    \
    {"sum", (PyCFunction)(void(*)(void))builtin_sum, METH_FASTCALL|METH_KEYWORDS, builtin_sum__doc__},

PyDoc_STRVAR(builtin_和__doc__,
"和($module, 可迭代对象, /, 起始=0)\n"
"--\n"
"\n"
"返回 \'起始\' 值 (默认值为 0) 与可迭代对象中的各数之和.\n"
"\n"
"可迭代对象为空时, 返回起始值.\n"
"此函数专门用于数字值,\n"
"会拒绝非数值类型.");

#define BUILTIN_和_METHODDEF    \
    {"和", (PyCFunction)(void(*)(void))builtin_sum, METH_FASTCALL|METH_KEYWORDS, builtin_和__doc__},

static PyObject *
builtin_sum_impl(PyObject *module, PyObject *iterable, PyObject *start);

static PyObject *
builtin_sum(PyObject *module, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "start", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "sum", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *iterable;
    PyObject *start = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    iterable = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    start = args[1];
skip_optional_pos:
    return_value = builtin_sum_impl(module, iterable, start);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_isinstance__doc__,
"isinstance($module, obj, class_or_tuple, /)\n"
"--\n"
"\n"
"Return whether an object is an instance of a class or of a subclass thereof.\n"
"\n"
"A tuple, as in ``isinstance(x, (A, B, ...))``, may be given as the target to\n"
"check against. This is equivalent to ``isinstance(x, A) or isinstance(x, B)\n"
"or ...`` etc.");

#define BUILTIN_ISINSTANCE_METHODDEF    \
    {"isinstance", (PyCFunction)(void(*)(void))builtin_isinstance, METH_FASTCALL, builtin_isinstance__doc__},

PyDoc_STRVAR(builtin_是实例__doc__,
"是实例($module, 对象, 类或元组, /)\n"
"--\n"
"\n"
"返回一个对象是否为一个类或子类的实例.\n"
"\n"
"可以给定一个元组作为要检查的目标, 例如 ``是实例(x, (A, B, ...))``,\n"
"这相当于 ``是实例(x, A) 或 是实例(x, B)\n"
"或 ...`` .");

#define BUILTIN_是实例_METHODDEF    \
    {"是实例", (PyCFunction)(void(*)(void))builtin_isinstance, METH_FASTCALL, builtin_是实例__doc__},

static PyObject *
builtin_isinstance_impl(PyObject *module, PyObject *obj,
                        PyObject *class_or_tuple);

static PyObject *
builtin_isinstance(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *obj;
    PyObject *class_or_tuple;

    if (!_PyArg_CheckPositional("isinstance", nargs, 2, 2)) {
        goto exit;
    }
    obj = args[0];
    class_or_tuple = args[1];
    return_value = builtin_isinstance_impl(module, obj, class_or_tuple);

exit:
    return return_value;
}

PyDoc_STRVAR(builtin_issubclass__doc__,
"issubclass($module, cls, class_or_tuple, /)\n"
"--\n"
"\n"
"Return whether \'cls\' is a derived from another class or is the same class.\n"
"\n"
"A tuple, as in ``issubclass(x, (A, B, ...))``, may be given as the target to\n"
"check against. This is equivalent to ``issubclass(x, A) or issubclass(x, B)\n"
"or ...`` etc.");

#define BUILTIN_ISSUBCLASS_METHODDEF    \
    {"issubclass", (PyCFunction)(void(*)(void))builtin_issubclass, METH_FASTCALL, builtin_issubclass__doc__},

PyDoc_STRVAR(builtin_是子类__doc__,
"是子类($module, 类, 类或元组, /)\n"
"--\n"
"\n"
"返回 \'类\' 是否派生自另一个类或是否为同一个类.\n"
"\n"
"可以给定一个元组作为要检查的目标, 例如 ``是子类(x, (A, B, ...))``,\n"
"这相当于 ``是子类(x, A) 或 是子类(x, B)\n"
"或 ...`` .");

#define BUILTIN_是子类_METHODDEF    \
    {"是子类", (PyCFunction)(void(*)(void))builtin_issubclass, METH_FASTCALL, builtin_是子类__doc__},

static PyObject *
builtin_issubclass_impl(PyObject *module, PyObject *cls,
                        PyObject *class_or_tuple);

static PyObject *
builtin_issubclass(PyObject *module, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *cls;
    PyObject *class_or_tuple;

    if (!_PyArg_CheckPositional("issubclass", nargs, 2, 2)) {
        goto exit;
    }
    cls = args[0];
    class_or_tuple = args[1];
    return_value = builtin_issubclass_impl(module, cls, class_or_tuple);

exit:
    return return_value;
}
/*[clinic end generated code: output=29686a89b739d600 input=a9049054013a1b77]*/
