/*[clinic input]
preserve
[clinic start generated code]*/

static PyObject *
mappingproxy_new_impl(PyTypeObject *type, PyObject *mapping);

static PyObject *
mappingproxy_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"mapping", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "mappingproxy", 0};
    PyObject *argsbuf[1];
    PyObject * const *fastargs;
    Py_ssize_t nargs = PyTuple_GET_SIZE(args);
    PyObject *mapping;

    fastargs = _PyArg_UnpackKeywords(_PyTuple_CAST(args)->ob_item, nargs, kwargs, NULL, &_parser, 1, 1, 0, argsbuf);
    if (!fastargs) {
        goto exit;
    }
    mapping = fastargs[0];
    return_value = mappingproxy_new_impl(type, mapping);

exit:
    return return_value;
}

PyDoc_STRVAR(property_init__doc__,
"属性(fget=空, fset=空, fdel=空, doc=空)\n"
"--\n"
"\n"
"定义属性.\n"
"\n"
"  fget\n"
"    取函数, 用于获取属性值\n"
"  fset\n"
"    设函数, 用于设置属性值\n"
"  fdel\n"
"    删函数, 用于删除属性值\n"
"  doc\n"
"    文档字符串\n"
"\n"
"典型用例是定义一个托管属性 x:\n"
"\n"
"类 C(对象):\n"
"    函 取x(自身): 返回 自身._x\n"
"    函 设x(自身, 值): 自身._x = 值\n"
"    函 删x(自身): 删 自身._x\n"
"    x = 属性(取x, 设x, 删x, \"我是 \'x\' 属性.\")\n"
"\n"
"使用装饰器可以轻松定义新属性或修改现有属性:\n"
"\n"
"类 C(对象):\n"
"    @属性\n"
"    函 x(自身):\n"
"        \"我是 \'x\' 属性.\"\n"
"        返回 自身._x\n"
"    @x.设置器\n"
"    函 x(自身, 值):\n"
"        自身._x = 值\n"
"    @x.删除器\n"
"    函 x(自身):\n"
"        删 自身._x");

static int
property_init_impl(propertyobject *self, PyObject *fget, PyObject *fset,
                   PyObject *fdel, PyObject *doc);

static int
property_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
    int return_value = -1;
    static const char * const _keywords[] = {"fget", "fset", "fdel", "doc", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "property", 0};
    PyObject *argsbuf[4];
    PyObject *const *fastargs;
    Py_ssize_t nargs = PyTuple_GET_SIZE(args);
    Py_ssize_t noptargs = nargs + (kwargs ? PyDict_GET_SIZE(kwargs) : 0) - 0;
    PyObject *fget = NULL;
    PyObject *fset = NULL;
    PyObject *fdel = NULL;
    PyObject *doc = NULL;

    fastargs = _PyArg_UnpackKeywords(_PyTuple_CAST(args)->ob_item, nargs, kwargs, NULL, &_parser, 0, 4, 0, argsbuf);
    if (!fastargs) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (fastargs[0]) {
        fget = fastargs[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (fastargs[1]) {
        fset = fastargs[1];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (fastargs[2]) {
        fdel = fastargs[2];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    doc = fastargs[3];
    
skip_optional_pos:
    return_value = property_init_impl((propertyobject *)self, fget, fset, fdel, doc);

exit:
    return return_value;
}
/*[clinic end generated code: output=916624e717862abc input=a9049054013a1b77]*/
