/*[clinic input]
preserve
[clinic start generated code]*/

static PyObject *
long_new_impl(PyTypeObject *type, PyObject *x, PyObject *obase);

static PyObject *
long_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "base", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "int", 0};
    PyObject *argsbuf[2];
    PyObject * const *fastargs;
    Py_ssize_t nargs = PyTuple_GET_SIZE(args);
    Py_ssize_t noptargs = nargs + (kwargs ? PyDict_GET_SIZE(kwargs) : 0) - 0;
    PyObject *x = NULL;
    PyObject *obase = NULL;

    fastargs = _PyArg_UnpackKeywords(_PyTuple_CAST(args)->ob_item, nargs, kwargs, NULL, &_parser, 0, 2, 0, argsbuf);
    if (!fastargs) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional_posonly;
    }
    noptargs--;
    x = fastargs[0];
skip_optional_posonly:
    if (!noptargs) {
        goto skip_optional_pos;
    }
    obase = fastargs[1];
skip_optional_pos:
    return_value = long_new_impl(type, x, obase);

exit:
    return return_value;
}

PyDoc_STRVAR(int___getnewargs____doc__,
"__getnewargs__($self, /)\n"
"--\n"
"\n");

#define INT___GETNEWARGS___METHODDEF    \
    {"__getnewargs__", (PyCFunction)int___getnewargs__, METH_NOARGS, int___getnewargs____doc__},

static PyObject *
int___getnewargs___impl(PyObject *self);

static PyObject *
int___getnewargs__(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return int___getnewargs___impl(self);
}

PyDoc_STRVAR(int___format____doc__,
"__格式化__($self, 格式规范, /)\n"
"--\n"
"\n");

#define INT___FORMAT___METHODDEF    \
    {"__format__", (PyCFunction)int___format__, METH_O, int___format____doc__},

#define INT___格式化___METHODDEF    \
    {"__格式化__", (PyCFunction)int___format__, METH_O, int___format____doc__},

static PyObject *
int___format___impl(PyObject *self, PyObject *format_spec);

static PyObject *
int___format__(PyObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    PyObject *format_spec;

    if (!PyUnicode_Check(arg)) {
        _PyArg_BadArgument("__format__", "参数", "字符串", arg);
        goto exit;
    }
    if (PyUnicode_READY(arg) == -1) {
        goto exit;
    }
    format_spec = arg;
    return_value = int___format___impl(self, format_spec);

exit:
    return return_value;
}

PyDoc_STRVAR(int___sizeof____doc__,
"__内存占用__($self, /)\n"
"--\n"
"\n"
"返回在内存中占用的字节数.");

#define INT___SIZEOF___METHODDEF    \
    {"__sizeof__", (PyCFunction)int___sizeof__, METH_NOARGS, int___sizeof____doc__},

#define INT___内存占用___METHODDEF    \
    {"__内存占用__", (PyCFunction)int___sizeof__, METH_NOARGS, int___sizeof____doc__},

static Py_ssize_t
int___sizeof___impl(PyObject *self);

static PyObject *
int___sizeof__(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    PyObject *return_value = NULL;
    Py_ssize_t _return_value;

    _return_value = int___sizeof___impl(self);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyLong_FromSsize_t(_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(int_bit_length__doc__,
"bit_length($self, /)\n"
"--\n"
"\n"
"Number of bits necessary to represent self in binary.\n"
"\n"
">>> bin(37)\n"
"\'0b100101\'\n"
">>> (37).bit_length()\n"
"6");

#define INT_BIT_LENGTH_METHODDEF    \
    {"bit_length", (PyCFunction)int_bit_length, METH_NOARGS, int_bit_length__doc__},

PyDoc_STRVAR(int_位长__doc__,
"位长($self, /)\n"
"--\n"
"\n"
"以二进制表示本身所需的位数.\n"
"\n"
">>> 二进制(37)\n"
"\'0b100101\'\n"
">>> (37).位长()\n"
"6");

#define INT_位长_METHODDEF    \
    {"位长", (PyCFunction)int_bit_length, METH_NOARGS, int_位长__doc__},

static PyObject *
int_bit_length_impl(PyObject *self);

static PyObject *
int_bit_length(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return int_bit_length_impl(self);
}

PyDoc_STRVAR(int_as_integer_ratio__doc__,
"整数比($self, /)\n"
"--\n"
"\n"
"返回整数比.\n"
"\n"
"返回一对整数, 其比值恰好等于原整数,\n"
"分母为正.\n"
"\n"
">>> (10).整数比()\n"
"(10, 1)\n"
">>> (-10).整数比()\n"
"(-10, 1)\n"
">>> (0).整数比()\n"
"(0, 1)");

#define INT_AS_INTEGER_RATIO_METHODDEF    \
    {"as_integer_ratio", (PyCFunction)int_as_integer_ratio, METH_NOARGS, int_as_integer_ratio__doc__},

#define INT_整数比_METHODDEF    \
    {"整数比", (PyCFunction)int_as_integer_ratio, METH_NOARGS, int_as_integer_ratio__doc__},

static PyObject *
int_as_integer_ratio_impl(PyObject *self);

static PyObject *
int_as_integer_ratio(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return int_as_integer_ratio_impl(self);
}

PyDoc_STRVAR(int_to_bytes__doc__,
"转字节($self, /, 长度, 字节序, *, 带符号=假)\n"
"--\n"
"\n"
"返回一个代表整数的字节数组.\n"
"\n"
"  长度\n"
"    要使用的字节对象的长度.  若该整数无法用给定数量的字节表示,\n"
"    则报 溢出错误类 错误.\n"
"  字节序\n"
"    用以表示整数的字节序.  若 字节序 为 \'big\',\n"
"    则最高有效位在字节数组的开头.\n"
"    若 字节序 为 \'little\', 则最高有效位在字节数组的末尾.\n"
"    要请求主机系统上的原生字节序, 请使用\n"
"    `系统.字节顺序\' 作为 字节序 值.\n"
"  带符号\n"
"    确定是否使用二进制补码来表示整数.\n"
"    如果 带符号 为 假 并给定一个负数, 则报 溢出错误类 错误.");

#define INT_TO_BYTES_METHODDEF    \
    {"to_bytes", (PyCFunction)(void(*)(void))int_to_bytes, METH_FASTCALL|METH_KEYWORDS, int_to_bytes__doc__},

#define INT_转字节_METHODDEF    \
    {"转字节", (PyCFunction)(void(*)(void))int_转字节, METH_FASTCALL|METH_KEYWORDS, int_to_bytes__doc__},

static PyObject *
int_to_bytes_impl(PyObject *self, Py_ssize_t length, PyObject *byteorder,
                  int is_signed);

static PyObject *
int_to_bytes(PyObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"length", "byteorder", "signed", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "to_bytes", 0};
    PyObject *argsbuf[3];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    Py_ssize_t length;
    PyObject *byteorder;
    int is_signed = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        length = ival;
    }
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("to_bytes", "参数 'byteorder'", "字符串", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1]) == -1) {
        goto exit;
    }
    byteorder = args[1];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    is_signed = PyObject_IsTrue(args[2]);
    if (is_signed < 0) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = int_to_bytes_impl(self, length, byteorder, is_signed);

exit:
    return return_value;
}

static PyObject *
int_转字节(PyObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"长度", "字节序", "带符号", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "转字节", 0};
    PyObject *argsbuf[3];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    Py_ssize_t length;
    PyObject *byteorder;
    int is_signed = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[0]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        length = ival;
    }
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("转字节", "参数 '字节序'", "字符串", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1]) == -1) {
        goto exit;
    }
    byteorder = args[1];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    is_signed = PyObject_IsTrue(args[2]);
    if (is_signed < 0) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = int_to_bytes_impl(self, length, byteorder, is_signed);

exit:
    return return_value;
}

PyDoc_STRVAR(int_from_bytes__doc__,
"从字节($type, /, 字节, 字节序, *, 带符号=假)\n"
"--\n"
"\n"
"返回给定字节数组所代表的整数.\n"
"\n"
"  字节\n"
"    保存要转换的字节数组.  该参数必须\n"
"    支持缓冲区协议, 或是一个产生字节的可迭代对象.\n"
"    字节和字节数组就是支持缓冲区协议的内置对象的例子.\n"
"  字节序\n"
"    用以表示整数的字节顺序.  如果 字节序 为 \'big\',\n"
"    则最高有效位在字节数组的开头.\n"
"    若 字节序 为 \'little\', 则最高有效位在字节数组的末尾.\n"
"    要请求主机系统上的原生字节序, 请使用\n"
"    `系统.字节顺序\' 作为 字节序 值.\n"
"  带符号\n"
"    表示是否使用二进制补码来表示整数.");

#define INT_FROM_BYTES_METHODDEF    \
    {"from_bytes", (PyCFunction)(void(*)(void))int_from_bytes, METH_FASTCALL|METH_KEYWORDS|METH_CLASS, int_from_bytes__doc__},

#define INT_从字节_METHODDEF    \
    {"从字节", (PyCFunction)(void(*)(void))int_从字节, METH_FASTCALL|METH_KEYWORDS|METH_CLASS, int_from_bytes__doc__},

static PyObject *
int_from_bytes_impl(PyTypeObject *type, PyObject *bytes_obj,
                    PyObject *byteorder, int is_signed);

static PyObject *
int_from_bytes(PyTypeObject *type, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"bytes", "byteorder", "signed", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "from_bytes", 0};
    PyObject *argsbuf[3];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    PyObject *bytes_obj;
    PyObject *byteorder;
    int is_signed = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    bytes_obj = args[0];
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("from_bytes", "参数 'byteorder'", "字符串", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1]) == -1) {
        goto exit;
    }
    byteorder = args[1];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    is_signed = PyObject_IsTrue(args[2]);
    if (is_signed < 0) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = int_from_bytes_impl(type, bytes_obj, byteorder, is_signed);

exit:
    return return_value;
}

static PyObject *
int_从字节(PyTypeObject *type, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"字节", "字节序", "带符号", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "从字节", 0};
    PyObject *argsbuf[3];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 2;
    PyObject *bytes_obj;
    PyObject *byteorder;
    int is_signed = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 2, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    bytes_obj = args[0];
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("从字节", "参数 '字节序'", "字符串", args[1]);
        goto exit;
    }
    if (PyUnicode_READY(args[1]) == -1) {
        goto exit;
    }
    byteorder = args[1];
    if (!noptargs) {
        goto skip_optional_kwonly;
    }
    is_signed = PyObject_IsTrue(args[2]);
    if (is_signed < 0) {
        goto exit;
    }
skip_optional_kwonly:
    return_value = int_from_bytes_impl(type, bytes_obj, byteorder, is_signed);

exit:
    return return_value;
}
/*[clinic end generated code: output=77bc3b2615822cb8 input=a9049054013a1b77]*/
