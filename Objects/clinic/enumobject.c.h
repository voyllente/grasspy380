/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(enum_new__doc__,
"枚举(可迭代对象, 起始=0)\n"
"--\n"
"\n"
"返回一个枚举对象.\n"
"\n"
"  可迭代对象\n"
"    一个支持迭代的对象\n"
"\n"
"枚举对象生成的 2-元组包含计数值 (从 起始 参数值开始, \n"
"默认起始值为 0) 和 可迭代对象 参数生成的值.\n"
"\n"
"枚举 可用于获得带索引的列表:\n"
"    (0, 序列[0]), (1, 序列[1]), (2, 序列[2]), ...");

static PyObject *
enum_new_impl(PyTypeObject *type, PyObject *iterable, PyObject *start);

static PyObject *
enum_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"iterable", "start", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "enumerate", 0};
    PyObject *argsbuf[2];
    PyObject * const *fastargs;
    Py_ssize_t nargs = PyTuple_GET_SIZE(args);
    Py_ssize_t noptargs = nargs + (kwargs ? PyDict_GET_SIZE(kwargs) : 0) - 1;
    PyObject *iterable;
    PyObject *start = 0;

    fastargs = _PyArg_UnpackKeywords(_PyTuple_CAST(args)->ob_item, nargs, kwargs, NULL, &_parser, 1, 2, 0, argsbuf);
    if (!fastargs) {
        goto exit;
    }
    iterable = fastargs[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    start = fastargs[1];
skip_optional_pos:
    return_value = enum_new_impl(type, iterable, start);

exit:
    return return_value;
}

PyDoc_STRVAR(reversed_new__doc__,
"反转(序列, /)\n"
"--\n"
"\n"
"返回给定序列的值反转后的迭代器.");

static PyObject *
reversed_new_impl(PyTypeObject *type, PyObject *seq);

static PyObject *
reversed_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    PyObject *return_value = NULL;
    PyObject *seq;

    if ((type == &PyReversed_Type) &&
        !_PyArg_NoKeywords("reversed", kwargs)) {
        goto exit;
    }
    if (!_PyArg_CheckPositional("reversed", PyTuple_GET_SIZE(args), 1, 1)) {
        goto exit;
    }
    seq = PyTuple_GET_ITEM(args, 0);
    return_value = reversed_new_impl(type, seq);

exit:
    return return_value;
}
/*[clinic end generated code: output=e18c3fefcf914ec7 input=a9049054013a1b77]*/
