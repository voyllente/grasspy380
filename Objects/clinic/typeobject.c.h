/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(type___instancecheck____doc__,
"__检查是否实例__($self, 实例, /)\n"
"--\n"
"\n"
"检查一个对象是否是实例.");

#define TYPE___INSTANCECHECK___METHODDEF    \
    {"__instancecheck__", (PyCFunction)type___instancecheck__, METH_O, type___instancecheck____doc__},

#define TYPE___检查是否实例___METHODDEF    \
    {"__检查是否实例__", (PyCFunction)type___instancecheck__, METH_O, type___instancecheck____doc__},

static int
type___instancecheck___impl(PyTypeObject *self, PyObject *instance);

static PyObject *
type___instancecheck__(PyTypeObject *self, PyObject *instance)
{
    PyObject *return_value = NULL;
    int _return_value;

    _return_value = type___instancecheck___impl(self, instance);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyBool_FromLong((long)_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(type___subclasscheck____doc__,
"__检查是否子类__($self, 子类, /)\n"
"--\n"
"\n"
"检查一个类是否是子类.");

#define TYPE___SUBCLASSCHECK___METHODDEF    \
    {"__subclasscheck__", (PyCFunction)type___subclasscheck__, METH_O, type___subclasscheck____doc__},

#define TYPE___检查是否子类___METHODDEF    \
    {"__检查是否子类__", (PyCFunction)type___subclasscheck__, METH_O, type___subclasscheck____doc__},

static int
type___subclasscheck___impl(PyTypeObject *self, PyObject *subclass);

static PyObject *
type___subclasscheck__(PyTypeObject *self, PyObject *subclass)
{
    PyObject *return_value = NULL;
    int _return_value;

    _return_value = type___subclasscheck___impl(self, subclass);
    if ((_return_value == -1) && PyErr_Occurred()) {
        goto exit;
    }
    return_value = PyBool_FromLong((long)_return_value);

exit:
    return return_value;
}

PyDoc_STRVAR(type_mro__doc__,
"mro($self, /)\n"
"--\n"
"\n"
"返回一个类型的方法解析顺序.");

#define TYPE_MRO_METHODDEF    \
    {"mro", (PyCFunction)type_mro, METH_NOARGS, type_mro__doc__},

static PyObject *
type_mro_impl(PyTypeObject *self);

static PyObject *
type_mro(PyTypeObject *self, PyObject *Py_UNUSED(ignored))
{
    return type_mro_impl(self);
}

PyDoc_STRVAR(type___subclasses____doc__,
"__所有子类__($self, /)\n"
"--\n"
"\n"
"返回直接子类的列表.");

#define TYPE___SUBCLASSES___METHODDEF    \
    {"__subclasses__", (PyCFunction)type___subclasses__, METH_NOARGS, type___subclasses____doc__},

#define TYPE___所有子类___METHODDEF    \
    {"__所有子类__", (PyCFunction)type___subclasses__, METH_NOARGS, type___subclasses____doc__},

static PyObject *
type___subclasses___impl(PyTypeObject *self);

static PyObject *
type___subclasses__(PyTypeObject *self, PyObject *Py_UNUSED(ignored))
{
    return type___subclasses___impl(self);
}

PyDoc_STRVAR(type___dir____doc__,
"__dir__($self, /)\n"
"--\n"
"\n"
"类型的专门 __dir__ 实现.");

#define TYPE___DIR___METHODDEF    \
    {"__dir__", (PyCFunction)type___dir__, METH_NOARGS, type___dir____doc__},

static PyObject *
type___dir___impl(PyTypeObject *self);

static PyObject *
type___dir__(PyTypeObject *self, PyObject *Py_UNUSED(ignored))
{
    return type___dir___impl(self);
}

PyDoc_STRVAR(type___sizeof____doc__,
"__sizeof__($self, /)\n"
"--\n"
"\n"
"Return memory consumption of the type object.");

#define TYPE___SIZEOF___METHODDEF    \
    {"__sizeof__", (PyCFunction)type___sizeof__, METH_NOARGS, type___sizeof____doc__},

PyDoc_STRVAR(type___内存占用____doc__,
"__内存占用__($self, /)\n"
"--\n"
"\n"
"返回类型对象占用的内存大小.");

#define TYPE___内存占用___METHODDEF    \
    {"__内存占用__", (PyCFunction)type___sizeof__, METH_NOARGS, type___内存占用____doc__},

static PyObject *
type___sizeof___impl(PyTypeObject *self);

static PyObject *
type___sizeof__(PyTypeObject *self, PyObject *Py_UNUSED(ignored))
{
    return type___sizeof___impl(self);
}

PyDoc_STRVAR(object___reduce____doc__,
"__归并__($self, /)\n"
"--\n"
"\n"
"用于支持序列化.");

#define OBJECT___REDUCE___METHODDEF    \
    {"__reduce__", (PyCFunction)object___reduce__, METH_NOARGS, object___reduce____doc__},

#define OBJECT___归并___METHODDEF    \
    {"__归并__", (PyCFunction)object___reduce__, METH_NOARGS, object___reduce____doc__},

static PyObject *
object___reduce___impl(PyObject *self);

static PyObject *
object___reduce__(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return object___reduce___impl(self);
}

PyDoc_STRVAR(object___reduce_ex____doc__,
"__归并ex__($self, 协议, /)\n"
"--\n"
"\n"
"用于支持序列化.");

#define OBJECT___REDUCE_EX___METHODDEF    \
    {"__reduce_ex__", (PyCFunction)object___reduce_ex__, METH_O, object___reduce_ex____doc__},

#define OBJECT___归并EX___METHODDEF    \
    {"__归并ex__", (PyCFunction)object___reduce_ex__, METH_O, object___reduce_ex____doc__},

static PyObject *
object___reduce_ex___impl(PyObject *self, int protocol);

static PyObject *
object___reduce_ex__(PyObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    int protocol;

    if (PyFloat_Check(arg)) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    protocol = _PyLong_AsInt(arg);
    if (protocol == -1 && PyErr_Occurred()) {
        goto exit;
    }
    return_value = object___reduce_ex___impl(self, protocol);

exit:
    return return_value;
}

PyDoc_STRVAR(object___format____doc__,
"__格式化__($self, 格式规范, /)\n"
"--\n"
"\n"
"默认的对象格式化器.");

#define OBJECT___FORMAT___METHODDEF    \
    {"__format__", (PyCFunction)object___format__, METH_O, object___format____doc__},

#define OBJECT___格式化___METHODDEF    \
    {"__格式化__", (PyCFunction)object___format__, METH_O, object___format____doc__},

static PyObject *
object___format___impl(PyObject *self, PyObject *format_spec);

static PyObject *
object___format__(PyObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    PyObject *format_spec;

    if (!PyUnicode_Check(arg)) {
        _PyArg_BadArgument("__format__", "参数", "字符串", arg);
        goto exit;
    }
    if (PyUnicode_READY(arg) == -1) {
        goto exit;
    }
    format_spec = arg;
    return_value = object___format___impl(self, format_spec);

exit:
    return return_value;
}

PyDoc_STRVAR(object___sizeof____doc__,
"__sizeof__($self, /)\n"
"--\n"
"\n"
"Size of object in memory, in bytes.");

#define OBJECT___SIZEOF___METHODDEF    \
    {"__sizeof__", (PyCFunction)object___sizeof__, METH_NOARGS, object___sizeof____doc__},

PyDoc_STRVAR(object___内存占用____doc__,
"__内存占用__($self, /)\n"
"--\n"
"\n"
"对象在内存中的大小, 单位为字节.");

#define OBJECT___内存占用___METHODDEF    \
    {"__内存占用__", (PyCFunction)object___sizeof__, METH_NOARGS, object___内存占用____doc__},

static PyObject *
object___sizeof___impl(PyObject *self);

static PyObject *
object___sizeof__(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return object___sizeof___impl(self);
}

PyDoc_STRVAR(object___dir____doc__,
"__dir__($self, /)\n"
"--\n"
"\n"
"默认 dir() 实现.");

#define OBJECT___DIR___METHODDEF    \
    {"__dir__", (PyCFunction)object___dir__, METH_NOARGS, object___dir____doc__},

static PyObject *
object___dir___impl(PyObject *self);

static PyObject *
object___dir__(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    return object___dir___impl(self);
}
/*[clinic end generated code: output=7a6d272d282308f3 input=a9049054013a1b77]*/
