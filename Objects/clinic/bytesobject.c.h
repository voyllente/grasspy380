/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(bytes_split__doc__,
"split($self, /, sep=None, maxsplit=-1)\n"
"--\n"
"\n"
"Return a list of the sections in the bytes, using sep as the delimiter.\n"
"\n"
"  sep\n"
"    The delimiter according which to split the bytes.\n"
"    None (the default value) means split on ASCII whitespace characters\n"
"    (space, tab, return, newline, formfeed, vertical tab).\n"
"  maxsplit\n"
"    Maximum number of splits to do.\n"
"    -1 (the default value) means no limit.");

#define BYTES_SPLIT_METHODDEF    \
    {"split", (PyCFunction)(void(*)(void))bytes_split, METH_FASTCALL|METH_KEYWORDS, bytes_split__doc__},

PyDoc_STRVAR(bytes_分割__doc__,
"分割($self, /, 分隔符=空, 最大分割次数=-1)\n"
"--\n"
"\n"
"用 分隔符 将字节分割成若干部分, 返回一个列表.\n"
"\n"
"  分隔符\n"
"    据以分割字节的字符.\n"
"    空 (默认值) 意味着依据 ASCII 空白字符\n"
"    (空格, tab, 回车, 换行, 换页, 垂直制表符) 进行分割.\n"
"  最大分割次数\n"
"    要执行的最大分割次数.\n"
"    -1 (默认值) 表示无限制.");

#define BYTES_分割_METHODDEF    \
    {"分割", (PyCFunction)(void(*)(void))bytes_分割, METH_FASTCALL|METH_KEYWORDS, bytes_分割__doc__},

static PyObject *
bytes_split_impl(PyBytesObject *self, PyObject *sep, Py_ssize_t maxsplit);

static PyObject *
bytes_split(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"sep", "maxsplit", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "split", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = Py_None;
    Py_ssize_t maxsplit = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "integer argument expected, got float" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[1]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        maxsplit = ival;
    }
skip_optional_pos:
    return_value = bytes_split_impl(self, sep, maxsplit);

exit:
    return return_value;
}

static PyObject *
bytes_分割(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"分隔符", "最大分割次数", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "分割", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = Py_None;
    Py_ssize_t maxsplit = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[1]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        maxsplit = ival;
    }
skip_optional_pos:
    return_value = bytes_split_impl(self, sep, maxsplit);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_partition__doc__,
"partition($self, sep, /)\n"
"--\n"
"\n"
"Partition the bytes into three parts using the given separator.\n"
"\n"
"This will search for the separator sep in the bytes. If the separator is found,\n"
"returns a 3-tuple containing the part before the separator, the separator\n"
"itself, and the part after it.\n"
"\n"
"If the separator is not found, returns a 3-tuple containing the original bytes\n"
"object and two empty bytes objects.");

#define BYTES_PARTITION_METHODDEF    \
    {"partition", (PyCFunction)bytes_partition, METH_O, bytes_partition__doc__},

PyDoc_STRVAR(bytes_划分__doc__,
"划分($self, 分隔符, /)\n"
"--\n"
"\n"
"使用给定分隔符将字节划分成三部分.\n"
"\n"
"此函数会在字节中搜索 分隔符. 如果找到,\n"
"则返回一个 3-元组, 包含分隔符之前的部分, 分隔符\n"
"本身和分隔符之后的部分.\n"
"\n"
"如果未找到分隔符, 则返回一个 3-元组, 包含原字节\n"
"对象和两个空字节对象.");

#define BYTES_划分_METHODDEF    \
    {"划分", (PyCFunction)bytes_partition, METH_O, bytes_划分__doc__},

static PyObject *
bytes_partition_impl(PyBytesObject *self, Py_buffer *sep);

static PyObject *
bytes_partition(PyBytesObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    Py_buffer sep = {NULL, NULL};

    if (PyObject_GetBuffer(arg, &sep, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&sep, 'C')) {
        _PyArg_BadArgument("partition", "参数", "连续缓冲区", arg);
        goto exit;
    }
    return_value = bytes_partition_impl(self, &sep);

exit:
    /* Cleanup for sep */
    if (sep.obj) {
       PyBuffer_Release(&sep);
    }

    return return_value;
}

PyDoc_STRVAR(bytes_rpartition__doc__,
"rpartition($self, sep, /)\n"
"--\n"
"\n"
"Partition the bytes into three parts using the given separator.\n"
"\n"
"This will search for the separator sep in the bytes, starting at the end. If\n"
"the separator is found, returns a 3-tuple containing the part before the\n"
"separator, the separator itself, and the part after it.\n"
"\n"
"If the separator is not found, returns a 3-tuple containing two empty bytes\n"
"objects and the original bytes object.");

#define BYTES_RPARTITION_METHODDEF    \
    {"rpartition", (PyCFunction)bytes_rpartition, METH_O, bytes_rpartition__doc__},

PyDoc_STRVAR(bytes_右划分__doc__,
"右划分($self, 分隔符, /)\n"
"--\n"
"\n"
"使用给定分隔符将字节划分成三部分.\n"
"\n"
"此函数会在字节中从末尾开始搜索 分隔符.\n"
"如果找到, 则返回一个 3-元组, 包含分隔符之前的部分, \n"
"分隔符本身和分隔符之后的部分.\n"
"\n"
"如果未找到分隔符, 则返回一个 3-元组, 包含两个空字节\n"
"对象和原字节对象.");

#define BYTES_右划分_METHODDEF    \
    {"右划分", (PyCFunction)bytes_rpartition, METH_O, bytes_右划分__doc__},

static PyObject *
bytes_rpartition_impl(PyBytesObject *self, Py_buffer *sep);

static PyObject *
bytes_rpartition(PyBytesObject *self, PyObject *arg)
{
    PyObject *return_value = NULL;
    Py_buffer sep = {NULL, NULL};

    if (PyObject_GetBuffer(arg, &sep, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&sep, 'C')) {
        _PyArg_BadArgument("rpartition", "参数", "连续缓冲区", arg);
        goto exit;
    }
    return_value = bytes_rpartition_impl(self, &sep);

exit:
    /* Cleanup for sep */
    if (sep.obj) {
       PyBuffer_Release(&sep);
    }

    return return_value;
}

PyDoc_STRVAR(bytes_rsplit__doc__,
"rsplit($self, /, sep=None, maxsplit=-1)\n"
"--\n"
"\n"
"Return a list of the sections in the bytes, using sep as the delimiter.\n"
"\n"
"  sep\n"
"    The delimiter according which to split the bytes.\n"
"    None (the default value) means split on ASCII whitespace characters\n"
"    (space, tab, return, newline, formfeed, vertical tab).\n"
"  maxsplit\n"
"    Maximum number of splits to do.\n"
"    -1 (the default value) means no limit.\n"
"\n"
"Splitting is done starting at the end of the bytes and working to the front.");

#define BYTES_RSPLIT_METHODDEF    \
    {"rsplit", (PyCFunction)(void(*)(void))bytes_rsplit, METH_FASTCALL|METH_KEYWORDS, bytes_rsplit__doc__},

PyDoc_STRVAR(bytes_右分割__doc__,
"右分割($self, /, 分隔符=空, 最大分割次数=-1)\n"
"--\n"
"\n"
"用 分隔符 将字节分割成若干部分, 返回一个列表.\n"
"\n"
"  分隔符\n"
"    据以分割字节的字符.\n"
"    空 (默认值) 意味着依据 ASCII 空白字符\n"
"    (空格, tab, 回车, 换行, 换页, 垂直制表符) 进行分割.\n"
"  最大分割次数\n"
"    要执行的最大分割次数.\n"
"    -1 (默认值) 表示无限制.\n"
"\n"
"分割从字节末尾开始向前执行.");

#define BYTES_右分割_METHODDEF    \
    {"右分割", (PyCFunction)(void(*)(void))bytes_右分割, METH_FASTCALL|METH_KEYWORDS, bytes_右分割__doc__},

static PyObject *
bytes_rsplit_impl(PyBytesObject *self, PyObject *sep, Py_ssize_t maxsplit);

static PyObject *
bytes_rsplit(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"sep", "maxsplit", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "rsplit", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = Py_None;
    Py_ssize_t maxsplit = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "integer argument expected, got float" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[1]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        maxsplit = ival;
    }
skip_optional_pos:
    return_value = bytes_rsplit_impl(self, sep, maxsplit);

exit:
    return return_value;
}

static PyObject *
bytes_右分割(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"分隔符", "最大分割次数", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "右分割", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = Py_None;
    Py_ssize_t maxsplit = -1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[1]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        maxsplit = ival;
    }
skip_optional_pos:
    return_value = bytes_rsplit_impl(self, sep, maxsplit);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_join__doc__,
"join($self, iterable_of_bytes, /)\n"
"--\n"
"\n"
"Concatenate any number of bytes objects.\n"
"\n"
"The bytes whose method is called is inserted in between each pair.\n"
"\n"
"The result is returned as a new bytes object.\n"
"\n"
"Example: b\'.\'.join([b\'ab\', b\'pq\', b\'rs\']) -> b\'ab.pq.rs\'.");

#define BYTES_JOIN_METHODDEF    \
    {"join", (PyCFunction)bytes_join, METH_O, bytes_join__doc__},

PyDoc_STRVAR(bytes_连接__doc__,
"连接($self, 字节可迭代对象, /)\n"
"--\n"
"\n"
"连接任意数量的字节对象.\n"
"\n"
"其方法被调用的字节插在每一对之间.\n"
"\n"
"结果作为新字节对象返回.\n"
"\n"
"例如: b\'.\'.连接([b\'ab\', b\'pq\', b\'rs\']) -> b\'ab.pq.rs\'.");

#define BYTES_连接_METHODDEF    \
    {"连接", (PyCFunction)bytes_join, METH_O, bytes_连接__doc__},

PyDoc_STRVAR(bytes_strip__doc__,
"strip($self, bytes=None, /)\n"
"--\n"
"\n"
"Strip leading and trailing bytes contained in the argument.\n"
"\n"
"If the argument is omitted or None, strip leading and trailing ASCII whitespace.");

#define BYTES_STRIP_METHODDEF    \
    {"strip", (PyCFunction)(void(*)(void))bytes_strip, METH_FASTCALL, bytes_strip__doc__},

PyDoc_STRVAR(bytes_修剪__doc__,
"修剪($self, 字节=空, /)\n"
"--\n"
"\n"
"剪除包含在参数中的前导和尾随字节.\n"
"\n"
"如果参数未给出或为 空, 则剪除前导和尾随 ASCII 空白字符.");

#define BYTES_修剪_METHODDEF    \
    {"修剪", (PyCFunction)(void(*)(void))bytes_strip, METH_FASTCALL, bytes_修剪__doc__},

static PyObject *
bytes_strip_impl(PyBytesObject *self, PyObject *bytes);

static PyObject *
bytes_strip(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *bytes = Py_None;

    if (!_PyArg_CheckPositional("strip", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    bytes = args[0];
skip_optional:
    return_value = bytes_strip_impl(self, bytes);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_lstrip__doc__,
"lstrip($self, bytes=None, /)\n"
"--\n"
"\n"
"Strip leading bytes contained in the argument.\n"
"\n"
"If the argument is omitted or None, strip leading  ASCII whitespace.");

#define BYTES_LSTRIP_METHODDEF    \
    {"lstrip", (PyCFunction)(void(*)(void))bytes_lstrip, METH_FASTCALL, bytes_lstrip__doc__},

PyDoc_STRVAR(bytes_左修剪__doc__,
"左修剪($self, 字节=空, /)\n"
"--\n"
"\n"
"剪除包含在参数中的前导字节.\n"
"\n"
"如果参数未给出或为 空, 则剪除前导 ASCII 空白字符.");

#define BYTES_左修剪_METHODDEF    \
    {"左修剪", (PyCFunction)(void(*)(void))bytes_lstrip, METH_FASTCALL, bytes_左修剪__doc__},

static PyObject *
bytes_lstrip_impl(PyBytesObject *self, PyObject *bytes);

static PyObject *
bytes_lstrip(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *bytes = Py_None;

    if (!_PyArg_CheckPositional("lstrip", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    bytes = args[0];
skip_optional:
    return_value = bytes_lstrip_impl(self, bytes);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_rstrip__doc__,
"rstrip($self, bytes=None, /)\n"
"--\n"
"\n"
"Strip trailing bytes contained in the argument.\n"
"\n"
"If the argument is omitted or None, strip trailing ASCII whitespace.");

#define BYTES_RSTRIP_METHODDEF    \
    {"rstrip", (PyCFunction)(void(*)(void))bytes_rstrip, METH_FASTCALL, bytes_rstrip__doc__},

PyDoc_STRVAR(bytes_右修剪__doc__,
"右修剪($self, 字节=空, /)\n"
"--\n"
"\n"
"剪除包含在参数中的尾随字节.\n"
"\n"
"如果参数未给出或为 空, 则剪除尾随 ASCII 空白字符.");

#define BYTES_右修剪_METHODDEF    \
    {"右修剪", (PyCFunction)(void(*)(void))bytes_rstrip, METH_FASTCALL, bytes_右修剪__doc__},

static PyObject *
bytes_rstrip_impl(PyBytesObject *self, PyObject *bytes);

static PyObject *
bytes_rstrip(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    PyObject *bytes = Py_None;

    if (!_PyArg_CheckPositional("rstrip", nargs, 0, 1)) {
        goto exit;
    }
    if (nargs < 1) {
        goto skip_optional;
    }
    bytes = args[0];
skip_optional:
    return_value = bytes_rstrip_impl(self, bytes);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_translate__doc__,
"translate($self, table, /, delete=b\'\')\n"
"--\n"
"\n"
"Return a copy with each character mapped by the given translation table.\n"
"\n"
"  table\n"
"    Translation table, which must be a bytes object of length 256.\n"
"\n"
"All characters occurring in the optional argument delete are removed.\n"
"The remaining characters are mapped through the given translation table.");

#define BYTES_TRANSLATE_METHODDEF    \
    {"translate", (PyCFunction)(void(*)(void))bytes_translate, METH_FASTCALL|METH_KEYWORDS, bytes_translate__doc__},

PyDoc_STRVAR(bytes_转换__doc__,
"转换($self, 表, /, 删除=b\'\')\n"
"--\n"
"\n"
"返回一个副本, 每个字符通过给定的转换表进行映射.\n"
"\n"
"  表\n"
"    转换表, 必须是长度为 256 的字节对象.\n"
"\n"
"所有出现在可选参数 删除 中的字符都会被移除.\n"
"其余字符通过给定的转换表进行映射.");

#define BYTES_转换_METHODDEF    \
    {"转换", (PyCFunction)(void(*)(void))bytes_转换, METH_FASTCALL|METH_KEYWORDS, bytes_转换__doc__},

static PyObject *
bytes_translate_impl(PyBytesObject *self, PyObject *table,
                     PyObject *deletechars);

static PyObject *
bytes_translate(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "delete", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "translate", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *table;
    PyObject *deletechars = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    table = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    deletechars = args[1];
skip_optional_pos:
    return_value = bytes_translate_impl(self, table, deletechars);

exit:
    return return_value;
}

static PyObject *
bytes_转换(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"", "删除", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "转换", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *table;
    PyObject *deletechars = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    table = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    deletechars = args[1];
skip_optional_pos:
    return_value = bytes_translate_impl(self, table, deletechars);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_maketrans__doc__,
"maketrans(frm, to, /)\n"
"--\n"
"\n"
"Return a translation table useable for the bytes or bytearray translate method.\n"
"\n"
"The returned table will be one where each byte in frm is mapped to the byte at\n"
"the same position in to.\n"
"\n"
"The bytes objects frm and to must be of the same length.");

#define BYTES_MAKETRANS_METHODDEF    \
    {"maketrans", (PyCFunction)(void(*)(void))bytes_maketrans, METH_FASTCALL|METH_STATIC, bytes_maketrans__doc__},

PyDoc_STRVAR(bytes_制转换表__doc__,
"制转换表(来源, 目标, /)\n"
"--\n"
"\n"
"返回一个可用于字节或字节数组 转换 方法的转换表.\n"
"\n"
"在返回的转换表中, 来源 中的每个字节映射到\n"
"目标 中相同位置的字节.\n"
"\n"
"字节对象 来源 和 目标 的长度须相同.");

#define BYTES_制转换表_METHODDEF    \
    {"制转换表", (PyCFunction)(void(*)(void))bytes_maketrans, METH_FASTCALL|METH_STATIC, bytes_制转换表__doc__},

static PyObject *
bytes_maketrans_impl(Py_buffer *frm, Py_buffer *to);

static PyObject *
bytes_maketrans(void *null, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_buffer frm = {NULL, NULL};
    Py_buffer to = {NULL, NULL};

    if (!_PyArg_CheckPositional("maketrans", nargs, 2, 2)) {
        goto exit;
    }
    if (PyObject_GetBuffer(args[0], &frm, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&frm, 'C')) {
        _PyArg_BadArgument("maketrans", "参数 1", "连续缓冲区", args[0]);
        goto exit;
    }
    if (PyObject_GetBuffer(args[1], &to, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&to, 'C')) {
        _PyArg_BadArgument("maketrans", "参数 2", "连续缓冲区", args[1]);
        goto exit;
    }
    return_value = bytes_maketrans_impl(&frm, &to);

exit:
    /* Cleanup for frm */
    if (frm.obj) {
       PyBuffer_Release(&frm);
    }
    /* Cleanup for to */
    if (to.obj) {
       PyBuffer_Release(&to);
    }

    return return_value;
}

PyDoc_STRVAR(bytes_replace__doc__,
"replace($self, old, new, count=-1, /)\n"
"--\n"
"\n"
"Return a copy with all occurrences of substring old replaced by new.\n"
"\n"
"  count\n"
"    Maximum number of occurrences to replace.\n"
"    -1 (the default value) means replace all occurrences.\n"
"\n"
"If the optional argument count is given, only the first count occurrences are\n"
"replaced.");

#define BYTES_REPLACE_METHODDEF    \
    {"replace", (PyCFunction)(void(*)(void))bytes_replace, METH_FASTCALL, bytes_replace__doc__},

PyDoc_STRVAR(bytes_替换__doc__,
"替换($self, 旧, 新, 次数=-1, /)\n"
"--\n"
"\n"
"返回一个副本, 所有出现的旧子串都被替换为新子串.\n"
"\n"
"  次数\n"
"    要替换的最大出现次数.\n"
"    -1 (默认值) 表示替换所有出现的旧子串.\n"
"\n"
"如果给出了可选参数 次数=n, 则仅替换前 n 次.");

#define BYTES_替换_METHODDEF    \
    {"替换", (PyCFunction)(void(*)(void))bytes_replace, METH_FASTCALL, bytes_替换__doc__},

static PyObject *
bytes_replace_impl(PyBytesObject *self, Py_buffer *old, Py_buffer *new,
                   Py_ssize_t count);

static PyObject *
bytes_replace(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs)
{
    PyObject *return_value = NULL;
    Py_buffer old = {NULL, NULL};
    Py_buffer new = {NULL, NULL};
    Py_ssize_t count = -1;

    if (!_PyArg_CheckPositional("replace", nargs, 2, 3)) {
        goto exit;
    }
    if (PyObject_GetBuffer(args[0], &old, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&old, 'C')) {
        _PyArg_BadArgument("replace", "参数 1", "连续缓冲区", args[0]);
        goto exit;
    }
    if (PyObject_GetBuffer(args[1], &new, PyBUF_SIMPLE) != 0) {
        goto exit;
    }
    if (!PyBuffer_IsContiguous(&new, 'C')) {
        _PyArg_BadArgument("replace", "参数 2", "连续缓冲区", args[1]);
        goto exit;
    }
    if (nargs < 3) {
        goto skip_optional;
    }
    if (PyFloat_Check(args[2])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    {
        Py_ssize_t ival = -1;
        PyObject *iobj = PyNumber_Index(args[2]);
        if (iobj != NULL) {
            ival = PyLong_AsSsize_t(iobj);
            Py_DECREF(iobj);
        }
        if (ival == -1 && PyErr_Occurred()) {
            goto exit;
        }
        count = ival;
    }
skip_optional:
    return_value = bytes_replace_impl(self, &old, &new, count);

exit:
    /* Cleanup for old */
    if (old.obj) {
       PyBuffer_Release(&old);
    }
    /* Cleanup for new */
    if (new.obj) {
       PyBuffer_Release(&new);
    }

    return return_value;
}

PyDoc_STRVAR(bytes_decode__doc__,
"decode($self, /, encoding=\'utf-8\', errors=\'strict\')\n"
"--\n"
"\n"
"Decode the bytes using the codec registered for encoding.\n"
"\n"
"  encoding\n"
"    The encoding with which to decode the bytes.\n"
"  errors\n"
"    The error handling scheme to use for the handling of decoding errors.\n"
"    The default is \'strict\' meaning that decoding errors raise a\n"
"    UnicodeDecodeError. Other possible values are \'ignore\' and \'replace\'\n"
"    as well as any other name registered with codecs.register_error that\n"
"    can handle UnicodeDecodeErrors.");

#define BYTES_DECODE_METHODDEF    \
    {"decode", (PyCFunction)(void(*)(void))bytes_decode, METH_FASTCALL|METH_KEYWORDS, bytes_decode__doc__},

PyDoc_STRVAR(bytes_解码__doc__,
"解码($self, /, 编码=\'utf-8\', 错误=\'严格\')\n"
"--\n"
"\n"
"使用已注册用于编码的编解码器解码字节.\n"
"\n"
"  编码\n"
"    用以解码字节的编码方式.\n"
"  错误\n"
"    用以处理解码错误的错误处理方案.\n"
"    默认为 \'严格\', 表示解码错误会报\n"
"    统一码解码错误类 错误. 其他可选值有 \'忽略\' 和 \'替换\'\n"
"    以及任何其他通过 codecs.register_error 注册并\n"
"    能处理统一码解码错误的名称.");

#define BYTES_解码_METHODDEF    \
    {"解码", (PyCFunction)(void(*)(void))bytes_解码, METH_FASTCALL|METH_KEYWORDS, bytes_解码__doc__},

static PyObject *
bytes_decode_impl(PyBytesObject *self, const char *encoding,
                  const char *errors);

static PyObject *
bytes_decode(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"encoding", "errors", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "decode", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    const char *encoding = NULL;
    const char *errors = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        if (!PyUnicode_Check(args[0])) {
            _PyArg_BadArgument("decode", "argument 'encoding'", "str", args[0]);
            goto exit;
        }
        Py_ssize_t encoding_length;
        encoding = PyUnicode_AsUTF8AndSize(args[0], &encoding_length);
        if (encoding == NULL) {
            goto exit;
        }
        if (strlen(encoding) != (size_t)encoding_length) {
            PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("decode", "argument 'errors'", "str", args[1]);
        goto exit;
    }
    Py_ssize_t errors_length;
    errors = PyUnicode_AsUTF8AndSize(args[1], &errors_length);
    if (errors == NULL) {
        goto exit;
    }
    if (strlen(errors) != (size_t)errors_length) {
        PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_decode_impl(self, encoding, errors);

exit:
    return return_value;
}

static PyObject *
bytes_解码(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"编码", "错误", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "解码", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    const char *encoding = NULL;
    const char *errors = NULL;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        if (!PyUnicode_Check(args[0])) {
            _PyArg_BadArgument("解码", "参数 '编码'", "字符串", args[0]);
            goto exit;
        }
        Py_ssize_t encoding_length;
        encoding = PyUnicode_AsUTF8AndSize(args[0], &encoding_length);
        if (encoding == NULL) {
            goto exit;
        }
        if (strlen(encoding) != (size_t)encoding_length) {
            PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
            goto exit;
        }
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (!PyUnicode_Check(args[1])) {
        _PyArg_BadArgument("解码", "参数 '错误'", "字符串", args[1]);
        goto exit;
    }
    Py_ssize_t errors_length;
    errors = PyUnicode_AsUTF8AndSize(args[1], &errors_length);
    if (errors == NULL) {
        goto exit;
    }
    if (strlen(errors) != (size_t)errors_length) {
        PyErr_SetString(PyExc_ValueError, "嵌入了空字符");
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_decode_impl(self, encoding, errors);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_splitlines__doc__,
"splitlines($self, /, keepends=False)\n"
"--\n"
"\n"
"Return a list of the lines in the bytes, breaking at line boundaries.\n"
"\n"
"Line breaks are not included in the resulting list unless keepends is given and\n"
"true.");

#define BYTES_SPLITLINES_METHODDEF    \
    {"splitlines", (PyCFunction)(void(*)(void))bytes_splitlines, METH_FASTCALL|METH_KEYWORDS, bytes_splitlines__doc__},

PyDoc_STRVAR(bytes_分行__doc__,
"分行($self, /, 保留换行符=假)\n"
"--\n"
"\n"
"返回字节中的行的列表, 在行边界处断开.\n"
"\n"
"换行符不包括在结果列表中, 除非给出参数 保留换行符=真.");

#define BYTES_分行_METHODDEF    \
    {"分行", (PyCFunction)(void(*)(void))bytes_分行, METH_FASTCALL|METH_KEYWORDS, bytes_分行__doc__},

static PyObject *
bytes_splitlines_impl(PyBytesObject *self, int keepends);

static PyObject *
bytes_splitlines(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"keepends", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "splitlines", 0};
    PyObject *argsbuf[1];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    int keepends = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "integer argument expected, got float" );
        goto exit;
    }
    keepends = _PyLong_AsInt(args[0]);
    if (keepends == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_splitlines_impl(self, keepends);

exit:
    return return_value;
}

static PyObject *
bytes_分行(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"保留换行符", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "分行", 0};
    PyObject *argsbuf[1];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    int keepends = 0;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (PyFloat_Check(args[0])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    keepends = _PyLong_AsInt(args[0]);
    if (keepends == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_splitlines_impl(self, keepends);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_fromhex__doc__,
"fromhex($type, string, /)\n"
"--\n"
"\n"
"Create a bytes object from a string of hexadecimal numbers.\n"
"\n"
"Spaces between two numbers are accepted.\n"
"Example: bytes.fromhex(\'B9 01EF\') -> b\'\\\\xb9\\\\x01\\\\xef\'.");

#define BYTES_FROMHEX_METHODDEF    \
    {"fromhex", (PyCFunction)bytes_fromhex, METH_O|METH_CLASS, bytes_fromhex__doc__},

PyDoc_STRVAR(bytes_从十六__doc__,
"从十六($type, 字符串, /)\n"
"--\n"
"\n"
"从十六进制数字符串创建一个字节对象.\n"
"\n"
"两个数之间可以有空格.\n"
"例如：字节.从十六(\'B9 01EF\') -> b\'\\\\xb9\\\\x01\\\\xef\'.");

#define BYTES_从十六_METHODDEF    \
    {"从十六", (PyCFunction)bytes_fromhex, METH_O|METH_CLASS, bytes_从十六__doc__},

static PyObject *
bytes_fromhex_impl(PyTypeObject *type, PyObject *string);

static PyObject *
bytes_fromhex(PyTypeObject *type, PyObject *arg)
{
    PyObject *return_value = NULL;
    PyObject *string;

    if (!PyUnicode_Check(arg)) {
        _PyArg_BadArgument("从十六", "参数", "字符串", arg);
        goto exit;
    }
    if (PyUnicode_READY(arg) == -1) {
        goto exit;
    }
    string = arg;
    return_value = bytes_fromhex_impl(type, string);

exit:
    return return_value;
}

PyDoc_STRVAR(bytes_hex__doc__,
"hex($self, /, sep=<unrepresentable>, bytes_per_sep=1)\n"
"--\n"
"\n"
"Create a str of hexadecimal numbers from a bytes object.\n"
"\n"
"  sep\n"
"    An optional single character or byte to separate hex bytes.\n"
"  bytes_per_sep\n"
"    How many bytes between separators.  Positive values count from the\n"
"    right, negative values count from the left.\n"
"\n"
"Example:\n"
">>> value = b\'\\xb9\\x01\\xef\'\n"
">>> value.hex()\n"
"\'b901ef\'\n"
">>> value.hex(\':\')\n"
"\'b9:01:ef\'\n"
">>> value.hex(\':\', 2)\n"
"\'b9:01ef\'\n"
">>> value.hex(\':\', -2)\n"
"\'b901:ef\'");

#define BYTES_HEX_METHODDEF    \
    {"hex", (PyCFunction)(void(*)(void))bytes_hex, METH_FASTCALL|METH_KEYWORDS, bytes_hex__doc__},

PyDoc_STRVAR(bytes_十六进制__doc__,
"十六进制($self, /, 分隔符=<无法表示>, 分隔字节数=1)\n"
"--\n"
"\n"
"返回一个字符串，字节实例的每个字节以两个十六进制数字表示.\n"
"\n"
"  分隔符\n"
"    可选的单个字符或字节, 用以分隔十六进制字节.\n"
"  分隔字节数\n"
"    两个分隔符之间有多少字节.  正值从右计数, \n"
"    负值从左计数.\n"
"\n"
"示例：\n"
">>> 值 = b\'\\xb9\\x01\\xef\'\n"
">>> 值.十六进制()\n"
"\'b901ef\'\n"
">>> 值.十六进制(\':\')\n"
"\'b9:01:ef\'\n"
">>> 值.十六进制(\':\', 2)\n"
"\'b9:01ef\'\n"
">>> 值.十六进制(\':\', -2)\n"
"\'b901:ef\'");

#define BYTES_十六进制_METHODDEF    \
    {"十六进制", (PyCFunction)(void(*)(void))bytes_十六进制, METH_FASTCALL|METH_KEYWORDS, bytes_十六进制__doc__},

static PyObject *
bytes_hex_impl(PyBytesObject *self, PyObject *sep, int bytes_per_sep);

static PyObject *
bytes_hex(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"sep", "bytes_per_sep", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "hex", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = NULL;
    int bytes_per_sep = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    bytes_per_sep = _PyLong_AsInt(args[1]);
    if (bytes_per_sep == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_hex_impl(self, sep, bytes_per_sep);

exit:
    return return_value;
}

static PyObject *
bytes_十六进制(PyBytesObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"分隔符", "分隔字节数", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "十六进制", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = NULL;
    int bytes_per_sep = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    bytes_per_sep = _PyLong_AsInt(args[1]);
    if (bytes_per_sep == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = bytes_hex_impl(self, sep, bytes_per_sep);

exit:
    return return_value;
}
/*[clinic end generated code: output=ca60dfccf8d51e88 input=a9049054013a1b77]*/
