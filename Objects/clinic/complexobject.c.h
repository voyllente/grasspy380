/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(complex_new__doc__,
"复数(实部=0, 虚部=0)\n"
"--\n"
"\n"
"用一个实部和一个可选的虚部创建一个复数.\n"
"\n"
"这相当于 (实部 + 虚部*1j), 其中 虚部 默认为 0.");

static PyObject *
complex_new_impl(PyTypeObject *type, PyObject *r, PyObject *i);

static PyObject *
complex_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"real", "imag", "实部", "虚部", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "complex", 0};
    PyObject *argsbuf[4];
    PyObject * const *fastargs;
    Py_ssize_t nargs = PyTuple_GET_SIZE(args);
    Py_ssize_t noptargs = nargs + (kwargs ? PyDict_GET_SIZE(kwargs) : 0) - 0;
    PyObject *r = _PyLong_Zero;
    PyObject *i = NULL;

    fastargs = _PyArg_UnpackKeywords(_PyTuple_CAST(args)->ob_item, nargs, kwargs, NULL, &_parser, 0, 2, 0, argsbuf);
    if (!fastargs) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    /* if (fastargs[0]) {
        r = fastargs[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    i = fastargs[1]; */
    if (fastargs[0]) {
        r = fastargs[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (fastargs[1]) {
        i = fastargs[1];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (fastargs[2]) {
        r = fastargs[2];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    i = fastargs[3];
    
skip_optional_pos:
    return_value = complex_new_impl(type, r, i);

exit:
    return return_value;
}
/*[clinic end generated code: output=a0fe23fdbdc9b06b input=a9049054013a1b77]*/
