/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(memoryview_hex__doc__,
"十六进制($self, /, 分隔符=<无法表示>, 分隔字节数=1)\n"
"--\n"
"\n"
"返回一个字符串，缓冲区中的每个字节以两个十六进制数字表示.\n"
"\n"
"  分隔符\n"
"    可选的单个字符或字节, 用以分隔十六进制字节.\n"
"  分隔字节数\n"
"    两个分隔符之间有多少字节.  正值从右计数, \n"
"    负值从左计数.\n"
"\n"
"示例:\n"
">>> 值 = 内存视图(b\'\\xb9\\x01\\xef\')\n"
">>> 值.十六进制()\n"
"\'b901ef\'\n"
">>> 值.十六进制(\':\')\n"
"\'b9:01:ef\'\n"
">>> 值.十六进制(\':\', 2)\n"
"\'b9:01ef\'\n"
">>> 值.十六进制(\':\', -2)\n"
"\'b901:ef\'");

#define MEMORYVIEW_HEX_METHODDEF    \
    {"hex", (PyCFunction)(void(*)(void))memoryview_hex, METH_FASTCALL|METH_KEYWORDS, memoryview_hex__doc__},

#define MEMORYVIEW_十六进制_METHODDEF    \
    {"十六进制", (PyCFunction)(void(*)(void))memoryview_hex, METH_FASTCALL|METH_KEYWORDS, memoryview_hex__doc__},

static PyObject *
memoryview_hex_impl(PyMemoryViewObject *self, PyObject *sep,
                    int bytes_per_sep);

static PyObject *
memoryview_hex(PyMemoryViewObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"sep", "bytes_per_sep", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "hex", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    PyObject *sep = NULL;
    int bytes_per_sep = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    if (args[0]) {
        sep = args[0];
        if (!--noptargs) {
            goto skip_optional_pos;
        }
    }
    if (PyFloat_Check(args[1])) {
        PyErr_SetString(PyExc_TypeError,
                        "期望整数参数，得到了浮点数" );
        goto exit;
    }
    bytes_per_sep = _PyLong_AsInt(args[1]);
    if (bytes_per_sep == -1 && PyErr_Occurred()) {
        goto exit;
    }
skip_optional_pos:
    return_value = memoryview_hex_impl(self, sep, bytes_per_sep);

exit:
    return return_value;
}
/*[clinic end generated code: output=ee265a73f68b0077 input=a9049054013a1b77]*/
