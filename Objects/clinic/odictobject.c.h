/*[clinic input]
preserve
[clinic start generated code]*/

PyDoc_STRVAR(OrderedDict_fromkeys__doc__,
"从键创建($type, /, 可迭代对象, 值=空)\n"
"--\n"
"\n"
"创建一个新有序字典, 键来自可迭代对象, 值设置为指定值.");

#define ORDEREDDICT_FROMKEYS_METHODDEF    \
    {"fromkeys", (PyCFunction)(void(*)(void))OrderedDict_fromkeys, METH_FASTCALL|METH_KEYWORDS|METH_CLASS, OrderedDict_fromkeys__doc__},

#define ORDEREDDICT_从键创建_METHODDEF    \
    {"从键创建", (PyCFunction)(void(*)(void))OrderedDict_从键创建, METH_FASTCALL|METH_KEYWORDS|METH_CLASS, OrderedDict_fromkeys__doc__},

static PyObject *
OrderedDict_fromkeys_impl(PyTypeObject *type, PyObject *seq, PyObject *value);

static PyObject *
OrderedDict_fromkeys(PyTypeObject *type, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"iterable", "value", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "fromkeys", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *seq;
    PyObject *value = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    seq = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    value = args[1];
skip_optional_pos:
    return_value = OrderedDict_fromkeys_impl(type, seq, value);

exit:
    return return_value;
}

static PyObject *
OrderedDict_从键创建(PyTypeObject *type, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"可迭代对象", "值", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "从键创建", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *seq;
    PyObject *value = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    seq = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    value = args[1];
skip_optional_pos:
    return_value = OrderedDict_fromkeys_impl(type, seq, value);

exit:
    return return_value;
}

PyDoc_STRVAR(OrderedDict_setdefault__doc__,
"设默认值($self, /, 键, 默认值=空)\n"
"--\n"
"\n"
"若字典中无指定键, 则插入指定键和默认值.\n"
"\n"
"若指定键在字典中, 则返回其值, 否则返回默认值.");

#define ORDEREDDICT_SETDEFAULT_METHODDEF    \
    {"setdefault", (PyCFunction)(void(*)(void))OrderedDict_setdefault, METH_FASTCALL|METH_KEYWORDS, OrderedDict_setdefault__doc__},

#define ORDEREDDICT_设默认值_METHODDEF    \
    {"设默认值", (PyCFunction)(void(*)(void))OrderedDict_设默认值, METH_FASTCALL|METH_KEYWORDS, OrderedDict_setdefault__doc__},

static PyObject *
OrderedDict_setdefault_impl(PyODictObject *self, PyObject *key,
                            PyObject *default_value);

static PyObject *
OrderedDict_setdefault(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"key", "default", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "setdefault", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *key;
    PyObject *default_value = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    key = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    default_value = args[1];
skip_optional_pos:
    return_value = OrderedDict_setdefault_impl(self, key, default_value);

exit:
    return return_value;
}

static PyObject *
OrderedDict_设默认值(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"键", "默认值", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "设默认值", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *key;
    PyObject *default_value = Py_None;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    key = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    default_value = args[1];
skip_optional_pos:
    return_value = OrderedDict_setdefault_impl(self, key, default_value);

exit:
    return return_value;
}

PyDoc_STRVAR(OrderedDict_popitem__doc__,
"弹出项($self, /, 最后=真)\n"
"--\n"
"\n"
"从字典中删除并返回一个 (键, 值) 对.\n"
"\n"
"若 最后 为 真, 键值对以后进先出 (LIFO) 顺序返回, 否则以先进先出 (FIFO) 顺序返回.");

#define ORDEREDDICT_POPITEM_METHODDEF    \
    {"popitem", (PyCFunction)(void(*)(void))OrderedDict_popitem, METH_FASTCALL|METH_KEYWORDS, OrderedDict_popitem__doc__},

#define ORDEREDDICT_弹出项_METHODDEF    \
    {"弹出项", (PyCFunction)(void(*)(void))OrderedDict_弹出项, METH_FASTCALL|METH_KEYWORDS, OrderedDict_popitem__doc__},

static PyObject *
OrderedDict_popitem_impl(PyODictObject *self, int last);

static PyObject *
OrderedDict_popitem(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"last", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "popitem", 0};
    PyObject *argsbuf[1];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    int last = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    last = PyObject_IsTrue(args[0]);
    if (last < 0) {
        goto exit;
    }
skip_optional_pos:
    return_value = OrderedDict_popitem_impl(self, last);

exit:
    return return_value;
}

static PyObject *
OrderedDict_弹出项(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"最后", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "弹出项", 0};
    PyObject *argsbuf[1];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 0;
    int last = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 0, 1, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    if (!noptargs) {
        goto skip_optional_pos;
    }
    last = PyObject_IsTrue(args[0]);
    if (last < 0) {
        goto exit;
    }
skip_optional_pos:
    return_value = OrderedDict_popitem_impl(self, last);

exit:
    return return_value;
}

PyDoc_STRVAR(OrderedDict_move_to_end__doc__,
"移至端($self, /, 键, 最后=真)\n"
"--\n"
"\n"
"将现有元素移至末尾 (或开头, 如果 最后 为 假).\n"
"\n"
"若元素不存在, 则报 键错误类 错误.");

#define ORDEREDDICT_MOVE_TO_END_METHODDEF    \
    {"move_to_end", (PyCFunction)(void(*)(void))OrderedDict_move_to_end, METH_FASTCALL|METH_KEYWORDS, OrderedDict_move_to_end__doc__},

#define ORDEREDDICT_移至端_METHODDEF    \
    {"移至端", (PyCFunction)(void(*)(void))OrderedDict_移至端, METH_FASTCALL|METH_KEYWORDS, OrderedDict_move_to_end__doc__},

static PyObject *
OrderedDict_move_to_end_impl(PyODictObject *self, PyObject *key, int last);

static PyObject *
OrderedDict_move_to_end(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"key", "last", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "move_to_end", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *key;
    int last = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    key = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    last = PyObject_IsTrue(args[1]);
    if (last < 0) {
        goto exit;
    }
skip_optional_pos:
    return_value = OrderedDict_move_to_end_impl(self, key, last);

exit:
    return return_value;
}

static PyObject *
OrderedDict_移至端(PyODictObject *self, PyObject *const *args, Py_ssize_t nargs, PyObject *kwnames)
{
    PyObject *return_value = NULL;
    static const char * const _keywords[] = {"键", "最后", NULL};
    static _PyArg_Parser _parser = {NULL, _keywords, "移至端", 0};
    PyObject *argsbuf[2];
    Py_ssize_t noptargs = nargs + (kwnames ? PyTuple_GET_SIZE(kwnames) : 0) - 1;
    PyObject *key;
    int last = 1;

    args = _PyArg_UnpackKeywords(args, nargs, NULL, kwnames, &_parser, 1, 2, 0, argsbuf);
    if (!args) {
        goto exit;
    }
    key = args[0];
    if (!noptargs) {
        goto skip_optional_pos;
    }
    last = PyObject_IsTrue(args[1]);
    if (last < 0) {
        goto exit;
    }
skip_optional_pos:
    return_value = OrderedDict_move_to_end_impl(self, key, last);

exit:
    return return_value;
}
/*[clinic end generated code: output=8eb1296df9142908 input=a9049054013a1b77]*/
