"""众多方便的扩展部件. 自 3.6 起已弃用."""

# 汉化时没看到弃用声明, 浪费了不少时间 - 2020-7-6
# Tix.py -- Tix widget wrappers.
#
#       For Tix, see http://tix.sourceforge.net
#
#       - Sudhir Shenoy (sshenoy@gol.com), Dec. 1995.
#         based on an idea of Jean-Marc Lugrin (lugrin@ms.com)
#
# NOTE: In order to minimize changes to Tkinter.py, some of the code here
#       (TixWidget.__init__) has been taken from Tkinter (Widget.__init__)
#       and will break if there are major changes in Tkinter.
#
# The Tix widgets are represented by a class hierarchy in python with proper
# inheritance of base classes.
#
# As a result after creating a 'w = StdButtonBox', I can write
#              w.ok['text'] = 'Who Cares'
#    or              w.ok['bg'] = w['bg']
# or even       w.ok.invoke()
# etc.
#
# Compare the demo tixwidgets.py to the original Tcl program and you will
# appreciate the advantages.
#

import os
# import tkinter
# from tkinter import *
# from tkinter import _cnfmerge
导入 图快
从 图快 导入 *
从 图快 导入 _cnfmerge

import _tkinter # If this fails your Python may not be configured for Tk

# Some more constants (for consistency with Tkinter)
WINDOW = 'window'
TEXT = 'text'
STATUS = 'status'
IMMEDIATE = 'immediate'
IMAGE = 'image'
IMAGETEXT = 'imagetext'
BALLOON = 'balloon'
AUTO = 'auto'
ACROSSTOP = 'acrosstop'

# A few useful constants for the Grid widget
ASCII = 'ascii'
CELL = 'cell'
COLUMN = 'column'
DECREASING = 'decreasing'
INCREASING = 'increasing'
INTEGER = 'integer'
MAIN = 'main'
MAX = 'max'
REAL = 'real'
ROW = 'row'
S_REGION = 's-region'
X_REGION = 'x-region'
Y_REGION = 'y-region'

# Some constants used by Tkinter dooneevent()
TCL_DONT_WAIT     = 1 << 1
TCL_WINDOW_EVENTS = 1 << 2
TCL_FILE_EVENTS   = 1 << 3
TCL_TIMER_EVENTS  = 1 << 4
TCL_IDLE_EVENTS   = 1 << 5
TCL_ALL_EVENTS    = 0

# BEWARE - this is implemented by copying some code from the Widget class
#          in Tkinter (to override Widget initialization) and is therefore
#          liable to break.

# Could probably add this to Tkinter.Misc
class tixCommand:
    """The tix commands provide access to miscellaneous  elements
    of  Tix's  internal state and the Tix application context.
    Most of the information manipulated by these  commands pertains
    to  the  application  as a whole, or to a screen or
    display, rather than to a particular window.

    This is a mixin class, assumed to be mixed to Tkinter.Tk
    that supports the self.tk.call method.
    """

    def tix_addbitmapdir(self, directory):
        """Tix maintains a list of directories under which
        the  tix_getimage  and tix_getbitmap commands will
        search for image files. The standard bitmap  directory
        is $TIX_LIBRARY/bitmaps. The addbitmapdir command
        adds directory into this list. By  using  this
        command, the  image  files  of an applications can
        also be located using the tix_getimage or tix_getbitmap
        command.
        """
        return self.tk.call('tix', 'addbitmapdir', directory)

    函 扩展_添加位图dir(自身, 目录):
        """扩展部件 (Tix) 维护一个目录列表, '扩展_获取图像' 和 '扩展_获取位图'
        在该目录下搜索图像文件. 标准位图目录为 $TIX_LIBRARY/bitmaps. '添加位图dir'
        命令将给定目录添加到此列表中. 通过使用此命令, 应用程序的图像文件也可以使用
        '扩展_获取图像' 和 '扩展_获取位图' 命令来定位.
        """
        返回 自身.tk.call('tix', 'addbitmapdir', 目录)

    def tix_cget(self, option):
        """Returns  the  current  value  of the configuration
        option given by option. Option may be  any  of  the
        options described in the CONFIGURATION OPTIONS section.
        """
        return self.tk.call('tix', 'cget', option)

    函 扩展_获取配置(自身, 选项):
        """返回给定配置选项的当前值. 选项参见 '配置选项' 部分所述.
        """
        返回 自身.tk.call('tix', 'cget', 选项)

    def tix_configure(self, cnf=None, **kw):
        """Query or modify the configuration options of the Tix application
        context. If no option is specified, returns a dictionary all of the
        available options.  If option is specified with no value, then the
        command returns a list describing the one named option (this list
        will be identical to the corresponding sublist of the value
        returned if no option is specified).  If one or more option-value
        pairs are specified, then the command modifies the given option(s)
        to have the given value(s); in this case the command returns an
        empty string. Option may be any of the configuration options.
        """
        # Copied from Tkinter.py
        if kw:
            cnf = _cnfmerge((cnf, kw))
        elif cnf:
            cnf = _cnfmerge(cnf)
        if cnf is None:
            return self._getconfigure('tix', 'configure')
        if isinstance(cnf, str):
            return self._getconfigure1('tix', 'configure', '-'+cnf)
        return self.tk.call(('tix', 'configure') + self._options(cnf))

    函 扩展_配置(自身, 配置字典=None, **关键词参数):
        """查询或修改应用程序上下文的配置选项. 如果未指定选项, 则返回所有可用选项.
        If option is specified with no value, then the
        command returns a list describing the one named option (this list
        will be identical to the corresponding sublist of the value
        returned if no option is specified).  If one or more option-value
        pairs are specified, then the command modifies the given option(s)
        to have the given value(s); in this case the command returns an
        empty string. Option may be any of the configuration options.
        """
        返回 自身.tix_configure(配置字典, 关键词参数)

    def tix_filedialog(self, dlgclass=None):
        """Returns the file selection dialog that may be shared among
        different calls from this application.  This command will create a
        file selection dialog widget when it is called the first time. This
        dialog will be returned by all subsequent calls to tix_filedialog.
        An optional dlgclass parameter can be passed to specified what type
        of file selection dialog widget is desired. Possible options are
        tix FileSelectDialog or tixExFileSelectDialog.
        """
        if dlgclass is not None:
            return self.tk.call('tix', 'filedialog', dlgclass)
        else:
            return self.tk.call('tix', 'filedialog')

    函 扩展_文件对话框(自身, 对话框类=None):
        """返回文件选项对话框, 它可以由应用程序的不同调用共享.
        首次调用时, 此命令会创建一个文件选择对话框部件.
        This dialog will be returned by all subsequent calls to tix_filedialog.
        An optional dlgclass parameter can be passed to specified what type
        of file selection dialog widget is desired. Possible options are
        tix FileSelectDialog or tixExFileSelectDialog.
        """
        返回 自身.tix_filedialog(对话框类)

    def tix_getbitmap(self, name):
        """Locates a bitmap file of the name name.xpm or name in one of the
        bitmap directories (see the tix_addbitmapdir command above).  By
        using tix_getbitmap, you can avoid hard coding the pathnames of the
        bitmap files in your application. When successful, it returns the
        complete pathname of the bitmap file, prefixed with the character
        '@'.  The returned value can be used to configure the -bitmap
        option of the TK and Tix widgets.
        """
        return self.tk.call('tix', 'getbitmap', name)

    函 扩展_获取位图(自身, 名称):
        """在位图目录之一中定位名为 '名称.xpm' 或 '名称' 的位图文件.
        By using tix_getbitmap, you can avoid hard coding the pathnames of the
        bitmap files in your application. When successful, it returns the
        complete pathname of the bitmap file, prefixed with the character
        '@'.  The returned value can be used to configure the -bitmap
        option of the TK and Tix widgets.
        """
        返回 self.tk.call('tix', 'getbitmap', 名称)

    def tix_getimage(self, name):
        """Locates an image file of the name name.xpm, name.xbm or name.ppm
        in one of the bitmap directories (see the addbitmapdir command
        above). If more than one file with the same name (but different
        extensions) exist, then the image type is chosen according to the
        depth of the X display: xbm images are chosen on monochrome
        displays and color images are chosen on color displays. By using
        tix_ getimage, you can avoid hard coding the pathnames of the
        image files in your application. When successful, this command
        returns the name of the newly created image, which can be used to
        configure the -image option of the Tk and Tix widgets.
        """
        return self.tk.call('tix', 'getimage', name)

    函 扩展_获取图像(自身, 名称):
        """在位图目录之一中定位名为 '名称.xpm' / '名称.xbm' / '名称.ppm'
        的图像文件. If more than one file with the same name (but different
        extensions) exist, then the image type is chosen according to the
        depth of the X display: xbm images are chosen on monochrome
        displays and color images are chosen on color displays. By using
        tix_ getimage, you can avoid hard coding the pathnames of the
        image files in your application. When successful, this command
        returns the name of the newly created image, which can be used to
        configure the -image option of the Tk and Tix widgets.
        """
        返回 自身.tk.call('tix', 'getimage', 名称)

    def tix_option_get(self, name):
        """Gets  the options  maintained  by  the  Tix
        scheme mechanism. Available options include:

            active_bg       active_fg      bg
            bold_font       dark1_bg       dark1_fg
            dark2_bg        dark2_fg       disabled_fg
            fg              fixed_font     font
            inactive_bg     inactive_fg    input1_bg
            input2_bg       italic_font    light1_bg
            light1_fg       light2_bg      light2_fg
            menu_font       output1_bg     output2_bg
            select_bg       select_fg      selector
            """
        # could use self.tk.globalgetvar('tixOption', name)
        return self.tk.call('tix', 'option', 'get', name)

    函 扩展_获取选项(自身, 名称):
        """获取 Tix 机制维护的选项. 可用选项包括:

            active_bg       active_fg      bg
            bold_font       dark1_bg       dark1_fg
            dark2_bg        dark2_fg       disabled_fg
            fg              fixed_font     font
            inactive_bg     inactive_fg    input1_bg
            input2_bg       italic_font    light1_bg
            light1_fg       light2_bg      light2_fg
            menu_font       output1_bg     output2_bg
            select_bg       select_fg      selector
            """
        # could use self.tk.globalgetvar('tixOption', name)
        返回 自身.tk.call('tix', 'option', 'get', 名称)

    def tix_resetoptions(self, newScheme, newFontSet, newScmPrio=None):
        """Resets the scheme and fontset of the Tix application to
        newScheme and newFontSet, respectively.  This affects only those
        widgets created after this call. Therefore, it is best to call the
        resetoptions command before the creation of any widgets in a Tix
        application.

        The optional parameter newScmPrio can be given to reset the
        priority level of the Tk options set by the Tix schemes.

        Because of the way Tk handles the X option database, after Tix has
        been has imported and inited, it is not possible to reset the color
        schemes and font sets using the tix config command.  Instead, the
        tix_resetoptions command must be used.
        """
        if newScmPrio is not None:
            return self.tk.call('tix', 'resetoptions', newScheme, newFontSet, newScmPrio)
        else:
            return self.tk.call('tix', 'resetoptions', newScheme, newFontSet)

    函 扩展_重置选项(自身, 新方案, 新字体集, 新方案优先级=None):
        """将 Tix 应用程序的方案和字体集分别重置为 '新方案' 和 '新字体集'.
        仅影响此调用之后创建的部件. Therefore, it is best to call the
        resetoptions command before the creation of any widgets in a Tix
        application.

        The optional parameter newScmPrio can be given to reset the
        priority level of the Tk options set by the Tix schemes.

        Because of the way Tk handles the X option database, after Tix has
        been has imported and inited, it is not possible to reset the color
        schemes and font sets using the tix config command.  Instead, the
        tix_resetoptions command must be used.
        """
        返回 自身.tix_resetoptions(新方案, 新字体集, 新方案优先级)

class Tk(图快.Tk, tixCommand):
    """Toplevel widget of Tix which represents mostly the main window
    of an application. It has an associated Tcl interpreter."""
    def __init__(self, screenName=None, baseName=None, className='Tix'):
        图快.Tk.__init__(self, screenName, baseName, className)
        tixlib = os.environ.get('TIX_LIBRARY')
        self.tk.eval('global auto_path; lappend auto_path [file dir [info nameof]]')
        if tixlib is not None:
            self.tk.eval('global auto_path; lappend auto_path {%s}' % tixlib)
            self.tk.eval('global tcl_pkgPath; lappend tcl_pkgPath {%s}' % tixlib)
        # Load Tix - this should work dynamically or statically
        # If it's static, tcl/tix8.1/pkgIndex.tcl should have
        #               'load {} Tix'
        # If it's dynamic under Unix, tcl/tix8.1/pkgIndex.tcl should have
        #               'load libtix8.1.8.3.so Tix'
        self.tk.eval('package require Tix')

    def destroy(self):
        # For safety, remove the delete_window binding before destroy
        self.protocol("WM_DELETE_WINDOW", "")
        图快.Tk.destroy(self)

    销毁 = destroy

类 主窗口类(Tk):
    """Tix 的顶级窗口部件, 大部分情况下代表应用程序的主窗口."""

    函 __init__(自身, 屏幕名称=None, 基本名称=None, 类名='Tix'):
        Tk.__init__(自身, screenName=屏幕名称, baseName=基本名称, className=类名)

# The Tix 'tixForm' geometry manager
class Form:
    """Tix 表单几何管理器

    可以指定部件之间的依附关系, 以此排列部件. 详见 Tix 文档"""

    def config(self, cnf={}, **kw):
        self.tk.call('tixForm', self._w, *self._options(cnf, kw))

    函 配置(自身, 配置字典={}, **关键词参数):
        自身.tk.call('tixForm', 自身._w, *自身._options(配置字典, 关键词参数))

    form = config
    表单 = 配置

    def __setitem__(self, key, value):
        Form.form(self, {key: value})

    def check(self):
        return self.tk.call('tixForm', 'check', self._w)

    检查 = check

    def forget(self):
        self.tk.call('tixForm', 'forget', self._w)

    忽略 = forget

    def grid(self, xsize=0, ysize=0):
        if (not xsize) and (not ysize):
            x = self.tk.call('tixForm', 'grid', self._w)
            y = self.tk.splitlist(x)
            z = ()
            for x in y:
                z = z + (self.tk.getint(x),)
            return z
        return self.tk.call('tixForm', 'grid', self._w, xsize, ysize)

    函 网格(自身, x大小=0, y大小=0):
        返回 自身.grid(xsize=x大小, ysize=y大小)

    def info(self, option=None):
        if not option:
            return self.tk.call('tixForm', 'info', self._w)
        if option[0] != '-':
            option = '-' + option
        return self.tk.call('tixForm', 'info', self._w, option)

    函 信息(自身, 选项=空):
        返回 自身.info(选项)

    def slaves(self):
        return [self._nametowidget(x) for x in
                self.tk.splitlist(
                       self.tk.call(
                       'tixForm', 'slaves', self._w))]

    从属部件 = slaves

表单类 = Form


图快.Widget.__bases__ = 图快.Widget.__bases__ + (Form,)

class TixWidget(图快.Widget):
    """A TixWidget class is used to package all (or most) Tix widgets.

    Widget initialization is extended in two ways:
       1) It is possible to give a list of options which must be part of
       the creation command (so called Tix 'static' options). These cannot be
       given as a 'config' command later.
       2) It is possible to give the name of an existing TK widget. These are
       child widgets created automatically by a Tix mega-widget. The Tk call
       to create these widgets is therefore bypassed in TixWidget.__init__

    Both options are for use by subclasses only.
    """
    def __init__ (self, master=None, widgetName=None,
                static_options=None, cnf={}, kw={}):
        # Merge keywords and dictionary arguments
        if kw:
            cnf = _cnfmerge((cnf, kw))
        else:
            cnf = _cnfmerge(cnf)

        # Move static options into extra. static_options must be
        # a list of keywords (or None).
        extra=()

        # 'options' is always a static option
        if static_options:
            static_options.append('options')
        else:
            static_options = ['options']

        for k,v in list(cnf.items()):
            if k in static_options:
                extra = extra + ('-' + k, v)
                del cnf[k]

        self.widgetName = widgetName
        Widget._setup(self, master, cnf)

        # If widgetName is None, this is a dummy creation call where the
        # corresponding Tk widget has already been created by Tix
        if widgetName:
            self.tk.call(widgetName, self._w, *extra)

        # Non-static options - to be done via a 'config' command
        if cnf:
            Widget.config(self, cnf)

        # Dictionary to hold subwidget names for easier access. We can't
        # use the children list because the public Tix names may not be the
        # same as the pathname component
        self.subwidget_list = {}

    # We set up an attribute access function so that it is possible to
    # do w.ok['text'] = 'Hello' rather than w.subwidget('ok')['text'] = 'Hello'
    # when w is a StdButtonBox.
    # We can even do w.ok.invoke() because w.ok is subclassed from the
    # Button class if you go through the proper constructors
    def __getattr__(self, name):
        if name in self.subwidget_list:
            return self.subwidget_list[name]
        raise AttributeError(name)

    def set_silent(self, value):
        """Set a variable without calling its action routine"""
        self.tk.call('tixSetSilent', self._w, value)

    函 设置静默(自身, 值):
        """设置一个变量而不调用其操作例程"""
        自身.tk.call('tixSetSilent', 自身._w, 值)

    def subwidget(self, name):
        """Return the named subwidget (which must have been created by
        the sub-class)."""
        n = self._subwidget_name(name)
        if not n:
            raise TclError("Subwidget " + name + " not child of " + self._name)
        # Remove header of name and leading dot
        n = n[len(self._w)+1:]
        return self._nametowidget(n)

    函 子部件(自身, 名称):
        """返回指定名称的子部件 (其必须已经由该子类创建)."""
        返回 自身.subwidget(名称)

    def subwidgets_all(self):
        """返回所有子部件."""
        names = self._subwidget_names()
        if not names:
            return []
        retlist = []
        for name in names:
            name = name[len(self._w)+1:]
            try:
                retlist.append(self._nametowidget(name))
            except:
                # some of the widgets are unknown e.g. border in LabelFrame
                pass
        return retlist

    所有子部件 = subwidgets_all

    def _subwidget_name(self,name):
        """Get a subwidget name (returns a String, not a Widget !)"""
        try:
            return self.tk.call(self._w, 'subwidget', name)
        except TclError:
            return None

    def _subwidget_names(self):
        """Return the name of all subwidgets."""
        try:
            x = self.tk.call(self._w, 'subwidgets', '-all')
            return self.tk.splitlist(x)
        except TclError:
            return None

    def config_all(self, option, value):
        """Set configuration options for all subwidgets (and self)."""
        if option == '':
            return
        elif not isinstance(option, str):
            option = repr(option)
        if not isinstance(value, str):
            value = repr(value)
        names = self._subwidget_names()
        for name in names:
            self.tk.call(name, 'configure', '-' + option, value)

    函 配置全部(自身, 选项, 值):
        """设置所有子部件 (及自身) 的配置选项."""
        自身.config_all(选项, 值)

    # These are missing from Tkinter
    def image_create(self, imgtype, cnf={}, master=None, **kw):
        if not master:
            master = 图快._default_root
            if not master:
                raise RuntimeError('Too early to create image')
        if kw and cnf: cnf = _cnfmerge((cnf, kw))
        elif kw: cnf = kw
        options = ()
        for k, v in cnf.items():
            if callable(v):
                v = self._register(v)
            options = options + ('-'+k, v)
        return master.tk.call(('image', 'create', imgtype,) + options)

    函 图像_创建(自身, 图像类型, 配置字典={}, 主对象=None, **关键词参数):
        返回 自身.image_create(图像类型, cnf=配置字典, master=主对象, **关键词参数)

    def image_delete(self, imgname):
        try:
            self.tk.call('image', 'delete', imgname)
        except TclError:
            # May happen if the root was destroyed
            pass

    函 图像_删除(自身, 图像名称):
        自身.image_delete(图像名称)

类 扩展部件类(TixWidget):
    """扩展部件类用于封装所有 (或大部分) 扩展部件."""

    函 __init__(自身, 主对象=None, 部件名称=None, 静态选项=None, 
                    配置字典={}, 关键词参数={}):
        TixWidget.__init__(自身, master=主对象, widgetName=部件名称,
            static_options=静态选项, cnf=配置字典, kw=关键词参数)


# Subwidgets are child widgets created automatically by mega-widgets.
# In python, we have to create these subwidgets manually to mirror their
# existence in Tk/Tix.
class TixSubWidget(TixWidget):
    """Subwidget class.

    This is used to mirror child widgets automatically created
    by Tix/Tk as part of a mega-widget in Python (which is not informed
    of this)"""

    def __init__(self, master, name,
               destroy_physically=1, check_intermediate=1):
        if check_intermediate:
            path = master._subwidget_name(name)
            try:
                path = path[len(master._w)+1:]
                plist = path.split('.')
            except:
                plist = []

        if not check_intermediate:
            # immediate descendant
            TixWidget.__init__(self, master, None, None, {'name' : name})
        else:
            # Ensure that the intermediate widgets exist
            parent = master
            for i in range(len(plist) - 1):
                n = '.'.join(plist[:i+1])
                try:
                    w = master._nametowidget(n)
                    parent = w
                except KeyError:
                    # Create the intermediate widget
                    parent = TixSubWidget(parent, plist[i],
                                          destroy_physically=0,
                                          check_intermediate=0)
            # The Tk widget name is in plist, not in name
            if plist:
                name = plist[-1]
            TixWidget.__init__(self, parent, None, None, {'name' : name})
        self.destroy_physically = destroy_physically

    def destroy(self):
        # For some widgets e.g., a NoteBook, when we call destructors,
        # we must be careful not to destroy the frame widget since this
        # also destroys the parent NoteBook thus leading to an exception
        # in Tkinter when it finally calls Tcl to destroy the NoteBook
        for c in list(self.children.values()): c.destroy()
        if self._name in self.master.children:
            del self.master.children[self._name]
        if self._name in self.master.subwidget_list:
            del self.master.subwidget_list[self._name]
        if self.destroy_physically:
            # This is bypassed only for a few widgets
            self.tk.call('destroy', self._w)

    销毁 = destroy

类 扩展子部件类(TixSubWidget):
    """子部件类"""

    函 __init__(自身, 主对象, 名称, 物理销毁=1, 检查直系=1):
        TixSubWidget.__init__(自身, 主对象, 名称, destroy_physically=物理销毁,
                    check_intermediate=检查直系)


# Useful class to create a display style - later shared by many items.
# Contributed by Steffen Kremser
class DisplayStyle:
    """DisplayStyle - handle configuration options shared by
    (multiple) Display Items"""

    def __init__(self, itemtype, cnf={}, *, master=None, **kw):
        if not master:
            if 'refwindow' in kw:
                master = kw['refwindow']
            elif 'refwindow' in cnf:
                master = cnf['refwindow']
            else:
                master = 图快._default_root
                if not master:
                    raise RuntimeError("Too early to create display style: "
                                       "no root window")
        self.tk = master.tk
        self.stylename = self.tk.call('tixDisplayStyle', itemtype,
                            *self._options(cnf,kw) )

    def __str__(self):
        return self.stylename

    def _options(self, cnf, kw):
        if kw and cnf:
            cnf = _cnfmerge((cnf, kw))
        elif kw:
            cnf = kw
        opts = ()
        for k, v in cnf.items():
            opts = opts + ('-'+k, v)
        return opts

    def delete(self):
        self.tk.call(self.stylename, 'delete')

    删除 = delete

    def __setitem__(self,key,value):
        self.tk.call(self.stylename, 'configure', '-%s'%key, value)

    def config(self, cnf={}, **kw):
        return self._getconfigure(
            self.stylename, 'configure', *self._options(cnf,kw))

    函 配置(自身, 配置字典={}, **关键词参数):
        return 自身._getconfigure(
            自身.stylename, 'configure', *自身._options(配置字典, 关键词参数))

    def __getitem__(self,key):
        return self.tk.call(self.stylename, 'cget', '-%s'%key)

类 显示样式类(DisplayStyle):
    """处理多个显示项目共享的配置选项"""

    函 __init__(自身, 项目类型, 配置字典={}, *, 主对象=None, **关键词参数):
        DisplayStyle.__init__(自身, 项目类型, cnf=配置字典, master=主对象, **关键词参数)


######################################################
### The Tix Widget classes - in alphabetical order ###
######################################################

class Balloon(TixWidget):
    """Balloon help widget.

    Subwidget       Class
    ---------       -----
    label           Label
    message         Message"""

    # FIXME: It should inherit -superclass tixShell
    def __init__(self, master=None, cnf={}, **kw):
        # static seem to be -installcolormap -initwait -statusbar -cursor
        static = ['options', 'installcolormap', 'initwait', 'statusbar',
                  'cursor']
        TixWidget.__init__(self, master, 'tixBalloon', static, cnf, kw)
        self.subwidget_list['label'] = _dummyLabel(self, 'label',
                                                   destroy_physically=0)
        self.subwidget_list['message'] = _dummyLabel(self, 'message',
                                                     destroy_physically=0)
        self.标签 = self.label
        self.消息 = self.message

    def bind_widget(self, widget, cnf={}, **kw):
        """Bind balloon widget to another.
        One balloon widget may be bound to several widgets at the same time"""
        self.tk.call(self._w, 'bind', widget._w, *self._options(cnf, kw))

    函 绑定部件(自身, 部件, 配置字典={}, **关键词参数):
        """将气球部件绑定到另一部件. 一个气球部件可以同时绑定多个部件."""
        自身.tk.call(自身._w, 'bind', 部件._w, *自身._options(配置字典, 关键词参数))

    def unbind_widget(self, widget):
        self.tk.call(self._w, 'unbind', widget._w)

    函 解除绑定部件(自身, 部件):
        自身.tk.call(自身._w, 'unbind', 部件._w)

类 气球类(Balloon):
    """气球状帮助部件.

    子部件         类
    ---------    -----
    标签         标签类\n
    消息         消息类"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        Balloon.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class ButtonBox(TixWidget):
    """ButtonBox - A container for pushbuttons.
    Subwidgets are the buttons added with the add method.
    """
    def __init__(self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixButtonBox',
                           ['orientation', 'options'], cnf, kw)

    def add(self, name, cnf={}, **kw):
        """Add a button with given name to box."""

        btn = self.tk.call(self._w, 'add', name, *self._options(cnf, kw))
        self.subwidget_list[name] = _dummyButton(self, name)
        return btn

    函 添加(自身, 名称, 配置字典={}, **关键词参数):
        """将指定名称的按钮添加到框中."""
        返回 自身.add(名称, cnf=配置字典, **关键词参数)

    def invoke(self, name):
        if name in self.subwidget_list:
            self.tk.call(self._w, 'invoke', name)

    函 调用(自身, 名称):
        自身.invoke(名称)

类 按钮框类(ButtonBox):
    """按钮容器. 子部件是利用 '添加' 方法添加的按钮."""
    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        ButtonBox.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class ComboBox(TixWidget):
    """ComboBox - an Entry field with a dropdown menu. The user can select a
    choice by either typing in the entry subwidget or selecting from the
    listbox subwidget.

    Subwidget       Class
    ---------       -----
    entry       Entry
    arrow       Button
    slistbox    ScrolledListBox
    tick        Button
    cross       Button : present if created with the fancy option"""

    # FIXME: It should inherit -superclass tixLabelWidget
    def __init__ (self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixComboBox',
                           ['editable', 'dropdown', 'fancy', 'options'],
                           cnf, kw)
        self.subwidget_list['label'] = _dummyLabel(self, 'label')
        self.subwidget_list['entry'] = _dummyEntry(self, 'entry')
        self.subwidget_list['arrow'] = _dummyButton(self, 'arrow')
        self.subwidget_list['slistbox'] = _dummyScrolledListBox(self,
                                                                'slistbox')
        self.滚动列表框 = self.slistbox
        self.滚动列表框.列表框 = self.slistbox.listbox
        try:
            self.subwidget_list['tick'] = _dummyButton(self, 'tick')
            self.subwidget_list['cross'] = _dummyButton(self, 'cross')
        except TypeError:
            # unavailable when -fancy not specified
            pass

    # align

    def add_history(self, str):
        self.tk.call(self._w, 'addhistory', str)

    函 添加记录(自身, 字符串):
        自身.tk.call(自身._w, 'addhistory', 字符串)

    def append_history(self, str):
        self.tk.call(self._w, 'appendhistory', str)

    函 追加记录(自身, 字符串):
        自身.tk.call(自身._w, 'appendhistory', 字符串)

    def insert(self, index, str):
        self.tk.call(self._w, 'insert', index, str)

    函 插入(自身, 索引, 字符串):
        自身.tk.call(自身._w, 'insert', 索引, 字符串)

    def pick(self, index):
        self.tk.call(self._w, 'pick', index)

    函 选取(自身, 索引):
        自身.tk.call(自身._w, 'pick', 索引)

类 组合框类(ComboBox):
    """带下拉列表的输入框. 

    子部件         类
    ---------     -----
    输入框        输入框类\n
    箭头          按钮类\n
    滚动列表框    滚动列表框类\n
    tick         按钮类\n
    cross        按钮类 : 利用花哨选项创建时存在"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        ComboBox.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class Control(TixWidget):
    """Control - An entry field with value change arrows.  The user can
    adjust the value by pressing the two arrow buttons or by entering
    the value directly into the entry. The new value will be checked
    against the user-defined upper and lower limits.

    Subwidget       Class
    ---------       -----
    incr       Button
    decr       Button
    entry       Entry
    label       Label"""

    # FIXME: It should inherit -superclass tixLabelWidget
    def __init__ (self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixControl', ['options'], cnf, kw)
        self.subwidget_list['incr'] = _dummyButton(self, 'incr')
        self.subwidget_list['decr'] = _dummyButton(self, 'decr')
        self.subwidget_list['label'] = _dummyLabel(self, 'label')
        self.subwidget_list['entry'] = _dummyEntry(self, 'entry')

    def decrement(self):
        self.tk.call(self._w, 'decr')

    递减 = decrement

    def increment(self):
        self.tk.call(self._w, 'incr')

    递增 = increment

    def invoke(self):
        self.tk.call(self._w, 'invoke')

    调用 = invoke

    def update(self):
        self.tk.call(self._w, 'update')

    更新 = update

类 控制类(Control):
    """带改变值的箭头的输入框. 用户可以指定上限和下限.

    子部件         类
    ---------     -----
    递增箭头       按钮类\n
    递减箭头       按钮类\n
    输入框         输入框类\n
    标签           标签类"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        Control.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class DirList(TixWidget):
    """DirList - displays a list view of a directory, its previous
    directories and its sub-directories. The user can choose one of
    the directories displayed in the list or change to another directory.

    Subwidget       Class
    ---------       -----
    hlist       HList
    hsb              Scrollbar
    vsb              Scrollbar"""

    # FIXME: It should inherit -superclass tixScrolledHList
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixDirList', ['options'], cnf, kw)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

    def chdir(self, dir):
        self.tk.call(self._w, 'chdir', dir)

    函 更换目录(自身, 目录):
        自身.tk.call(自身._w, 'chdir', 目录)

类 目录列表类(DirList):
    """显示目录、其之前的目录和子目录的列表视图。

    子部件           类
    ---------       -----
    hlist           HList\n
    水平滚动条       滚动条类\n
    垂直滚动条       滚动条类"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        DirList.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class DirTree(TixWidget):
    """DirTree - Directory Listing in a hierarchical view.
    Displays a tree view of a directory, its previous directories and its
    sub-directories. The user can choose one of the directories displayed
    in the list or change to another directory.

    Subwidget       Class
    ---------       -----
    hlist           HList
    hsb             Scrollbar
    vsb             Scrollbar"""

    # FIXME: It should inherit -superclass tixScrolledHList
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixDirTree', ['options'], cnf, kw)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

    def chdir(self, dir):
        self.tk.call(self._w, 'chdir', dir)

    函 更换目录(自身, 目录):
        自身.tk.call(自身._w, 'chdir', 目录)

类 目录树类(DirTree):
    """层次化目录结构. 显示目录、其之前的目录和子目录的树视图。

    子部件           类
    ---------       -----
    hlist           HList\n
    水平滚动条       滚动条类\n
    垂直滚动条       滚动条类"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        DirTree.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class DirSelectBox(TixWidget):
    """DirSelectBox - Motif style file select box.
    It is generally used for
    the user to choose a file. FileSelectBox stores the files mostly
    recently selected into a ComboBox widget so that they can be quickly
    selected again.

    Subwidget       Class
    ---------       -----
    selection       ComboBox
    filter          ComboBox
    dirlist         ScrolledListBox
    filelist        ScrolledListBox"""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixDirSelectBox', ['options'], cnf, kw)
        self.subwidget_list['dirlist'] = _dummyDirList(self, 'dirlist')
        self.subwidget_list['dircbx'] = _dummyFileComboBox(self, 'dircbx')

类 目录选择框类(DirSelectBox):
    """Motif 式文件选择框, 一般供用户选择文件. 最近选择的文件会被存储在组合框部件中,
    方便再次快速选择.

    子部件           类
    ---------       -----
    选择            组合框类
    筛选器          组合框类
    目录列表        滚动列表框类
    文件列表        滚动列表框类"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        DirSelectBox.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ExFileSelectBox(TixWidget):
    """ExFileSelectBox - MS Windows style file select box.
    It provides a convenient method for the user to select files.

    Subwidget       Class
    ---------       -----
    cancel       Button
    ok              Button
    hidden       Checkbutton
    types       ComboBox
    dir              ComboBox
    file       ComboBox
    dirlist       ScrolledListBox
    filelist       ScrolledListBox"""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixExFileSelectBox', ['options'], cnf, kw)
        self.subwidget_list['cancel'] = _dummyButton(self, 'cancel')
        self.subwidget_list['ok'] = _dummyButton(self, 'ok')
        self.subwidget_list['hidden'] = _dummyCheckbutton(self, 'hidden')
        self.subwidget_list['types'] = _dummyComboBox(self, 'types')
        self.subwidget_list['dir'] = _dummyComboBox(self, 'dir')
        self.subwidget_list['dirlist'] = _dummyDirList(self, 'dirlist')
        self.subwidget_list['file'] = _dummyComboBox(self, 'file')
        self.subwidget_list['filelist'] = _dummyScrolledListBox(self, 'filelist')

    def filter(self):
        self.tk.call(self._w, 'filter')

    筛选 = filter

    def invoke(self):
        self.tk.call(self._w, 'invoke')

    调用 = invoke

类 增强文件选择框类(ExFileSelectBox):
    """MS Windows 样式的文件选择框.

    子部件       类
    ---------       -----
    cancel       Button
    ok              Button
    hidden       Checkbutton
    types       ComboBox
    dir              ComboBox
    file       ComboBox
    dirlist       ScrolledListBox
    filelist       ScrolledListBox"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ExFileSelectBox.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


# Should inherit from a Dialog class
class DirSelectDialog(TixWidget):
    """The DirSelectDialog widget presents the directories in the file
    system in a dialog window. The user can use this dialog window to
    navigate through the file system to select the desired directory.

    Subwidgets       Class
    ----------       -----
    dirbox       DirSelectDialog"""

    # FIXME: It should inherit -superclass tixDialogShell
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixDirSelectDialog',
                           ['options'], cnf, kw)
        self.subwidget_list['dirbox'] = _dummyDirSelectBox(self, 'dirbox')
        # cancel and ok buttons are missing

    def popup(self):
        self.tk.call(self._w, 'popup')

    上弹出 = popup

    def popdown(self):
        self.tk.call(self._w, 'popdown')

    下弹出 = popdown

类 目录选择对话框类(DirSelectDialog):
    """将文件系统中的目录以对话框呈现.

    子部件       类
    ---------       -----
    dirbox       DirSelectDialog"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        DirSelectDialog.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


# Should inherit from a Dialog class
class ExFileSelectDialog(TixWidget):
    """ExFileSelectDialog - MS Windows style file select dialog.
    It provides a convenient method for the user to select files.

    Subwidgets       Class
    ----------       -----
    fsbox       ExFileSelectBox"""

    # FIXME: It should inherit -superclass tixDialogShell
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixExFileSelectDialog',
                           ['options'], cnf, kw)
        self.subwidget_list['fsbox'] = _dummyExFileSelectBox(self, 'fsbox')

    def popup(self):
        self.tk.call(self._w, 'popup')

    上弹出 = popup

    def popdown(self):
        self.tk.call(self._w, 'popdown')

    下弹出 = popdown

类 增强文件选择对话框类(ExFileSelectDialog):
    """MS Windows 样式的文件选择对话框.

    子部件       类
    ---------       -----
    fsbox       ExFileSelectBox"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ExFileSelectDialog.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class FileSelectBox(TixWidget):
    """ExFileSelectBox - Motif style file select box.
    It is generally used for
    the user to choose a file. FileSelectBox stores the files mostly
    recently selected into a ComboBox widget so that they can be quickly
    selected again.

    Subwidget       Class
    ---------       -----
    selection       ComboBox
    filter          ComboBox
    dirlist         ScrolledListBox
    filelist        ScrolledListBox"""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixFileSelectBox', ['options'], cnf, kw)
        self.subwidget_list['dirlist'] = _dummyScrolledListBox(self, 'dirlist')
        self.subwidget_list['filelist'] = _dummyScrolledListBox(self, 'filelist')
        self.subwidget_list['filter'] = _dummyComboBox(self, 'filter')
        self.subwidget_list['selection'] = _dummyComboBox(self, 'selection')

    def apply_filter(self):              # name of subwidget is same as command
        self.tk.call(self._w, 'filter')

    应用筛选 = apply_filter

    def invoke(self):
        self.tk.call(self._w, 'invoke')

    调用 = invoke

类 文件选择框类(FileSelectBox):
    """Motif 样式的文件选择框.

    子部件       类
    ---------       -----
    selection       ComboBox
    filter          ComboBox
    dirlist         ScrolledListBox
    filelist        ScrolledListBox"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        FileSelectBox.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


# Should inherit from a Dialog class
class FileSelectDialog(TixWidget):
    """FileSelectDialog - Motif style file select dialog.

    Subwidgets       Class
    ----------       -----
    btns       StdButtonBox
    fsbox       FileSelectBox"""

    # FIXME: It should inherit -superclass tixStdDialogShell
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixFileSelectDialog',
                           ['options'], cnf, kw)
        self.subwidget_list['btns'] = _dummyStdButtonBox(self, 'btns')
        self.subwidget_list['fsbox'] = _dummyFileSelectBox(self, 'fsbox')

    def popup(self):
        self.tk.call(self._w, 'popup')

    上弹出 = popup

    def popdown(self):
        self.tk.call(self._w, 'popdown')

    下弹出 = popdown

类 文件选择对话框类(FileSelectDialog):
    """Motif 样式的文件选择对话框.

    子部件       类
    ---------       -----
    btns       StdButtonBox
    fsbox       FileSelectBox"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        FileSelectDialog.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class FileEntry(TixWidget):
    """FileEntry - Entry field with button that invokes a FileSelectDialog.
    The user can type in the filename manually. Alternatively, the user can
    press the button widget that sits next to the entry, which will bring
    up a file selection dialog.

    Subwidgets       Class
    ----------       -----
    button       Button
    entry       Entry"""

    # FIXME: It should inherit -superclass tixLabelWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixFileEntry',
                           ['dialogtype', 'options'], cnf, kw)
        self.subwidget_list['button'] = _dummyButton(self, 'button')
        self.subwidget_list['entry'] = _dummyEntry(self, 'entry')

    def invoke(self):
        self.tk.call(self._w, 'invoke')

    调用 = invoke

    def file_dialog(self):
        # FIXME: return python object
        pass

    文件对话框 = file_dialog

类 文件输入框类(FileEntry):
    """带按钮的输入框, 按下按钮会弹出文件选择对话框.

    子部件       类
    ---------       -----
    button       Button
    entry       Entry"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        FileEntry.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class HList(TixWidget, XView, YView):
    """HList - Hierarchy display  widget can be used to display any data
    that have a hierarchical structure, for example, file system directory
    trees. The list entries are indented and connected by branch lines
    according to their places in the hierarchy.

    Subwidgets - None"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixHList',
                           ['columns', 'options'], cnf, kw)

    def add(self, entry, cnf={}, **kw):
        return self.tk.call(self._w, 'add', entry, *self._options(cnf, kw))

    函 添加(self, 条目, 配置字典={}, **关键词参数):
        return self.tk.call(self._w, 'add', 条目, *self._options(配置字典, 关键词参数))

    def add_child(self, parent=None, cnf={}, **kw):
        if not parent:
            parent = ''
        return self.tk.call(
                     self._w, 'addchild', parent, *self._options(cnf, kw))

    函 添加子级(self, 父级=None, 配置字典={}, **关键词参数):
        if not 父级:
            父级 = ''
        return self.tk.call(
                     self._w, 'addchild', 父级, *self._options(配置字典, 关键词参数))

    def anchor_set(self, entry):
        self.tk.call(self._w, 'anchor', 'set', entry)

    函 设置锚点(self, 条目):
        self.tk.call(self._w, 'anchor', 'set', 条目)
        
    def anchor_clear(self):
        self.tk.call(self._w, 'anchor', 'clear')

    清除锚点 = anchor_clear

    def column_width(self, col=0, width=None, chars=None):
        if not chars:
            return self.tk.call(self._w, 'column', 'width', col, width)
        else:
            return self.tk.call(self._w, 'column', 'width', col,
                                '-char', chars)

    函 列宽(自身, 列=0, 宽=None, 字符串=None):
        返回 自身.column_width(col=列, width=宽, chars=字符串)

    def delete_all(self):
        self.tk.call(self._w, 'delete', 'all')

    删除全部 = delete_all

    def delete_entry(self, entry):
        self.tk.call(self._w, 'delete', 'entry', entry)

    函 删除条目(self, 条目):
        self.tk.call(self._w, 'delete', 'entry', 条目)

    def delete_offsprings(self, entry):
        self.tk.call(self._w, 'delete', 'offsprings', entry)

    函 删除后代条目(self, 条目):
        self.tk.call(self._w, 'delete', 'offsprings', 条目)

    def delete_siblings(self, entry):
        self.tk.call(self._w, 'delete', 'siblings', entry)

    函 删除兄弟条目(self, 条目):
        self.tk.call(self._w, 'delete', 'siblings', 条目)

    def dragsite_set(self, index):
        self.tk.call(self._w, 'dragsite', 'set', index)

    函 设置拖动点(self, 索引):
        self.tk.call(self._w, 'dragsite', 'set', 索引)

    def dragsite_clear(self):
        self.tk.call(self._w, 'dragsite', 'clear')

    清除拖动点 = dragsite_clear

    def dropsite_set(self, index):
        self.tk.call(self._w, 'dropsite', 'set', index)

    函 设置放下点(self, 索引):
        self.tk.call(self._w, 'dropsite', 'set', 索引)

    def dropsite_clear(self):
        self.tk.call(self._w, 'dropsite', 'clear')

    清除放下点 = dropsite_clear

    def header_create(self, col, cnf={}, **kw):
        self.tk.call(self._w, 'header', 'create', col, *self._options(cnf, kw))

    函 头部_创建(self, 列, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'header', 'create', 列, *self._options(配置字典, 关键词参数))

    def header_configure(self, col, cnf={}, **kw):
        if cnf is None:
            return self._getconfigure(self._w, 'header', 'configure', col)
        self.tk.call(self._w, 'header', 'configure', col,
                     *self._options(cnf, kw))

    函 头部_配置(self, 列, 配置字典={}, **关键词参数):
        if cnf is None:
            return self._getconfigure(self._w, 'header', 'configure', 列)
        self.tk.call(self._w, 'header', 'configure', 列,
                     *self._options(配置字典, 关键词参数))

    def header_cget(self,  col, opt):
        return self.tk.call(self._w, 'header', 'cget', col, opt)

    函 头部_获取配置(self, 列, 选项):
        return self.tk.call(self._w, 'header', 'cget', 列, 选项)

    def header_exists(self,  col):
        # A workaround to Tix library bug (issue #25464).
        # The documented command is "exists", but only erroneous "exist" is
        # accepted.
        return self.tk.getboolean(self.tk.call(self._w, 'header', 'exist', col))
    header_exist = header_exists

    def 头部_存在(self, 列):
        return self.tk.getboolean(self.tk.call(self._w, 'header', 'exist', 列))

    def header_delete(self, col):
        self.tk.call(self._w, 'header', 'delete', col)

    函 头部_删除(self, 列):
        self.tk.call(self._w, 'header', 'delete', 列)

    def header_size(self, col):
        return self.tk.call(self._w, 'header', 'size', col)

    函 头部_大小(self, 列):
        return self.tk.call(self._w, 'header', 'size', 列)

    def hide_entry(self, entry):
        self.tk.call(self._w, 'hide', 'entry', entry)

    函 隐藏条目(self, 条目):
        self.tk.call(self._w, 'hide', 'entry', 条目)

    def indicator_create(self, entry, cnf={}, **kw):
        self.tk.call(
              self._w, 'indicator', 'create', entry, *self._options(cnf, kw))

    函 指示符_创建(self, 条目, 配置字典={}, **关键词参数):
        self.tk.call(
              self._w, 'indicator', 'create', 条目, *self._options(配置字典, 关键词参数))

    def indicator_configure(self, entry, cnf={}, **kw):
        if cnf is None:
            return self._getconfigure(
                self._w, 'indicator', 'configure', entry)
        self.tk.call(
              self._w, 'indicator', 'configure', entry, *self._options(cnf, kw))

    函 指示符_配置(self, 条目, 配置字典={}, **关键词参数):
        if 配置字典 is None:
            return self._getconfigure(
                self._w, 'indicator', 'configure', 条目)
        self.tk.call(
              self._w, 'indicator', 'configure', 条目, *self._options(配置字典, 关键词参数))

    def indicator_cget(self,  entry, opt):
        return self.tk.call(self._w, 'indicator', 'cget', entry, opt)

    函 指示符_获取配置(self, 条目, 选项):
        return self.tk.call(self._w, 'indicator', 'cget', 条目, 选项)

    def indicator_exists(self,  entry):
        return self.tk.call (self._w, 'indicator', 'exists', entry)

    函 指示符_存在(self, 条目):
        return self.tk.call (self._w, 'indicator', 'exists', 条目)

    def indicator_delete(self, entry):
        self.tk.call(self._w, 'indicator', 'delete', entry)

    函 指示符_删除(self, 条目):
        self.tk.call(self._w, 'indicator', 'delete', 条目)

    def indicator_size(self, entry):
        return self.tk.call(self._w, 'indicator', 'size', entry)

    函 指示符_大小(self, 条目):
        return self.tk.call(self._w, 'indicator', 'size', 条目)

    def info_anchor(self):
        return self.tk.call(self._w, 'info', 'anchor')

    信息_锚点 = info_anchor

    def info_bbox(self, entry):
        return self._getints(
                self.tk.call(self._w, 'info', 'bbox', entry)) or None

    函 信息_包围盒(self, 条目):
        return self._getints(
                self.tk.call(self._w, 'info', 'bbox', 条目)) or None
    
    def info_children(self, entry=None):
        c = self.tk.call(self._w, 'info', 'children', entry)
        return self.tk.splitlist(c)

    函 信息_子条目(self, 条目=None):
        c = self.tk.call(self._w, 'info', 'children', 条目)
        return self.tk.splitlist(c)

    def info_data(self, entry):
        return self.tk.call(self._w, 'info', 'data', entry)

    函 信息_数据(self, 条目):
        return self.tk.call(self._w, 'info', 'data', 条目)
    
    def info_dragsite(self):
        return self.tk.call(self._w, 'info', 'dragsite')

    信息_拖动点 = info_dragsite

    def info_dropsite(self):
        return self.tk.call(self._w, 'info', 'dropsite')

    信息_放下点 = info_dropsite

    def info_exists(self, entry):
        return self.tk.call(self._w, 'info', 'exists', entry)

    函 信息_存在(self, 条目):
        return self.tk.call(self._w, 'info', 'exists', 条目)

    def info_hidden(self, entry):
        return self.tk.call(self._w, 'info', 'hidden', entry)

    函 信息_隐藏(self, 条目):
        return self.tk.call(self._w, 'info', 'hidden', 条目)

    def info_next(self, entry):
        return self.tk.call(self._w, 'info', 'next', entry)

    函 信息_下一条(self, 条目):
        return self.tk.call(self._w, 'info', 'next', 条目)

    def info_parent(self, entry):
        return self.tk.call(self._w, 'info', 'parent', entry)

    函 信息_父条目(self, 条目):
        return self.tk.call(self._w, 'info', 'parent', 条目)

    def info_prev(self, entry):
        return self.tk.call(self._w, 'info', 'prev', entry)

    函 信息_上一条(self, 条目):
        return self.tk.call(self._w, 'info', 'prev', 条目)

    def info_selection(self):
        c = self.tk.call(self._w, 'info', 'selection')
        return self.tk.splitlist(c)

    信息_选定内容 = info_selection

    def item_cget(self, entry, col, opt):
        return self.tk.call(self._w, 'item', 'cget', entry, col, opt)

    函 项目_获取配置(self, 条目, 列, 选项):
        return self.tk.call(self._w, 'item', 'cget', 条目, 列, 选项)
    
    def item_configure(self, entry, col, cnf={}, **kw):
        if cnf is None:
            return self._getconfigure(self._w, 'item', 'configure', entry, col)
        self.tk.call(self._w, 'item', 'configure', entry, col,
              *self._options(cnf, kw))

    函 项目_配置(self, 条目, 列, 配置字典={}, **关键词参数):
        if 配置字典 is None:
            return self._getconfigure(self._w, 'item', 'configure', 条目, 列)
        self.tk.call(self._w, 'item', 'configure', 条目, 列,
              *self._options(配置字典, 关键词参数))

    def item_create(self, entry, col, cnf={}, **kw):
        self.tk.call(
              self._w, 'item', 'create', entry, col, *self._options(cnf, kw))

    函 项目_创建(self, 条目, 列, 配置字典={}, **关键词参数):
        self.tk.call(
              self._w, 'item', 'create', 条目, 列, *self._options(配置字典, 关键词参数))

    def item_exists(self, entry, col):
        return self.tk.call(self._w, 'item', 'exists', entry, col)

    函 项目_存在(self, 条目, 列):
        return self.tk.call(self._w, 'item', 'exists', 条目, 列)

    def item_delete(self, entry, col):
        self.tk.call(self._w, 'item', 'delete', entry, col)

    函 项目_删除(self, 条目, 列):
        self.tk.call(self._w, 'item', 'delete', 条目, 列)

    def entrycget(self, entry, opt):
        return self.tk.call(self._w, 'entrycget', entry, opt)

    函 条目_获取配置(self, 条目, 选项):
        return self.tk.call(self._w, 'entrycget', 条目, 选项)

    def entryconfigure(self, entry, cnf={}, **kw):
        if cnf is None:
            return self._getconfigure(self._w, 'entryconfigure', entry)
        self.tk.call(self._w, 'entryconfigure', entry,
              *self._options(cnf, kw))

    函 条目_配置(self, 条目, 配置字典={}, **关键词参数):
        if 配置字典 is None:
            return self._getconfigure(self._w, 'entryconfigure', 条目)
        self.tk.call(self._w, 'entryconfigure', 条目,
              *self._options(配置字典, 关键词参数))

    def nearest(self, y):
        return self.tk.call(self._w, 'nearest', y)

    最近 = nearest

    def see(self, entry):
        self.tk.call(self._w, 'see', entry)

    函 看见(self, 条目):
        self.tk.call(self._w, 'see', 条目)

    def selection_clear(self, cnf={}, **kw):
        self.tk.call(self._w, 'selection', 'clear', *self._options(cnf, kw))
    
    函 选定内容_清除(self, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'selection', 'clear', *self._options(配置字典, 关键词参数))

    def selection_includes(self, entry):
        return self.tk.call(self._w, 'selection', 'includes', entry)

    函 选定内容_包括(self, 条目):
        return self.tk.call(self._w, 'selection', 'includes', 条目)

    def selection_set(self, first, last=None):
        self.tk.call(self._w, 'selection', 'set', first, last)

    函 选定内容_设置(self, 首, 末=None):
        self.tk.call(self._w, 'selection', 'set', 首, 末)

    def show_entry(self, entry):
        return self.tk.call(self._w, 'show', 'entry', entry)

    函 显示条目(self, 条目):
        return self.tk.call(self._w, 'show', 'entry', 条目)

类 层级列表类(HList):
    """层级式显示部件, 可用来显示任何具有层次结构的数据, 例如文件系统目录树.

    子部件 - 无"""

    函 __init__(自身, 主对象=空, 配置字典={}, **关键词参数):
        HList.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class InputOnly(TixWidget):
    """InputOnly - Invisible widget. Unix only.

    Subwidgets - None"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixInputOnly', None, cnf, kw)

class LabelEntry(TixWidget):
    """LabelEntry - Entry field with label. Packages an entry widget
    and a label into one mega widget. It can be used to simplify the creation
    of ``entry-form'' type of interface.

    Subwidgets       Class
    ----------       -----
    label       Label
    entry       Entry"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixLabelEntry',
                           ['labelside','options'], cnf, kw)
        self.subwidget_list['label'] = _dummyLabel(self, 'label')
        self.subwidget_list['entry'] = _dummyEntry(self, 'entry')

类 标签输入框类(LabelEntry):
    """带标签的输入框.

    子部件       类
    ----------       -----
    label       Label
    entry       Entry"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        LabelEntry.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class LabelFrame(TixWidget):
    """LabelFrame - Labelled Frame container. Packages a frame widget
    and a label into one mega widget. To create widgets inside a
    LabelFrame widget, one creates the new widgets relative to the
    frame subwidget and manage them inside the frame subwidget.

    Subwidgets       Class
    ----------       -----
    label       Label
    frame       Frame"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixLabelFrame',
                           ['labelside','options'], cnf, kw)
        self.subwidget_list['label'] = _dummyLabel(self, 'label')
        self.subwidget_list['frame'] = _dummyFrame(self, 'frame')

类 标签框架类(LabelFrame):
    """带标签的框架容器.

    子部件       类
    ----------       -----
    label       Label
    frame       Frame"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        LabelFrame.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class ListNoteBook(TixWidget):
    """A ListNoteBook widget is very similar to the TixNoteBook widget:
    it can be used to display many windows in a limited space using a
    notebook metaphor. The notebook is divided into a stack of pages
    (windows). At one time only one of these pages can be shown.
    The user can navigate through these pages by
    choosing the name of the desired page in the hlist subwidget."""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixListNoteBook', ['options'], cnf, kw)
        # Is this necessary? It's not an exposed subwidget in Tix.
        self.subwidget_list['pane'] = _dummyPanedWindow(self, 'pane',
                                                        destroy_physically=0)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['shlist'] = _dummyScrolledHList(self, 'shlist')

    def add(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', name, *self._options(cnf, kw))
        self.subwidget_list[name] = TixSubWidget(self, name)
        return self.subwidget_list[name]

    函 添加(自身, 名称, 配置字典={}, **关键词参数):
        返回 自身.add(名称, cnf=配置字典, **关键词参数)

    def page(self, name):
        return self.subwidget(name)

    函 页面(self, 名称):
        return self.subwidget(名称)

    def pages(self):
        # Can't call subwidgets_all directly because we don't want .nbframe
        names = self.tk.splitlist(self.tk.call(self._w, 'pages'))
        ret = []
        for x in names:
            ret.append(self.subwidget(x))
        return ret

    页面列表 = pages

    def raise_page(self, name):              # raise is a python keyword
        self.tk.call(self._w, 'raise', name)

    函 凸显页面(self, 名称):
        self.tk.call(self._w, 'raise', 名称)

类 列表笔记本类(ListNoteBook):
    """可用来在有限空间中显示许多窗口, 类似笔记本."""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ListNoteBook.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class Meter(TixWidget):
    """The Meter widget can be used to show the progress of a background
    job which may take a long time to execute.
    """

    def __init__(self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixMeter',
                           ['options'], cnf, kw)

类 进度计类(TixWidget):
    """可用来显示后台任务的进度.
    """

    函 __init__(self, 主对象=None, 配置字典={}, **关键词参数):
        TixWidget.__init__(self, 主对象, 'tixMeter',
                           ['options'], 配置字典, 关键词参数)


class NoteBook(TixWidget):
    """NoteBook - Multi-page container widget (tabbed notebook metaphor).

    Subwidgets       Class
    ----------       -----
    nbframe       NoteBookFrame
    <pages>       page widgets added dynamically with the add method"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self,master,'tixNoteBook', ['options'], cnf, kw)
        self.subwidget_list['nbframe'] = TixSubWidget(self, 'nbframe',
                                                      destroy_physically=0)

    def add(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', name, *self._options(cnf, kw))
        self.subwidget_list[name] = TixSubWidget(self, name)
        return self.subwidget_list[name]

    函 添加(自身, 名称, 配置字典={}, **关键词参数):
        返回 自身.add(名称, cnf=配置字典, **关键词参数)

    def delete(self, name):
        self.tk.call(self._w, 'delete', name)
        self.subwidget_list[name].destroy()
        del self.subwidget_list[name]

    函 删除(自身, 名称):
        自身.delete(名称)

    def page(self, name):
        return self.subwidget(name)

    函 页面(self, 名称):
        return self.subwidget(名称)

    def pages(self):
        # Can't call subwidgets_all directly because we don't want .nbframe
        names = self.tk.splitlist(self.tk.call(self._w, 'pages'))
        ret = []
        for x in names:
            ret.append(self.subwidget(x))
        return ret

    页面列表 = pages

    def raise_page(self, name):              # raise is a python keyword
        self.tk.call(self._w, 'raise', name)

    函 凸显页面(self, 名称):
        self.tk.call(self._w, 'raise', 名称)

    def raised(self):
        return self.tk.call(self._w, 'raised')

    已凸显 = raised

类 笔记本类(NoteBook):
    """多页容器部件, 类似笔记本.
    
    子部件         类
    ----------       -----
    nbframe       NoteBookFrame
    <pages>       利用 '添加' 方法动态加载的页面部件"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        NoteBook.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class NoteBookFrame(TixWidget):
    # FIXME: This is dangerous to expose to be called on its own.
    pass

class OptionMenu(TixWidget):
    """OptionMenu - creates a menu button of options.

    Subwidget       Class
    ---------       -----
    menubutton      Menubutton
    menu            Menu"""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixOptionMenu', ['options'], cnf, kw)
        self.subwidget_list['menubutton'] = _dummyMenubutton(self, 'menubutton')
        self.subwidget_list['menu'] = _dummyMenu(self, 'menu')

    def add_command(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', 'command', name, *self._options(cnf, kw))

    函 添加命令(self, 名称, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'add', 'command', 名称, *self._options(配置字典, 关键词参数))

    def add_separator(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', 'separator', name, *self._options(cnf, kw))

    函 添加分割线(self, 名称, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'add', 'separator', 名称, *self._options(配置字典, 关键词参数))

    def delete(self, name):
        self.tk.call(self._w, 'delete', name)

    函 删除(self, 名称):
        self.tk.call(self._w, 'delete', 名称)

    def disable(self, name):
        self.tk.call(self._w, 'disable', name)

    函 禁用(self, 名称):
        self.tk.call(self._w, 'disable', 名称)

    def enable(self, name):
        self.tk.call(self._w, 'enable', name)
    
    函 启用(self, 名称):
        self.tk.call(self._w, 'enable', 名称)

类 选项菜单类(OptionMenu):
    """创建选项菜单按钮.

    子部件       类
    ---------       -----
    menubutton      Menubutton
    menu            Menu"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        OptionMenu.__init__(自身, 主对象, cnf=配置字典, **关键词参数)

        
class PanedWindow(TixWidget):
    """PanedWindow - Multi-pane container widget
    allows the user to interactively manipulate the sizes of several
    panes. The panes can be arranged either vertically or horizontally.The
    user changes the sizes of the panes by dragging the resize handle
    between two panes.

    Subwidgets       Class
    ----------       -----
    <panes>       g/p widgets added dynamically with the add method."""

    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixPanedWindow', ['orientation', 'options'], cnf, kw)

    # add delete forget panecget paneconfigure panes setsize
    def add(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', name, *self._options(cnf, kw))
        self.subwidget_list[name] = TixSubWidget(self, name,
                                                 check_intermediate=0)
        return self.subwidget_list[name]

    函 添加(自身, 名称, 配置字典={}, **关键词参数):
        返回 自身.add(名称, cnf=配置字典, **关键词参数)

    def delete(self, name):
        self.tk.call(self._w, 'delete', name)
        self.subwidget_list[name].destroy()
        del self.subwidget_list[name]

    函 删除(自身, 名称):
        自身.delete(名称)

    def forget(self, name):
        self.tk.call(self._w, 'forget', name)

    函 忽略(self, 名称):
        self.tk.call(self._w, 'forget', 名称)

    def panecget(self,  entry, opt):
        return self.tk.call(self._w, 'panecget', entry, opt)

    函 窗格_获取配置(self, 条目, 选项):
        return self.tk.call(self._w, 'panecget', 条目, 选项)
    
    def paneconfigure(self, entry, cnf={}, **kw):
        if cnf is None:
            return self._getconfigure(self._w, 'paneconfigure', entry)
        self.tk.call(self._w, 'paneconfigure', entry, *self._options(cnf, kw))

    函 窗格_配置(自身, 条目, 配置字典={}, **关键词参数):
        返回 自身.paneconfigure(条目, cnf=配置字典, **关键词参数)

    def panes(self):
        names = self.tk.splitlist(self.tk.call(self._w, 'panes'))
        return [self.subwidget(x) for x in names]

    窗格列表 = panes

类 格窗类(PanedWindow):
    """多窗格容器部件.
    allows the user to interactively manipulate the sizes of several
    panes. The panes can be arranged either vertically or horizontally.The
    user changes the sizes of the panes by dragging the resize handle
    between two panes.

    子部件       类
    ----------       -----
    <panes>       利用 '添加' 方法动态添加的窗格部件."""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        PanedWindow.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class PopupMenu(TixWidget):
    """PopupMenu widget can be used as a replacement of the tk_popup command.
    The advantage of the Tix PopupMenu widget is it requires less application
    code to manipulate.


    Subwidgets       Class
    ----------       -----
    menubutton       Menubutton
    menu       Menu"""

    # FIXME: It should inherit -superclass tixShell
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixPopupMenu', ['options'], cnf, kw)
        self.subwidget_list['menubutton'] = _dummyMenubutton(self, 'menubutton')
        self.subwidget_list['menu'] = _dummyMenu(self, 'menu')

    def bind_widget(self, widget):
        self.tk.call(self._w, 'bind', widget._w)

    函 绑定部件(self, 部件):
        self.tk.call(self._w, 'bind', 部件._w)

    def unbind_widget(self, widget):
        self.tk.call(self._w, 'unbind', widget._w)

    函 解除绑定部件(self, 部件):
        self.tk.call(self._w, 'unbind', 部件._w)

    def post_widget(self, widget, x, y):
        self.tk.call(self._w, 'post', widget._w, x, y)

    函 发布部件(self, 部件, x, y): # 发布?
        self.tk.call(self._w, 'post', 部件._w, x, y)

类 弹出菜单类(PopupMenu):
    """可用来取代 '弹出_tk' command.
    
    子部件       类
    ----------       -----
    menubutton       Menubutton
    menu       Menu"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        PopupMenu.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ResizeHandle(TixWidget):
    """Internal widget to draw resize handles on Scrolled widgets."""
    def __init__(self, master, cnf={}, **kw):
        # There seems to be a Tix bug rejecting the configure method
        # Let's try making the flags -static
        flags = ['options', 'command', 'cursorfg', 'cursorbg',
                 'handlesize', 'hintcolor', 'hintwidth',
                 'x', 'y']
        # In fact, x y height width are configurable
        TixWidget.__init__(self, master, 'tixResizeHandle',
                           flags, cnf, kw)

    def attach_widget(self, widget):
        self.tk.call(self._w, 'attachwidget', widget._w)

    def detach_widget(self, widget):
        self.tk.call(self._w, 'detachwidget', widget._w)

    def hide(self, widget):
        self.tk.call(self._w, 'hide', widget._w)

    def show(self, widget):
        self.tk.call(self._w, 'show', widget._w)

class ScrolledHList(TixWidget):
    """ScrolledHList - HList with automatic scrollbars."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixScrolledHList', ['options'],
                           cnf, kw)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

类 滚动层级列表类(ScrolledHList):
    """带自动滚动条的层级列表部件"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ScrolledHList.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ScrolledListBox(TixWidget):
    """ScrolledListBox - Listbox with automatic scrollbars."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixScrolledListBox', ['options'], cnf, kw)
        self.subwidget_list['listbox'] = _dummyListbox(self, 'listbox')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')
        self.列表框 = self.listbox

类 滚动列表框类(ScrolledListBox):
    """带自动滚动条的列表框部件"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ScrolledListBox.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ScrolledText(TixWidget):
    """ScrolledText - Text with automatic scrollbars."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixScrolledText', ['options'], cnf, kw)
        self.subwidget_list['text'] = _dummyText(self, 'text')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

类 滚动文本框类(ScrolledText):
    """带自动滚动条的文本框部件"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ScrolledText.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ScrolledTList(TixWidget):
    """ScrolledTList - TList with automatic scrollbars."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixScrolledTList', ['options'],
                           cnf, kw)
        self.subwidget_list['tlist'] = _dummyTList(self, 'tlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

类 滚动表格类(ScrolledTList):
    """带自动滚动条的表格部件"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ScrolledTList.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class ScrolledWindow(TixWidget):
    """ScrolledWindow - Window with automatic scrollbars."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixScrolledWindow', ['options'], cnf, kw)
        self.subwidget_list['window'] = _dummyFrame(self, 'window')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

类 滚动窗口类(ScrolledWindow):
    """带自动滚动条的窗口"""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        ScrolledWindow.__init__(自身, 主对象, cnf=配置字典, **关键词参数)

class Select(TixWidget):
    """Select - Container of button subwidgets. It can be used to provide
    radio-box or check-box style of selection options for the user.

    Subwidgets are buttons added dynamically using the add method."""

    # FIXME: It should inherit -superclass tixLabelWidget
    def __init__(self, master, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixSelect',
                           ['allowzero', 'radio', 'orientation', 'labelside',
                            'options'],
                           cnf, kw)
        self.subwidget_list['label'] = _dummyLabel(self, 'label')

    def add(self, name, cnf={}, **kw):
        self.tk.call(self._w, 'add', name, *self._options(cnf, kw))
        self.subwidget_list[name] = _dummyButton(self, name)
        return self.subwidget_list[name]

    函 添加(自身, 名称, 配置字典={}, **关键词参数):
        返回 自身.add(名称, cnf=配置字典, **关键词参数)

    def invoke(self, name):
        self.tk.call(self._w, 'invoke', name)

    函 调用(self, 名称):
        self.tk.call(self._w, 'invoke', 名称)

类 选择类(Select):
    """按钮子部件容器, 可提供单选或复选框样式的选项供用户选择."""

    函 __init__(自身, 主对象, 配置字典={}, **关键词参数):
        Select.__init__(自身, 主对象, cnf=配置字典, **关键词参数)


class Shell(TixWidget):
    """Toplevel window.

    Subwidgets - None"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixShell', ['options', 'title'], cnf, kw)

类 外壳类(TixWidget):
    """顶级窗口.

    子部件 - 无"""

    函 __init__ (自身, 主对象=None, 配置字典={}, **关键词参数):
        TixWidget.__init__(自身, 主对象, 'tixShell', ['options', 'title'], 配置字典, 关键词参数)

class DialogShell(TixWidget):
    """Toplevel window, with popup popdown and center methods.
    It tells the window manager that it is a dialog window and should be
    treated specially. The exact treatment depends on the treatment of
    the window manager.

    Subwidgets - None"""

    # FIXME: It should inherit from  Shell
    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master,
                           'tixDialogShell',
                           ['options', 'title', 'mapped',
                            'minheight', 'minwidth',
                            'parent', 'transient'], cnf, kw)

    def popdown(self):
        self.tk.call(self._w, 'popdown')

    下弹出 = popdown

    def popup(self):
        self.tk.call(self._w, 'popup')

    上弹出 = popup

    def center(self):
        self.tk.call(self._w, 'center')

    居中 = center

类 对话框外壳类(DialogShell):
    """顶级窗口, 具有 '上弹出' / '下弹出' / '居中' 方法. 告知窗口管理器它是
    一个对话框, 应特殊对待.

    子部件 - 无"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        DialogShell.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class StdButtonBox(TixWidget):
    """StdButtonBox - Standard Button Box (OK, Apply, Cancel and Help) """

    def __init__(self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixStdButtonBox',
                           ['orientation', 'options'], cnf, kw)
        self.subwidget_list['ok'] = _dummyButton(self, 'ok')
        self.subwidget_list['apply'] = _dummyButton(self, 'apply')
        self.subwidget_list['cancel'] = _dummyButton(self, 'cancel')
        self.subwidget_list['help'] = _dummyButton(self, 'help')

    def invoke(self, name):
        if name in self.subwidget_list:
            self.tk.call(self._w, 'invoke', name)

    函 调用(self, 名称):
        if 名称 in self.subwidget_list:
            self.tk.call(self._w, 'invoke', 名称)

类 标准按钮框类(StdButtonBox):
    """标准按钮框 - (OK, Apply, Cancel, Help) """

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        StdButtonBox.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class TList(TixWidget, XView, YView):
    """TList - Hierarchy display widget which can be
    used to display data in a tabular format. The list entries of a TList
    widget are similar to the entries in the Tk listbox widget. The main
    differences are (1) the TList widget can display the list entries in a
    two dimensional format and (2) you can use graphical images as well as
    multiple colors and fonts for the list entries.

    Subwidgets - None"""

    def __init__ (self,master=None,cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixTList', ['options'], cnf, kw)

    def active_set(self, index):
        self.tk.call(self._w, 'active', 'set', index)

    函 设置活动状态(self, 索引):
        self.tk.call(self._w, 'active', 'set', 索引)

    def active_clear(self):
        self.tk.call(self._w, 'active', 'clear')

    清除活动状态 = active_clear

    def anchor_set(self, index):
        self.tk.call(self._w, 'anchor', 'set', index)

    函 设置锚点(self, 索引):
        self.tk.call(self._w, 'anchor', 'set', 索引)

    def anchor_clear(self):
        self.tk.call(self._w, 'anchor', 'clear')

    清除锚点 = anchor_clear

    def delete(self, from_, to=None):
        self.tk.call(self._w, 'delete', from_, to)
    
    函 删除(self, 从_, 至=None):
        self.tk.call(self._w, 'delete', 从_, 至)

    def dragsite_set(self, index):
        self.tk.call(self._w, 'dragsite', 'set', index)
    
    函 设置拖动点(self, 索引):
        self.tk.call(self._w, 'dragsite', 'set', 索引)

    def dragsite_clear(self):
        self.tk.call(self._w, 'dragsite', 'clear')

    清除拖动点 = dragsite_clear

    def dropsite_set(self, index):
        self.tk.call(self._w, 'dropsite', 'set', index)

    函 设置放下点(self, 索引):
        self.tk.call(self._w, 'dropsite', 'set', 索引)
        
    def dropsite_clear(self):
        self.tk.call(self._w, 'dropsite', 'clear')

    清除放下点 = dropsite_clear

    def insert(self, index, cnf={}, **kw):
        self.tk.call(self._w, 'insert', index, *self._options(cnf, kw))

    函 插入(self, 索引, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'insert', 索引, *self._options(配置字典, 关键词参数))

    def info_active(self):
        return self.tk.call(self._w, 'info', 'active')

    信息_活动 = info_active

    def info_anchor(self):
        return self.tk.call(self._w, 'info', 'anchor')

    信息_锚点 = info_anchor

    def info_down(self, index):
        return self.tk.call(self._w, 'info', 'down', index)

    函 信息_下(self, 索引):
        return self.tk.call(self._w, 'info', 'down', 索引)

    def info_left(self, index):
        return self.tk.call(self._w, 'info', 'left', index)

    函 信息_左(self, 索引):
        return self.tk.call(self._w, 'info', 'left', 索引)

    def info_right(self, index):
        return self.tk.call(self._w, 'info', 'right', index)

    函 信息_右(self, 索引):
        return self.tk.call(self._w, 'info', 'right', 索引)

    def info_selection(self):
        c = self.tk.call(self._w, 'info', 'selection')
        return self.tk.splitlist(c)
    
    信息_选定内容 = info_selection

    def info_size(self):
        return self.tk.call(self._w, 'info', 'size')

    信息_大小 = info_size

    def info_up(self, index):
        return self.tk.call(self._w, 'info', 'up', index)

    函 信息_上(self, 索引):
        return self.tk.call(self._w, 'info', 'up', 索引)
    
    def nearest(self, x, y):
        return self.tk.call(self._w, 'nearest', x, y)

    最近 = nearest

    def see(self, index):
        self.tk.call(self._w, 'see', index)

    函 看见(self, 索引):
        self.tk.call(self._w, 'see', 索引)

    def selection_clear(self, cnf={}, **kw):
        self.tk.call(self._w, 'selection', 'clear', *self._options(cnf, kw))

    函 选定内容_清除(self, 配置字典={}, **关键词参数):
        self.tk.call(self._w, 'selection', 'clear', *self._options(配置字典, 关键词参数))

    def selection_includes(self, index):
        return self.tk.call(self._w, 'selection', 'includes', index)

    函 选定内容_包括(self, 索引):
        return self.tk.call(self._w, 'selection', 'includes', 索引)

    def selection_set(self, first, last=None):
        self.tk.call(self._w, 'selection', 'set', first, last)

    函 选定内容_设置(self, 首, 末=None):
        self.tk.call(self._w, 'selection', 'set', 首, 末)

类 表格类(TList):
    """以表格形式显示数据的部件. 与列表框的主要区别: (1) 表格部件的条目可以是二维形式
    (2) 条目可以是图片, 可以使用多种颜色和字体.

    子部件 - 无"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        TList.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


class Tree(TixWidget):
    """Tree - The tixTree widget can be used to display hierarchical
    data in a tree form. The user can adjust
    the view of the tree by opening or closing parts of the tree."""

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixTree',
                           ['options'], cnf, kw)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

    def autosetmode(self):
        '''This command calls the setmode method for all the entries in this
        Tree widget: if an entry has no child entries, its mode is set to
        none. Otherwise, if the entry has any hidden child entries, its mode is
        set to open; otherwise its mode is set to close.'''
        self.tk.call(self._w, 'autosetmode')

    自动设置模式 = autosetmode

    def close(self, entrypath):
        '''Close the entry given by entryPath if its mode is close.'''
        self.tk.call(self._w, 'close', entrypath)

    函 关闭(self, 条目路径):
        '''关闭 '条目路径' 表示的条目, 如果其模式为关闭.'''
        self.tk.call(self._w, 'close', 条目路径)

    def getmode(self, entrypath):
        '''Returns the current mode of the entry given by entryPath.'''
        return self.tk.call(self._w, 'getmode', entrypath)

    函 获取模式(self, 条目路径):
        '''返回 '条目路径' 表示的条目的当前模式.'''
        return self.tk.call(self._w, 'getmode', 条目路径)

    def open(self, entrypath):
        '''Open the entry given by entryPath if its mode is open.'''
        self.tk.call(self._w, 'open', entrypath)

    函 打开(self, 条目路径):
        '''打开 '条目路径' 表示的条目, 如果其模式为打开.'''
        self.tk.call(self._w, 'open', 条目路径)

    def setmode(self, entrypath, mode='none'):
        '''This command is used to indicate whether the entry given by
        entryPath has children entries and whether the children are visible. mode
        must be one of open, close or none. If mode is set to open, a (+)
        indicator is drawn next the entry. If mode is set to close, a (-)
        indicator is drawn next the entry. If mode is set to none, no
        indicators will be drawn for this entry. The default mode is none. The
        open mode indicates the entry has hidden children and this entry can be
        opened by the user. The close mode indicates that all the children of the
        entry are now visible and the entry can be closed by the user.'''
        self.tk.call(self._w, 'setmode', entrypath, mode)

    函 设置模式(self, 条目路径, 模式='无'):
        """模式须为 '打开' / '关闭' / '无' 之一. 设置为 '打开' 模式的条目会显示 (+) 指示符,
        设置为 '关闭' 模式的条目会显示 (-) 指示符."""
        如果 模式 == '打开':
            模式 = 'open'
        或如 模式 == '关闭':
            模式 = 'close'
        或如 模式 == '无':
            模式 = 'none'
        self.tk.call(self._w, 'setmode', 条目路径, 模式)


类 树类(Tree):
    """树部件可用来以树形式显示层次化数据."""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        Tree.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)


# Could try subclassing Tree for CheckList - would need another arg to init
class CheckList(TixWidget):
    """The CheckList widget
    displays a list of items to be selected by the user. CheckList acts
    similarly to the Tk checkbutton or radiobutton widgets, except it is
    capable of handling many more items than checkbuttons or radiobuttons.
    """
    # FIXME: It should inherit -superclass tixTree
    def __init__(self, master=None, cnf={}, **kw):
        TixWidget.__init__(self, master, 'tixCheckList',
                           ['options', 'radio'], cnf, kw)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

    def autosetmode(self):
        '''This command calls the setmode method for all the entries in this
     Tree widget: if an entry has no child entries, its mode is set to
     none. Otherwise, if the entry has any hidden child entries, its mode is
     set to open; otherwise its mode is set to close.'''
        self.tk.call(self._w, 'autosetmode')

    自动设置模式 = autosetmode

    def close(self, entrypath):
        '''Close the entry given by entryPath if its mode is close.'''
        self.tk.call(self._w, 'close', entrypath)

    函 关闭(self, 条目路径):
        '''关闭 '条目路径' 表示的条目, 如果其模式为关闭.'''
        self.tk.call(self._w, 'close', 条目路径)

    def getmode(self, entrypath):
        '''Returns the current mode of the entry given by entryPath.'''
        return self.tk.call(self._w, 'getmode', entrypath)

    函 获取模式(self, 条目路径):
        '''返回 '条目路径' 表示的条目的当前模式.'''
        return self.tk.call(self._w, 'getmode', 条目路径)

    def open(self, entrypath):
        '''Open the entry given by entryPath if its mode is open.'''
        self.tk.call(self._w, 'open', entrypath)

    函 打开(self, 条目路径):
        '''打开 '条目路径' 表示的条目, 如果其模式为打开.'''
        self.tk.call(self._w, 'open', 条目路径)

    def getselection(self, mode='on'):
        '''Returns a list of items whose status matches status. If status is
        not specified, the list of items in the "on" status will be returned.
        Mode can be on, off, default'''
        return self.tk.splitlist(self.tk.call(self._w, 'getselection', mode))

    函 获取选定内容(self, 模式='选中'):
        """返回匹配模式的项目列表. 模式: '选中', '未选中', '默认'."""
        如果 模式 == '选中':
            模式 == 'on'
        或如 模式 == '未选中':
            模式 = 'off'
        或如 模式 == '默认':
            模式 = 'default'
        return self.tk.splitlist(self.tk.call(self._w, 'getselection', 模式))

    def getstatus(self, entrypath):
        '''Returns the current status of entryPath.'''
        return self.tk.call(self._w, 'getstatus', entrypath)

    函 获取状态(self, 条目路径):
        '''返回条目路径的当前状态.'''
        return self.tk.call(self._w, 'getstatus', 条目路径)

    def setstatus(self, entrypath, mode='on'):
        '''Sets the status of entryPath to be status. A bitmap will be
        displayed next to the entry its status is on, off or default.'''
        self.tk.call(self._w, 'setstatus', entrypath, mode)

    函 设置状态(self, 条目路径, 模式='选中'):
        '''将条目路径的状态设置为指定模式. 条目旁边会显示相应的位图.'''
        如果 模式 == '选中':
            模式 == 'on'
        或如 模式 == '未选中':
            模式 = 'off'
        或如 模式 == '默认':
            模式 = 'default'
        self.tk.call(self._w, 'setstatus', 条目路径, 模式)

class 检查表类(CheckList):
    """检查表部件可显示一系列项目供用户选择. 能够处理比复选或单选按钮更多的项目."""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        CheckList.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)

###########################################################################
### The subclassing below is used to instantiate the subwidgets in each ###
### mega widget. This allows us to access their methods directly.       ###
###########################################################################

class _dummyButton(Button, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyCheckbutton(Checkbutton, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyEntry(Entry, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyFrame(Frame, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyLabel(Label, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyListbox(Listbox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyMenu(Menu, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyMenubutton(Menubutton, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyScrollbar(Scrollbar, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyText(Text, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyScrolledListBox(ScrolledListBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['listbox'] = _dummyListbox(self, 'listbox')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

class _dummyHList(HList, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyScrolledHList(ScrolledHList, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

class _dummyTList(TList, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyComboBox(ComboBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, ['fancy',destroy_physically])
        self.subwidget_list['label'] = _dummyLabel(self, 'label')
        self.subwidget_list['entry'] = _dummyEntry(self, 'entry')
        self.subwidget_list['arrow'] = _dummyButton(self, 'arrow')

        self.subwidget_list['slistbox'] = _dummyScrolledListBox(self,
                                                                'slistbox')
        try:
            self.subwidget_list['tick'] = _dummyButton(self, 'tick')
            #cross Button : present if created with the fancy option
            self.subwidget_list['cross'] = _dummyButton(self, 'cross')
        except TypeError:
            # unavailable when -fancy not specified
            pass

class _dummyDirList(DirList, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['hlist'] = _dummyHList(self, 'hlist')
        self.subwidget_list['vsb'] = _dummyScrollbar(self, 'vsb')
        self.subwidget_list['hsb'] = _dummyScrollbar(self, 'hsb')

class _dummyDirSelectBox(DirSelectBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['dirlist'] = _dummyDirList(self, 'dirlist')
        self.subwidget_list['dircbx'] = _dummyFileComboBox(self, 'dircbx')

class _dummyExFileSelectBox(ExFileSelectBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['cancel'] = _dummyButton(self, 'cancel')
        self.subwidget_list['ok'] = _dummyButton(self, 'ok')
        self.subwidget_list['hidden'] = _dummyCheckbutton(self, 'hidden')
        self.subwidget_list['types'] = _dummyComboBox(self, 'types')
        self.subwidget_list['dir'] = _dummyComboBox(self, 'dir')
        self.subwidget_list['dirlist'] = _dummyScrolledListBox(self, 'dirlist')
        self.subwidget_list['file'] = _dummyComboBox(self, 'file')
        self.subwidget_list['filelist'] = _dummyScrolledListBox(self, 'filelist')

class _dummyFileSelectBox(FileSelectBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['dirlist'] = _dummyScrolledListBox(self, 'dirlist')
        self.subwidget_list['filelist'] = _dummyScrolledListBox(self, 'filelist')
        self.subwidget_list['filter'] = _dummyComboBox(self, 'filter')
        self.subwidget_list['selection'] = _dummyComboBox(self, 'selection')

class _dummyFileComboBox(ComboBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['dircbx'] = _dummyComboBox(self, 'dircbx')

class _dummyStdButtonBox(StdButtonBox, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)
        self.subwidget_list['ok'] = _dummyButton(self, 'ok')
        self.subwidget_list['apply'] = _dummyButton(self, 'apply')
        self.subwidget_list['cancel'] = _dummyButton(self, 'cancel')
        self.subwidget_list['help'] = _dummyButton(self, 'help')

class _dummyNoteBookFrame(NoteBookFrame, TixSubWidget):
    def __init__(self, master, name, destroy_physically=0):
        TixSubWidget.__init__(self, master, name, destroy_physically)

class _dummyPanedWindow(PanedWindow, TixSubWidget):
    def __init__(self, master, name, destroy_physically=1):
        TixSubWidget.__init__(self, master, name, destroy_physically)

########################
### Utility Routines ###
########################

#mike Should tixDestroy be exposed as a wrapper? - but not for widgets.

def OptionName(widget):
    '''Returns the qualified path name for the widget. Normally used to set
    default options for subwidgets. See tixwidgets.py'''
    return widget.tk.call('tixOptionName', widget._w)

# Called with a dictionary argument of the form
# {'*.c':'C source files', '*.txt':'Text Files', '*':'All files'}
# returns a string which can be used to configure the fsbox file types
# in an ExFileSelectBox. i.e.,
# '{{*} {* - All files}} {{*.c} {*.c - C source files}} {{*.txt} {*.txt - Text Files}}'
def FileTypeList(dict):
    s = ''
    for type in dict.keys():
        s = s + '{{' + type + '} {' + type + ' - ' + dict[type] + '}} '
    return s

# Still to be done:
# tixIconView
class CObjView(TixWidget):
    """This file implements the Canvas Object View widget. This is a base
    class of IconView. It implements automatic placement/adjustment of the
    scrollbars according to the canvas objects inside the canvas subwidget.
    The scrollbars are adjusted so that the canvas is just large enough
    to see all the objects.
    """
    # FIXME: It should inherit -superclass tixScrolledWidget
    pass


class Grid(TixWidget, XView, YView):
    '''The Tix Grid command creates a new window  and makes it into a
    tixGrid widget. Additional options, may be specified on the command
    line or in the option database to configure aspects such as its cursor
    and relief.

    A Grid widget displays its contents in a two dimensional grid of cells.
    Each cell may contain one Tix display item, which may be in text,
    graphics or other formats. See the DisplayStyle class for more information
    about Tix display items. Individual cells, or groups of cells, can be
    formatted with a wide range of attributes, such as its color, relief and
    border.

    Subwidgets - None'''
    # valid specific resources as of Tk 8.4
    # editdonecmd, editnotifycmd, floatingcols, floatingrows, formatcmd,
    # highlightbackground, highlightcolor, leftmargin, itemtype, selectmode,
    # selectunit, topmargin,
    def __init__(self, master=None, cnf={}, **kw):
        static= []
        self.cnf= cnf
        TixWidget.__init__(self, master, 'tixGrid', static, cnf, kw)

    # valid options as of Tk 8.4
    # anchor, bdtype, cget, configure, delete, dragsite, dropsite, entrycget,
    # edit, entryconfigure, format, geometryinfo, info, index, move, nearest,
    # selection, set, size, unset, xview, yview
    def anchor_clear(self):
        """移除选定内容锚点."""
        self.tk.call(self, 'anchor', 'clear')

    清除锚点 = anchor_clear

    def anchor_get(self):
        "获取当前锚点单元的 (x,y) 坐标"
        return self._getints(self.tk.call(self, 'anchor', 'get'))

    获取锚点 = anchor_get

    def anchor_set(self, x, y):
        """将选定内容锚点设置为 (x, y) 单元."""
        self.tk.call(self, 'anchor', 'set', x, y)

    设置锚点 = anchor_set

    def delete_row(self, from_, to=None):
        """Delete rows between from_ and to inclusive.
        If to is not provided,  delete only row at from_"""
        if to is None:
            self.tk.call(self, 'delete', 'row', from_)
        else:
            self.tk.call(self, 'delete', 'row', from_, to)

    函 删除行(自身, 从_, 至=None):
        """删除指定范围的行. 如果 '至' 未指定, 则仅删除 '从_' 行"""
        自身.delete_row(从_, 至)

    def delete_column(self, from_, to=None):
        """Delete columns between from_ and to inclusive.
        If to is not provided,  delete only column at from_"""
        if to is None:
            self.tk.call(self, 'delete', 'column', from_)
        else:
            self.tk.call(self, 'delete', 'column', from_, to)

    函 删除列(自身, 从_, 至=None):
        """删除指定范围的列. 如果 '至' 未指定, 则仅删除 '从_' 列"""
        自身.delete_column(从_, 至)

    def edit_apply(self):
        """如有单元正在编辑, 则取消该单元的高亮并应用更改."""
        self.tk.call(self, 'edit', 'apply')

    编辑_应用 = edit_apply

    def edit_set(self, x, y):
        """高亮显示 (x, y) 单元以供编辑, 如果对此单元的 -editnotify 命令返回 真."""
        self.tk.call(self, 'edit', 'set', x, y)

    编辑_设置 = edit_set

    def entrycget(self, x, y, option):
        "Get the option value for cell at (x,y)"
        if option and option[0] != '-':
            option = '-' + option
        return self.tk.call(self, 'entrycget', x, y, option)
        
    函 条目_获取配置(self, x, y, 选项):
        "获取 (x,y) 单元的选项值"
        if 选项 and 选项[0] != '-':
            选项 = '-' + 选项
        return self.tk.call(self, 'entrycget', x, y, 选项)

    def entryconfigure(self, x, y, cnf=None, **kw):
        return self._configure(('entryconfigure', x, y), cnf, kw)

    函 条目_配置(self, x, y, 配置字典=None, **关键词参数):
        return self._configure(('entryconfigure', x, y), 配置字典, 关键词参数)

    # def format
    # def index

    def info_exists(self, x, y):
        "如果 (x,y) 存在显示条目, 则返回 真"
        return self._getboolean(self.tk.call(self, 'info', 'exists', x, y))

    信息_存在 = info_exists

    def info_bbox(self, x, y):
        # This seems to always return '', at least for 'text' displayitems
        return self.tk.call(self, 'info', 'bbox', x, y)

    信息_包围盒 = info_bbox

    def move_column(self, from_, to, offset):
        """Moves the range of columns from position FROM through TO by
        the distance indicated by OFFSET. For example, move_column(2, 4, 1)
        moves the columns 2,3,4 to columns 3,4,5."""
        self.tk.call(self, 'move', 'column', from_, to, offset)

    函 移动列(self, 从_, 至, 偏移量):
        """将指定范围的列移动 '偏移量' 指示的距离. 例如, 移动列(2, 4, 1)
        将列 2,3,4 移动到列 3,4,5."""
        self.tk.call(self, 'move', 'column', 从_, 至, 偏移)

    def move_row(self, from_, to, offset):
        """Moves the range of rows from position FROM through TO by
        the distance indicated by OFFSET.
        For example, move_row(2, 4, 1) moves the rows 2,3,4 to rows 3,4,5."""
        self.tk.call(self, 'move', 'row', from_, to, offset)

    函 移动行(self, 从_, 至, 偏移量):
        """将指定范围的行移动 '偏移量' 指示的距离. 例如, 移动行(2, 4, 1)
        将行 2,3,4 移动到行 3,4,5."""
        self.tk.call(self, 'move', 'row', 从_, 至, 偏移)

    def nearest(self, x, y):
        "返回最靠近像素坐标 (x,y) 的单元坐标"
        return self._getints(self.tk.call(self, 'nearest', x, y))

    最近 = nearest

    # def selection adjust
    # def selection clear
    # def selection includes
    # def selection set
    # def selection toggle

    def set(self, x, y, itemtype=None, **kw):
        args= self._options(self.cnf, kw)
        if itemtype is not None:
            args= ('-itemtype', itemtype) + args
        self.tk.call(self, 'set', x, y, *args)

    函 设置(自身, x, y, 项目类型=None, **关键词参数):
        自身.set(x, y, itemtype=项目类型, **关键词参数)

    def size_column(self, index, **kw):
        """Queries or sets the size of the column given by
        INDEX.  INDEX may be any non-negative
        integer that gives the position of a given column.
        INDEX can also be the string "default"; in this case, this command
        queries or sets the default size of all columns.
        When no option-value pair is given, this command returns a tuple
        containing the current size setting of the given column.  When
        option-value pairs are given, the corresponding options of the
        size setting of the given column are changed. Options may be one
        of the follwing:
              pad0 pixels
                     Specifies the paddings to the left of a column.
              pad1 pixels
                     Specifies the paddings to the right of a column.
              size val
                     Specifies the width of a column.  Val may be:
                     "auto" -- the width of the column is set to the
                     width of the widest cell in the column;
                     a valid Tk screen distance unit;
                     or a real number following by the word chars
                     (e.g. 3.4chars) that sets the width of the column to the
                     given number of characters."""
        return self.tk.splitlist(self.tk.call(self._w, 'size', 'column', index,
                             *self._options({}, kw)))

    函 列大小(self, 索引, **关键词参数):
        """查询或设置索引对应列的大小"""
        return self.tk.splitlist(self.tk.call(self._w, 'size', 'column', 索引,
                             *self._options({}, 关键词参数)))

    def size_row(self, index, **kw):
        """Queries or sets the size of the row given by
        INDEX. INDEX may be any non-negative
        integer that gives the position of a given row .
        INDEX can also be the string "default"; in this case, this command
        queries or sets the default size of all rows.
        When no option-value pair is given, this command returns a list con-
        taining the current size setting of the given row . When option-value
        pairs are given, the corresponding options of the size setting of the
        given row are changed. Options may be one of the follwing:
              pad0 pixels
                     Specifies the paddings to the top of a row.
              pad1 pixels
                     Specifies the paddings to the bottom of a row.
              size val
                     Specifies the height of a row.  Val may be:
                     "auto" -- the height of the row is set to the
                     height of the highest cell in the row;
                     a valid Tk screen distance unit;
                     or a real number following by the word chars
                     (e.g. 3.4chars) that sets the height of the row to the
                     given number of characters."""
        return self.tk.splitlist(self.tk.call(
                    self, 'size', 'row', index, *self._options({}, kw)))

    函 行大小(self, 索引, **关键词参数):
        """查询或设置索引对应行的大小"""
        return self.tk.splitlist(self.tk.call(
                    self, 'size', 'row', 索引, *self._options({}, 关键词参数)))

    def unset(self, x, y):
        """清空 (x, y) 单元的显示项目."""
        self.tk.call(self._w, 'unset', x, y)

    取消设置 = unset

类 网格类(Grid):
    """The Tix Grid command creates a new window  and makes it into a
    tixGrid widget. Additional options, may be specified on the command
    line or in the option database to configure aspects such as its cursor
    and relief.

    网格部件以二维网格单元显示内容. 每个单元可包含文本/图片或其他格式的项目.
    See the DisplayStyle class for more information about Tix display items.
    Individual cells, or groups of cells, can be formatted with a wide
    range of attributes, such as its color, relief and border.

    子部件 - 无"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        Grid.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)

class ScrolledGrid(Grid):
    '''Scrolled Grid widgets'''

    # FIXME: It should inherit -superclass tixScrolledWidget
    def __init__(self, master=None, cnf={}, **kw):
        static= []
        self.cnf= cnf
        TixWidget.__init__(self, master, 'tixScrolledGrid', static, cnf, kw)

类 滚动网格类(ScrolledGrid):
    """滚动网格部件"""

    函 __init__(自身, 主对象=None, 配置字典={}, **关键词参数):
        ScrolledGrid.__init__(自身, master=主对象, cnf=配置字典, **关键词参数)
