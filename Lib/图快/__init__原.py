"""tkinter 的功能封装, 简单直观的图形编程 (GUI) 模块.

使用 GUI 可实现很多直观的功能. 比如想开发一个计算器，有一个图形化的小窗口就是非常必要的。

对于图形编程，可以用两个比喻来理解：
第一个，作画。我们都见过美术生写生的情景，先支一个画架，放上画板，蒙上画布，构思内容，用铅笔画草图，组织结构和比例，调色板调色，最后画笔勾勒。
相应的，对应到图形编程，我们的显示屏就是支起来的画架，根窗体就是画板，在tkinter中则是顶级窗口 (Toplevel)，画布就是 tkinter 中的容器 (Frame)，
画板上可以放很多张画布 (Canvas)，tkinter 中的容器中也可以放很多个容器，绘画中的构图布局则是 tkinter 中的布局管理器（几何管理器），
绘画的内容就是 tkinter 中的一个个小组件，一幅画由许多元素构成，而我们的 GUI 界面，就是由一个个组件拼装起来的，它们就是 widget。
第二个，我们小时候都玩过积木，只要发挥创意，相同的积木可以堆出各种造型。tkinter 的组件也可以看做一个个积木，形状或许不同，其本质都是一样的，
就是一个积木，不管它长什么样子，它始终就是积木！

顶层积木是 <主窗口类> 和 <顶级窗口类>, 其他积木有: <框架类>, <标签类>, <输入框类>, <文本框类>, <画布类>, <按钮类>, <单选按钮类>, <复选按钮类>,
<刻度条类>, <列表框类>, <滚动条类>, <选项菜单类>, <旋钮控件类>, <标签框架类>, <菜单类> 和 <格窗类>.

各种积木的布局方法有三种: 常规布局, 网格布局, 位置布局.

参考链接:
http://effbot.org/tkinterbook/ \n
https://blog.csdn.net/ahilll/article/details/81531587 \n
https://blog.csdn.net/qq_41556318/category_9283243.html
"""

_版本 = "20191221 - 草蟒380 - 20191210"
_汉化人 = "老吴"

导入 tkinter 为 tk
导入 tkinter.messagebox 为 _msgbox
从 图快.通用字典 导入 _锚点字典, _部件通用选项字典, _部件通用选项值字典, \
    _颜色字典, _对齐字典, _边框样式字典, _验证字典, _菜单配置选项字典
从 汉化通用 导入 _关键词参数中转英

'''
类 事件类型类(tk.EventType):
    键按下 = KeyPress = '2'
    键释放 = KeyRelease = '3'
    按钮按下 = ButtonPress = '4'
    按钮释放  = ButtonRelease = '5'
    运动 = Motion = '6'
    进入 = Enter = '7'
    离开 = Leave = '8'
    获得焦点 = FocusIn = '9'
    失去焦点 = FocusOut = '10'
    键映射 = Keymap = '11'           # undocumented
    暴露 = Expose = '12'
    图形暴露 = GraphicsExpose = '13'   # undocumented
    不暴露 = NoExpose = '14'         # undocumented
    可见性 = Visibility = '15'
    创建 = Create = '16'
    销毁 = Destroy = '17'
    解除映射 = Unmap = '18'
    映射 = Map = '19'
    映射请求 = MapRequest = '20'
    重定父对象 = Reparent = '21'
    配置 = Configure = '22'
    配置请求 = ConfigureRequest = '23'
    重力 = Gravity = '24'
    调整请求 = ResizeRequest = '25'
    传阅 = Circulate = '26'
    传阅请求 = CirculateRequest = '27'
    属性 = Property = '28'
    选定内容清除 = SelectionClear = '29'   # undocumented
    选定内容请求 = SelectionRequest = '30' # undocumented
    选定内容 = Selection = '31'        # undocumented
    颜色映射 = Colormap = '32'
    客户端消息 = ClientMessage = '33'    # undocumented
    建立映射 = Mapping = '34'          # undocumented
    虚拟事件 = VirtualEvent = '35',    # undocumented
    激活 = Activate = '36',
    停用 = Deactivate = '37',
    鼠标轮 = MouseWheel = '38',

    函 __str__(自身):
        返回 自身.name
'''

类 事件类(tk.Event):
    """事件属性的容器.
    """
    无操作


函 无默认根窗口():
    """禁止设置默认根窗口.
    """
    tk.NoDefaultRoot()


类 变量类(tk.Variable):

    函 __init__(自身, 主对象=空, 值=空, 名称=空):
        超类().__init__(主对象, 值, 名称)

    函 设置(自身, 值):
        返回 自身.set(值)

    函 获取(自身):
        返回 自身.get()

    函 跟踪_增加(自身, 模式, 回调):
        返回 自身.trace_add(模式, 回调)

    函 跟踪_移除(自身, 模式, 回调名称):
        自身.trace_remove(模式, 回调名称)

    函 跟踪_信息(自身):
        返回 自身.trace_info(模式, 回调名称)


类 串变量类(tk.StringVar, 变量类):

    函 __init__(自身, 主对象=空, 值=空, 名称=空):
        tk.StringVar.__init__(自身, 主对象, 值, 名称)

    函 获取(自身):
        返回 tk.StringVar.get(自身)


类 整型变量类(tk.IntVar, 变量类):

    函 __init__(自身, 主对象=空, 值=空, 名称=空):
        tk.IntVar.__init__(自身, 主对象, 值, 名称)

    函 获取(自身):
        返回 tk.IntVar.get(自身)


类 双精度变量类(tk.DoubleVar, 变量类):

    函 __init__(自身, 主对象=空, 值=空, 名称=空):
        tk.DoubleVar.__init__(自身, 主对象, 值, 名称)

    函 获取(自身):
        返回 tk.DoubleVar.get(自身)


类 布尔变量类(tk.BooleanVar, 变量类):

    函 __init__(自身, 主对象=空, 值=空, 名称=空):
        tk.BooleanVar().__init__(自身, 主对象, 值, 名称)

    函 设置(自身, 值):
        返回 tk.BooleanVar.set(自身, 值)

    函 获取(自身):
        返回 tk.BooleanVar.get(自身)


函 主循环(n=0):
    tk.mainloop(n)


类 杂项类(tk.Misc):

    函 销毁(自身):
        自身.destroy()

    函 删除命令(自身, 名称):
        自身.deletecommand(名称)

    # tk_strictMotif, tk_bisque, tk_setPalette ?

    函 等待变量修改(自身, 名称='PY_VAR'):
        自身.wait_variable(名称)

    函 等待部件销毁(自身, 窗口=空):
        自身.wait_window(窗口)

    函 等待可见性变化(自身, 窗口=空):
        自身.wait_visibility(窗口)

    函 设置变量(自身, 名称='PY_VAR', 值='1'):
        自身.setvar(名称, 值)

    函 获取变量(自身, 名称='PY_VAR'):
        返回 自身.setvar(名称)

    函 获取整数(自身, s):
        返回 自身.getint(s)

    函 获取双精度(自身, s):
        返回 自身.getdouble(s)

    函 获取布尔值(自身, s):
        返回 自身.getboolean(s)

    函 设置焦点(自身):
        自身.focus_set()

    焦点 = 设置焦点

    函 强制焦点(自身):
        自身.focus_force()
    
    函 焦点部件(自身):
        返回 自身.focus_get()
    
    函 显示焦点(自身):
        返回 自身.focus_displayof()

    # focus_lastfor, tk_focusFollowsMouse, tk_focusNext, tk_focusPrev

    函 之后(自身, 毫秒数, 函数=空, *参数):
        返回 自身.after(毫秒数, 函数, *参数)

    函 空闲后(自身, 函数, *参数):
        返回 自身.after_idle(函数, *参数)

    函 之后_取消(自身, id):
        自身.after_cancel(id)

    函 响铃(自身, 显示=0):
        自身.bell(显示)

    函 获取剪贴板(自身, **关键词):
        返回 自身.clipboard_get(**关键词)

    函 清除剪贴板(自身, **关键词):
        自身.clipboard_clear(**关键词)

    函 追加剪贴板(自身, 字符串, **关键词):
        自身.clipboard_append(字符串, **关键词)

    # grab_current, grab_release, grab_set, grab_set_global, grab_status

    # option_add, option_clear, option_get, option_readfile

    # selection_clear, selection_get, selection_handle, selection_own, selection_own_get

    # send, lower, tkraise

    # winfo_atom, winfo_atomname, winfo_cells, winfo_children, winfo_class, winfo_colormapfull, winfo_containing
    # winfo_depth, winfo_exists, winfo_fpixels, winfo_geometry, winfo_height, winfo_id, winfo_interps
    # winfo_ismapped, winfo_manager, winfo_name, winfo_parent, winfo_pathname, winfo_pixels, winfo_pointerx
    # winfo_pointerxy, winfo_pointery, winfo_reqheight, winfo_reqwidth, winfo_rgb, winfo_rootx, winfo_rooty
    # winfo_screen, winfo_screencells, winfo_screendepth, winfo_screenheight, winfo_screenmmheight, winfo_screenmmwidth
    # winfo_screenvisual, winfo_screenwidth, winfo_server, winfo_toplevel, winfo_viewable, winfo_visual
    # winfo_visualid, winfo_visualsavailable, winfo_vrootheight, winfo_vrootwidth, winfo_vrootx, winfo_vrooty
    # winfo_width, winfo_x, winfo_y

    函 更新(自身):
        自身.update()

    函 更新空闲任务(自身):
        自身.update_idletasks()

    函 绑定标记(自身, 标记列表=空):
        返回 自身.bindtags(标记列表)

    函 绑定(自身, 序列=空, 函数=空, 添加=空):
        返回 自身.bind(序列, 函数, 添加)

    函 解除绑定(自身, 序列, 函数id=空):
        自身.unbind(序列, 函数id)

    函 全部绑定(自身, 序列=空, 函数=空, 添加=空):
        返回 自身.bind_all(序列, 函数, 添加)

    函 全部解除绑定(自身, 序列):
        自身.unbind_all(序列)

    函 类绑定(自身, 类名, 序列=空, 函数=空, 添加=空):
        返回 自身.bind_class(类名, 序列, 函数, 添加)

    函 类解除绑定(自身, 类名, 序列):
        自身.unbind_class(序列)

    函 主循环(自身, n=0):
        自身.mainloop(n)

    函 退出(自身):
        自身.quit()

    函 部件名称(自身, 名称): # ?
        返回 自身.nametowidget(名称)

    # register

    函 配置(自身, 配置=空, **关键词):
        返回 自身.configure(配置, **关键词)

    函 获取配置(自身, 键):
        返回 自身.cget(键)

    函 键列表(自身):
        返回 自身.keys()

    函 常规布局_传播(自身, 标志=['_noarg_']):
        返回 自身.pack_propagate(标志)

    函 常规布局_从对象列表(自身):
        返回 自身.pack_slaves()
    
    函 位置布局_从对象列表(自身):
        返回 自身.place_slaves()

    函 网格布局_锚点(自身, 锚点=空):
        自身.grid_anchor(锚点)

    函 网格布局_包围盒(自身, 列=空, 行=空, 列2=空, 行2=空):
        返回 自身.grid_bbox(列=空, 行=空, 列2=空, 行2=空)

    函 网格布局_列配置(自身, 索引, 配置={}, **关键词):
        返回 自身.grid_columnconfigure(索引, 配置={}, **关键词)

    函 网格布局_位置(自身, x, y):
        返回 自身.grid_location(索引, x, y)

    函 网格布局_传播(自身, 标志=['_noarg_']):
        返回 自身.grid_propagate(标志)

    函 网格布局_行配置(自身, 索引, 配置={}, **关键词):
        返回 自身.grid_rowconfigure(索引, 配置={}, **关键词)

    函 网格布局_大小(自身):
        返回 自身.grid_size()

    函 网格布局_从对象列表(自身, 行=空, 列=空):
        返回 自身.grid_slaves(行, 列)

    # event_add, event_delete, event_generate, event_info

    函 图像名称(自身):
        返回 自身.image_names()

    函 图像类型(自身):
        返回 自身.image_types()

# CallWrapper


类 视图X类(tk.XView):

    函 视图x(自身, *参数):
        返回 超类().xview(*参数)

    函 视图x移至(自身, 分数):
        返回 超类().xview_moveto(分数)

    函 视图x滚动(自身, 数值, 单位):
        返回 超类().xview_scroll(数值, 单位)


类 视图Y类(tk.YView):

    函 视图y(自身, *参数):
        返回 超类().yview(*参数)

    函 视图y移至(自身, 分数):
        返回 超类().yview_moveto(分数)

    函 视图y滚动(自身, 数值, 单位):
        返回 超类().yview_scroll(数值, 单位)


类 窗口管理类(tk.Wm):

    函 纵横比(自身,最小数=空, 最小分母=空, 最大数=空, 最大分母=空):
        返回 自身.wm_aspect(最小数, 最小分母, 最大数, 最大分母)

    函 平台属性(自身, *参数):
        返回 自身.wm_attributes(*参数)

    函 客户端(自身, 名称=空):
        返回 自身.wm_client(名称)

    函 颜色映射窗口(自身, *窗口列表):
        返回 自身.wm_colormapwindows(*窗口列表)

    函 命令(自身, 值=空):
        返回 自身.wm_command(值)

    函 显示窗口(自身):
        '''撤消最小化状态, 显示窗口'''
        返回 自身.wm_deiconify()

    函 焦点模型(自身, 模型=空):
        返回 自身.wm_focusmodel(模型)

    函 忽略(自身, 窗口):
        自身.wm_forget(窗口)

    函 框架(自身):
        返回 自身.wm_frame()

    函 尺寸(自身, 新尺寸=空):
        '''参数格式为："宽x高+水平偏移+垂直偏移"'''
        返回 自身.wm_geometry(新尺寸)

    函 网格(自身, 基本宽度=空, 基本高度=空, 宽度增量=空, 高度增量=空):
        返回 自身.wm_grid(基本宽度, 基本高度, 宽度增量, 高度增量)

    函 组(自身, 路径名=空):
        返回 自身.wm_group(路径名)

    函 图标位图(自身, 位图=空, 默认=空):
        返回 自身.wm_iconbitmap(位图, 默认)

    函 最小化(自身):
        '''窗口最小化'''
        返回 自身.wm_iconify()

    函 图标蒙版(自身, 位图=空):
        返回 自身.wm_iconmask(位图)

    函 图标名(自身, 新名称=空):
        返回 自身.wm_iconname(新名称)

    函 图标照片(自身, 默认=假, *参数):
        自身.wm_iconphoto(默认, *参数)

    函 图标位置(自身, x=空, y=空):
        返回 自身.wm_iconposition(x, y)

    函 图标窗口(自身, 路径名=空):
        返回 自身.wm_iconwindow(路径名)

    函 管理(自身, 部件):
        返回 自身.wm_manage(部件)

    函 最大尺寸(自身, 宽度=空, 高度=空):
        返回 自身.wm_maxsize(宽度, 高度)

    函 最小尺寸(自身, 宽度=空, 高度=空):
        返回 自身.wm_minsize(宽度, 高度)

    函 无视(自身, 布尔值=空):
        返回 自身.wm_overrideredirect(布尔值)

    函 定位(自身, 定位者=空):
        返回 自身.wm_positionfrom(定位者)

    函 协议(自身, 名称=空, 函数=空):
        返回 自身.wm_protocol(名称, 函数)

    函 可调整(自身, 宽度=空, 高度=空):
        返回 自身.wm_resizable(宽度, 高度)

    函 定尺寸(自身, 定尺寸者=空):
        返回 自身.wm_sizefrom(定尺寸者)

    函 状态(自身, 新状态=空):
        返回 自身.wm_state(新状态)

    函 标题(自身, 字符串=空):
        '''窗口标题'''
        返回 自身.wm_title(字符串)

    函 暂态(自身, 主对象=空):
        返回 自身.wm_transient(主对象)

    函 撤回(自身):
        返回 自身.wm_withdraw()


类 主窗口类(tk.Tk, 杂项类, 窗口管理类):
    """大多数情况下代表应用程序的主窗口."""

    函 __init__(自身, 屏幕名称=空, 基本名称=空, 类名='Tk', 使用Tk=1, 同步=0, 使用=空):
        tk.Tk.__init__(自身, 屏幕名称, 基本名称, 类名, 使用Tk, 同步, 使用)

    函 销毁(自身):
        """销毁自身及所有从属积木. 终结应用程序.
        """
        tk.Tk.destroy(自身)

    函 配置(自身, 配置=空, **关键词):
        _选项字典 = {
            '菜单': 'menu',
        }
        关键词 = _关键词参数中转英(关键词, _选项字典)
        返回 tk.Tk.configure(自身, 配置, **关键词)

类 常规布局类(tk.Pack):
    
    函 常规布局(自身, 配置={}, **关键词):
        常规布局选项字典 = {
            '前面部件':     'after',
            '锚点':         'anchor',
            '后面部件':     'before',
            '扩展':         'expand',
            '填充':         'fill',
            '主对象':       'in_',
            '内水平边距':   'ipadx',
            '内垂直边距':   'ipady',
            '水平边距':     'padx',
            '垂直边距':     'pady',
            '边':           'side'
        }
        常规布局选项值字典 = {
            '无':   'none',
            '水平': 'x',
            '垂直': 'y',
            '同时': 'both',
            '左边': 'left',
            '右边': 'right',
            '顶边': 'top',
            '底边': 'bottom',
        }
        常规布局选项值字典.更新(_锚点字典)
        关键词 = _关键词参数中转英(关键词, 常规布局选项字典, 常规布局选项值字典)
        自身.pack_configure(配置, **关键词)

    函 常规布局_忽略(自身):
        自身.pack_forget()

    函 常规布局_信息(自身):
        自身.pack_info()

    常规布局_传播 = 杂项类.常规布局_传播
    常规布局_从对象列表 = 杂项类.常规布局_从对象列表


类 位置布局类(tk.Place):
    
    函 位置布局(自身, 配置={}, **关键词):
        位置布局选项字典 = {
            '主对象':   'in_',
            '相对x':    'relx',
            '相对y':    'rely',
            '锚点':     'anchor',
            '宽度':     'width',
            '高度':     'height',
            '相对宽度': 'relwidth',
            '相对高度': 'relheight',
            '边框模式': 'bordermode'
        }
        位置布局选项值字典 = {
            '内部': 'inside',
            '外部': 'outside'
        }
        位置布局选项值字典.更新(_锚点字典)
        关键词 = _关键词参数中转英(关键词, 位置布局选项字典, 位置布局选项值字典)
        自身.place_configure(配置, **关键词)

    函 位置布局_忽略(自身):
        自身.place_forget()

    函 位置布局_信息(自身):
        自身.place_info()

    位置布局_从对象列表 = 杂项类.位置布局_从对象列表


类 网格布局类(tk.Grid):
    
    函 网格布局(自身, 配置={}, **关键词):
        网格布局选项字典 = {
            '列':           'column',
            '跨列':         'columnspan',
            '行':           'row',
            '跨行':         'rowspan',
            '主对象':       'in_',
            '内水平边距':   'ipadx',
            '内垂直边距':   'ipady',
            '水平边距':     'padx',
            '垂直边距':     'pady',
            '贴边':         'sticky'
        }
        网格布局选项值字典 = _锚点字典
        关键词 = _关键词参数中转英(关键词, 网格布局选项字典, 网格布局选项值字典)
        自身.grid_configure(配置, **关键词)

    网格布局_包围盒 = 杂项类.网格布局_包围盒
    网格布局_列配置 = 杂项类.网格布局_列配置

    函 网格布局_忽略(自身):
        自身.grid_forget()

    函 网格布局_移除(自身):
        自身.grid_remove()

    函 网格布局_信息(自身):
        自身.grid_info()

    网格布局_位置 = 杂项类.网格布局_位置
    网格布局_传播 = 杂项类.网格布局_传播
    网格布局_行配置 = 杂项类.网格布局_行配置
    网格布局_大小 = 杂项类.网格布局_大小
    网格布局_从对象列表 = 杂项类.网格布局_从对象列表


类 基本部件类(tk.BaseWidget, 杂项类):
    
    函 销毁(自身):
        tk.BaseWidget.destroy(自身)


类 部件类(基本部件类, 常规布局类, 位置布局类, 网格布局类):
    无操作


类 顶级窗口类(tk.Toplevel, 基本部件类, 窗口管理类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        tk.Toplevel.__init__(自身, 主对象, 配置, **关键词)


类 按钮类(tk.Button, 部件类):
    """按钮部件."""
    
    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        """在父容器中构建一个按钮部件.

        标准选项:


        该部件特定的选项:

        """
        自身._按钮选项字典 = {
            '命令':     'command', 
            '混合模式': 'compound', 
            '默认':     'default', 
            '高度':     'height',
            '悬停样式': 'overrelief', 
            '重复延迟': 'repeatdelay',
            '重复间隔': 'repeatinterval',             
            '状态':     'state', 
            '宽度':     'width'
        }
        自身._按钮选项字典.更新(_部件通用选项字典)
        自身._按钮选项值字典 = {
            '上方': 'top',
            '下方': 'bottom',
            '无':   'none',
            '正常': 'normal',
            '活动': 'active',
            '禁用': 'disabled'
        }
        自身._按钮选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._按钮选项字典, 自身._按钮选项值字典)
        tk.Button.__init__(自身, 主对象, 配置, **关键词)

    函 闪烁(自身):
        tk.Button.flash(自身)

    函 调用(自身):
        返回 tk.Button.invoke(自身)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._按钮选项字典, 自身._按钮选项值字典)
        返回 tk.Button.configure(自身, 配置, **关键词)


类 画布类(tk.Canvas, 部件类, 视图X类, 视图Y类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._画布选项字典 = {
            '背景色':           'background', 
            '边框宽度':         'borderwidth', 
            '足够近':           'closeenough',
            '限制':             'confine', 
            '鼠标样式':         'cursor', 
            '高度':             'height', 
            '高亮背景':         'highlightbackground', 
            '高亮颜色':         'highlightcolor',
            '高亮厚度':         'highlightthickness', 
            '光标颜色':     'insertbackground', 
            '光标边框宽度':   'insertborderwidth', 
            '光标灭时间':     'insertofftime', 
            '光标亮时间':     'insertontime', 
            '光标宽度':         'insertwidth', 
            '偏移':             'offset', 
            '边框样式':         'relief', 
            '滚动区域':         'scrollregion', 
            '选中时背景色':     'selectbackground', 
            '选中时边框宽度':   'selectborderwidth', 
            '选中时前景色':     'selectforeground', 
            '状态':             'state', 
            '获得焦点':         'takefocus', 
            '宽度':             'width', 
            '水平滚动命令':     'xscrollcommand', 
            '水平滚动增量':     'xscrollincrement', 
            '垂直滚动命令':     'yscrollcommand', 
            '垂直滚动增量':     'yscrollincrement'
        }
        自身._画布选项值字典 = {
            '正常': 'normal',
            '禁用': 'disabled'
        }
        自身._画布选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._画布选项字典, 自身._画布选项值字典)
        tk.Canvas.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._画布选项字典, 自身._画布选项值字典)
        返回 tk.Canvas.configure(自身, 配置, **关键词)

    #函 添加标记(自身, *参数):
    #    tk.Canvas.addtag(自身, *参数)

    函 添加标记_其上(自身, 新标记, 标记或Id):
        tk.Canvas.addtag_above(自身, 新标记, 标记或Id)

    函 添加标记_全部(自身, 新标记):
        tk.Canvas.addtag_all(自身, 新标记)

    函 添加标记_其下(自身, 新标记, 标记或Id):
        tk.Canvas.addtag_below(自身, 新标记, 标记或Id)

    函 添加标记_最接近(自身, 新标记, x, y, 环=空, 起始=空):
        tk.Canvas.addtag_closest(自身, 新标记, x, y, 环, 起始)

    函 添加标记_矩形内(自身, 新标记, x1, y1, x2, y2):
        tk.Canvas.addtag_enclosed(自身, 新标记, x1, y1, x2, y2)

    函 添加标记_交叠(自身, 新标记, x1, y1, x2, y2):
        tk.Canvas.addtag_overlapping(自身, 新标记, x1, y1, x2, y2)

    函 添加标记_有标记(自身, 新标记, 标记或Id):
        tk.Canvas.addtag_withtag(自身, 新标记, 标记或Id)

    函 包围盒(自身, *参数):
        返回 tk.Canvas.bbox(自身, *参数)
    
    函 标记解除绑定(自身, 标记或Id, 序列, 函数id=空):
        tk.Canvas.tag_unbind(自身, 标记或Id, 序列, 函数id)

    函 标记绑定(自身, 标记或Id, 序列=空, 函数=空, 添加=空):
        返回 tk.Canvas.tag_bind(自身, 标记或Id, 序列, 函数, 添加)
    
    函 画布x(自身, 屏幕x, 网格间距=空):
        返回 tk.Canvas.canvasx(自身, 屏幕x, 网格间距)

    函 画布y(自身, 屏幕y, 网格间距=空):
        返回 tk.Canvas.canvasy(自身, 屏幕y, 网格间距)

    函 坐标(自身, *参数):
        返回 tk.Canvas.coords(自身, *参数)

    函 创建弧(自身, *参数, **关键词):
        返回 tk.Canvas.create_arc(自身, *参数, **关键词)

    函 创建位图(自身, *参数, **关键词):
        返回 tk.Canvas.create_bitmap(自身, *参数, **关键词)

    函 创建图像(自身, *参数, **关键词):
        返回 tk.Canvas.create_image(自身, *参数, **关键词)

    函 创建线段(自身, *参数, **关键词):
        返回 tk.Canvas.create_line(自身, *参数, **关键词)

    函 创建椭圆(自身, *参数, **关键词):
        返回 tk.Canvas.create_oval(自身, *参数, **关键词)

    函 创建多边形(自身, *参数, **关键词):
        返回 tk.Canvas.create_polygon(自身, *参数, **关键词)

    函 创建矩形(自身, *参数, **关键词):
        返回 tk.Canvas.create_rectangle(自身, *参数, **关键词)

    函 创建文本(自身, *参数, **关键词):
        返回 tk.Canvas.create_text(自身, *参数, **关键词)

    函 创建窗口(自身, *参数, **关键词):
        返回 tk.Canvas.create_window(自身, *参数, **关键词)

    函 删字符(自身, *参数):
        返回 tk.Canvas.dchars(自身, *参数)

    函 删除(自身, *参数):
        返回 tk.Canvas.delete(自身, *参数)

    函 删标记(自身, *参数):
        返回 tk.Canvas.dtag(自身, *参数)

    函 查找(自身, *参数):
        返回 tk.Canvas.find(自身, *参数)

    函 查找其上(自身, 标记或Id):
        返回 tk.Canvas.find_above(自身, 标记或Id)

    函 查找全部(自身):
        返回 tk.Canvas.find_all(自身)

    函 查找其下(自身, 标记或Id):
        返回 tk.Canvas.find_below(自身, 标记或Id)

    函 查找最接近(自身,  x, y, 环=空, 起始=空):
        返回 tk.Canvas.find_closest(自身, x, y, 环, 起始)

    函 查找矩形内(自身, x1, y1, x2, y2):
        返回 tk.Canvas.find_enclosed(自身,  x1, y1, x2, y2)

    函 查找交叠(自身, x1, y1, x2, y2):
        返回 tk.Canvas.find_overlapping(自身, x1, y1, x2, y2)

    函 查找有标记(自身, 标记或Id):
        返回 tk.Canvas.find_withtag(自身, 标记或Id)

    函 焦点(自身, *参数):
        返回 tk.Canvas.focus(自身, *参数)

    函 获取标记(自身, *参数):
        返回 tk.Canvas.gettags(自身, *参数)

    函 插入光标(自身, *参数):
        tk.Canvas.icursor(自身, *参数)

    函 光标位置(自身, *参数):
        返回 tk.Canvas.index(自身, *参数)

    函 插入(自身, *参数):
        tk.Canvas.insert(自身, *参数)

    函 项获取(自身, 标记或Id, 选项):
        返回 tk.Canvas.itemcget(自身, 标记或Id, 选项)

    函 项配置(自身, 标记或Id, 配置=空, **关键词):
        返回 tk.Canvas.itemconfigure(自身, 标记或Id, 配置, **关键词)

    函 降低标记(自身, *参数):
        tk.Canvas.tag_lower(自身, *参数)

    函 移动(自身, *参数):
        tk.Canvas.move(自身, *参数)

    函 移至(自身, 标记或Id, x='', y=''):
        tk.Canvas.move(自身, 标记或Id, x='', y='')

    # postscript

    函 提升标记(自身, *参数):
        tk.Canvas.tag_raise(自身, *参数)

    函 缩放(自身, *参数):
        tk.Canvas.scale(自身, *参数)

    函 滚动画布_标志(自身, x, y):
        tk.Canvas.scan_mark(自身, x, y)

    函 滚动画布_拖至(自身, x, y, 倍数=10):
        tk.Canvas.scan_dragto(自身, x, y, 倍数)

    函 选定内容_调整(自身, 标记或Id, 索引):
        tk.Canvas.select_adjust(自身, 标记或Id, 索引)

    函 选定内容_清除(自身):
        tk.Canvas.select_clear(自身)

    函 选定内容_从(自身, 标记或Id, 索引):
        tk.Canvas.select_from(自身, 标记或Id, 索引)
    
    函 选定内容_项(自身):
        返回 tk.Canvas.select_item(自身)

    函 选定内容_到(自身, 标记或Id, 索引):
        tk.Canvas.select_to(自身, 标记或Id, 索引)

    函 类型(自身, 标记或Id):
        返回 tk.Canvas.type(自身, 标记或Id)


类 复选按钮类(tk.Checkbutton, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        """
        """
        自身._复选按钮选项字典 = {
            '命令':         'command', 
            '混合模式':     'compound', 
            '高度':         'height',
            '指示开':       'indicatoron',
            '未选中值':     'offvalue', 
            '选中值':       'onvalue',
            '选中时颜色':   'selectcolor', 
            '选中时图像':   'selectimage',
            '状态':         'state', 
            '变量':         'variable',
            '宽度':         'width'
        }
        自身._复选按钮选项字典.更新(_部件通用选项字典)
        自身._复选按钮选项值字典 = {
            '上方': 'top',
            '下方': 'bottom',
            '无':   'none',
            '正常': 'normal',
            '活动': 'active',
            '禁用': 'disabled'
        }
        自身._复选按钮选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._复选按钮选项字典, 自身._复选按钮选项值字典)
        tk.Checkbutton.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._复选按钮选项字典, 自身._复选按钮选项值字典)
        返回 tk.Checkbutton.configure(自身, 配置, **关键词)

    函 取消选择(自身):
        tk.Checkbutton.deselect(自身)   

    函 闪烁(自身):
        tk.Checkbutton.flash(自身)   

    函 调用(自身):
        返回 tk.Checkbutton.invoke(自身)   

    函 选择(自身):
        tk.Checkbutton.select(自身)   

    函 切换(自身):
        tk.Checkbutton.toggle(自身)   


类 输入框类(tk.Entry, 部件类, 视图X类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        """
        """
        自身._输入框选项字典 = {
            '背景色':           'background',
            '边框宽度':         'borderwidth', 
            '鼠标样式':         'cursor',
            '选定内容可复制':    'exportselection', 
            '字体':             'font', 
            '前景色':           'foreground',
            '高亮背景':         'highlightbackground', 
            '高亮颜色':         'highlightcolor',
            '高亮厚度':         'highlightthickness', 
            '光标颜色':         'insertbackground', 
            '光标边框宽度':     'insertborderwidth', 
            '光标灭时间':       'insertofftime', 
            '光标亮时间':       'insertontime', 
            '光标宽度':         'insertwidth', 
            '无效命令':         'invalidcommand', 
            '对齐':             'justify',
            '边框样式':         'relief', 
            '选中时背景色':     'selectbackground', 
            '选中时边框宽度':   'selectborderwidth', 
            '选中时前景色':     'selectforeground',  
            '显示':             'show', 
            '状态':             'state', 
            '获得焦点':         'takefocus',
            '文本变量':         'textvariable', 
            '验证':             'validate', 
            '验证命令':         'validatecommand', 
            '宽度':             'width',
            '水平滚动命令':     'xscrollcommand', 
        }
        #自身._输入框选项字典.更新(_部件通用选项字典)
        自身._输入框选项值字典 = {
            '正常': 'normal',
            '只读': 'readonly',
            '禁用': 'disabled'
        }
        自身._输入框选项值字典.更新(_颜色字典)
        自身._输入框选项值字典.更新(_边框样式字典)
        自身._输入框选项值字典.更新(_验证字典)
        自身._输入框选项值字典.更新(_对齐字典)
        关键词 = _关键词参数中转英(关键词, 自身._输入框选项字典, 自身._输入框选项值字典)
        tk.Entry.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._输入框选项字典, 自身._输入框选项值字典)
        返回 tk.Entry.configure(自身, 配置, **关键词)

    函 删除(自身, 首, 尾=空):
        如果 尾 == '末尾':
            尾 = 'end'
        tk.Entry.delete(自身, 首, 尾)

    函 获取(自身):
        返回 tk.Entry.get(自身)

    函 插入光标(自身, 索引):
        tk.Entry.icursor(自身, 索引)

    函 光标位置(自身, 索引):
        返回 tk.Entry.index(自身, 索引)

    函 插入(自身, 索引, 字符串):
        如果 索引 == '末尾':
            索引 = 'end'
        或如 索引 == '光标':
            索引 = 'insert'
        tk.Entry.insert(自身, 索引, 字符串)

    函 滚动画布_标志(自身, x):
        tk.Entry.scan_mark(自身, x)

    函 滚动画布_拖至(自身, x):
        tk.Entry.scan_dragto(自身, x)

    函 选定内容_调整(自身, 索引):
        tk.Entry.selection_adjust(自身, 索引)

    函 选定内容_清除(自身):
        tk.Entry.selection_clear(自身)

    函 选定内容_从(自身, 索引):
        tk.Entry.selection_from(自身, 索引)

    函 选定内容_存在(自身):
        返回 tk.Entry.selection_present(自身)

    函 选定内容_范围(自身, 起, 止):
        tk.Entry.selection_range(自身, 起, 止)

    函 选定内容_到(自身, 索引):
        tk.Entry.selection_to(自身, 索引)


类 框架类(tk.Frame, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._框架选项字典 = {
            '背景色':       'background', 
            '边框宽度':     'borderwidth',
            '类_':          'class_',
            '颜色映射':     'colormap', 
            '容器':         'container', 
            '鼠标样式':     'cursor',
            '高度':         'height', 
            '高亮背景':     'highlightbackground', 
            '高亮颜色':     'highlightcolor',
            '高亮厚度':     'highlightthickness', 
            '水平边距':     'padx', 
            '垂直边距':     'pady', 
            '边框样式':     'relief', 
            '获得焦点':     'takefocus', 
            '视觉':         'visual', 
            '宽度':         'width'
        }
        #自身._框架选项字典.更新(_部件通用选项字典)
        自身._框架选项值字典 = {

        }
        自身._框架选项值字典.更新(_颜色字典)
        自身._框架选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._框架选项字典, 自身._框架选项值字典)
        tk.Frame.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._框架选项字典, 自身._框架选项值字典)
        返回 tk.Frame.configure(自身, 配置, **关键词)


类 标签类(tk.Label, 部件类):
    
    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._标签选项字典 = {
            '混合模式': 'compound',
            '高度':     'height', 
            '状态':     'state', 
            '宽度':     'width'
        }
        自身._标签选项字典.更新(_部件通用选项字典)
        自身._标签选项值字典 = {
            '上方': 'top',
            '下方': 'bottom',
            '无':   'none',
            '正常': 'normal',
            '活动': 'active',
            '禁用': 'disabled'
        }
        自身._标签选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._标签选项字典, 自身._标签选项值字典)
        tk.Label.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._标签选项字典, 自身._标签选项值字典)
        返回 tk.Label.configure(自身, 配置, **关键词)


类 列表框类(tk.Listbox, 部件类, 视图X类, 视图Y类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._列表框选项字典 = {
            '背景色':       'background',
            '边框宽度':     'borderwidth', 
            '鼠标样式':     'cursor',
            '选定内容可复制':    'exportselection',  
            '字体':         'font', 
            '前景色':       'foreground',
            '高度':         'height', 
            '高亮背景':     'highlightbackground', 
            '高亮颜色':     'highlightcolor',
            '高亮厚度':     'highlightthickness',
            '边框样式':     'relief',
            '选中时背景色':     'selectbackground', 
            '选中时边框宽度':   'selectborderwidth', 
            '选中时前景色':     'selectforeground', 
            '选择模式':         'selectmode', 
            '设置网格':         'setgrid', 
            '获得焦点':         'takefocus',
            '宽度':             'width', 
            '水平滚动命令':      'xscrollcommand', 
            '垂直滚动命令':      'yscrollcommand', 
            '列表变量':         'listvariable'
        }
        #自身._列表框选项字典.更新(_部件通用选项字典)
        自身._列表框选项值字典 = {
            '单选': 'single',
            '浏览': 'browse',
            '多选': 'multiple',
            '扩展': 'extended'
        }
        自身._列表框选项值字典.更新(_颜色字典)
        自身._列表框选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._列表框选项字典, 自身._列表框选项值字典)
        tk.Listbox.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._列表框选项字典, 自身._列表框选项值字典)
        返回 tk.Listbox.configure(自身, 配置, **关键词)

    函 激活(自身, 索引):
        tk.Listbox.activate(自身, 索引)

    函 包围盒(自身, 索引):
        返回 tk.Listbox.bbox(自身, 索引)

    函 当前选择(自身):
        返回 tk.Listbox.curselection(自身)

    函 删除(自身, 首, 尾=空):
        如果 尾 == '末尾':
            尾 = 'end'
        tk.Listbox.delete(自身, 首, 尾)

    函 获取(自身, 首, 尾=空):
        返回 tk.Listbox.get(自身, 首, 尾)

    函 索引(自身, 索引):
        如果 索引 == '末尾':
            索引 = 'end'
        返回 tk.Listbox.index(自身, 索引)

    函 插入(自身, 索引, *元素):
        如果 索引 == '末尾':
            索引 = 'end'
        tk.Listbox.insert(自身, 索引, *元素)

    函 最近(自身, y):
        返回 tk.Listbox.nearest(自身, y)

    函 滚动画布_标志(自身, x, y):
        tk.Listbox.scan_mark(自身, x, y)

    函 滚动画布_拖至(自身, x, y):
        tk.Listbox.scan_dragto(自身, x, y)

    函 看见(自身, 索引):
        tk.Listbox.see(自身, 索引)

    函 选定内容_锚点(自身, 索引):
        tk.Listbox.selection_anchor(自身, 索引)
    
    函 选定内容_清除(自身, 首, 尾=空):
        tk.Listbox.selection_clear(自身, 首, 尾)

    函 选定内容_包括(自身, 索引):
        返回 tk.Listbox.selection_includes(自身, 索引)

    函 选定内容_设置(自身, 首, 尾=空):
        tk.Listbox.selection_set(自身, 首, 尾)

    函 大小(自身):
        返回 tk.Listbox.size(自身)

    函 项获取(自身, 索引, 选项):
        返回 tk.Listbox.itemcget(自身, 索引, 选项)

    函 项配置(自身, 索引, 配置=空, **关键词):
        返回 tk.Listbox.itemconfigure(自身, 索引, 配置, **关键词)


类 菜单类(tk.Menu, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._菜单选项字典 = {
            '活动背景色':   'activebackground', 
            '活动边框宽度': 'activeborderwidth',
            '活动前景色':   'activeforeground',
            '背景色':       'background',
            '边框宽度':     'borderwidth', 
            '鼠标样式':     'cursor',
            '禁用时前景色':  'disabledforeground', 
            '字体':         'font', 
            '前景色':       'foreground',
            '打开后命令':   'postcommand', 
            '边框样式':     'relief', 
            '选中时颜色':   'selectcolor', 
            '获得焦点':     'takefocus',  
            '撕下':         'tearoff', 
            '撕下时命令':   'tearoffcommand', 
            '标题':         'title', 
            '类型_':        'type_'
        }
        #自身._菜单选项字典.更新(_部件通用选项字典)
        自身._菜单选项值字典 = {
            '上方': 'top',
            '下方': 'bottom',
            '无':   'none',
            '正常': 'normal',
            '活动': 'active',
            '禁用': 'disabled'
        }
        自身._菜单选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._菜单选项字典, 自身._菜单选项值字典)
        tk.Menu.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._菜单选项字典, 自身._菜单选项值字典)
        返回 tk.Menu.configure(自身, 配置, **关键词)

    # tk_popup

    函 激活(自身, 索引):
        tk.Menu.activate(自身, 索引)

    函 添加(自身, 项类型, 配置={}, **关键词):
        tk.Menu.add(自身, 项类型, 配置, **关键词)

    函 添加子菜单(自身, 配置={}, **关键词):
        关键词 = _关键词参数中转英(关键词, _菜单配置选项字典)
        tk.Menu.add_cascade(自身, 配置, **关键词)

    函 添加复选按钮(自身, 配置={}, **关键词):
        _菜单配置选项字典.更新({'变量': 'variable', '值': 'value'})
        关键词 = _关键词参数中转英(关键词, _菜单配置选项字典)
        tk.Menu.add_checkbutton(自身, 配置, **关键词)

    函 添加命令(自身, 配置={}, **关键词):
        关键词 = _关键词参数中转英(关键词, _菜单配置选项字典)
        tk.Menu.add_command(自身, 配置, **关键词)

    函 添加单选按钮(自身, 配置={}, **关键词):
        _菜单配置选项字典.更新({'变量': 'variable', '值': 'value'})
        关键词 = _关键词参数中转英(关键词, _菜单配置选项字典)
        tk.Menu.add_radiobutton(自身, 配置, **关键词)

    函 添加分割线(自身, 配置={}, **关键词):
        tk.Menu.add_separator(自身, 配置, **关键词)
    
    函 插入(自身, 索引, 项类型, 配置={}, **关键词):
        tk.Menu.insert(自身, 索引, 项类型, 配置, **关键词)
    
    函 插入子菜单(自身, 索引, 配置={}, **关键词):
        tk.Menu.insert_cascade(自身, 索引, 配置, **关键词)

    函 插入复选按钮(自身, 索引, 配置={}, **关键词):
        tk.Menu.insert_checkbutton(自身, 索引, 配置, **关键词)

    函 插入命令(自身, 索引, 配置={}, **关键词):
        tk.Menu.insert_command(自身, 索引, 配置, **关键词)

    函 插入单选按钮(自身, 索引, 配置={}, **关键词):
        tk.Menu.insert_radiobutton(自身, 索引, 配置, **关键词)

    函 插入分割线(自身, 索引, 配置={}, **关键词):
        tk.Menu.insert_separator(自身, 索引, 配置, **关键词)

    函 删除(自身, 索引1, 索引2=空):
        tk.Menu.delete(自身, 索引1, 索引2)

    函 条目获取(自身, 索引, 选项):
        返回 tk.Menu.entrycget(自身, 索引, 选项)

    函 条目配置(自身, 索引, 配置=空, **关键词):
        返回 tk.Menu.entryconfigure(自身, 索引, 配置=空, **关键词)

    函 索引(自身, 索引):
        返回 tk.Menu.index(自身, 索引)

    函 调用(自身, 索引):
        返回 tk.Menu.invoke(自身, 索引)

    函 弹出(自身, x, y):
        tk.Menu.post(自身, x, y)
    
    函 类型(自身, 索引):
        返回 tk.Menu.type(自身, 索引)
    
    函 移除弹出菜单(自身):
        tk.Menu.post(自身)

    函 x位置(自身, 索引):
        返回 tk.Menu.xposition(自身, 索引)

    函 y位置(自身, 索引):
        返回 tk.Menu.yposition(自身, 索引)


类 单选按钮类(tk.Radiobutton, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._单选按钮选项字典 = {
            '命令':         'command', 
            '混合模式':     'compound', 
            '高度':         'height',
            '指示开':       'indicatoron',
            '选中时颜色':   'selectcolor', 
            '选中时图像':   'selectimage',
            '状态':         'state',  
            '值':           'value', 
            '变量':         'variable',
            '宽度':         'width' 
        }
        自身._单选按钮选项字典.更新(_部件通用选项字典)
        自身._单选按钮选项值字典 = {
            '上方': 'top',
            '下方': 'bottom',
            '无':   'none',
            '正常': 'normal',
            '活动': 'active',
            '禁用': 'disabled'
        }
        自身._单选按钮选项值字典.更新(_部件通用选项值字典)
        关键词 = _关键词参数中转英(关键词, 自身._单选按钮选项字典, 自身._单选按钮选项值字典)
        tk.Radiobutton.__init__(自身, 主对象, 配置, **关键词)

    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._单选按钮选项字典, 自身._单选按钮选项值字典)
        返回 tk.Radiobutton.configure(自身, 配置, **关键词)

    函 取消选择(自身):
        tk.Radiobutton.deselect(自身)   

    函 闪烁(自身):
        tk.Radiobutton.flash(自身)   

    函 调用(自身):
        返回 tk.Radiobutton.invoke(自身)   

    函 选择(自身):
        tk.Radiobutton.select(自身)   


类 刻度条类(tk.Scale, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._刻度条选项字典 = {
            '活动背景色':   'activebackground',
            '背景色':       'background',
            '大增量':       'bigincrement', 
            '边框宽度':     'borderwidth',
            '命令':         'command', 
            '鼠标样式':     'cursor',
            '位数':         'digits', 
            '字体':         'font', 
            '前景色':       'foreground',
            '起':          'from_',
            '高亮背景':     'highlightbackground', 
            '高亮颜色':     'highlightcolor',
            '高亮厚度':     'highlightthickness',
            '标签':         'label',
            '长度':         'length', 
            '方向':         'orient', 
            '边框样式':     'relief', 
            '重复延迟':     'repeatdelay',
            '重复间隔':     'repeatinterval',
            '分辨率':       'resolution',
            '显示值':       'showvalue', 
            '滑块长度':      'sliderlength', 
            '滑块样式':      'sliderrelief', 
            '状态':         'state', 
            '获得焦点':     'takefocus',
            '刻度间隔':     'tickinterval', 
            '止':           'to', 
            '凹槽颜色':     'troughcolor', 
            '变量':         'variable', 
            '宽度':         'width'
        }
        #自身._刻度条选项字典.更新(_部件通用选项字典)
        自身._刻度条选项值字典 = {
            '横向': 'horizontal',
            '纵向': 'vertical'
        }
        自身._刻度条选项值字典.更新(_颜色字典)
        自身._刻度条选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._刻度条选项字典, 自身._刻度条选项值字典)
        tk.Scale.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._刻度条选项字典, 自身._刻度条选项值字典)
        返回 tk.Scale.configure(自身, 配置, **关键词)

    函 获取(自身):
        返回 tk.Scale.get(自身)
    
    函 设置(自身, 值):
        tk.Scale.set(自身, 值)

    函 坐标(自身, 值=空):
        返回 tk.Scale.coords(自身, 值)

    函 识别(自身, x, y):
        返回 tk.Scale.identify(自身, x, y)


类 滚动条类(tk.Scrollbar, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._滚动条选项字典 = {
            '活动背景色':   'activebackground', 
            '活动滑块样式': 'activerelief',
            '背景色':       'background',  
            '边框宽度':     'borderwidth', 
            '命令':         'command', 
            '鼠标样式':     'cursor',
            '元素边框宽度': 'elementborderwidth', 
            '高亮背景':     'highlightbackground', 
            '高亮颜色':     'highlightcolor',
            '高亮厚度':     'highlightthickness', 
            '跳过':         'jump', 
            '方向':         'orient',
            '边框样式':     'relief', 
            '重复延迟':     'repeatdelay',
            '重复间隔':     'repeatinterval',
            '获得焦点':     'takefocus', 
            '凹槽颜色':     'troughcolor', 
            '宽度':         'width'
        }
        #自身._滚动条选项字典.更新(_部件通用选项字典)
        自身._滚动条选项值字典 = {
            '横向': 'horizontal',
            '纵向': 'vertical'
        }
        自身._滚动条选项值字典.更新(_颜色字典)
        自身._滚动条选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._滚动条选项字典, 自身._滚动条选项值字典)
        tk.Scrollbar.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._滚动条选项字典, 自身._滚动条选项值字典)
        返回 tk.Scrollbar.configure(自身, 配置, **关键词)

    函 激活(自身, 索引=空):
        tk.Scrollbar.activate(自身, 索引)
    
    函 移动量(自身, 移动量x, 移动量y):
        返回 tk.Scrollbar.delta(自身, 移动量x, 移动量y)

    函 分数(自身, x, y):
        返回 tk.Scrollbar.fraction(自身, x, y)

    函 识别(自身, x, y):
        返回 tk.Scrollbar.identify(自身, x, y)

    函 获取(自身):
        返回 tk.Scrollbar.get(自身)
    
    函 设置(自身, 首, 尾):
        tk.Scrollbar.set(自身, 首, 尾)


类 文本框类(tk.Text, 部件类, 视图X类, 视图Y类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._文本框选项字典 = {
            '背景色':           'background',
            '边框宽度':         'borderwidth', 
            '鼠标样式':         'cursor',
            '选定内容可复制':    'exportselection', 
            '字体':             'font', 
            '前景色':           'foreground',
            '高亮背景':         'highlightbackground', 
            '高亮颜色':         'highlightcolor',
            '高亮厚度':         'highlightthickness', 
            '光标颜色':         'insertbackground', 
            '光标边框宽度':     'insertborderwidth', 
            '光标灭时间':       'insertofftime', 
            '光标亮时间':       'insertontime', 
            '光标宽度':         'insertwidth', 
            '水平边距':         'padx', 
            '垂直边距':         'pady',
            '边框样式':         'relief', 
            '选中时背景色':     'selectbackground', 
            '选中时边框宽度':   'selectborderwidth', 
            '选中时前景色':     'selectforeground',  
            '设置网格':         'setgrid', 
            '获得焦点':         'takefocus',
            '水平滚动命令':     'xscrollcommand',
            '垂直滚动命令':     'yscrollcommand',
            '自动分隔符':       'autoseparators', 
            '高度':             'height', 
            '最大撤消次数':     'maxundo',
            '间距1':            'spacing1', 
            '间距2':            'spacing2', 
            '间距3':            'spacing3',
            '状态':             'state', 
            '制表符宽':         'tabs', 
            '撤消':             'undo', 
            '宽度':             'width', 
            '自动换行':         'wrap'
        }
        #自身._文本框选项字典.更新(_部件通用选项字典)
        自身._文本框选项值字典 = {
            '正常': 'normal',
            '禁用': 'disabled',
            '无':   'none',
            '字符': 'char',
            '单词': 'word'
        }
        自身._文本框选项值字典.更新(_颜色字典)
        自身._文本框选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._文本框选项字典, 自身._文本框选项值字典)
        tk.Text.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._文本框选项字典, 自身._文本框选项值字典)
        返回 tk.Text.configure(自身, 配置, **关键词)

    函 包围盒(自身, 索引):
        返回 tk.Text.bbox(自身, 索引)

    函 比较(自身, 索引1, 操作符, 索引2):
        返回 tk.Text.compare(自身, 索引1, 操作符, 索引2)

    函 计数(自身, 索引1, 索引2, *参数):
        返回 tk.Text.count(自身, 索引1, 索引2, *参数)

    函 调试(自身, 布尔值=空):
        返回 tk.Text.debug(自身, 布尔值)

    函 删除(自身, 索引1, 索引2=空):
        如果 索引2 == '末尾':
            索引2 = 'end'
        tk.Text.delete(自身, 索引1, 索引2)

    函 行信息(自身, 索引):
        返回 tk.Text.dlineinfo(自身, 索引)

    函 转储(自身, 索引1, 索引2=空, 命令=空, **关键词):
        返回 tk.Text.dump(自身, 索引1, 索引2, 命令, **关键词)

    函 编辑(自身, *参数):
        返回 tk.Text.edit(自身, *参数)

    函 编辑_修改(自身, 参数=空):
        返回 tk.Text.edit_modified(自身, 参数)

    函 编辑_恢复(自身):
        返回 tk.Text.edit_redo(自身)

    函 编辑_重置(自身):
        返回 tk.Text.edit_reset(自身)

    函 编辑_分隔符(自身):
        返回 tk.Text.edit_separator(自身)

    函 编辑_撤消(自身):
        返回 tk.Text.edit_undo(自身)

    函 获取(自身, 索引1, 索引2=空):
        返回 tk.Text.get(自身, 索引1, 索引2)

    函 图像获取(自身, 索引, 选项):
        返回 tk.Text.image_cget(自身, 索引, 选项)

    函 图像配置(自身, 索引, 配置=空, **关键词):
        返回 tk.Text.image_configure(自身, 索引, 配置, **关键词)

    函 图像创建(自身, 索引, 配置={}, **关键词):
        返回 tk.Text.image_create(自身, 索引, 配置, **关键词)

    函 图像名称(自身):
        返回 tk.Text.image_names(自身)

    函 索引(自身, 索引):
        返回 tk.Text.index(自身, 索引)

    函 插入(自身, 索引, 字符串, *参数):
        如果 索引 == '末尾':
            索引 = 'end'
        或如 索引 == '光标':
            索引 = 'insert'
        返回 tk.Text.insert(自身, 索引, 字符串, *参数)

    函 标志方向(自身, 标志名称, 方向=空):
        返回 tk.Text.mark_gravity(自身, 标志名称, 方向)

    函 标志名称(自身):
        返回 tk.Text.mark_names(自身)

    函 设置标志(自身, 标志名, 索引):
        tk.Text.mark_set(自身, 标志名, 索引)

    函 取消设置标志(自身, *标志名):
        tk.Text.mark_unset(自身, *标志名)

    函 下一标志(自身, 索引):
        返回 tk.Text.mark_next(自身, 索引)

    函 上一标志(自身, 索引):
        返回 tk.Text.mark_previous(自身, 索引)

    函 创建同伴(自身, 新路径名, 配置={}, **关键词):
        tk.Text.peer_create(自身, 新路径名, 配置={}, **关键词)

    函 同伴名称(自身):
        返回 tk.Text.peer_names(自身)

    函 替换(自身, 索引1, 索引2, 字符串, *参数):
        tk.Text.replace(自身, 索引1, 索引2, 字符串, *参数)

    函 滚动画布_标志(自身, x, y):
        tk.Text.scan_mark(自身, x, y)

    函 滚动画布_拖至(自身, x, y):
        tk.Text.scan_dragto(自身, x, y)

    函 搜索(自身, 模式, 索引, 停止索引=空, 向前=空, 
            向后=空, 完全匹配=空, 正则表达式=空, 
            忽略大小写=空, 个数=空, elide=空):
        返回 tk.Text.search(自身, 模式, 索引, 停止索引, 向前, 
                            向后, 完全匹配, 正则表达式, 
                            忽略大小写, 个数, elide)

    函 看见(自身, 索引):
        tk.Text.see(自身, 索引)

    函 标记_添加(自身, 标记名称, 索引1, *参数):
        tk.Text.tag_add(自身, 标记名称, 索引1, *参数)

    函 标记_解除绑定(自身, 标记名称, 序列, 函数id=空):
        tk.Text.tag_unbind(自身, 标记名称, 序列, 函数id)

    函 标记_绑定(自身, 标记名称, 序列, 函数, 添加=空):
        返回 tk.Text.tag_bind(自身, 标记名称, 序列, 函数, 添加)

    函 标记_获取(自身, 标记名称, 选项):
        返回 tk.Text.tag_cget(自身, 标记名称, 选项)

    函 标记_配置(自身, 标记名称, 配置=空, **关键词):
        返回 tk.Text.tag_configure(自身, 标记名称, 配置, **关键词)

    函 标记_删除(自身, *标记名称):
        tk.Text.tag_delete(自身, *标记名称)

    函 标记_降低(自身, 标记名称, 低于此=空):
        tk.Text.tag_lower(自身, 标记名称, 低于此)

    函 标记_名称(自身, 索引=空):
        返回 tk.Text.tag_names(自身, 索引)

    函 标记_下一范围(自身, 标记名称, 索引1, 索引2=空):
        返回 tk.Text.tag_nextrange(自身, 标记名称, 索引1, 索引2)

    函 标记_上一范围(自身, 标记名称, 索引1, 索引2=空):
        返回 tk.Text.tag_prevrange(自身, 标记名称, 索引1, 索引2)

    函 标记_提升(自身, 标记名称, 高于此=空):
        tk.Text.tag_raise(自身, 标记名称, 高于此)

    函 标记_范围(自身, 标记名称):
        返回 tk.Text.tag_ranges(自身, 标记名称)

    函 标记_移除(自身, 标记名称, 索引1, 索引2=空):
        tk.Text.tag_remove(自身, 标记名称, 索引1, 索引2)

    函 窗口获取(自身, 索引, 选项):
        返回 tk.Text.window_cget(自身, 索引, 选项)

    函 窗口配置(自身, 索引, 配置=空, **关键词):
        返回 tk.Text.window_configure(自身, 索引, 配置, **关键词)

    函 窗口创建(自身, 索引, 配置={}, **关键词):
        tk.Text.window_create(自身, 索引, 配置, **关键词)

    函 窗口名称(自身):
        返回 tk.Text.window_names(自身)


类 选项菜单类(tk.OptionMenu, 菜单类):
    """事实上是下拉菜单的改版，它弥补了列表框无法实现下拉列表框的遗憾.\n
    <变量> - 用于存放选定的选项值.\n
    <值>/<其他值> - 选项值
    """
    函 __init__(自身, 主对象, 变量, 值, *其他值, **关键词):
        tk.OptionMenu.__init__(自身, 主对象, 变量, 值, *其他值, **关键词)

    函 销毁(自身):
        tk.OptionMenu.destroy(自身)


类 图像类(tk.Image):
    '''图像基类'''

    函 __init__(自身, 图像类型, 名称=空, 配置={}, 主对象=空, **关键词):
        超类().__init__(图像类型, 名称, 配置, 主对象, **关键词)

    函 配置(自身, **关键词):
        超类().configure(**关键词)

    函 高度(自身):
        返回 超类().height()

    函 类型(自身):
        返回 超类().type()

    函 宽度(自身):
        返回 超类().width()


类 照片图像类(tk.PhotoImage, 图像类):
    
    函 __init__(自身, 名称=空, 配置={}, 主对象=空, **关键词):
        tk.PhotoImage.__init__(自身, 名称, 配置, 主对象, **关键词)

    函 空白(自身):
        tk.PhotoImage.blank(自身)

    函 获取配置(自身, 选项): # ?
        返回 tk.PhotoImage.cget(自身, 选项)

    函 复制(自身):
        返回 tk.PhotoImage.copy(自身)

    函 缩放(自身, x, y=''):
        返回 tk.PhotoImage.zoom(自身, x, y='')

    函 子样本(自身, x, y=''):
        返回 tk.PhotoImage.subsample(自身, x, y='')

    函 获取(自身, x, y):
        返回 tk.PhotoImage.get(自身, x, y)

    函 放到(自身, 数据, 位置=空):
        返回 tk.PhotoImage.put(自身, 数据, 位置)

    函 写入(自身, 文件名, 格式=空, 从坐标=空):
        返回 tk.PhotoImage.write(自身, 文件名, 格式, 从坐标)
    
    函 获取透明性(自身, x, y):
        返回 tk.PhotoImage.transparency_get(自身, x, y)

    函 设置透明性(自身, x, y, 布尔值):
        返回 tk.PhotoImage.transparency_set(自身, x, y, 布尔值)


类 位图图像类(tk.BitmapImage, 图像类):
    
    函 __init__(自身, 名称=空, 配置={}, 主对象=空, **关键词):
        tk.BitmapImage.__init__(自身, 名称, 配置, 主对象, **关键词)

函 图像名称():
    返回 tk.image_names()

函 图像类型():
    返回 tk.image_types()

类 旋钮控件类(tk.Spinbox, 部件类, 视图X类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._旋钮控件选项字典 = {
            '活动背景色':   'activebackground', 
            '背景色':           'background',
            '边框宽度':         'borderwidth', 
            '鼠标样式':         'cursor',
            '选定内容可复制':    'exportselection', 
            '字体':             'font', 
            '前景色':           'foreground',
            '高亮背景':         'highlightbackground', 
            '高亮颜色':         'highlightcolor',
            '高亮厚度':         'highlightthickness', 
            '光标颜色':         'insertbackground', 
            '光标边框宽度':     'insertborderwidth', 
            '光标灭时间':       'insertofftime', 
            '光标亮时间':       'insertontime', 
            '光标宽度':         'insertwidth', 
            '对齐':             'justify',
            '边框样式':         'relief', 
            '重复延迟':         'repeatdelay',
            '重复间隔':         'repeatinterval',
            '选中时背景色':     'selectbackground', 
            '选中时边框宽度':   'selectborderwidth', 
            '选中时前景色':     'selectforeground',  
            '获得焦点':         'takefocus',
            '文本变量':         'textvariable', 
            '水平滚动命令':     'xscrollcommand', 
            '旋钮背景色':       'buttonbackground', 
            '旋钮鼠标':         'buttoncursor',
            '下箭头样式':       'buttondownrelief', 
            '上箭头样式':       'buttonuprelief',
            '命令':             'command', 
            '禁用时背景色':     'disabledbackground',
            '禁用时前景色':     'disabledforeground', 
            '格式':             'format', 
            '起':               'from_',
            '无效命令':         'invalidcommand', 
            '增量':             'increment',
            '只读背景色':       'readonlybackground', 
            '状态':             'state', 
            '止':               'to',
            '验证':             'validate', 
            '验证命令':         'validatecommand', 
            '值':               'values',
            '宽度':             'width', 
            '换行':             'wrap'
        }
        #自身._旋钮控件选项字典.更新(_部件通用选项字典)
        自身._旋钮控件选项值字典 = {
            '正常': 'normal',
            '只读': 'readonly',
            '禁用': 'disabled'
        }
        自身._旋钮控件选项值字典.更新(_颜色字典)
        自身._旋钮控件选项值字典.更新(_对齐字典)
        自身._旋钮控件选项值字典.更新(_边框样式字典)
        自身._旋钮控件选项值字典.更新(_验证字典)
        关键词 = _关键词参数中转英(关键词, 自身._旋钮控件选项字典, 自身._旋钮控件选项值字典)
        tk.Spinbox.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._旋钮控件选项字典, 自身._旋钮控件选项值字典)
        返回 tk.Spinbox.configure(自身, 配置, **关键词)

    函 包围盒(自身, 索引):
        返回 tk.Spinbox.bbox(自身, 索引)

    函 删除(自身, 首, 尾=空):
        返回 tk.Spinbox.delete(自身, 首, 尾)

    函 获取(自身):
        返回 tk.Spinbox.get(自身)

    函 插入光标(自身, 索引):
        返回 tk.Spinbox.icursor(自身, 索引)

    函 识别(自身, x, y):
        返回 tk.Spinbox.identify(自身, x, y)

    函 索引(自身, 索引):
        返回 tk.Spinbox.index(自身, 索引)

    函 插入(自身, 索引, 字符串):
        返回 tk.Spinbox.insert(自身, 索引, 字符串)

    函 调用(自身, 元素):
        返回 tk.Spinbox.invoke(自身, 元素)

    函 滚动画布(自身, *参数):
        返回 tk.Spinbox.scan(自身, *参数)
    
    函 滚动画布_标志(自身, x):
        返回 tk.Spinbox.scan_mark(自身, x)

    函 滚动画布_拖至(自身, x):
        返回 tk.Spinbox.scan_dragto(自身, x)

    函 选定内容(自身, *参数):
        返回 tk.Spinbox.selection(自身, *参数)

    函 选定内容_调整(自身, 索引):
        返回 tk.Spinbox.selection_adjust(自身, 索引)

    函 选定内容_清除(自身):
        返回 tk.Spinbox.selection_clear(自身)

    函 选定内容_元素(自身, 元素=空):
        返回 tk.Spinbox.selection_element(自身, 元素)

    函 选定内容_从(自身, 索引):
        tk.Spinbox.selection_from(自身, 索引)

    函 选定内容_存在(自身):
        返回 tk.Spinbox.selection_present(自身)

    函 选定内容_范围(自身, 起, 止):
        tk.Spinbox.selection_range(自身, 起, 止)

    函 选定内容_到(自身, 索引):
        tk.Spinbox.selection_to(自身, 索引)


类 标签框架类(tk.LabelFrame, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._标签框架选项字典 = {
            '背景色':       'background', 
            '边框宽度':     'borderwidth',
            '类_':          'class_',
            '颜色映射':     'colormap', 
            '容器':         'container', 
            '鼠标样式':     'cursor',
            '高度':         'height', 
            '高亮背景':     'highlightbackground', 
            '高亮颜色':     'highlightcolor',
            '高亮厚度':     'highlightthickness', 
            '水平边距':     'padx', 
            '垂直边距':     'pady', 
            '边框样式':     'relief', 
            '获得焦点':     'takefocus', 
            '视觉':         'visual', 
            '宽度':         'width',
            '字体':         'font', 
            '前景色':       'foreground',
            '文本':         'text',
            '标签锚点':     'labelanchor', 
            '标签部件':     'labelwidget'
        }
        #自身._标签框架选项字典.更新(_部件通用选项字典)
        自身._标签框架选项值字典 = {

        }
        自身._标签框架选项值字典.更新(_锚点字典)
        自身._标签框架选项值字典.更新(_颜色字典)
        自身._标签框架选项值字典.更新(_边框样式字典)
        关键词 = _关键词参数中转英(关键词, 自身._标签框架选项字典, 自身._标签框架选项值字典)
        tk.LabelFrame.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._标签框架选项字典, 自身._标签框架选项值字典)
        返回 tk.LabelFrame.configure(自身, 配置, **关键词)


类 格窗类(tk.PanedWindow, 部件类):

    函 __init__(自身, 主对象=空, 配置={}, **关键词):
        自身._格窗选项字典 = {
            '背景色':       'background', 
            '边框宽度':     'borderwidth',
            '鼠标样式':     'cursor',
            '高度':         'height', 
            '方向':         'orient', 
            '边框样式':     'relief', 
            '宽度':         'width',
            '手柄边距':     'handlepad', 
            '手柄大小':     'handlesize', 
            '跟随鼠标调整':  'opaqueresize',
            '窗框鼠标':     'sashcursor', 
            '窗框边距':     'sashpad', 
            '窗框样式':     'sashrelief',
            '窗框宽度':     'sashwidth', 
            '显示手柄':     'showhandle'
        }
        #自身._格窗选项字典.更新(_部件通用选项字典)
        自身._格窗选项值字典 = {
            '横向': 'horizontal',
            '纵向': 'vertical'
        }
        自身._格窗选项值字典.更新(_边框样式字典)
        自身._格窗选项值字典.更新(_颜色字典)
        关键词 = _关键词参数中转英(关键词, 自身._格窗选项字典, 自身._格窗选项值字典)
        tk.PanedWindow.__init__(自身, 主对象, 配置, **关键词)
    
    函 配置(自身, 配置=空, **关键词):
        关键词 = _关键词参数中转英(关键词, 自身._格窗选项字典, 自身._格窗选项值字典)
        返回 tk.PanedWindow.configure(自身, 配置, **关键词)

    函 添加(自身, 子部件, **关键词):
        tk.PanedWindow.add(自身, 子部件, **关键词)
    
    函 移除(自身, 子部件):
        tk.PanedWindow.remove(自身, 子部件)

    函 识别(自身, x, y):
        返回 tk.PanedWindow.identify(自身, x, y)

    函 代理(自身, *参数):
        返回 tk.PanedWindow.proxy(自身, *参数)
    
    函 代理坐标(自身):
        返回 tk.PanedWindow.proxy_coord(自身)

    函 移除代理(自身):
        返回 tk.PanedWindow.proxy_forget(自身)

    函 放置代理(自身, x, y):
        返回 tk.PanedWindow.proxy_place(自身, x, y)

    函 窗框(自身, *参数):
        返回 tk.PanedWindow.sash(自身, *参数)
    
    函 窗框坐标(自身, 索引):
        返回 tk.PanedWindow.sash_coord(自身, 索引)

    函 窗框标志(自身, 索引):
        返回 tk.PanedWindow.sash_mark(自身, 索引)

    函 放置窗框(自身, 索引, x, y):
        返回 tk.PanedWindow.sash_place(自身, 索引, x, y)

    函 窗格配置获取(自身, 子部件, 选项):
        返回 tk.PanedWindow.panecget(自身, 子部件, 选项)

    函 窗格配置(自身, 标记或Id, 配置=空, **关键词):
        返回 tk.PanedWindow.paneconfigure(自身, 标记或Id, 配置, **关键词)

    函 窗格(自身):
        返回 tk.PanedWindow.panes(自身)


# 各种消息框

函 消息框_信息(标题=空, 消息=空, **选项):
    "显示一条普通消息"
    返回 _msgbox.showinfo(标题, 消息, **选项)

函 消息框_警告(标题=空, 消息=空, **选项):
    "显示一条警告消息"
    返回 _msgbox.showwarning(标题, 消息, **选项)

函 消息框_出错(标题=空, 消息=空, **选项):
    "显示一条出错消息"
    返回 _msgbox.showerror(标题, 消息, **选项)

函 消息框_询问(标题=空, 消息=空, **选项):
    "问一个问题"
    返回 '是' 如果 _msgbox.askquestion(标题, 消息, **选项) == 'yes' 否则 '否'

函 消息框_确定取消(标题=空, 消息=空, **选项):
    "询问是否执行操作; 如果答案为 <确定> 则返回 <真> "
    返回 _msgbox.askokcancel(标题, 消息, **选项)

函 消息框_是否(标题=空, 消息=空, **选项):
    "问一个问题; 如果答案为 <是> 则返回 <真> "
    返回 _msgbox.askyesno(标题, 消息, **选项)

函 消息框_是否取消(标题=空, 消息=空, **选项):
    "问一个问题; 如果答案为 <是> 则返回 <真>, 如果取消则返回<空> "
    返回 _msgbox.askyesnocancel(标题, 消息, **选项)

函 消息框_重试取消(标题=空, 消息=空, **选项):
    "询问是否再次尝试执行操作; 如果答案为 <是> 则返回 <真> "
    返回 _msgbox.askretrycancel(标题, 消息, **选项)






