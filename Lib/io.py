"""io 模块, 提供流处理的 Python 接口. 预置的 open|打开 函数便是在此模块中定义.

I/O 层级结构的顶层是抽象基类 IOBase. 它定义流的基本接口. 但请注意, 流的读写
没有分开; 如果不支持给定的操作, 允许实现抛出 OSError.

RawIOBase 是对 IOBase 的扩展, 处理原始字节到流的读写. 文件IO 是 RawIOBase 的
子类, 提供 OS 文件接口.

BufferedIOBase 处理原始字节流 (RawIOBase) 的缓冲. 其子类有 BufferedWriter|缓冲写入类, 
BufferedReader|缓冲读取类 和 BufferedRWPair|缓冲读写对, 分别用于缓冲可读, 可写和读写均可的流.
BufferedRandom 提供随机访问流的缓冲接口. 字节IO 是一个简单的内存中字节流.

TextIOBase 是 IOBase 的另一个子类, 处理流的文本编码和解码. 
TextIOWrapper|文本IO包装盒 是其扩展, 提供缓冲原始流 (`BufferedIOBase`) 的
缓冲文本接口. 最后, StringIO|字符串IO 是文本的内存中流.

参数名称不是规范的一部分, 仅有意将 open()|打开() 的参数用作关键词参数.

数据:

默认缓冲区大小|DEFAULT_BUFFER_SIZE

   一个整数, 表示模块的缓冲 I/O 类使用的默认缓冲区大小. 如可能, open()|打开() 使用
   文件的 blksize (由 os.stat 获得).
"""
# New I/O library conforming to PEP 3116.

__author__ = ("Guido van Rossum <guido@python.org>, "
              "Mike Verdone <mike.verdone@gmail.com>, "
              "Mark Russell <mark.russell@zen.co.uk>, "
              "Antoine Pitrou <solipsis@pitrou.net>, "
              "Amaury Forgeot d'Arc <amauryfa@gmail.com>, "
              "Benjamin Peterson <benjamin@python.org>")

__all__ = ["BlockingIOError", "open", "open_code", "IOBase", "RawIOBase",
           "FileIO", "BytesIO", "StringIO", "BufferedIOBase",
           "BufferedReader", "BufferedWriter", "BufferedRWPair",
           "BufferedRandom", "TextIOBase", "TextIOWrapper",
           "UnsupportedOperation", "SEEK_SET", "SEEK_CUR", "SEEK_END",
           "打开代码", "文件IO", "字节IO", "字符串IO", "缓冲读取类",
           "缓冲写入类", "缓冲读写对", "缓冲随机类", "文本IO包装盒",
           "起始啊", "当前啊", "末尾啊"]


import _io
import abc

from _io import (DEFAULT_BUFFER_SIZE, BlockingIOError, UnsupportedOperation,
                 open, open_code, FileIO, BytesIO, StringIO, BufferedReader,
                 BufferedWriter, BufferedRWPair, BufferedRandom,
                 IncrementalNewlineDecoder, TextIOWrapper)

OpenWrapper = _io.open # for compatibility with _pyio
OpenWrapperz = _io.打开 # 汉化

# Pretend this exception was created here.
UnsupportedOperation.__module__ = "io"

# for seek()
起始啊 = SEEK_SET = 0
当前啊 = SEEK_CUR = 1
末尾啊 = SEEK_END = 2

# Declaring ABCs in C is tricky so we do it here.
# Method descriptions and default implementations are inherited from the C
# version however.
class IOBase(_io._IOBase, metaclass=abc.ABCMeta):
    __doc__ = _io._IOBase.__doc__

class RawIOBase(_io._RawIOBase, IOBase):
    __doc__ = _io._RawIOBase.__doc__

class BufferedIOBase(_io._BufferedIOBase, IOBase):
    __doc__ = _io._BufferedIOBase.__doc__

class TextIOBase(_io._TextIOBase, IOBase):
    __doc__ = _io._TextIOBase.__doc__

RawIOBase.register(FileIO)

for klass in (BytesIO, BufferedReader, BufferedWriter, BufferedRandom,
              BufferedRWPair):
    BufferedIOBase.register(klass)

for klass in (StringIO, TextIOWrapper):
    TextIOBase.register(klass)
del klass

try:
    from _io import _WindowsConsoleIO
except ImportError:
    pass
else:
    RawIOBase.register(_WindowsConsoleIO)

默认缓冲区大小 = DEFAULT_BUFFER_SIZE
打开代码 = open_code

类 缓冲读取类(BufferedReader):
    """使用给定的可读原始 IO 对象新建一个缓冲读取对象."""
    函 __init__(自身, 原始, 缓冲区大小=默认缓冲区大小):
        super().__init__(原始, 缓冲区大小)

类 缓冲写入类(BufferedWriter):
    """可写顺序 RawIO 对象的缓冲区.
    
    构造函数为给定的可写原始流创建一个缓冲写入对象.
    """
    函 __init__(自身, 原始, 缓冲区大小=默认缓冲区大小):
        super().__init__(原始, 缓冲区大小)

缓冲读写对 = BufferedRWPair

类 缓冲随机类(BufferedRandom):
    """随机访问流的缓冲接口.
    
    构造函数为第一个参数给出的可寻位流 '原始' 创建读取和写入对象.
    """
    函 __init__(自身, 原始, 缓冲区大小=默认缓冲区大小):
        super().__init__(原始, 缓冲区大小)

类 文件IO(FileIO):
    """
    打开一个文件.
    """
    函 __init__(自身, 文件, 模式='r', 关闭文符=真, 打开器=空):
        super().__init__(文件, mode=模式, closefd=关闭文符, opener=打开器)

类 字符串IO(StringIO):
    """使用内存中缓冲区的文本 I/O 实现.

    初始值 参数设置对象的值. 换行符 参数类似于 TextIOWrapper 构造器的换行符.
    """
    函 __init__(自身, 初始值='', 换行符='\n'):
        super().__init__(初始值, 换行符)

类 字节IO(BytesIO):
    """使用内存中字节缓冲区的缓冲 I/O 实现.
    """
    函 __init__(自身, 初始字节=b''):
        super().__init__(初始字节)

类 文本IO包装盒(TextIOWrapper):
    """BufferedIOBase 对象 (缓冲区) 之上的基于字符和行的层.
    """
    函 __init__(自身, 缓冲区, 编码=空, 错误=空, 换行符=空,
                行缓冲=假, 直写=假):
        super().__init__(缓冲区, encoding=编码, errors=错误,
                newline=换行符, line_buffering=行缓冲, write_through=直写)

