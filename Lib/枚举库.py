从 enum 导入 *
从 types 导入 DynamicClassAttribute


类 自动(auto):
    """
    实例用枚举类成员的适当值替换.
    """

类 枚举类(Enum):
    """从该类派生来定义新的枚举对象."""

    @DynamicClassAttribute
    函 名称(自身):
        """枚举类成员的名称."""
        返回 自身._name_

    @DynamicClassAttribute
    函 值(自身):
        """枚举类成员的值."""
        返回 自身._value_

类 整数枚举类(IntEnum):
    """成员也是 (而且必须是) 整数的枚举对象."""

类 标志类(Flag):
    """支持标志."""

类 整数标志类(IntFlag):
    """支持基于整数的标志."""

函 唯一(枚举对象):
    """枚举对象的类装饰器, 确保成员值唯一."""
    duplicates = []
    for name, member in 枚举对象.__members__.items():
        if name != member.name:
            duplicates.append((name, member.name))
    if duplicates:
        alias_details = ', '.join(
                ["%s -> %s" % (alias, name) for (alias, name) in duplicates])
        raise 值错误类('%r 中发现重复值: %s' %
                (枚举对象, alias_details))
    return 枚举对象

