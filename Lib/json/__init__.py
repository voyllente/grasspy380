r"""JSON (JavaScript Object Notation) <http://json.org> 是
JavaScript 语法 (ECMA-262 第三版) 的一个子集, 作为一种轻量级数据交换格式使用.

:mod:`json` 暴露的 API 是标准库 :mod:`marshal` 和 :mod:`pickle` 模块用户熟悉的 API.
它源自外部维护的 simplejson 库的一个版本.

Python 基本对象编码::

    >>> 导入 json
    >>> json.转储串(['foo', {'bar': ('baz', None, 1.0, 2)}])
    '["foo", {"bar": ["baz", null, 1.0, 2]}]'
    >>> 打印(json.转储串("\"foo\bar"))
    "\"foo\bar"
    >>> 打印(json.转储串('\u1234'))
    "\u1234"
    >>> 打印(json.转储串('\\'))
    "\\"
    >>> 打印(json.转储串({"c": 0, "b": 0, "a": 0}, 按键排序=真))
    {"a": 0, "b": 0, "c": 0}
    >>> 从 io 导入 StringIO
    >>> io = StringIO()
    >>> json.转储(['流式传输 API'], io)
    >>> io.获取值()
    '["流式传输 API"]'

紧凑编码::

    >>> 导入 json
    >>> 我的字典 = {'4': 5, '6': 7}
    >>> json.转储串([1,2,3,我的字典], 分隔符=(',', ':'))
    '[1,2,3,{"4":5,"6":7}]'

美化打印::

    >>> 导入 json
    >>> 打印(json.转储串({'4': 5, '6': 7}, 按键排序=True, 缩进=4))
    {
        "4": 5,
        "6": 7
    }

JSON 解码::

    >>> 导入 json
    >>> 小对象 = ['foo', {'bar': ['baz', None, 1.0, 2]}]
    >>> json.加载串('["foo", {"bar":["baz", null, 1.0, 2]}]') == 小对象
    True
    >>> json.加载串('"\\"foo\\bar"') == '"foo\x08ar'
    True
    >>> 从 io 导入 字符串IO
    >>> io = 字符串IO('["流式传输 API"]')
    >>> json.加载(io)[0] == '流式传输 API'
    True

特殊 JSON 对象解码::

    >>> 导入 json
    >>> 函 转为复数(小字典):
    ...     如果 '__复数__' 在 小字典:
    ...         返回 复数(小字典['实部'], 小字典['虚部'])
    ...     返回 小字典
    ...
    >>> json.加载串('{"__复数__": true, "实部": 1, "虚部": 2}', # 注意 json 语法
    ...     对象钩子=转为复数)
    (1+2j)
    >>> 从 decimal 导入 Decimal
    >>> json.加载串('1.1', 解析浮点数=Decimal) == Decimal('1.1')
    True

特殊 JSON 对象编码 (此例有问题, 字符串也不会报错?)::

    >>> 导入 json
    >>> 函 编码复数(小对象):
    ...     如果 是实例(小对象, 复数):
    ...         返回 [小对象.实部, 小对象.虚部]
    ...     报 类型错误类(f'{小对象.__类__.__名称__} 类型的对象'
    ...                     f'不支持 JSON 序列化')
    ...
    >>> json.转储串(2 + 1j, 默认=编码复数)
    '[2.0, 1.0]'
    >>> json.JSONEncoder(default=编码复数).encode(2 + 1j)
    '[2.0, 1.0]'
    >>> ''.join(json.JSONEncoder(default=编码复数).iterencode(2 + 1j))
    '[2.0, 1.0]'


从 shell 使用 json.tool 验证并美化输出::

    $ echo '{"json":"小对象"}' | python -m json.tool
    {
        "json": "小对象"
    }
    $ echo '{ 1.2:3.4}' | python -m json.tool
    期望属性名称用双引号围起来: 行 1 列 3 (字符 2)
"""
__version__ = '2.0.9'
__all__ = [
    'dump', 'dumps', 'load', 'loads',
    '转储', '转储串', '加载', '加载串',
    'JSONDecoder', 'JSONDecodeError', 'JSONEncoder',
]

__author__ = 'Bob Ippolito <bob@redivi.com>'

from .decoder import JSONDecoder, JSONDecodeError
from .encoder import JSONEncoder
import codecs

_default_encoder = JSONEncoder(
    skipkeys=False,
    ensure_ascii=True,
    check_circular=True,
    allow_nan=True,
    indent=None,
    separators=None,
    default=None,
)

def dump(obj, fp, *, skipkeys=False, ensure_ascii=True, check_circular=True,
        allow_nan=True, cls=None, indent=None, separators=None,
        default=None, sort_keys=False, **kw):
    """Serialize ``obj`` as a JSON formatted stream to ``fp`` (a
    ``.write()``-supporting file-like object).

    If ``skipkeys`` is true then ``dict`` keys that are not basic types
    (``str``, ``int``, ``float``, ``bool``, ``None``) will be skipped
    instead of raising a ``TypeError``.

    If ``ensure_ascii`` is false, then the strings written to ``fp`` can
    contain non-ASCII characters if they appear in strings contained in
    ``obj``. Otherwise, all such characters are escaped in JSON strings.

    If ``check_circular`` is false, then the circular reference check
    for container types will be skipped and a circular reference will
    result in an ``OverflowError`` (or worse).

    If ``allow_nan`` is false, then it will be a ``ValueError`` to
    serialize out of range ``float`` values (``nan``, ``inf``, ``-inf``)
    in strict compliance of the JSON specification, instead of using the
    JavaScript equivalents (``NaN``, ``Infinity``, ``-Infinity``).

    If ``indent`` is a non-negative integer, then JSON array elements and
    object members will be pretty-printed with that indent level. An indent
    level of 0 will only insert newlines. ``None`` is the most compact
    representation.

    If specified, ``separators`` should be an ``(item_separator, key_separator)``
    tuple.  The default is ``(', ', ': ')`` if *indent* is ``None`` and
    ``(',', ': ')`` otherwise.  To get the most compact JSON representation,
    you should specify ``(',', ':')`` to eliminate whitespace.

    ``default(obj)`` is a function that should return a serializable version
    of obj or raise TypeError. The default simply raises TypeError.

    If *sort_keys* is true (default: ``False``), then the output of
    dictionaries will be sorted by key.

    To use a custom ``JSONEncoder`` subclass (e.g. one that overrides the
    ``.default()`` method to serialize additional types), specify it with
    the ``cls`` kwarg; otherwise ``JSONEncoder`` is used.

    """
    # cached encoder
    if (not skipkeys and ensure_ascii and
        check_circular and allow_nan and
        cls is None and indent is None and separators is None and
        default is None and not sort_keys and not kw):
        iterable = _default_encoder.iterencode(obj)
    else:
        if cls is None:
            cls = JSONEncoder
        iterable = cls(skipkeys=skipkeys, ensure_ascii=ensure_ascii,
            check_circular=check_circular, allow_nan=allow_nan, indent=indent,
            separators=separators,
            default=default, sort_keys=sort_keys, **kw).iterencode(obj)
    # could accelerate with writelines in some versions of Python, at
    # a debuggability cost
    for chunk in iterable:
        fp.write(chunk)

函 转储(对象_, 文件指针, *, 跳过键=假, 确保ascii=真, 检查循环=真,
        允许非数字=真, 类_=空, 缩进=空, 分隔符=空, 默认=空,
        按键排序=假, **关键词参数):
    """将 ``对象_`` 序列化为 json 格式流并存储到 ``文件指针`` 所代表的文件类对象.

    如果 ``跳过键`` 为真, 则跳过不属于基本类型的字典键, 而不抛出 ``类型错误``.

    如果 ``确保ascii`` 为假, 则写入 ``文件指针`` 的字符串可以包含非 ascii 字符.

    如果 ``检查循环`` 为假, 则跳过容器类型的循环引用检查, 循环引用将导致 ``溢出错误``
    (或更严重的错误).

    如果 ``允许非数字`` 为假, 则严格按照 json 规范序列化超范围浮点值
    (``非数字``/``无穷大``/``-无穷大``) 将导致 ``值错误``, 而不是使用 js 等效值.

    如果 ``缩进`` 为非负整数, 则 json 数组元素和对象成员将使用该缩进值美化打印. 缩进值
    0 只会插入新行. ``空`` 为最紧凑的表示方式.

    如果指定 ``分隔符``, 它应为一个 ``(项分隔符, 键分隔符)`` 元组. 缩进为空时, 默认值
    为 ``(', ', ': ')`` , 其他情况下为 ``(',', ': ')``. 要获得最紧凑的 json 表示, 应
    指定 ``(',', ':')`` 以消除空格.

    ``默认(对象_)`` 应为一个函数, 返回 ``对象_`` 的可序列化版本或报类型错误. 默认情况是
    报类型错误.

    如果 ``按键排序`` 为真, 则字典的输出将按键排序.

    要使用自定义 ``JSONEncoder`` 子类, 请用 ``类_`` 参数指定.

    """
    dump(对象_, 文件指针, skipkeys=跳过键, ensure_ascii=确保ascii, 
        check_circular=检查循环, allow_nan=允许非数字, cls=类_, indent=缩进,
        separators=分隔符, default=默认, sort_keys=按键排序, **关键词参数)

def dumps(obj, *, skipkeys=False, ensure_ascii=True, check_circular=True,
        allow_nan=True, cls=None, indent=None, separators=None,
        default=None, sort_keys=False, **kw):
    """Serialize ``obj`` to a JSON formatted ``str``.

    If ``skipkeys`` is true then ``dict`` keys that are not basic types
    (``str``, ``int``, ``float``, ``bool``, ``None``) will be skipped
    instead of raising a ``TypeError``.

    If ``ensure_ascii`` is false, then the return value can contain non-ASCII
    characters if they appear in strings contained in ``obj``. Otherwise, all
    such characters are escaped in JSON strings.

    If ``check_circular`` is false, then the circular reference check
    for container types will be skipped and a circular reference will
    result in an ``OverflowError`` (or worse).

    If ``allow_nan`` is false, then it will be a ``ValueError`` to
    serialize out of range ``float`` values (``nan``, ``inf``, ``-inf``) in
    strict compliance of the JSON specification, instead of using the
    JavaScript equivalents (``NaN``, ``Infinity``, ``-Infinity``).

    If ``indent`` is a non-negative integer, then JSON array elements and
    object members will be pretty-printed with that indent level. An indent
    level of 0 will only insert newlines. ``None`` is the most compact
    representation.

    If specified, ``separators`` should be an ``(item_separator, key_separator)``
    tuple.  The default is ``(', ', ': ')`` if *indent* is ``None`` and
    ``(',', ': ')`` otherwise.  To get the most compact JSON representation,
    you should specify ``(',', ':')`` to eliminate whitespace.

    ``default(obj)`` is a function that should return a serializable version
    of obj or raise TypeError. The default simply raises TypeError.

    If *sort_keys* is true (default: ``False``), then the output of
    dictionaries will be sorted by key.

    To use a custom ``JSONEncoder`` subclass (e.g. one that overrides the
    ``.default()`` method to serialize additional types), specify it with
    the ``cls`` kwarg; otherwise ``JSONEncoder`` is used.

    """
    # cached encoder
    if (not skipkeys and ensure_ascii and
        check_circular and allow_nan and
        cls is None and indent is None and separators is None and
        default is None and not sort_keys and not kw):
        return _default_encoder.encode(obj)
    if cls is None:
        cls = JSONEncoder
    return cls(
        skipkeys=skipkeys, ensure_ascii=ensure_ascii,
        check_circular=check_circular, allow_nan=allow_nan, indent=indent,
        separators=separators, default=default, sort_keys=sort_keys,
        **kw).encode(obj)

函 转储串(对象_, *, 跳过键=假, 确保ascii=真, 检查循环=真,
        允许非数字=真, 类_=空, 缩进=空, 分隔符=空, 默认=空,
        按键排序=假, **关键词参数):
    """将 ``对象_`` 序列化为 json 格式的字符串.

    如果 ``跳过键`` 为真, 则跳过不属于基本类型的字典键, 而不抛出 ``类型错误``.

    如果 ``确保ascii`` 为假, 则写入 ``文件指针`` 的字符串可以包含非 ascii 字符.

    如果 ``检查循环`` 为假, 则跳过容器类型的循环引用检查, 循环引用将导致 ``溢出错误``
    (或更严重的错误).

    如果 ``允许非数字`` 为假, 则严格按照 json 规范序列化超范围浮点值
    (``非数字``/``无穷大``/``-无穷大``) 将导致 ``值错误``, 而不是使用 js 等效值.

    如果 ``缩进`` 为非负整数, 则 json 数组元素和对象成员将使用该缩进值美化打印. 缩进值
    0 只会插入新行. ``空`` 为最紧凑的表示方式.

    如果指定 ``分隔符``, 它应为一个 ``(项分隔符, 键分隔符)`` 元组. 缩进为空时, 默认值
    为 ``(', ', ': ')`` , 其他情况下为 ``(',', ': ')``. 要获得最紧凑的 json 表示, 应
    指定 ``(',', ':')`` 以消除空格.

    ``默认(对象_)`` 应为一个函数, 返回 ``对象_`` 的可序列化版本或报类型错误. 默认情况是
    报类型错误.

    如果 ``按键排序`` 为真, 则字典的输出将按键排序.

    要使用自定义 ``JSONEncoder`` 子类, 请用 ``类_`` 参数指定.

    """
    返回 dumps(对象_, skipkeys=跳过键, ensure_ascii=确保ascii, 
        check_circular=检查循环, allow_nan=允许非数字, cls=类_, indent=缩进,
        separators=分隔符, default=默认, sort_keys=按键排序, **关键词参数)

_default_decoder = JSONDecoder(object_hook=None, object_pairs_hook=None)


def detect_encoding(b):
    bstartswith = b.startswith
    if bstartswith((codecs.BOM_UTF32_BE, codecs.BOM_UTF32_LE)):
        return 'utf-32'
    if bstartswith((codecs.BOM_UTF16_BE, codecs.BOM_UTF16_LE)):
        return 'utf-16'
    if bstartswith(codecs.BOM_UTF8):
        return 'utf-8-sig'

    if len(b) >= 4:
        if not b[0]:
            # 00 00 -- -- - utf-32-be
            # 00 XX -- -- - utf-16-be
            return 'utf-16-be' if b[1] else 'utf-32-be'
        if not b[1]:
            # XX 00 00 00 - utf-32-le
            # XX 00 00 XX - utf-16-le
            # XX 00 XX -- - utf-16-le
            return 'utf-16-le' if b[2] or b[3] else 'utf-32-le'
    elif len(b) == 2:
        if not b[0]:
            # 00 XX - utf-16-be
            return 'utf-16-be'
        if not b[1]:
            # XX 00 - utf-16-le
            return 'utf-16-le'
    # default
    return 'utf-8'


def load(fp, *, cls=None, object_hook=None, parse_float=None,
        parse_int=None, parse_constant=None, object_pairs_hook=None, **kw):
    """Deserialize ``fp`` (a ``.read()``-supporting file-like object containing
    a JSON document) to a Python object.

    ``object_hook`` is an optional function that will be called with the
    result of any object literal decode (a ``dict``). The return value of
    ``object_hook`` will be used instead of the ``dict``. This feature
    can be used to implement custom decoders (e.g. JSON-RPC class hinting).

    ``object_pairs_hook`` is an optional function that will be called with the
    result of any object literal decoded with an ordered list of pairs.  The
    return value of ``object_pairs_hook`` will be used instead of the ``dict``.
    This feature can be used to implement custom decoders.  If ``object_hook``
    is also defined, the ``object_pairs_hook`` takes priority.

    To use a custom ``JSONDecoder`` subclass, specify it with the ``cls``
    kwarg; otherwise ``JSONDecoder`` is used.
    """
    return loads(fp.read(),
        cls=cls, object_hook=object_hook,
        parse_float=parse_float, parse_int=parse_int,
        parse_constant=parse_constant, object_pairs_hook=object_pairs_hook, **kw)

函 加载(文件指针, *, 类_=空, 对象钩子=空, 解析浮点数=空, 解析整数=空,
        解析常数=空, 对象对钩子=空, **关键词参数):
        """将 ``文件指针`` 代表的文件类对象反序列化为 Python 对象.

        ``对象钩子`` 是一个可选函数, 使用对象字面量解码的结果 (一个 ``字典``) 加以调用.
        ``对象钩子`` 的返回值取代该 ``字典``. 此特性可用来实现自定义解码器 (例如 JSON-RPC 类提示).

        ``对象对钩子`` 是一个可选函数, 使用对象字面量的有序对列表解码结果加以调用.
        ``对象对钩子`` 的返回值取代该 ``字典``. 此特性可用来实现自定义解码器.
        如果同时定义了 ``对象钩子``, 则 ``对象对钩子`` 优先.

        要使用自定义 ``JSONDecoder`` 子类, 请用 ``类_`` 参数指定.

        其他参数的说明参见 ``加载串``.
        """
        返回 load(文件指针, cls=类_, object_hook=对象钩子, parse_float=解析浮点数,
            parse_int=解析整数, parse_constant=解析常数, 
            object_pairs_hook=对象对钩子, **关键词参数)

def loads(s, *, cls=None, object_hook=None, parse_float=None,
        parse_int=None, parse_constant=None, object_pairs_hook=None, **kw):
    """Deserialize ``s`` (a ``str``, ``bytes`` or ``bytearray`` instance
    containing a JSON document) to a Python object.

    ``object_hook`` is an optional function that will be called with the
    result of any object literal decode (a ``dict``). The return value of
    ``object_hook`` will be used instead of the ``dict``. This feature
    can be used to implement custom decoders (e.g. JSON-RPC class hinting).

    ``object_pairs_hook`` is an optional function that will be called with the
    result of any object literal decoded with an ordered list of pairs.  The
    return value of ``object_pairs_hook`` will be used instead of the ``dict``.
    This feature can be used to implement custom decoders.  If ``object_hook``
    is also defined, the ``object_pairs_hook`` takes priority.

    ``parse_float``, if specified, will be called with the string
    of every JSON float to be decoded. By default this is equivalent to
    float(num_str). This can be used to use another datatype or parser
    for JSON floats (e.g. decimal.Decimal).

    ``parse_int``, if specified, will be called with the string
    of every JSON int to be decoded. By default this is equivalent to
    int(num_str). This can be used to use another datatype or parser
    for JSON integers (e.g. float).

    ``parse_constant``, if specified, will be called with one of the
    following strings: -Infinity, Infinity, NaN.
    This can be used to raise an exception if invalid JSON numbers
    are encountered.

    To use a custom ``JSONDecoder`` subclass, specify it with the ``cls``
    kwarg; otherwise ``JSONDecoder`` is used.

    The ``encoding`` argument is ignored and deprecated since Python 3.1.
    """
    if isinstance(s, str):
        if s.startswith('\ufeff'):
            raise JSONDecodeError("意外的 UTF-8 BOM (使用 utf-8-sig 解码)",
                                  s, 0)
    else:
        if not isinstance(s, (bytes, bytearray)):
            raise TypeError(f'JSON 对象须为字符串, 字节或字节数组, '
                            f'不能是 {s.__class__.__name__}')
        s = s.decode(detect_encoding(s), 'surrogatepass')

    if "encoding" in kw:
        import warnings
        warnings.warn(
            "'encoding' is ignored and deprecated. It will be removed in Python 3.9",
            DeprecationWarning,
            stacklevel=2
        )
        del kw['encoding']

    if (cls is None and object_hook is None and
            parse_int is None and parse_float is None and
            parse_constant is None and object_pairs_hook is None and not kw):
        return _default_decoder.decode(s)
    if cls is None:
        cls = JSONDecoder
    if object_hook is not None:
        kw['object_hook'] = object_hook
    if object_pairs_hook is not None:
        kw['object_pairs_hook'] = object_pairs_hook
    if parse_float is not None:
        kw['parse_float'] = parse_float
    if parse_int is not None:
        kw['parse_int'] = parse_int
    if parse_constant is not None:
        kw['parse_constant'] = parse_constant
    return cls(**kw).decode(s)

函 加载串(s, *, 类_=空, 对象钩子=空, 解析浮点数=空, 解析整数=空,
        解析常数=空, 对象对钩子=空, **关键词参数):
        """将 ``s`` (包含 json 文档的字符串、字节或字节数组实例) 反序列化为 Python 对象.

        ``对象钩子`` 是一个可选函数, 使用对象字面量解码的结果 (一个 ``字典``) 加以调用.
        ``对象钩子`` 的返回值取代该 ``字典``. 此特性可用来实现自定义解码器 (例如 JSON-RPC 类提示).

        ``对象对钩子`` 是一个可选函数, 使用对象字面量的有序对列表解码结果加以调用.
        ``对象对钩子`` 的返回值取代该 ``字典``. 此特性可用来实现自定义解码器.
        如果同时定义了 ``对象钩子``, 则 ``对象对钩子`` 优先.

        ``解析浮点数`` (如果指定) 将被调用以解码每个 JSON 浮点数字符串. 
        默认相当于 浮点数(数字字符串). 可以指定其他数据类型或解析器来解码
        JSON 浮点数 (例如 decimal.Decimal).
        
        ``解析整数`` (如果指定) 将被调用以解码每个 JSON 整数字符串. 
        默认相当于 整数(数字字符串). 可以指定其他数据类型或解析器来解码
        JSON 整数 (例如 浮点数).

        ``解析常数`` (如果指定) 将被调用以解码如下字符串: -Infinity, Infinity, NaN.
        这可以用来在遇到无效 JSON 数字时抛出异常.
        
        要使用自定义 ``JSONDecoder`` 子类, 请用 ``类_`` 参数指定.
        """
        返回 loads(s, cls=类_, object_hook=对象钩子, parse_float=解析浮点数,
            parse_int=解析整数, parse_constant=解析常数, 
            object_pairs_hook=对象对钩子, **关键词参数)
